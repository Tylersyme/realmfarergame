//Created by Scriptable Object Database plugin.

using MyLib.Shared.Database;

[System.Serializable]
public class TextureDatabaseFile : DatabaseFileBase<TextureDatabaseClass, TextureDatabaseAsset>
{
    public TextureDatabaseFile(string name, short id16) : base(name, id16) { }

    public override int AddNew(string name, UnityEngine.Object value)
    {
        return AddNew(new TextureDatabaseAsset(name, data.ID16, data.UniqueAssetKey, value as TextureContainer));
    }
}

[System.Serializable]
public class TextureDatabaseClass : Database<TextureDatabaseAsset> { };

[System.Serializable]
public class TextureDatabaseAsset : DatabaseAsset<TextureContainer>
{
    public TextureDatabaseAsset(string name, short databaseId16, short assetId, TextureContainer objectReference)
        : base(name, databaseId16, assetId, objectReference) { }
}