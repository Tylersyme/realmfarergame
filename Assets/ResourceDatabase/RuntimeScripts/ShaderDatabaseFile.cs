//Created by Scriptable Object Database plugin.

using MyLib.Shared.Database;

[System.Serializable]
public class ShaderDatabaseFile : DatabaseFileBase<ShaderDatabaseClass, ShaderDatabaseAsset>
{
    public ShaderDatabaseFile(string name, short id16) : base(name, id16) { }

    public override int AddNew(string name, UnityEngine.Object value)
    {
        return AddNew(new ShaderDatabaseAsset(name, data.ID16, data.UniqueAssetKey, value as ShaderContainer));
    }
}

[System.Serializable]
public class ShaderDatabaseClass : Database<ShaderDatabaseAsset> { };

[System.Serializable]
public class ShaderDatabaseAsset : DatabaseAsset<ShaderContainer>
{
    public ShaderDatabaseAsset(string name, short databaseId16, short assetId, ShaderContainer objectReference)
        : base(name, databaseId16, assetId, objectReference) { }
}