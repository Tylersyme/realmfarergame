//Created by Scriptable Object Database plugin.

using MyLib.Shared.Database;

[System.Serializable]
public class MaterialDatabaseFile : DatabaseFileBase<MaterialDatabaseClass, MaterialDatabaseAsset>
{
    public MaterialDatabaseFile(string name, short id16) : base(name, id16) { }

    public override int AddNew(string name, UnityEngine.Object value)
    {
        return AddNew(new MaterialDatabaseAsset(name, data.ID16, data.UniqueAssetKey, value as MaterialContainer));
    }
}

[System.Serializable]
public class MaterialDatabaseClass : Database<MaterialDatabaseAsset> { };

[System.Serializable]
public class MaterialDatabaseAsset : DatabaseAsset<MaterialContainer>
{
    public MaterialDatabaseAsset(string name, short databaseId16, short assetId, MaterialContainer objectReference)
        : base(name, databaseId16, assetId, objectReference) { }
}