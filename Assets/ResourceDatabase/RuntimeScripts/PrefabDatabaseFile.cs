//Created by Scriptable Object Database plugin.

using MyLib.Shared.Database;

[System.Serializable]
public class PrefabDatabaseFile : DatabaseFileBase<PrefabDatabaseClass, PrefabDatabaseAsset>
{
    public PrefabDatabaseFile(string name, short id16) : base(name, id16) { }

    public override int AddNew(string name, UnityEngine.Object value)
    {
        return AddNew(new PrefabDatabaseAsset(name, data.ID16, data.UniqueAssetKey, value as PrefabContainer));
    }
}

[System.Serializable]
public class PrefabDatabaseClass : Database<PrefabDatabaseAsset> { };

[System.Serializable]
public class PrefabDatabaseAsset : DatabaseAsset<PrefabContainer>
{
    public PrefabDatabaseAsset(string name, short databaseId16, short assetId, PrefabContainer objectReference)
        : base(name, databaseId16, assetId, objectReference) { }
}