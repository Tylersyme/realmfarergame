﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
[CreateAssetMenu(fileName = "ShaderContainer", menuName = "Database/Shader Container")]
public class ShaderContainer : ScriptableObject
{
    [SerializeField]
    private Shader _shader;
    public Shader Shader { get => _shader; set => _shader = value; }
}
