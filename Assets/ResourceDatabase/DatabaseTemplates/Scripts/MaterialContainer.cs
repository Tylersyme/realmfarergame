﻿using Sirenix.OdinInspector;
using Sirenix.Serialization;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
[CreateAssetMenu(fileName = "MaterialContainer", menuName = "Database/Material Container")]
public class MaterialContainer : SerializedScriptableObject
{
    [SerializeField]
    private Material _material;
    public Material Material { get => _material; set => _material = value; }

    [OdinSerialize]
    [Required]
    private DatabaseRecord<MaterialContainer> _databaseRecord = new DatabaseRecord<MaterialContainer>(-1, -1);
    public DatabaseRecord<MaterialContainer> DatabaseRecord { get => _databaseRecord; set => _databaseRecord = value; }
}
