﻿using Sirenix.OdinInspector;
using Sirenix.Serialization;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
[CreateAssetMenu(fileName = "TextureContainer", menuName = "Database/Texture Container")]
public class TextureContainer : SerializedScriptableObject
{
    [SerializeField]
    private Texture2D _texture;
    public Texture2D Texture { get => _texture; set => _texture = value; }

    [OdinSerialize]
    [Required]
    private DatabaseRecord<TextureContainer> _databaseRecord = new DatabaseRecord<TextureContainer>(-1, -1);
    public DatabaseRecord<TextureContainer> DatabaseRecord { get => _databaseRecord; set => _databaseRecord = value; }
}
