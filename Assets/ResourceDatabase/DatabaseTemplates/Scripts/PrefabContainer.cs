﻿using Sirenix.OdinInspector;
using Sirenix.Serialization;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
[CreateAssetMenu(fileName = "PrefabContainer", menuName = "Database/Prefab Container")]
public class PrefabContainer : SerializedScriptableObject
{
    [SerializeField]
    private GameObject _prefab;
    public GameObject Prefab { get => _prefab; set => _prefab = value; }

    [OdinSerialize]
    [Required]
    private DatabaseRecord<PrefabContainer> _databaseRecord = new DatabaseRecord<PrefabContainer>(-1, -1);
    public DatabaseRecord<PrefabContainer> DatabaseRecord { get => _databaseRecord; set => _databaseRecord = value; }
}
