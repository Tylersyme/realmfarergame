﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class CraftingController : SerializedMonoBehaviour
{
    // TODO: Replace with Entity instead of its controller
    [SerializeField]
    [Required]
    private EntityController _entityController;
    public EntityController EntityController { get => _entityController; set => _entityController = value; }

    [SerializeField]
    [Required]
    private GameObject _craftingRecipePanel;
    public GameObject CraftingRecipePanel { get => _craftingRecipePanel; set => _craftingRecipePanel = value; }

    [SerializeField]
    [Required]
    private GameObject _craftingItemHolderPrefab;
    public GameObject CraftingItemHolderPrefab { get => _craftingItemHolderPrefab; set => _craftingItemHolderPrefab = value; }

    [SerializeField]
    [Required]
    private CraftingDetailsView _craftingDetailsView;
    public CraftingDetailsView CraftingDetailsView { get => _craftingDetailsView; set => _craftingDetailsView = value; }

    [SerializeField]
    [Required]
    private TabUiComponent _craftingTabUiComponent;
    public TabUiComponent CraftingTabUiComponent { get => _craftingTabUiComponent; set => _craftingTabUiComponent = value; }

    private CraftingItemHolderUiComponent SelectedCraftingItemHolderUiComponent { get; set; }

    private EntityContainer EntityContainer { get => this.EntityController.GetContainerModule().Container; }

    private IHubSubscriptionCache _hubSubscriptionCache;
    private ISubscriptionCache _subscriptionCache;
    private IItemCrafter _itemCrafter;

    private void Awake()
    {
        _subscriptionCache = new SubscriptionCache();
        _itemCrafter = new ItemCrafter();

        this.Subscribe();
    }

    private void Start()
    {
        this.CreateCraftingItemHolders();
    }

    private void OnDestroy()
    {
        _hubSubscriptionCache.UnsubscribeAll();
        _subscriptionCache.UnsubscribeAll();
    }

    protected void OnInventoryChanged(InventoryChangedEventArgs eventArgs)
    {
        var canCraft = this.CanCraftCurrentlySelectedRecipe();

        this.CraftingDetailsView.SetCraftButtonInteractable(canCraft);
    }

    protected void OnCraftingItemHolderClicked(object sender, UiComponentClickedEventArgs eventArgs)
    {
        var craftingItemHolderUiComponent = (CraftingItemHolderUiComponent)eventArgs.ClickedUiComponent;

        this.SelectCraftingRecipe(craftingItemHolderUiComponent);
    }

    protected void OnCraftItemClicked(object sender, CraftItemClickedEventArgs eventArgs)
    {
        var craftingRecipeDefinition = this.SelectedCraftingItemHolderUiComponent.CraftingRecipeDefinition;

        var craftingResult = _itemCrafter.CraftItems(craftingRecipeDefinition.CraftingRecipes.First(), 1);

        this.EntityContainer.Inventory.RemoveCraftingItems(craftingResult.SourceItems);
        this.EntityContainer.Inventory.AddItems(craftingResult.CraftedItems);

        EventManager.Instance.UiEventHub.Publish(new InventoryChangedEventArgs
        {
            Inventory = this.EntityContainer.Inventory,
        });
    }

    protected void OnCraftingTabContentPanelVisibilityChanged(
        object sender, 
        TabContentPanelVisibilityChangedEventArgs eventArgs)
    {
        if (!eventArgs.IsVisible)
            return;

        this.SelectCraftingRecipe(this.SelectedCraftingItemHolderUiComponent);
    }

    private void CreateCraftingItemHolders()
    {
        var craftingRecipeDefinitions = MappingDatabase.Instance.CraftingRecipeDefinitions.CraftingRecipeDefinitions;

        foreach (var craftingRecipeDefinition in craftingRecipeDefinitions)
        {
            var craftingRecipeItemHolder = Object.Instantiate(this.CraftingItemHolderPrefab);
            craftingRecipeItemHolder.SetParent(this.CraftingRecipePanel);
            var craftingItemHolderUiComponent = craftingRecipeItemHolder.GetComponent<CraftingItemHolderUiComponent>();

            craftingItemHolderUiComponent.CraftingRecipeDefinition = craftingRecipeDefinition;
            craftingItemHolderUiComponent.Initialize();

            _subscriptionCache.Subscribe<UiComponentClickedEventArgs>(
                this.OnCraftingItemHolderClicked,
                handler => craftingItemHolderUiComponent.ClickedEvent += handler,
                handler => craftingItemHolderUiComponent.ClickedEvent -= handler);

            if (this.SelectedCraftingItemHolderUiComponent == null)
                this.SelectCraftingRecipe(craftingItemHolderUiComponent);
        }
    }

    private void SelectCraftingRecipe(CraftingItemHolderUiComponent craftingItemHolderUiComponent)
    {
        this.CraftingDetailsView.UpdateDetails(craftingItemHolderUiComponent.CraftingRecipeDefinition);

        this.SelectedCraftingItemHolderUiComponent = craftingItemHolderUiComponent;

        var canCraft = this.CanCraftCurrentlySelectedRecipe();

        this.CraftingDetailsView.SetCraftButtonInteractable(canCraft);
    }

    private bool CanCraftCurrentlySelectedRecipe()
    {
        var selectedCraftingRecipe =
            this.SelectedCraftingItemHolderUiComponent.CraftingRecipeDefinition.CraftingRecipes.First();

        return _itemCrafter.CanCraft(selectedCraftingRecipe, 1, this.EntityContainer.Inventory);
    }

    private void Subscribe()
    {
        _hubSubscriptionCache = new HubSubscriptionCache();

        _subscriptionCache.Subscribe<CraftItemClickedEventArgs>(
            this.OnCraftItemClicked,
            handler => this.CraftingDetailsView.CraftItemClickedEvent += handler,
            handler => this.CraftingDetailsView.CraftItemClickedEvent -= handler);

        _subscriptionCache.Subscribe<TabContentPanelVisibilityChangedEventArgs>(
            this.OnCraftingTabContentPanelVisibilityChanged,
            handler => this.CraftingTabUiComponent.TabContentPanelVisibilityChangedEvent += handler,
            handler => this.CraftingTabUiComponent.TabContentPanelVisibilityChangedEvent -= handler);

        _hubSubscriptionCache.Subscribe<InventoryChangedEventArgs>(EventManager.Instance.UiEventHub, this.OnInventoryChanged);
    }
}
