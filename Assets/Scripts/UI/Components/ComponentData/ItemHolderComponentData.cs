﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemHolderComponentData : UiComponentData
{
    public ItemHolderViewModel ItemHolderViewModel { get; set; }

    public ItemStackUiComponent HeldItemStackComponent { get; set; }

    public bool IsSelected { get; set; }
}
