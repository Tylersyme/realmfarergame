﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemStackComponentData : UiComponentData
{
    public ItemViewModel ItemViewModel { get; set; }

    public ItemHolderUiComponent ItemHolderUiComponent { get; set; }
}
