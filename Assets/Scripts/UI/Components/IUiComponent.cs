﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IUiComponent
{
    IUiView UiView { get; set; }

    object ComponentData { get; set; }

    void Initialize();
}
