﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(ClickListener))]
public class CraftingItemHolderUiComponent : UiComponent
{
    public CraftingRecipeDefinition CraftingRecipeDefinition { get; set; }

    private ISubscriptionCache _subscriptionCache;
    private IInputListenerService _inputListenerService;

    public event EventHandler<UiComponentClickedEventArgs> ClickedEvent;

    private void Awake()
    {
        _subscriptionCache = new SubscriptionCache();
        _inputListenerService = new InputListenerService(this.gameObject);

        this.Subscribe();
    }

    private void OnDestroy() =>
        _subscriptionCache.UnsubscribeAll();

    public override void Initialize() =>
        this.SetItemTexture(this.CraftingRecipeDefinition.GetUiTexture());

    private void SetItemTexture(Texture2D texture)
    {
        var rawImage = this
            .GetRawImageObject()
            .GetComponent<RawImage>();

        rawImage.texture = texture;

        rawImage.ScaleToCurrentTextureSize();
    }

    protected void OnClicked(object sender, ClickedEventArgs eventArgs)
    {
        this.ClickedEvent?.Invoke(this, new UiComponentClickedEventArgs
        {
            ClickedUiComponent = this,
        });
    }

    private void Subscribe()
    {
        _subscriptionCache.Subscribe<ClickedEventArgs>(
            this.OnClicked,
            handler => _inputListenerService.ClickedEvent += handler,
            handler => _inputListenerService.ClickedEvent -= handler);
    }

    private GameObject GetRawImageObject() =>
        this.gameObject.GetChildren()[0];
}
