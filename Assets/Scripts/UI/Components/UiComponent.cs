﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UiComponent : SerializedMonoBehaviour, IUiComponent
{
    public IUiView UiView { get; set; }

    public object ComponentData { get; set; }

    public virtual void Initialize() { }

    public T GetViewModelAs<T>() =>
        (T)this.UiView.ViewModel;

    public T GetComponentDataAs<T>() =>
        (T)this.ComponentData;
}
