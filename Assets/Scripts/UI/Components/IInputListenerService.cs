﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IInputListenerService
{
    EventHandler<ClickedEventArgs> ClickedEvent { get; set; }
}
