﻿using Sirenix.OdinInspector;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class TabGroupUiComponent : UiComponent
{
    [SerializeField]
    private bool _getTabsFromChildren = true;
    public bool GetTabsFromChildren { get => _getTabsFromChildren; set => _getTabsFromChildren = value; }

    [SerializeField]
    [HideIf(nameof(GetTabsFromChildren))]
    private IList<TabUiComponent> _tabUiComponents = new List<TabUiComponent>();
    public IList<TabUiComponent> TabUiComponents { get => _tabUiComponents; set => _tabUiComponents = value; }

    private TabUiComponent SelectedTab { get; set; }

    private ISubscriptionCache _subscriptionCache;

    private void Awake()
    {
        _subscriptionCache = new SubscriptionCache();
    }

    private void Start()
    {
        if (this.GetTabsFromChildren)
            this.LoadTabsFromChildren();

        this.DisableTabContentPanels();

        var firstTabUiComponent = this.TabUiComponents.FirstOrDefault();

        if (firstTabUiComponent != null)
            this.ChangeSelectedTab(firstTabUiComponent);

        this.SubscribeToTabUiComponents();
    }

    protected void OnTabClicked(object sender, UiComponentClickedEventArgs eventArgs)
    {
        if (this.SelectedTab == eventArgs.ClickedUiComponent)
            return;

        this.ChangeSelectedTab((TabUiComponent)eventArgs.ClickedUiComponent);
    }

    private void ChangeSelectedTab(TabUiComponent tabUiComponent)
    {
        // Hide the currently visible content panels
        this.SelectedTab?.DisableContentPanels();

        tabUiComponent.EnableContentPanels();

        this.SelectedTab = tabUiComponent;
    }

    private void LoadTabsFromChildren()
    {
        this.TabUiComponents.Clear();

        foreach (var tabUiComponent in this.gameObject.GetComponentsInChildren<TabUiComponent>())
            this.TabUiComponents.Add(tabUiComponent);
    }

    private void DisableTabContentPanels() =>
        this.TabUiComponents
            .ToList()
            .ForEach(t => t.DisableContentPanels());

    private void SubscribeToTabUiComponents()
    {
        foreach (var tabUiComponent in this.TabUiComponents)
            _subscriptionCache.Subscribe<UiComponentClickedEventArgs>(
                this.OnTabClicked,
                handler => tabUiComponent.ClickedEvent += this.OnTabClicked,
                handler => tabUiComponent.ClickedEvent -= this.OnTabClicked);
    }
}
