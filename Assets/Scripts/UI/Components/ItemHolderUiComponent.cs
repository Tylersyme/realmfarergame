﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(UiImageColorChangerModule))]
public class ItemHolderUiComponent : UiComponent
{
    public new ItemHolderComponentData ComponentData { get => base.GetComponentDataAs<ItemHolderComponentData>(); }

    [HideInInspector]
    public EventHandler<ItemSwappedEventArgs> ItemSwappedEvent;

    private ISubscriptionCache _subscriptionCache;
    private IInputListenerService _inputListenerService;

    private void Start()
    {
        _subscriptionCache = new SubscriptionCache();
        _inputListenerService = new InputListenerService(this.gameObject);

        this.Subscribe();
    }

    private void OnDisable() =>
        _subscriptionCache?.UnsubscribeAll();

    public override void Initialize()
    {
        if (this.ComponentData.IsSelected)
            this.Select();
        else
            this.Deselect();
    }

    protected void OnClicked(object sender, ClickedEventArgs eventArgs)
    {
        var inventoryViewModel = base.GetViewModelAs<InventoryViewModel>();

        if (inventoryViewModel.HasItemStackSelected)
        {
            if (this.ComponentData.ItemHolderViewModel.HasItem &&
                !this.IsHoldingItemStackUiComponent(inventoryViewModel.SelectedItemStackComponent))
                this.SwapWithSelectedItem();
            else
                this.PlaceItem();
        }
        else
        {
            if (this.ComponentData.ItemHolderViewModel.HasItem)
                this.PickupItem();
        }
    }

    public void SwapWithSelectedItem()
    {
        this.StopLerpItemToCenter();

        var inventoryViewModel = base.GetViewModelAs<InventoryViewModel>();

        var oldSlotPosition = inventoryViewModel.SelectedItemHolderComponent
            .GetComponentDataAs<ItemHolderComponentData>()
            .ItemHolderViewModel
            .SlotPosition;

        var newSlotPosition = this.ComponentData.ItemHolderViewModel.SlotPosition;

        var otherItemHolder = inventoryViewModel.SelectedItemStackComponent
            .GetComponentDataAs<ItemStackComponentData>()
            .ItemHolderUiComponent;

        var heldItemStackComponent = this.ComponentData.HeldItemStackComponent;

        this.ClearHeldItem();
        this.SetHeldItem(inventoryViewModel.SelectedItemStackComponent, true);

        otherItemHolder.SetHeldItem(heldItemStackComponent, false);

        inventoryViewModel.SetSelectedItemStackComponent(heldItemStackComponent);

        if (oldSlotPosition != newSlotPosition)
            this.ItemSwappedEvent?.Invoke(this, new ItemSwappedEventArgs
            {
                FirstSlotPosition = oldSlotPosition,
                SecondSlotPosition = newSlotPosition,
            });
    }

    public void PlaceItem()
    {
        var inventoryViewModel = base.GetViewModelAs<InventoryViewModel>();

        var oldSlotPosition = inventoryViewModel.SelectedItemHolderComponent
            .GetComponentDataAs<ItemHolderComponentData>()
            .ItemHolderViewModel
            .SlotPosition;

        var newSlotPosition = this.ComponentData.ItemHolderViewModel.SlotPosition;

        // Remove item from item holder that it originated from
        inventoryViewModel.SelectedItemHolderComponent.ClearHeldItem();
            
        this.SetHeldItem(inventoryViewModel.SelectedItemStackComponent, true);

        inventoryViewModel.ClearSelectedItemStackComponent();

        if (oldSlotPosition != newSlotPosition)
            this.ItemSwappedEvent?.Invoke(this, new ItemSwappedEventArgs
            {
                FirstSlotPosition = oldSlotPosition,
                SecondSlotPosition = newSlotPosition,
            });
    }

    public void PickupItem()
    {
        var inventoryViewModel = base.GetViewModelAs<InventoryViewModel>();

        inventoryViewModel.SetSelectedItemStackComponent(this.ComponentData.HeldItemStackComponent);

        this.StopLerpItemToCenter();
    }

    public void SetHeldItem(ItemStackUiComponent itemStackComponent, bool shouldLerp)
    {
        this.ComponentData.HeldItemStackComponent = itemStackComponent;
        this.ComponentData.ItemHolderViewModel.Item = itemStackComponent.Item;
        itemStackComponent.GetComponentDataAs<ItemStackComponentData>().ItemHolderUiComponent = this;

        itemStackComponent.gameObject.SetParent(this.gameObject);

        if (shouldLerp)
            this.LerpItemToCenter();
    }

    public void ClearHeldItem()
    {
        this.ComponentData.HeldItemStackComponent = null;
        this.ComponentData.ItemHolderViewModel.Item = null;
    }

    public void MoveItemToCenter()
    {
        if (this.ComponentData.HeldItemStackComponent == null)
            return;

        this.ComponentData.HeldItemStackComponent.gameObject.transform.position = this.gameObject.transform.position;
    }

    public void Select()
    {
        this.ComponentData.IsSelected = true;

        this.GetUiImageColorChangerModule().SetSaturationPercentage(0.5f);
    }

    public void Deselect()
    {
        this.ComponentData.IsSelected = false;

        this.GetUiImageColorChangerModule().ResetColor();
    }

    public void LerpItemToCenter() =>
        this.ComponentData.HeldItemStackComponent
            .GetUiLerpModule()
            .StartLerp(this.gameObject.transform.position);

    public void StopLerpItemToCenter() =>
        this.ComponentData.HeldItemStackComponent
            .GetUiLerpModule()
            .StopLerp();

    private bool IsHoldingItemStackUiComponent(ItemStackUiComponent itemStackUiComponent) =>
        this.ComponentData.HeldItemStackComponent == itemStackUiComponent;

    private void Subscribe()
    {
        _subscriptionCache.UnsubscribeAll();

        _subscriptionCache.Subscribe<ClickedEventArgs>(
            this.OnClicked,
            handler => _inputListenerService.ClickedEvent += handler,
            handler => _inputListenerService.ClickedEvent -= handler);
    }

    public UiImageColorChangerModule GetUiImageColorChangerModule() =>
        this.gameObject.GetComponent<UiImageColorChangerModule>();
}
