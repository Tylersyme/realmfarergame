﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputListenerService : IInputListenerService
{
    private ListenerHub _listenerHub;

    public InputListenerService(GameObject current)
    {
        _listenerHub = this.GetListenerHub(current);
    }

    public EventHandler<ClickedEventArgs> ClickedEvent { get => _listenerHub.ClickedEvent; set => _listenerHub.ClickedEvent = value; }

    private ListenerHub GetListenerHub(GameObject current)
    {
        return current.GetComponent<ListenerHub>();
    }
}
