﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ClickListener))]
public class TabUiComponent : UiComponent
{
    [SerializeField]
    private IList<GameObject> _tabContentPanels = new List<GameObject>();
    public IList<GameObject> TabContentPanels { get => _tabContentPanels; set => _tabContentPanels = value; }

    private ISubscriptionCache _subscriptionCache;
    private IInputListenerService _inputListenerService;

    [HideInInspector]
    public event EventHandler<UiComponentClickedEventArgs> ClickedEvent;

    [HideInInspector]
    public event EventHandler<TabContentPanelVisibilityChangedEventArgs> TabContentPanelVisibilityChangedEvent;

    private void Awake()
    {
        _subscriptionCache = new SubscriptionCache();
        _inputListenerService = new InputListenerService(this.gameObject);

        this.Subscribe();
    }

    private void OnDestroy() =>
        _subscriptionCache.UnsubscribeAll();

    public void DisableContentPanels()
    {
        foreach (var contentPanel in this.TabContentPanels)
            contentPanel.DisableSelf();

        this.TabContentPanelVisibilityChangedEvent?.Invoke(this, new TabContentPanelVisibilityChangedEventArgs
        {
            IsVisible = false,
        });
    }

    public void EnableContentPanels()
    {
        foreach (var contentPanel in this.TabContentPanels)
            contentPanel.EnableSelf();

        this.TabContentPanelVisibilityChangedEvent?.Invoke(this, new TabContentPanelVisibilityChangedEventArgs
        {
            IsVisible = true,
        });
    }

    protected void OnClicked(object sender, ClickedEventArgs eventArgs)
    {
        this.ClickedEvent?.Invoke(this, new UiComponentClickedEventArgs
        {
            ClickedUiComponent = this,
        });
    }

    private void Subscribe()
    {
        _subscriptionCache.Subscribe<ClickedEventArgs>(
            this.OnClicked,
            handler => _inputListenerService.ClickedEvent += handler,
            handler => _inputListenerService.ClickedEvent -= handler);
    }
}
