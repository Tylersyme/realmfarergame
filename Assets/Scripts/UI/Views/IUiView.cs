﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IUiView
{
    object ViewModel { get; set; }
}
