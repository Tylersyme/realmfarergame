﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(UiMouseFollowerModule))]
[RequireComponent(typeof(UiLerpModule))]
public class ItemStackUiComponent : UiComponent
{
    public ItemViewModel Item { get => base.GetComponentDataAs<ItemStackComponentData>().ItemViewModel; }

    public void UpdateDisplay()
    {
        UpdateStackSizeText();

        UpdateItemImage();
    }

    private void UpdateStackSizeText()
    {
        // Do not show stack size text if there is only one item in the stack
        var stackSizeText = this.Item.StackSize == 1
            ? string.Empty
            : this.Item.StackSize.ToString();

        GetTextMeshPro().text = stackSizeText;
    }

    private void UpdateItemImage()
    {
        var itemImage = GetItemImage();

        itemImage.texture = this.Item.UiTexture;

        itemImage.ScaleToCurrentTextureSize();
    }

    private TextMeshProUGUI GetTextMeshPro()
    {
        return this.gameObject.GetComponentInChildren<TextMeshProUGUI>();
    }

    private RawImage GetItemImage()
    {
        return this.gameObject.GetComponentInChildren<RawImage>();
    }

    public UiMouseFollowerModule GetUiMouseFollowerModule() =>
        this.gameObject.GetComponent<UiMouseFollowerModule>();

    public UiLerpModule GetUiLerpModule() =>
        this.gameObject.GetComponent<UiLerpModule>();
}
