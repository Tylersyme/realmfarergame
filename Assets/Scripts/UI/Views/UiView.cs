﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UiView : MonoBehaviour, IUiView
{
    public object ViewModel { get; set; }


    public void InitializeUiComponent(IUiComponent uiComponent, object componentData)
    {
        uiComponent.UiView = this;
        uiComponent.ComponentData = componentData;

        uiComponent.Initialize();
    }

    public T GetViewModelAs<T>() =>
        (T)this.ViewModel;
}
