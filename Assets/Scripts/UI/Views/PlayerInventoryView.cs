﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Used to display a container's inventory contents in UI.
/// </summary>
public class PlayerInventoryView : UiView
{
    [SerializeField]
    private EntityContainer _entityContainer;
    public EntityContainer EntityContainer { get => _entityContainer; set => _entityContainer = value; }

    [SerializeField]
    [Required]
    private GameObject _itemsParentObject;
    public GameObject ItemsParentObject { get => _itemsParentObject; set => _itemsParentObject = value; }

    [SerializeField]
    [Required]
    private GameObject _hotbarItemsParentObject;
    public GameObject HotbarItemsParentObject { get => _hotbarItemsParentObject; set => _hotbarItemsParentObject = value; }

    [SerializeField]
    [Required]
    private GameObject _itemHolderPrefab;
    public GameObject ItemHolderPrefab { get => _itemHolderPrefab; set => _itemHolderPrefab = value; }

    [SerializeField]
    [Required]
    private GameObject _itemStackPrefab;
    public GameObject ItemStackPrefab { get => _itemStackPrefab; set => _itemStackPrefab = value; }

    private IList<ItemHolderUiComponent> ItemHolderUiComponents { get; set; } = new List<ItemHolderUiComponent>();

    public int SelectedSlotPosition { get; set; } = 0;

    private IHubSubscriptionCache _hubSubscriptionCache;
    private ISubscriptionCache _subscriptionCache;

    private void Awake()
    {
        _subscriptionCache = new SubscriptionCache();

        this.Subscribe();
    }

    private void Start() =>
        this.UpdateInventoryUI();

    private void Update()
    {
        if (InputManager.Instance.IsActionActive(InputAction.CHANGE_SELECTED_SLOT))
        {
            var inputActionData = InputManager.Instance.GetActionData(InputAction.CHANGE_SELECTED_SLOT);
            var hotbarSlotPosition = inputActionData.KeyPressed.GetIntegerFromNumberKeycode();

            // Account for zero indexing and the fact that 0 is positioned after 9 on the keyboard
            hotbarSlotPosition = hotbarSlotPosition > 0
                ? hotbarSlotPosition - 1
                : 9;

            this.SetSelectedHotbarSlotPosition(hotbarSlotPosition);
        }
    }

    private void OnDestroy()
    {
        _hubSubscriptionCache.UnsubscribeAll();
        _subscriptionCache.UnsubscribeAll();
    }

    private void SaveSwapItems(int firstSlotPosition, int secondSlotPosition) =>
        this.EntityContainer.Inventory.SwapItems(firstSlotPosition, secondSlotPosition);

    private void SaveSelectedSlotPosition(int selectedSlotPosition)
    {
        this.EntityContainer.SetSelectedSlotPosition(selectedSlotPosition);

        EventManager.Instance.UiEventHub.Publish(new SelectedHotbarSlotChangedEventArgs
        {
            SelectedSlotPosition = selectedSlotPosition,
            SelectedItem = this.EntityContainer.SelectedItem,
        });
    }

    protected void OnSwapItems(object sender, ItemSwappedEventArgs eventArgs) =>
        this.SaveSwapItems(eventArgs.FirstSlotPosition, eventArgs.SecondSlotPosition);

    private void SetSelectedHotbarSlotPosition(int relativeSlotPosition)
    {
        var currentlySelectedHotbarItemHolder = this.ItemHolderUiComponents[this.SelectedSlotPosition];
        currentlySelectedHotbarItemHolder.Deselect();

        var slotPosition = this.EntityContainer.GetHotbarSlotAt(relativeSlotPosition).SlotPosition;

        this.ItemHolderUiComponents[slotPosition].Select();
        this.SelectedSlotPosition = slotPosition;
        this.SaveSelectedSlotPosition(this.SelectedSlotPosition);
    }

    private void UpdateInventoryUI()
    {
        this.DestroyItemHolders();

        this.ViewModel = AutoMapperConfig.ViewModelMapper.Map<InventoryViewModel>(this.EntityContainer.Inventory);

        var viewModel = base.GetViewModelAs<InventoryViewModel>();

        this.CreateInventoryUi(viewModel);

        // Updating inventory may change the item in the selected slot
        EventManager.Instance.UiEventHub.Publish(new SelectedHotbarSlotChangedEventArgs
        {
            SelectedSlotPosition = this.SelectedSlotPosition,
            SelectedItem = this.EntityContainer.SelectedItem,
        });
    }

    private void OnPreToggleOpenInventory(PreToggleInventoryEventArgs eventArgs)
    {
        if (!eventArgs.IsOpening)
            return;

        this.EntityContainer = eventArgs.EntityContainer;

        this.SelectedSlotPosition = this.EntityContainer.SelectedSlotPosition;

        UpdateInventoryUI();
    }

    private void OnInventoryChanged(InventoryChangedEventArgs eventArgs)
    {
        if (eventArgs.Inventory != this.EntityContainer.Inventory)
            return;

        this.UpdateInventoryUI();
    }

    private void CreateInventoryUi(InventoryViewModel viewModel)
    {
        this.ItemHolderUiComponents.Clear();

        foreach (var inventorySlot in viewModel.InventorySlotViewModels)
        {
            var parentObject = this.GetParentObjectOfInventorySubtypeId(inventorySlot.CategoryId);

            var itemHolderObject = Object.Instantiate(this.ItemHolderPrefab);
            itemHolderObject.SetParent(parentObject);

            var itemStackObject = default(GameObject);

            var itemStackComponent = default(ItemStackUiComponent);
            var itemHolderComponent = itemHolderObject.GetComponent<ItemHolderUiComponent>();

            // Only instantiate an item stack object if the slot isn't empty
            if (inventorySlot.Item != null)
            {
                itemStackObject = Object.Instantiate(this.ItemStackPrefab);
                itemStackObject.SetParent(itemHolderObject);

                itemStackComponent = itemStackObject.GetComponent<ItemStackUiComponent>();

                base.InitializeUiComponent(itemStackComponent, new ItemStackComponentData
                {
                    ItemViewModel = inventorySlot.Item,
                    ItemHolderUiComponent = itemHolderComponent,
                });

                itemStackComponent.UpdateDisplay();
            }

            base.InitializeUiComponent(itemHolderComponent, new ItemHolderComponentData
            {
                ItemHolderViewModel = inventorySlot,
                HeldItemStackComponent = itemStackComponent,
                IsSelected = inventorySlot.SlotPosition == this.SelectedSlotPosition,
            });

            itemHolderComponent.MoveItemToCenter();

            this.ItemHolderUiComponents.Add(itemHolderComponent);

            _subscriptionCache.Subscribe<ItemSwappedEventArgs>(
                this.OnSwapItems,
                handler => itemHolderComponent.ItemSwappedEvent += handler,
                handler => itemHolderComponent.ItemSwappedEvent -= handler);
        }
    }

    private void DestroyItemHolders()
    {
        this.ItemsParentObject
            .GetChildren()
            .ForEach(go => go.DestroySelf());

        this.HotbarItemsParentObject
            .GetChildren()
            .ForEach(go => go.DestroySelf());
    }

    private GameObject GetParentObjectOfInventorySubtypeId(string inventorySubtypeId)
    {
        if (inventorySubtypeId == EntityInventorySubtype.PRIMARY)
            return this.ItemsParentObject;
        else if (inventorySubtypeId == EntityInventorySubtype.HOTBAR)
            return this.HotbarItemsParentObject;

        throw new System.Exception(
            $"Error getting parent object of inventory type: The inventory subtype {inventorySubtypeId} has no been implemented.");
    }

    private void Subscribe()
    {
        _hubSubscriptionCache = new HubSubscriptionCache();

        _hubSubscriptionCache.Subscribe<PreToggleInventoryEventArgs>(EventManager.Instance.UiEventHub, this.OnPreToggleOpenInventory);
        _hubSubscriptionCache.Subscribe<InventoryChangedEventArgs>(EventManager.Instance.UiEventHub, this.OnInventoryChanged);
    }
}
