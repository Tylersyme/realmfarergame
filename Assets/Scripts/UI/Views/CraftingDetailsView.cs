﻿using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CraftingDetailsView : MonoBehaviour
{
    [SerializeField]
    [Required]
    private TextMeshProUGUI _craftingItemTitle;
    public TextMeshProUGUI CraftingItemTitle { get => _craftingItemTitle; set => _craftingItemTitle = value; }

    [SerializeField]
    [Required]
    private Button _craftButton;
    private Button CraftButton { get => _craftButton; set => _craftButton = value; }

    public event EventHandler<CraftItemClickedEventArgs> CraftItemClickedEvent;

    public void UpdateDetails(CraftingRecipeDefinition craftingRecipeDefinition)
    {
        var craftingResult = craftingRecipeDefinition.GetResult();

        this.CraftingItemTitle.text = craftingResult.ItemTemplate.UiName;
    }

    public void SetCraftButtonInteractable(bool isInteractable) =>
        this.CraftButton.interactable = isInteractable;

    public void OnCraftItemClicked() =>
        this.CraftItemClickedEvent?.Invoke(this, new CraftItemClickedEventArgs());
}
