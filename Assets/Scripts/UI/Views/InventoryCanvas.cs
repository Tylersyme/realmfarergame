﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryCanvas : MonoBehaviour
{
    [SerializeField]
    [Required]
    private GameObject _inventoryCanvasObject;
    public GameObject InventoryCanvasObject { get => _inventoryCanvasObject; set => _inventoryCanvasObject = value; }

    public bool IsOpen
    {
        get => this.InventoryCanvasObject.activeSelf;
    }

    private IHubSubscriptionCache _subscriptionCache;

    private void Awake() => Subscribe();

    private void OnDestroy() => _subscriptionCache.UnsubscribeAll();

    private void Start()
    {
        // Starts out closed
        SetInventoryOpen(false);
    }

    private void OnToggleOpenInventory(ToggleInventoryEventArgs eventArgs)
    {
        var isOpening = !this.IsOpen;

        // Allow inventory items to load before displaying
        EventManager.Instance.UiEventHub.Publish(new PreToggleInventoryEventArgs
        {
            IsOpening = isOpening,
            EntityContainer = eventArgs.EntityContainer,
        });

        SetInventoryOpen(isOpening);

        UpdateInputActionActiveState(isOpening);
    }

    /// <summary>
    /// Will show or hide the inventory UI.
    /// </summary>
    /// <param name="isInventoryOpen"></param>
    private void SetInventoryOpen(bool isInventoryOpen) =>
        this.InventoryCanvasObject.SetActive(isInventoryOpen);

    private void UpdateInputActionActiveState(bool isInventoryOpen)
    {
        if (isInventoryOpen)
            InputManager.Instance.DisableActions(InputActionSubgroup.MOVEMENT);
        else
            InputManager.Instance.EnableActions(InputActionSubgroup.MOVEMENT);
    }

    private void Subscribe()
    {
        _subscriptionCache = new HubSubscriptionCache();

        _subscriptionCache.Subscribe<ToggleInventoryEventArgs>(EventManager.Instance.UiEventHub, OnToggleOpenInventory);
    }
}
