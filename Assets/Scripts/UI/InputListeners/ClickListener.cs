﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

[RequireComponent(typeof(ListenerHub))]
public class ClickListener : MonoBehaviour, IPointerClickHandler
{
    public void OnPointerClick(PointerEventData eventData)
    {
        this.GetListenerHub().ClickedEvent?.Invoke(this, new ClickedEventArgs
        {
            InputButton = eventData.button,
        });
    }

    public ListenerHub GetListenerHub()
    {
        return this.gameObject.GetComponent<ListenerHub>();
    }
}
