﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.EventSystems.PointerEventData;

public class ClickedEventArgs
{
    public InputButton InputButton { get; set; }
}
