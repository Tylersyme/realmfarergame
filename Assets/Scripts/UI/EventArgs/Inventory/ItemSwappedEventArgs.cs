﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemSwappedEventArgs
{
    public int FirstSlotPosition { get; set; }

    public int SecondSlotPosition { get; set; }
}
