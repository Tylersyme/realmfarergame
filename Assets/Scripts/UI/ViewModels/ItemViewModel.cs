﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemViewModel
{
    public int StackSize { get; set; }

    public Texture UiTexture { get; set; }
}
