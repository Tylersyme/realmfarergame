﻿using AutoMapper;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemViewModelProfile : Profile
{
    public ItemViewModelProfile()
    {
        base.CreateMap<Item, ItemViewModel>()
            .ForMember(dest => dest.UiTexture, opt => opt.MapFrom(src => src.ItemTemplate.UiTexture));
    }
}
