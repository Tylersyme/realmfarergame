﻿using AutoMapper;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class InventoryViewModelProfile : Profile
{
    public InventoryViewModelProfile()
    {
        base.CreateMap<Inventory, InventoryViewModel>()
            .ForMember(dest => dest.InventorySlotViewModels, opt => opt.MapFrom(src => src.InventorySlots));

        base.CreateMap<InventoryViewModel, Inventory>()
            .ForMember(dest => dest.InventorySlots, opt => opt.Ignore());
    }
}
