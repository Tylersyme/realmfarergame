﻿using AutoMapper;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemHolderViewModelProfile : Profile
{
    public ItemHolderViewModelProfile()
    {
        base.CreateMap<InventorySlot, ItemHolderViewModel>()
            .ForMember(dest => dest.CategoryId, opt => opt.MapFrom(src => src.InventoryCategory.CategoryId));
    }
}
