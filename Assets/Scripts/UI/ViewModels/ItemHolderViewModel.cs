﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemHolderViewModel
{
    public int SlotPosition { get; set; }

    public string CategoryId { get; set; }

    public ItemViewModel Item { get; set; }

    public bool HasItem { get => this.Item != null; }
}
