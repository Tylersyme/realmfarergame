﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryViewModel
{
    public IList<ItemHolderViewModel> InventorySlotViewModels { get; set; }

    public ItemStackUiComponent SelectedItemStackComponent { get; private set; }
    public ItemHolderUiComponent SelectedItemHolderComponent
    {
        get => this.SelectedItemStackComponent
            .GetComponentDataAs<ItemStackComponentData>()
            .ItemHolderUiComponent;
    }

    public bool HasItemStackSelected { get => this.SelectedItemStackComponent != null; }

    public InventoryViewModel()
    {
        this.InventorySlotViewModels = new List<ItemHolderViewModel>();
    }

    public void SetSelectedItemStackComponent(ItemStackUiComponent itemStackComponent)
    {
        if (this.SelectedItemStackComponent != null)
            this.SelectedItemStackComponent.GetUiMouseFollowerModule().UnlockFromMousePosition();

        this.SelectedItemStackComponent = itemStackComponent;

        this.SelectedItemStackComponent.GetUiMouseFollowerModule().LockToMousePosition();
    }

    public void ClearSelectedItemStackComponent()
    {
        if (this.SelectedItemStackComponent == null)
            return;

        this.SelectedItemStackComponent.GetUiMouseFollowerModule().UnlockFromMousePosition();

        this.SelectedItemStackComponent = null;
    }
}
