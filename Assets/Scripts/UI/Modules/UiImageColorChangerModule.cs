﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class UiImageColorChangerModule : MonoBehaviour
{
    private Color OriginalColor { get; set; }

    private void Awake()
    {
        this.OriginalColor = this.GetImage().color.GetCopy();
    }

    public void SetColor(Color color) =>
        this.GetImage().color = color;

    public void ResetColor() =>
        this.GetImage().color = this.OriginalColor;

    public void SetValuePercentage(float valuePercentage)
    {
        var image = this.GetImage();

        image.color = image.color.SetValuePercentage(valuePercentage);
    }

    public void SetSaturationPercentage(float saturationPercentage)
    {
        var image = this.GetImage();

        image.color = image.color.SetSaturationPercentage(saturationPercentage);
    }

    public Image GetImage() =>
        this.gameObject.GetComponent<Image>();
}
