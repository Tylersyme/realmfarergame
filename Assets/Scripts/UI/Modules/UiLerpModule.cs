﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UiLerpModule : MonoBehaviour
{
    [SerializeField]
    private float _defaultSpeed = 20.0f;
    public float DefaultSpeed { get => _defaultSpeed; set => _defaultSpeed = value; }

    [SerializeField]
    private float _lerpEndDistance = 0.1f;
    public float LerpEndDistance { get => _lerpEndDistance; set => _lerpEndDistance = value; }

    public bool IsMoving { get; private set; }

    private Vector3 TargetPosition { get; set; }

    private void Update()
    {
        if (this.IsMoving)
        {
            this.UpdatePosition();

            if (this.IsCloseToTargetPosition())
            {
                this.StopLerp();

                this.gameObject.transform.position = this.TargetPosition;
            }
        }
    }

    public void StartLerp(Vector3 targetPosition)
    {
        this.IsMoving = true;

        this.TargetPosition = targetPosition;
    }

    public void StopLerp() =>
        this.IsMoving = false;

    private void UpdatePosition() =>
        this.gameObject.transform.position = 
            Vector3.Lerp(this.gameObject.transform.position, this.TargetPosition, this.DefaultSpeed * Time.deltaTime);

    private bool IsCloseToTargetPosition() =>
        Vector2.Distance(this.gameObject.transform.position, this.TargetPosition) <= this.LerpEndDistance;
}
