﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameObjectHolderModule : MonoBehaviour
{
    [SerializeField]
    private bool _willCenterHeldObject = true;
    public bool WillCenterHeldObject { get => _willCenterHeldObject; set => _willCenterHeldObject = value; }

    private GameObject HeldObject { get; set; }

    public void SetHeldObject(GameObject gameObject)
    {
        this.DestroyHeldObject();

        this.HeldObject = gameObject;

        if (this.WillCenterHeldObject)
            this.CenterHeldObject();
    }

    private void CenterHeldObject() =>
        this.HeldObject.transform.position = this.gameObject.transform.position;

    private void DestroyHeldObject()
    {
        this.HeldObject.DestroySelf();

        this.HeldObject = null;
    }
}
