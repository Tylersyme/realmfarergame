﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UiMouseFollowerModule : MonoBehaviour
{
    private Vector2 PositionBeforeMouseLocked { get; set; }

    public bool IsLockedToMousePosition { get; private set; } = false;

    public void Update()
    {
        if (this.IsLockedToMousePosition)
            this.MoveToMousePosition();
    }

    public void LockToMousePosition()
    {
        this.IsLockedToMousePosition = true;

        this.PositionBeforeMouseLocked = this.gameObject.transform.AsRectTransform().position;
    }

    public void UnlockFromMousePosition(bool returnToOriginalPosition = false)
    {
        this.IsLockedToMousePosition = false;

        if (returnToOriginalPosition)
            this.gameObject.transform.position = this.PositionBeforeMouseLocked;
    }

    public void MoveToMousePosition() =>
        this.gameObject.transform.position = Input.mousePosition;
}
