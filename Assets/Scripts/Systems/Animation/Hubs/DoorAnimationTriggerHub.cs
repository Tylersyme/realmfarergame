﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorAnimationTriggerHub : AnimationTriggerHub
{
    private static readonly string[] DOOR_ANIMATION_NAMES = new string[]
    {
        "DoorOpenAnimation",
        "DoorCloseAnimation",
    };

    [SerializeField]
    [Required]
    private DoorBehavior _doorBehavior;
    public DoorBehavior DoorBehavior { get => _doorBehavior; set => _doorBehavior = value; }

    public string DoorOpenAnimationName { get => DOOR_ANIMATION_NAMES[0]; }

    public string DoorCloseAnimationName { get => DOOR_ANIMATION_NAMES[1]; }

    private ISubscriptionCache _subscriptionCache;

    private void Awake()
    {
        _subscriptionCache = new SubscriptionCache();

        this.Subscribe();
    }

    private void OnDestroy() =>
        _subscriptionCache.UnsubscribeAll();

    protected void OnDoorOpenToggled(object sender, OpenDoorToggledEventArgs eventArgs)
    {
        if (eventArgs.WasOpened)
            base.Animator.Play(this.DoorOpenAnimationName);
        else
            base.Animator.Play(this.DoorCloseAnimationName);
    }

    private void Subscribe()
    {
        _subscriptionCache.Subscribe<OpenDoorToggledEventArgs>(
            this.OnDoorOpenToggled,
            handler => _doorBehavior.OpenDoorToggledEvent += handler,
            handler => _doorBehavior.OpenDoorToggledEvent -= handler);
    }
}
