﻿using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationTriggerHub : SerializedMonoBehaviour
{
    [SerializeField]
    [Required]
    private Animator _animator;
    public Animator Animator { get => _animator; set => _animator = value; }
}
