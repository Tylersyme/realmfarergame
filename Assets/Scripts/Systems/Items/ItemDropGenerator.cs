﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ItemDropGenerator
{
    [SerializeField]
    [Required]
    private ItemDropGeneratorData _data;
    public ItemDropGeneratorData Data { get => _data; set => _data = value; }

    [SerializeField]
    [Required]
    private Condition _condition;
    public Condition Condition { get => _condition; set => _condition = value; }
}
