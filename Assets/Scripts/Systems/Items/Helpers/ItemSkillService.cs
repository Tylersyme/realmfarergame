﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ItemSkillService : IItemSkillService
{
    public IList<ItemSkillActivationData> ActivateItemSkillSlot<TContextData>(
        Item item, 
        ItemSkillSlot itemSkillSlot, 
        ItemActivationCause itemActivationCause,
        ItemSkillActivationStrategyManager<TContextData> itemSkillDataStrategyManager,
        TContextData contextData)
    {
        var itemSkillActivationData = new List<ItemSkillActivationData>();

        if (!item.ItemTemplate.SlotSkills.ContainsKey(itemSkillSlot))
            return itemSkillActivationData;

        var itemSkills = item.ItemTemplate.SlotSkills[itemSkillSlot];

        foreach (var itemSkill in itemSkills)
        {
            var queryableInteraction = itemSkillDataStrategyManager.ActivateItemSkill(
                item,
                itemSkill,
                itemActivationCause,
                contextData);

            itemSkillActivationData.Add(new ItemSkillActivationData
            {
                ItemSkill = itemSkill,
                QueryableInteraction = queryableInteraction,
            });
        }

        return itemSkillActivationData;
    }

    public ItemSkillActivationData ActivateItemSkillOfType<TContextData>(
        Item item,
        ItemSkillType itemSkillType,
        ItemActivationCause itemActivationCause,
        ItemSkillActivationStrategyManager<TContextData> itemSkillDataStrategyManager,
        TContextData contextData)
    {
        var itemSkill = item.ItemTemplate.AllSkills.First(s => s.Type == itemSkillType);

        var queryableInteraction = itemSkillDataStrategyManager.ActivateItemSkill(
            item,
            itemSkill,
            itemActivationCause,
            contextData);

        return new ItemSkillActivationData
        {
            ItemSkill = itemSkill,
            QueryableInteraction = queryableInteraction,
        };
    }

    public TItemSkill GetSkill<TItemSkill>(Item item) where TItemSkill : IItemSkill =>
        (TItemSkill)item.ItemTemplate.AllSkills.First(s => s.GetType() == typeof(TItemSkill));

    public bool ItemHasSkillOfType(Item item, ItemSkillType itemSkillType) =>
        item.ItemTemplate.AllSkills.Any(s => s.Type == itemSkillType);
}
