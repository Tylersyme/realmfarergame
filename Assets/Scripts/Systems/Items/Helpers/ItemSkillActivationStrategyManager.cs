﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ItemSkillActivationStrategyManager<TContextData>
{
    private IDictionary<ItemSkillType, Func<Item, ItemActivationCause, IItemSkill, TContextData, QueryableInteraction>> Strategies { get; set; }

    public ItemSkillActivationStrategyManager()
    {
        this.Strategies = new Dictionary<ItemSkillType, Func<Item, ItemActivationCause, IItemSkill, TContextData, QueryableInteraction>>();
    }

    public QueryableInteraction ActivateItemSkill(
        Item item, 
        IItemSkill itemSkill, 
        ItemActivationCause itemActivationCause, 
        TContextData contextData)
    {
        if (!this.Strategies.ContainsKey(itemSkill.Type))
            return ((ItemSkill<object>)itemSkill).Activate(item, itemActivationCause, null);

        return this.Strategies[itemSkill.Type].Invoke(item, itemActivationCause, itemSkill, contextData);
    }

    protected void AddStrategy(
        ItemSkillType itemSkillType, 
        Func<Item, ItemActivationCause, IItemSkill, TContextData, QueryableInteraction> strategy) =>
        this.Strategies.Add(itemSkillType, strategy);
}
