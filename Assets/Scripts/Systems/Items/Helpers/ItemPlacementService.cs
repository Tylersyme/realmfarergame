﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ItemPlacementService : IItemPlacementService
{
    private readonly IItemSkillService _itemSkillService;

    public ItemPlacementService()
    {
        _itemSkillService = new ItemSkillService();
    }

    public OffsetSprite GetOffsetSpriteAtDirection(Item item, DirectionType directionType)
    {
        var placedItemPrefab = this.GetPlacedItemPrefab(item);

        var placeableBehavior = this.GetPlaceableBehavior(placedItemPrefab);

        return placeableBehavior.GetOffsetSpriteAtDirection(directionType);
    }

    public RotationType GetRotationType(Item item)
    {
        var placedItemPrefab = this.GetPlacedItemPrefab(item);

        var placeableBehavior = this.GetPlaceableBehavior(placedItemPrefab);

        return placeableBehavior.RotationType;
    }

    private GameObject GetPlacedItemPrefab(Item item)
    {
        var placeSelfSkill = _itemSkillService.GetSkill<PlaceSelfSkill>(item);

        return placeSelfSkill.PlacedItemPrefab;
    }

    public bool IsRotateable(Item item)
    {
        var placedItemPrefab = this.GetPlacedItemPrefab(item);

        return this.GetPlaceableBehavior(placedItemPrefab).CanRotate;
    }

    public bool IsPlaceable(Item item)
    {
        var hasPlaceSelfSkill = _itemSkillService.ItemHasSkillOfType(item, ItemSkillType.PlaceSelf);

        if (!hasPlaceSelfSkill)
            return false;

        var placedItemPrefab = this.GetPlacedItemPrefab(item);

        return this.GetPlaceableBehavior(placedItemPrefab) != null;
    }

    private PlaceableBehavior GetPlaceableBehavior(GameObject itemObject) =>
        itemObject.GetComponent<PlaceableBehavior>();
}
