﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IItemSpawnService
{
    void SpawnDroppedItem(Item item, Vector2 position);

    void SpawnDroppedItemFromTemplate(ItemTemplate itemTemplate, int stackSize, Vector2 position);

    GameObject SpawnPlacedItem(Item item, GameObject placedItemPrefab, Vector2 position);

    GameObject SpawnPlacedItemWithDirection(Item item, GameObject placedItemPrefab, Vector2 position, DirectionType directionType);
}
