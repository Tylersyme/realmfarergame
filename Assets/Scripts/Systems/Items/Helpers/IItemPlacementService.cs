﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IItemPlacementService
{
    OffsetSprite GetOffsetSpriteAtDirection(Item item, DirectionType directionType);

    RotationType GetRotationType(Item item);

    bool IsRotateable(Item item);

    bool IsPlaceable(Item item);
}
