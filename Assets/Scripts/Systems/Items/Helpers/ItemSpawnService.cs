﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemSpawnService : IItemSpawnService
{
    private readonly IGameObjectBuilder _gameObjectBuilder;
    private readonly IItemFactory _itemFactory;

    public ItemSpawnService()
    {
        _gameObjectBuilder = new GameObjectBuilder();
        _itemFactory = new ItemFactory();
    }

    public void SpawnDroppedItem(Item item, Vector2 position)
    {
        _gameObjectBuilder
            .CreateFromPrefab(item.ItemTemplate.DroppedItemPrefab)
            .WithPosition(position)
            .ModifyComponent<ItemModule>()
                .WithValue(im => im.Item, item)
            .ModifyImmediateChildByComponent<SpriteRenderer>(builder =>
            {
                builder
                    .ModifyComponent<SpriteRenderer>()
                    .WithValue(s => s.sprite, item.ItemTemplate.Sprite);
            })
            .Build();
    }

    public void SpawnDroppedItemFromTemplate(ItemTemplate itemTemplate, int stackSize, Vector2 position)
    {
        var items = _itemFactory.CreateStacksFromTemplate(itemTemplate, stackSize);

        items.ForEach(i => this.SpawnDroppedItem(i, position));
    }

    public GameObject SpawnPlacedItem(Item item, GameObject placedItemPrefab, Vector2 position)
    {
        var placedItemObject = _gameObjectBuilder
            .CreateFromPrefab(placedItemPrefab)
            .WithPosition(position)
            .ModifyComponent<ItemModule>()
                .WithValue(im => im.Item, item)
            .Build();

        return placedItemObject;
    }

    public GameObject SpawnPlacedItemWithDirection(Item item, GameObject placedItemPrefab, Vector2 position, DirectionType directionType)
    {
        var placedItemObject = this.SpawnPlacedItem(item, placedItemPrefab, position);

        placedItemObject
            .GetComponent<PlaceableBehavior>()
            .SetDirection(directionType);

        return placedItemObject;
    }
}
