﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IItemSkillService
{
    IList<ItemSkillActivationData> ActivateItemSkillSlot<TContextData>(
        Item item,
        ItemSkillSlot itemSkillSlot,
        ItemActivationCause itemActivationCause,
        ItemSkillActivationStrategyManager<TContextData> itemSkillDataStrategyManager,
        TContextData contextData);

    ItemSkillActivationData ActivateItemSkillOfType<TContextData>(
        Item item,
        ItemSkillType itemSkillType,
        ItemActivationCause itemActivationCause,
        ItemSkillActivationStrategyManager<TContextData> itemSkillDataStrategyManager,
        TContextData contextData);

    TItemSkill GetSkill<TItemSkill>(Item item) where TItemSkill : IItemSkill;

    bool ItemHasSkillOfType(Item item, ItemSkillType itemSkillType);
}
