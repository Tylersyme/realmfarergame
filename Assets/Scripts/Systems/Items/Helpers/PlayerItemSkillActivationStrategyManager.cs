﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerItemSkillActivationStrategyManager : ItemSkillActivationStrategyManager<EntityController>
{
    private readonly ITilemapMouseDataService _tilemapMouseDataService;

    public PlayerItemSkillActivationStrategyManager()
    {
        _tilemapMouseDataService = new TilemapMouseDataService();

        base.AddStrategy(ItemSkillType.PlaceSelf, this.ActivatePlaceSelfSkill);
    }

    private QueryableInteraction ActivatePlaceSelfSkill(
        Item item, ItemActivationCause itemActivationCause, IItemSkill itemSkill, EntityController entityController)
    {
        var placeSelfSkill = (PlaceSelfSkill)itemSkill;

        var placementPosition = default(Vector2);

        if (placeSelfSkill.SnapToTile)
            placementPosition = _tilemapMouseDataService.GetHoveredTileCenterWorldPosition();
        else
            placementPosition = Camera.main.MouseWorldPosition();

        var placeSelfSkillData =  new PlaceSelfSkill.PlaceSelfSkillData
        {
            Position = placementPosition,
        };

        return placeSelfSkill.Activate(
                item,
                itemActivationCause,
                placeSelfSkillData);
    }
}
