﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ItemTemplate", menuName = "Items/Item Drop Config")]
public class ItemDropGeneratorData : SerializedScriptableObject
{
    [SerializeField]
    private IDictionary<ItemType, ItemDrop> _configuration = new Dictionary<ItemType, ItemDrop>();
    public IDictionary<ItemType, ItemDrop> Configuration { get => _configuration; set => _configuration = value; }
}
