﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IItemSkill
{
    ItemSkillType Type { get; }

    bool InteractsWithActivator { get; }
}
