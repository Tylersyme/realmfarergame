﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlaceTileSkill : ItemSkill<object>
{
    private static readonly ObjectQueryProfile INTERACT_QUERY = new ObjectQueryProfile
    {
        ObjectQueries = new List<IObjectQuery>
        {

        },
    };

    public override ItemSkillType Type { get => ItemSkillType.PlaceTile; }

    public override ObjectQueryProfile ToInteractQuery { get => INTERACT_QUERY; }

    public override bool InteractsWithActivator { get => true; }

    [SerializeField]
    [Required]
    private TileType _tileTypeToPlace;
    public TileType TileTypeToPlace { get => _tileTypeToPlace; set => _tileTypeToPlace = value; }

    public override QueryableInteraction Activate(Item item, ItemActivationCause itemActivationCause, object _) =>
        base.CreateQueryableInteraction(
            new ItemInteraction
            {
                ItemUsed = item,
                ItemActivationCause = itemActivationCause,
                Adjectives = new List<InteractionAdjective>
                {
                    new PlaceTileInteractionAdjective
                    {
                        TileItem = item,
                        TileTypeToPlace = this.TileTypeToPlace,
                    },
                },
            });
}
