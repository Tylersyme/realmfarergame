﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public abstract class ItemSkill<TData> : IItemSkill
{
    public abstract bool InteractsWithActivator { get; }

    public abstract ObjectQueryProfile ToInteractQuery { get; }

    public abstract ItemSkillType Type { get; }

    public abstract QueryableInteraction Activate(Item item, ItemActivationCause itemActivationCause, TData data);

    protected QueryableInteraction CreateQueryableInteraction(Interaction interaction) =>
        new QueryableInteraction
        {
            Interaction = interaction,
            ObjectQueryProfile = this.ToInteractQuery,
        };
}
