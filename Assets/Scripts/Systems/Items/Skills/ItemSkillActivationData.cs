﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemSkillActivationData
{
    public IItemSkill ItemSkill { get; set; }

    public QueryableInteraction QueryableInteraction { get; set; }
}
