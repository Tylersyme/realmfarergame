﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#pragma warning disable 0169

[System.Serializable]
public class SwingSkill : ItemSkill<object>
{
    public override ItemSkillType Type { get => ItemSkillType.Swing; }

    [SerializeField]
    [Required]
    private ObjectQueryProfile _toInteractQuery = new ObjectQueryProfile();
    public override ObjectQueryProfile ToInteractQuery { get => _toInteractQuery; }

    public override bool InteractsWithActivator { get => false; }

    [SerializeField]
    [Required]
    private Damage _damage;
    public Damage Damage { get => _damage; set => _damage = value; }

    public override QueryableInteraction Activate(Item item, ItemActivationCause itemActivationCause, object _) =>
        base.CreateQueryableInteraction(
            new ItemInteraction
            {
                ItemUsed = item,
                ItemActivationCause = itemActivationCause,
                Adjectives = new List<InteractionAdjective>
                {
                    new DamagesInteractionAdjective
                    {
                        Damage = this.Damage,
                    },
                },
            });
}

#pragma warning restore 0169
