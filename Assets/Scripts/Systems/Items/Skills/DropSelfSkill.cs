﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class DropSelfSkill : ItemSkill<object>
{
    private static readonly ObjectQueryProfile INTERACT_QUERY = new ObjectQueryProfile
    {
        ObjectQueries = new List<IObjectQuery>
        {

        },
    };

    public override ItemSkillType Type { get => ItemSkillType.DropSelf; }

    public override ObjectQueryProfile ToInteractQuery { get => INTERACT_QUERY; }

    public override bool InteractsWithActivator { get => true; }

    public override QueryableInteraction Activate(Item item, ItemActivationCause itemActivationCause, object _) =>
        base.CreateQueryableInteraction(
            new ItemInteraction
            {
                ItemUsed = item,
                ItemActivationCause = itemActivationCause,
                Adjectives = new List<InteractionAdjective>
                {
                    new DropItemInteractionAdjective
                    {
                        DroppedItem = item,
                    },
                },
            });
}
