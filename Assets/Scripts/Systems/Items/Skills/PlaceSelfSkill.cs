﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlaceSelfSkill : ItemSkill<PlaceSelfSkill.PlaceSelfSkillData>
{
    private static readonly ObjectQueryProfile INTERACT_QUERY = new ObjectQueryProfile
    {
        ObjectQueries = new List<IObjectQuery>
        {

        },
    };

    public override ItemSkillType Type { get => ItemSkillType.PlaceSelf; }

    public override ObjectQueryProfile ToInteractQuery { get => INTERACT_QUERY; }

    public override bool InteractsWithActivator { get => true; }

    [SerializeField]
    private bool _snapToTile = false;
    public bool SnapToTile { get => _snapToTile; set => _snapToTile = value; }

    [SerializeField]
    [Required]
    private GameObject _placedItemPrefab;
    public GameObject PlacedItemPrefab { get => _placedItemPrefab; set => _placedItemPrefab = value; }

    public override QueryableInteraction Activate(Item item, ItemActivationCause itemActivationCause, PlaceSelfSkillData placeSelfSkillData) =>
        base.CreateQueryableInteraction(
            new ItemInteraction
            {
                ItemUsed = item,
                ItemActivationCause = itemActivationCause,
                Adjectives = new List<InteractionAdjective>
                {
                    new PlaceItemInteractionAdjective
                    {
                        PlacedItem = item,
                        PlaceItemPrefab = _placedItemPrefab,
                        Position = placeSelfSkillData.Position,
                    },
                },
            });

    public class PlaceSelfSkillData
    {
        public Vector2 Position { get; set; }
    }
}
