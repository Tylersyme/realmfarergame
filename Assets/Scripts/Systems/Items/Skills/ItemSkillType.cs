﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ItemSkillType
{
    PlaceTile,
    DropSelf,
    PlaceSelf,
    Swing,
}
