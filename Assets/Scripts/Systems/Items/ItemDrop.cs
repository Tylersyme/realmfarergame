﻿using Sirenix.OdinInspector;
using Sirenix.Serialization;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ItemDrop
{
    [SerializeField]
    [Required]
    private ItemTemplate _itemTemplate;
    public ItemTemplate ItemTemplate { get => _itemTemplate; set => _itemTemplate = value; }

    [SerializeField]
    [Required]
    private int _stackSize;
    public int StackSize { get => _stackSize; set => _stackSize = value; }
}
