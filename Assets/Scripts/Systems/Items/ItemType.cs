﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ItemType
{
    Branch,
    IronAxe,
    WoodTile,
    Log,
    ElephantEasterEgg,
    Apple,
    WoodStool,
    WoodDoor,
    Rock,
    SimpleCraftingTable,
    StoneAxe,
}
