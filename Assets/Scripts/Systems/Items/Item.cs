﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// Stores the data for an item which may be contained within an inventory.
/// </summary>
[System.Serializable]
public class Item
{
    [SerializeField]
    [Required]
    private ItemTemplate _itemTemplate;
    public ItemTemplate ItemTemplate { get => _itemTemplate; protected set => _itemTemplate = value; }

    [SerializeField]
    [MinValue(1)]
    private int _stackSize = 1;
    public int StackSize
    {
        get => _stackSize;
        set
        {
            if (value > this.MaxStackSize)
                throw new System.ArgumentOutOfRangeException($"Error setting item stack size: The stack size {value} exceeds the max stack size of {this.MaxStackSize}.");

            _stackSize = value;
        }
    }

    public int MaxStackSize { get => this.ItemTemplate.MaxStackSize; }

    public int AvailableStackSpace { get => this.MaxStackSize - this.StackSize; }

    public bool IsMaxStack { get => this.StackSize == this.MaxStackSize; }

    public bool HasAnyItems { get => this.StackSize > 0; }

    public ItemType Type { get => this.ItemTemplate.Type; }

    public Item(ItemTemplate itemTemplate)
    {
        this.ItemTemplate = itemTemplate;
    }

    public Item()
    {
    }
}
