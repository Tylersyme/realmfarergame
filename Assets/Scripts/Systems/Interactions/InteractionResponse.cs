﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Represents the data produced by something being interacted with.
/// This data describes the result of the interaction, such as whether the interaction was successful.
/// </summary>
public class InteractionResponse 
{
    public bool WasSuccessful { get; set; } = true;

    public InteractionType InteractionType { get; set; }
}
