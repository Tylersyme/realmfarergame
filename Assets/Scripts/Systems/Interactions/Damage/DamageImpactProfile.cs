﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(fileName = "DamageImpactProfile", menuName = "Object/DamageImpactProfile")]
public class DamageImpactProfile : SerializedScriptableObject
{
    private static readonly DamageImpact DEFAULT_DAMAGE_IMPACT = new DamageImpact
    {
        DamageModifier = 1.0f,
    };

    [SerializeField]
    [Required]
    private List<DamageImpactMapping> _damageImpactMappings = new List<DamageImpactMapping>();
    private List<DamageImpactMapping> DamageImpactMappings { get => _damageImpactMappings; set => _damageImpactMappings = value; }

    public DamageImpact GetDamageImpactOfType(DamageType damageType)
    {
        var damageImpactMapping = this.DamageImpactMappings.FirstOrDefault(dim => dim.DamageTypes.Contains(damageType));

        if (damageImpactMapping == null)
            return DEFAULT_DAMAGE_IMPACT;

        return damageImpactMapping.DamageImpact;
    }

    [System.Serializable]
    private class DamageImpactMapping
    {
        [SerializeField]
        [Required]
        private List<DamageType> _damageTypes = new List<DamageType>();
        public List<DamageType> DamageTypes { get => _damageTypes; set => _damageTypes = value; }

        [SerializeField]
        [Required]
        private DamageImpact _damageImpact;
        public DamageImpact DamageImpact { get => _damageImpact; set => _damageImpact = value; }
    }
}
