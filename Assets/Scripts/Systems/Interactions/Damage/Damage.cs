﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Damage", menuName = "Object/Damage")]
public class Damage : SerializedScriptableObject
{
    [SerializeField]
    private DamageType type;
    public DamageType Type { get => type; set => type = value; }

    [SerializeField]
    private float amount;
    public float Amount { get => amount; set => amount = value; }
}
