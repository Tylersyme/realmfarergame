﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class DamageImpact
{
    [SerializeField]
    private float _damageModifier = 1.0f;
    public float DamageModifier { get => _damageModifier; set => _damageModifier = value; }
}
