﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interaction
{
    public virtual InteractionType Type { get; set; }

    public IList<InteractionAdjective> Adjectives { get; set; }

    /// <summary>
    /// Whether the tilemap object's default interaction type will be used
    /// </summary>
    public bool IsDefaultInteraction { get; set; } = false;

    public Interaction()
    {
        this.Adjectives = new List<InteractionAdjective>();
    }
}
