﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Used to invoke an TilemapObject's interaction to other TilemapObjects.
/// </summary>
public interface IInteractionInvoker
{
    T Invoke<T>(GameObject target, Interaction interaction) where T : InteractionResponse;
    InteractionType GetDefaultInteractionType(GameObject target);
    bool IsInteractable(GameObject target);
}
