﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// This is responsible for interfacing directly with the InteractionEventHub and provides event filters
/// so that the subscribers only receive the interaction events they need.
/// </summary>
public class InteractionSubscriber : IInteractionSubscriber
{
    private GameObject SubscribingGameObject { get; set; }

    private EventHandler<InteractionEventArgs> InteractionEventHandler;

    private IDictionary<InteractionAdjectiveType, EventHandler<InteractionAdjectiveEventArgs>> InteractionAdjectiveEventHandlers { get; set; }

    private IDictionary<InteractionType, EventHandler<InteractionEventArgs>> InteractionTypeEventHandlers { get; set; }

    private EventHandler<InteractionEventArgs> InteractionEvent;

    public InteractionSubscriber(GameObject subscribingGameObject)
    {
        this.SubscribingGameObject = subscribingGameObject;
        this.InteractionAdjectiveEventHandlers = new Dictionary<InteractionAdjectiveType, EventHandler<InteractionAdjectiveEventArgs>>();
        this.InteractionTypeEventHandlers = new Dictionary<InteractionType, EventHandler<InteractionEventArgs>>();

        this.GetInteractionEventHub().InteractionEvent += this.OnInteract;
    }

    public void SubscribeAllInteractions(EventHandler<InteractionEventArgs> eventHandler)
    {
        this.InteractionEvent += eventHandler;

        this.InteractionEventHandler = eventHandler;
    }

    public void SubscribeToInteractionAdjective(
        InteractionAdjectiveType interactionAdjectiveType, 
        EventHandler<InteractionAdjectiveEventArgs> eventHandler)
    {
        this.InteractionAdjectiveEventHandlers.Add(interactionAdjectiveType, eventHandler);

        this.RegisterInteractionAdjectiveType(interactionAdjectiveType);
    }

    public void SubscribeToInteractionType(InteractionType interactionType, EventHandler<InteractionEventArgs> eventHandler) =>
        this.InteractionTypeEventHandlers.Add(interactionType, eventHandler);

    public void UnsubscribeAll()
    {
        var interactionEventHub = GetInteractionEventHub();

        this.InteractionEvent -= this.InteractionEventHandler;

        interactionEventHub.InteractionEvent -= this.OnInteract;
    }

    public IInteractionSubscriber WithDefaultInteraction(InteractionType defaultInterationType)
    {
        this.GetInteractionEventHub().DefaultInteractionType = defaultInterationType;

        return this;
    }

    private IInteractionSubscriber RegisterInteractionAdjectiveType(InteractionAdjectiveType interactionAdjectiveType)
    {
        this
            .GetInteractionEventHub()
            .RegisterInteractionAdjectiveType(interactionAdjectiveType);

        return this;
    }

    protected void OnInteract(object sender, InteractionEventArgs eventArgs)
    {
        this.InteractionEvent?.Invoke(sender, eventArgs);

        var interactionAdjectives = eventArgs.Interaction.Adjectives;

        foreach (var interactionAdjective in interactionAdjectives)
        {
            if (!this.InteractionAdjectiveEventHandlers.ContainsKey(interactionAdjective.Type))
                continue;

            this.InteractionAdjectiveEventHandlers[interactionAdjective.Type]?.Invoke(sender, new InteractionAdjectiveEventArgs
            {
                Interaction = eventArgs.Interaction,
                InteractionAdjective = interactionAdjective,
                AllAdjectiveTypes = eventArgs.Interaction.Adjectives
                    .Select(a => a.Type)
                    .ToList(),
            });
        }

        if (!this.InteractionTypeEventHandlers.ContainsKey(eventArgs.Interaction.Type))
            return;

        this.InteractionTypeEventHandlers[eventArgs.Interaction.Type]?.Invoke(sender, eventArgs);
    }

    private InteractionEventHub GetInteractionEventHub()
    {
        return this.SubscribingGameObject
            .GetRootParent()
            .GetComponent<InteractionEventHub>();
    }
}
