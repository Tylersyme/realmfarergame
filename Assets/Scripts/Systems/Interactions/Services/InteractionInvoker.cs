﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractionInvoker : IInteractionInvoker
{
    public T Invoke<T>(GameObject target, Interaction interaction) where T : InteractionResponse
    {
        var interactionEventHub = GetInteractionEventHub(target);

        return interactionEventHub.Interact<T>(interaction);
    }

    public InteractionType GetDefaultInteractionType(GameObject target)
    {
        return GetInteractionEventHub(target).DefaultInteractionType;
    }

    public bool IsInteractable(GameObject target)
    {
        return GetInteractionEventHub(target) != null;
    }

    private InteractionEventHub GetInteractionEventHub(GameObject target)
    {
        return target.GetComponent<InteractionEventHub>();
    }
}
