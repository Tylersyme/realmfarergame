﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IInteractionSubscriber
{
    void SubscribeAllInteractions(EventHandler<InteractionEventArgs> eventHandler);

    void SubscribeToInteractionAdjective(
        InteractionAdjectiveType interactionAdjectiveType,
        EventHandler<InteractionAdjectiveEventArgs> eventHandler);

    void UnsubscribeAll();

    void SubscribeToInteractionType(InteractionType interactionType, EventHandler<InteractionEventArgs> eventHandler);

    IInteractionSubscriber WithDefaultInteraction(InteractionType defaultInterationType);
}
