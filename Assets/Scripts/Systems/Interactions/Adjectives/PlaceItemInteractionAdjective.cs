﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaceItemInteractionAdjective : InteractionAdjective
{
    public override InteractionAdjectiveType Type { get => InteractionAdjectiveType.PlacesItem; }

    public Item PlacedItem { get; set; }

    public GameObject PlaceItemPrefab { get; set; }

    public Vector2 Position { get; set; }
}
