﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaceTileInteractionAdjective : InteractionAdjective
{
    public override InteractionAdjectiveType Type { get => InteractionAdjectiveType.PlacesTile; }

    public Item TileItem { get; set; }

    public TileType TileTypeToPlace { get; set; }
}
