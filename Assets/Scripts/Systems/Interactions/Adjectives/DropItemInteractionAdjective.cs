﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropItemInteractionAdjective : InteractionAdjective
{
    public override InteractionAdjectiveType Type { get => InteractionAdjectiveType.DropsItem; }

    public Item DroppedItem { get; set; }
}
