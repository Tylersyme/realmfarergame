﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamagesInteractionAdjective : InteractionAdjective
{
    public override InteractionAdjectiveType Type { get => InteractionAdjectiveType.Damages; }

    public Damage Damage { get; set; }
}
