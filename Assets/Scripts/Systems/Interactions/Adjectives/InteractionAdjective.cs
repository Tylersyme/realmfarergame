﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class InteractionAdjective
{
    public abstract InteractionAdjectiveType Type { get; }
}
