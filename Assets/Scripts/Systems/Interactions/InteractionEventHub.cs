﻿using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractionEventHub : MonoBehaviour
{
    public IList<InteractionAdjectiveType> ActiveAdjectiveTypes { get; private set; } = new List<InteractionAdjectiveType>();

    /// <summary>
    /// Represents the objects default interaction type for the player.
    /// </summary>
    [SerializeField]
    [HideInInspector]
    private InteractionType _defaultInteractionType = InteractionType.None;
    public InteractionType DefaultInteractionType { get => _defaultInteractionType; set => _defaultInteractionType = value; }

    public EventHandler<InteractionEventArgs> InteractionEvent;

    public T Interact<T>(Interaction interaction) where T : InteractionResponse
    {
        if (interaction.IsDefaultInteraction)
            interaction.Type = this.DefaultInteractionType;

        var interactionEventArgs = new InteractionEventArgs
        {
            Interaction = interaction,
        };

        this.InteractionEvent?.Invoke(interaction, interactionEventArgs);

        return (T)interactionEventArgs.InteractionResponse;
    }

    public void RegisterInteractionAdjectiveType(InteractionAdjectiveType interactionAdjectiveType)
    {
        if (this.ActiveAdjectiveTypes.Contains(interactionAdjectiveType))
            return;

        this.ActiveAdjectiveTypes.Add(interactionAdjectiveType);
    }
}

public enum InteractionPriority
{
    ItemModule,
    TreeModule,
}
