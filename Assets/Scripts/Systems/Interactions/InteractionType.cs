﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum InteractionType
{
    None,
    Item,
    PickupIntoInventory,
    OpenDoorToggle,
}
