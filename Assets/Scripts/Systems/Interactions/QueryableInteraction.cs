﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QueryableInteraction
{
    public Interaction Interaction { get; set; }

    public ObjectQueryProfile ObjectQueryProfile { get; set; }
}
