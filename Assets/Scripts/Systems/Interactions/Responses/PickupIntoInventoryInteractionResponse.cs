﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupIntoInventoryInteractionResponse : InteractionResponse
{
    /// <summary>
    /// The item which is being picked up.
    /// </summary>
    public Item Item { get; set; }
}
