﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenDoorInteractionResponse : InteractionResponse
{
    public bool WasOpened { get; set; }
}
