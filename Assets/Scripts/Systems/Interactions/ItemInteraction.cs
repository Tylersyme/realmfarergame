﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemInteraction : Interaction
{
    public override InteractionType Type { get => InteractionType.Item; }

    public Item ItemUsed { get; set; }

    public ItemActivationCause ItemActivationCause { get; set; }
}
