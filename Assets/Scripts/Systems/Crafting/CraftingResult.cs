﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CraftingResult
{
    public IList<Item> CraftedItems { get; set; }

    public IList<CraftingItem> SourceItems { get; set; }
}
