﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Describes an item used in the context of crafting.
/// This does not represent a physical item but instead it is the data which crafting recipes rely upon
/// to define itself.
/// </summary>
[System.Serializable]
public class CraftingItem
{
    /// <summary>
    /// The item used or produced in the crafting recipe.
    /// </summary>
    [SerializeField]
    private ItemTemplate itemTemplate;
    public ItemTemplate ItemTemplate { get => itemTemplate; set => itemTemplate = value; }

    /// <summary>
    /// The amount of the item used or produced in the crafting recipe.
    /// </summary>
    [SerializeField]
    [MinValue(1)]
    private int _quantity = 1;
    public int Quantity { get => _quantity; set => _quantity = value; }
}
