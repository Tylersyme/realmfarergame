﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "CraftingRecipe", menuName = "Database/Crafting Recipe")]
public class CraftingRecipe : SerializedScriptableObject
{
    /// <summary>
    /// The items required to produce the result.
    /// There may be more than one possible recipe which produces the same result.
    /// </summary>
    [SerializeField]
    private IList<CraftingItem> _recipe = new List<CraftingItem>();
    public IList<CraftingItem> Recipe { get => _recipe; set => _recipe = value; }

    /// <summary>
    /// The items produced as a result of crafting.
    /// </summary>
    [SerializeField]
    private IList<CraftingItem> _craftedItems = new List<CraftingItem>();
    public IList<CraftingItem> CraftedItems { get => _craftedItems; set => _craftedItems = value; }
}
