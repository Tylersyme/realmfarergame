﻿using Sirenix.OdinInspector;
using Sirenix.Serialization;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
#if UNITY_EDITOR
using UnityEngine;
#endif

[CreateAssetMenu(fileName = "CraftingRecipeDefinition", menuName = "Database/Crafting Recipe Definition")]
public class CraftingRecipeDefinition : SerializedScriptableObject
{
    [SerializeField]
    private IList<CraftingRecipe> _craftingRecipes = new List<CraftingRecipe>();
    public IList<CraftingRecipe> CraftingRecipes { get => _craftingRecipes; set => _craftingRecipes = value; }

    public CraftingItem GetResult() =>
        this.CraftingRecipes
            .ToList()
            .First()
            .CraftedItems
            .First();

    public Texture2D GetUiTexture() =>
        this.GetResult()
            .ItemTemplate
            .UiTexture;

#if UNITY_EDITOR

    [Button]
    [TitleGroup("Functions")]
    [PropertySpace(5)]
    private void CreateAndAddCraftingRecipe(string fileName = "CraftingRecipe")
    {
        fileName = Path.GetFileNameWithoutExtension(fileName);

        var currentPath = AssetDatabase.GetAssetPath(this);

        var craftingRecipeAsset = ScriptableObject.CreateInstance<CraftingRecipe>();

        var folderPath = Path.Combine(Path.GetDirectoryName(currentPath), "..", "Recipes", fileName) + ".asset";

        var assetPath = AssetDatabase.GenerateUniqueAssetPath(folderPath);

        AssetDatabase.CreateAsset(craftingRecipeAsset, assetPath);
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();

        this.CraftingRecipes.Add(AssetDatabase.LoadAssetAtPath<CraftingRecipe>(assetPath));
    }

#endif
}
