﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IItemSorter
{
    List<Item> CombineItems(List<Item> items);
    void CombineTwoItems(Item destination, Item source);
}
