﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IItemFactory
{
    Item Create(ItemTemplate itemTemplate);
    List<Item> CreateStacksFromTemplate(ItemTemplate itemTemplate, int quantity);
    Item Split(Item item, int amountToSplit);
}
