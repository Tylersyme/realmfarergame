﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Responsible for crafting items with crafting recipes and determining the item cost.
/// Note: This should never interact directly with inventories.
/// </summary>
public interface IItemCrafter
{
    CraftingResult CraftItems(CraftingRecipe craftingRecipe, int quantity);
    bool CanCraft(CraftingRecipe craftingRecipe, int quantity, Inventory inventory);
}
