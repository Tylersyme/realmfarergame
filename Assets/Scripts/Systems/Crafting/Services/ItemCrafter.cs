﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class ItemCrafter : IItemCrafter
{
    private IItemFactory _itemFactory;

    public ItemCrafter()
    {
        _itemFactory = new ItemFactory();
    }

    /// <summary>
    /// Will return the items produced by the crafting recipe and determine the source items
    /// which were used.
    /// </summary>
    /// <param name="craftingRecipe"></param>
    /// <param name="quantity"></param>
    /// <param name="container"></param>
    /// <returns></returns>
    public CraftingResult CraftItems(CraftingRecipe craftingRecipe, int quantity)
    {
        var craftedItems = this.GetCraftedItems(craftingRecipe, quantity);
        var sourceItems = this.GetSourceItems(craftingRecipe, quantity);

        return new CraftingResult
        {
            CraftedItems = craftedItems,
            SourceItems = sourceItems,
        };
    }

    public bool CanCraft(CraftingRecipe craftingRecipe, int quantity, Inventory inventory) =>
        craftingRecipe.Recipe.All(r => inventory.HasItemsOfType(r.ItemTemplate.Type, r.Quantity * quantity));

    private IList<Item> GetCraftedItems(CraftingRecipe craftingRecipe, int quantity) =>
        craftingRecipe.CraftedItems
            .SelectMany(ci => _itemFactory.CreateStacksFromTemplate(ci.ItemTemplate, ci.Quantity * quantity))
            .ToList();

    private IList<CraftingItem> GetSourceItems(CraftingRecipe craftingRecipe, int quantity) =>
        craftingRecipe.Recipe
            .Select(ci => new CraftingItem
            {
                ItemTemplate = ci.ItemTemplate,
                Quantity = ci.Quantity * quantity,
            })
            .ToList();
}
