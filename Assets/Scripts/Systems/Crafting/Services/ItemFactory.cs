﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ItemFactory : IItemFactory
{
    /// <summary>
    /// Creates a single item with a stack size of one.
    /// </summary>
    /// <param name="itemTemplate"></param>
    /// <returns></returns>
    public Item Create(ItemTemplate itemTemplate)
    {
        return this
            .CreateStacksFromTemplate(itemTemplate, 1)
            .Single();
    }

    /// <summary>
    /// Creates items in the form of stacks given the provided quantity.
    /// This will respect the item's max stack size.
    /// </summary>
    /// <param name="itemTemplate"></param>
    /// <param name="quantity"></param>
    /// <returns></returns>
    public List<Item> CreateStacksFromTemplate(ItemTemplate itemTemplate, int quantity)
    {
        if (quantity <= 0)
            return new List<Item>();

        var stackedItems = new List<Item>();

        var maxStackSize = itemTemplate.MaxStackSize;

        var totalStacks = Mathf.Max(1, quantity / maxStackSize);

        var remainingQuantity = quantity;

        for (var stackIdx = 0; stackIdx < totalStacks; stackIdx++)
        {
            var itemStackQuantity = Mathf.Min(maxStackSize, remainingQuantity);

            remainingQuantity -= itemStackQuantity;

            var createdItem = new Item(itemTemplate);
            createdItem.StackSize = itemStackQuantity;

            stackedItems.Add(createdItem);
        }

        return stackedItems;
    }

    public Item Split(Item item, int amountToSplit)
    {
        if (amountToSplit <= 0)
            return null;

        var amountRemaining = Mathf.Max(item.StackSize - amountToSplit, 0);

        item.StackSize = amountRemaining;

        return this
            .CreateStacksFromTemplate(item.ItemTemplate, amountToSplit)
            .Single();
    }

}
