﻿using Sirenix.Utilities;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ItemSorter : IItemSorter
{
    /// <summary>
    /// Combines items together so that they are stacked to max capacity.
    /// </summary>
    /// <param name="items"></param>
    /// <returns></returns>
    public List<Item> CombineItems(List<Item> items)
    {
        return items
            .GroupBy(i => i.Type)
            .SelectMany(ig => CombineLikeItems(ig))
            .ToList();
    }

    /// <summary>
    /// Combines items of the same type into max capacity stacks.
    /// </summary>
    /// <param name="items"></param>
    /// <returns></returns>
    private List<Item> CombineLikeItems(IEnumerable<Item> items)
    {
        var combinedItems = new List<Item>();

        var previousItem = default(Item);

        foreach (var item in items)
        {
            if (previousItem == null)
            {
                previousItem = item;
                continue;
            }

            CombineTwoItems(previousItem, item);

            if (!previousItem.IsMaxStack)
                continue;

            combinedItems.Add(previousItem);

            previousItem = item;
        }

        if (previousItem.HasAnyItems)
            combinedItems.Add(previousItem);

        return combinedItems;
    }

    /// <summary>
    /// Combines as much of item 2 into item 1 as possible;
    /// </summary>
    /// <param name="destination"></param>
    /// <param name="source"></param>
    /// <returns></returns>
    public void CombineTwoItems(Item destination, Item source)
    {
        if (destination.StackSize + source.StackSize > destination.MaxStackSize)
        {
            // Remove enough items from stack 2 to fill up stack 1
            source.StackSize -= destination.AvailableStackSpace;
            destination.StackSize = destination.MaxStackSize;
        }
        else
        {
            destination.StackSize += source.StackSize;
            source.StackSize = 0;
        }
    }
}
