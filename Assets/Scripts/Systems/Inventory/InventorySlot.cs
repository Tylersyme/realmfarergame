﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class InventorySlot
{
    [SerializeField]
    private Item _item;
    public Item Item { get => _item; set => _item = value; }

    [SerializeField]
    private int _slotPosition;
    public int SlotPosition { get => _slotPosition; set => _slotPosition = value; }

    [SerializeField]
    private InventoryCategory _inventoryCategory;
    public InventoryCategory InventoryCategory { get => _inventoryCategory; set => _inventoryCategory = value; }

    public bool HasItem { get => this.Item != null; }

    public InventorySlot(int slotPosition, InventoryCategory inventoryCategory)
    {
        this.SlotPosition = slotPosition;
        this.InventoryCategory = inventoryCategory;
    }
}
