﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Stores inventory related data for a container.
/// </summary>
[System.Serializable]
public abstract class Container
{
    [SerializeField]
    private Inventory _inventory;
    public Inventory Inventory { get => _inventory; set => _inventory = value; }
}
