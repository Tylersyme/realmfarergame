﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class EntityInventorySubtype
{
    public const string PRIMARY = "primary";
    public const string HOTBAR = "hotbar";
}
