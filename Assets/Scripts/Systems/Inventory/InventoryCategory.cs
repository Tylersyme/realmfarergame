﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class InventoryCategory
{
    [SerializeField]
    private string _categoryId;
    public string CategoryId { get => _categoryId; set => _categoryId = value; }

    [SerializeField]
    private int _startIndex;
    public int StartIndex { get => _startIndex; set => _startIndex = value; }

    [SerializeField]
    private int _endIndex;
    public int EndIndex { get => _endIndex; set => _endIndex = value; }

    public int Size { get => this.EndIndex - this.StartIndex + 1; }
}
