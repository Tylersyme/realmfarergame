﻿using Sirenix.Utilities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// Stores inventory data for a container.
/// </summary>
[System.Serializable]
public class Inventory
{
    [SerializeField]
    private IList<InventorySlot> _inventorySlots = new List<InventorySlot>();
    public IList<InventorySlot> InventorySlots { get => _inventorySlots; set => _inventorySlots = value; }

    [SerializeField]
    private IList<InventoryCategory> _inventoryCategories = new List<InventoryCategory>();
    private IList<InventoryCategory> InventoryCategories { get => _inventoryCategories; set => _inventoryCategories = value; }

    public int MaxSize { get => this.InventorySlots.Count; }

    public List<Item> Items
    {
        get => this.InventorySlots
            .Where(i => i.Item != null)
            .Select(i => i.Item)
            .ToList();
    }

    private IItemSorter _itemSorter;
    public IItemSorter ItemSorter
    {
        get
        {
            if (_itemSorter == null)
                _itemSorter = new ItemSorter();

            return _itemSorter;
        }
    }

    private IItemFactory _itemFactory;
    public IItemFactory ItemFactory 
    {
        get
        {
            if (_itemFactory == null)
                _itemFactory = new ItemFactory();

            return _itemFactory;
        }
    }

    public Inventory WithCategory(string categoryId, int size)
    {
        var inventoryCategory = new InventoryCategory
        {
            CategoryId = categoryId,
            StartIndex = this.InventorySlots.Count,
            EndIndex = this.InventorySlots.Count + size - 1,
        };

        this.InventoryCategories.Add(inventoryCategory);

        for (var sizeIdx = 0; sizeIdx < inventoryCategory.Size; sizeIdx++)
            this.InventorySlots.Add(new InventorySlot(this.InventorySlots.Count, inventoryCategory));

        return this;
    }

    public IList<InventorySlot> GetSlotsInCategory(string categoryId)
    {
        var category = this.GetCategory(categoryId);

        return this.InventorySlots
            .ToList()
            .GetRange(category.StartIndex, category.Size);
    }

    public InventorySlot GetSlotInCategory(string categoryId, int relativeSlotPosition)
    {
        var category = this.GetCategory(categoryId);

        if (relativeSlotPosition >= category.Size)
            throw new System.ArgumentOutOfRangeException(
                $"Error getting slot in category: The relative slot position {relativeSlotPosition} is out of bounds.");

        var slotPosition = category.StartIndex + relativeSlotPosition;

        return this.GetSlotAt(slotPosition);
    }

    public int GetCategorySize(string categoryId) =>
        this.GetCategory(categoryId).Size;

    public InventorySlot GetSlotContaining(Item item) =>
        this.InventorySlots.FirstOrDefault(s => s.Item == item);

    public InventorySlot GetSlotAt(int slotPosition) => 
        this.InventorySlots[slotPosition];

    public Item GetItemAt(int slotPosition) =>
        this.GetSlotAt(slotPosition).Item;

    public void SwapItems(int firstSlotPosition, int secondSlotPosition)
    {
        var firstSlot = this.GetSlotAt(firstSlotPosition);
        var secondSlot = this.GetSlotAt(secondSlotPosition);

        var firstItem = firstSlot.Item;

        firstSlot.Item = secondSlot.Item;
        secondSlot.Item = firstItem;
    }

    public void AddItems(IList<Item> items) =>
        items
            .GroupBy(i => i.Type)
            .ForEach(ig =>
            {
                ig.ForEach(i => AddItem(i));
            });

    public void AddItem(Item item)
    {
        var slotsWithStackSpace = this.GetSlotsWithStackSpace(item.Type);

        var slotIdx = 0;

        var slotWithStackSpace = default(InventorySlot);

        while (slotIdx < slotsWithStackSpace.Count && item.HasAnyItems)
        {
            slotWithStackSpace = slotsWithStackSpace[slotIdx];

            this.ItemSorter.CombineTwoItems(slotWithStackSpace.Item, item);

            ++slotIdx;
        }

        // Prevent items with a stack size of 0 from being added to the inventory
        if (!item.HasAnyItems)
            return;

        var nextEmptySlotPosition = this.GetNextEmptySlotPosition();

        if (!nextEmptySlotPosition.HasValue)
            throw new System.Exception("Error adding item: No empty slot was available.");

        this.AddItemAtSlotPosition(item, nextEmptySlotPosition.Value);
    }

    public void RemoveItems(Func<Item, bool> predicate, int quantity)
    {
        var slots = this.GetSlotsWhere(predicate, quantity);

        var remainingQuantity = quantity;

        for (var slotIdx = 0; slotIdx < slots.Count; slotIdx++)
        {
            var slot = slots[slotIdx];

            // The very last item stack may have more items than what needs to be removed
            // In this case subtract from the stack size rather than removing the entire
            // item stack
            if (slotIdx == slots.Count - 1 &&
                slot.Item.StackSize > remainingQuantity)
            {
                slot.Item.StackSize -= remainingQuantity;

                return;
            }

            remainingQuantity -= slot.Item.StackSize;

            this.RemoveItemAt(slot.SlotPosition);
        }
    }

    public void RemoveCraftingItems(IList<CraftingItem> craftingItems)
    {
        foreach (var craftingItem in craftingItems)
            this.RemoveItems(i => i.Type == craftingItem.ItemTemplate.Type, craftingItem.Quantity);
    }

    /// <summary>
    /// Returns enough item stacks such that the total number of items are >= the target quantity.
    /// The target quantity is a target which is not guranteed (or likely) to be met perfectly.
    /// </summary>
    /// <param name="itemType"></param>
    /// <param name="targetQuantity"></param>
    /// <returns></returns>
    public List<InventorySlot> GetSlotsWhere(Func<Item, bool> predicate, int targetQuantity)
    {
        var totalFound = 0;

        var slotsFound = new List<InventorySlot>();

        var slots = this.InventorySlots
            .Where(s => s.HasItem && 
                        predicate.Invoke(s.Item))
            .OrderBy(s => s.Item.StackSize)
            .ToList();

        foreach (var slot in slots)
        {
            if (totalFound >= targetQuantity)
                break;

            totalFound += slot.Item.StackSize;

            slotsFound.Add(slot);
        }

        return slotsFound;
    }

    public void RemoveItemAt(int slotPosition) =>
        this.GetSlotAt(slotPosition).Item = null;

    /// <summary>
    /// Removes the provided number of items from the stack.
    /// If the stack has less than one item remaining, it is removed.
    /// </summary>
    /// <param name="slotPosition"></param>
    /// <param name="toSubtract"></param>
    public void SubtractFromItemAt(int slotPosition, int toSubtract)
    {
        var item = this.GetItemAt(slotPosition);
        item.StackSize -= toSubtract;

        if (item.StackSize <= 0)
            this.RemoveItemAt(slotPosition);
    }

    public Item SplitItem(Item item, int amountToSplit)
    {
        var slotPosition = this.GetSlotContaining(item).SlotPosition;

        return this.SplitItemAt(slotPosition, amountToSplit);
    }

    public Item SplitItemAt(int slotPosition, int amountToSplit)
    {
        var item = this.GetItemAt(slotPosition);

        var splitItem = this.ItemFactory.Split(item, amountToSplit);

        if (item.StackSize <= 0)
            this.RemoveItemAt(slotPosition);

        return splitItem;
    }

    public IEnumerable<InventorySlot> GetSlotsContainingType(ItemType itemType) =>
        this.InventorySlots.Where(i => i.HasItem && 
                                       i.Item.Type == itemType);

    public List<InventorySlot> GetSlotsWithStackSpace(ItemType itemType)
    {
        return this.GetSlotsContainingType(itemType)
            .Where(i => !i.Item.IsMaxStack)
            .OrderByDescending(i => i.Item.StackSize)
            .ToList();
    }

    private int? GetNextEmptySlotPosition()
    {
        foreach (var inventorySlot in this.InventorySlots)
        {
            if (inventorySlot.HasItem)
                continue;

            return inventorySlot.SlotPosition;
        }

        return null;
    }

    public void AddItemAtSlotPosition(Item item, int slotPosition)
    {
        var inventorySlot = this.GetSlotAt(slotPosition);

        // TODO: Add cases when inventory slot is not empty
        if (!inventorySlot.HasItem)
            inventorySlot.Item = item;
    }

    public bool HasItemsOfType(ItemType itemType, int quantity)
    {
        var hasItems = this
            .GetSlotsContainingType(itemType)
            .Sum(s => s.Item.StackSize) >= quantity;

        return hasItems;
    }

    private InventoryCategory GetCategory(string categoryId) =>
        this.InventoryCategories.First(c => c.CategoryId == categoryId);
}
