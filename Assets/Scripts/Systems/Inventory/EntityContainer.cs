﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[System.Serializable]
public class EntityContainer : Container
{
    [SerializeField]
    private int _selectedSlotPosition;
    public int SelectedSlotPosition { get => _selectedSlotPosition; private set => _selectedSlotPosition = value; }

    public Item SelectedItem { get => base.Inventory.GetItemAt(this.SelectedSlotPosition); }

    public EntityContainer()
    {
        base.Inventory = new Inventory()
            .WithCategory(EntityInventorySubtype.HOTBAR, 10)
            .WithCategory(EntityInventorySubtype.PRIMARY, 32);

        // Starts at first item in hotbar
        this.SelectedSlotPosition = base.Inventory.GetSlotInCategory(EntityInventorySubtype.HOTBAR, 0).SlotPosition;
    }

    public IList<Item> GetPrimaryItems() =>
        base.Inventory
            .GetSlotsInCategory(EntityInventorySubtype.PRIMARY)
            .Select(s => s.Item)
            .ToList();

    public IList<Item> GetHotbarItems() =>
        base.Inventory
            .GetSlotsInCategory(EntityInventorySubtype.HOTBAR)
            .Select(s => s.Item)
            .ToList();

    public int GetPrimaryInventorySize() =>
        base.Inventory.GetCategorySize(EntityInventorySubtype.PRIMARY);

    public int GetHotbarInventorySize() =>
        base.Inventory.GetCategorySize(EntityInventorySubtype.HOTBAR);

    public void SetSelectedSlotPosition(int slotPosition) =>
        this.SelectedSlotPosition = slotPosition;

    public InventorySlot GetHotbarSlotAt(int relativeSlotPosition) =>
        base.Inventory.GetSlotInCategory(EntityInventorySubtype.HOTBAR, relativeSlotPosition);
}
