﻿using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

public class DayNightCycle : SerializedMonoBehaviour
{
    [SerializeField]
    [Required]
    [ListDrawerSettings(CustomAddFunction = nameof(TimeOfDays_Add))]
    private List<TimeOfDay> _timeOfDays = new List<TimeOfDay>();
    public List<TimeOfDay> TimeOfDays { get => _timeOfDays; private set => _timeOfDays = value; }

    [SerializeField]
    [Required]
    private AnimationCurve _globalLightIntensity = new AnimationCurve(new Keyframe(0, 0), new Keyframe(1, 1));
    public AnimationCurve GlobalLightIntensity { get => _globalLightIntensity; set => _globalLightIntensity = value; }

    [SerializeField]
    [Required]
    private Light2D _globalLight;
    public Light2D GlobalLight { get => _globalLight; set => _globalLight = value; }

    [SerializeField]
    [MinValue(0)]
    [MaxValue(nameof(SecondsPerCycle))]
    [SuffixLabel("Seconds")]
    private float _startTime;
    public float StartTime { get => _startTime; set => _startTime = value; }

    [ShowInInspector]
    public float CurrentTime { get; private set; }

    [ShowInInspector]
    [TitleGroup("Info")]
    public float SecondsPerCycle
    {
        get => this.TimeOfDays.Any()
            ? this.TimeOfDays.Max(t => t.TimeRange.End)
            : -1.0f;
    }

    [ShowInInspector]
    [TitleGroup("Info")]
    public float MinutesPerCyle
    {
        get => (float)Math.Round(this.SecondsPerCycle * .01666666f, 2);
    }

    [ShowInInspector]
    private string DebugTimeOfDay
    {
        get => this.CurrentTimeOfDay.Name;
    }

    public TimeOfDay CurrentTimeOfDay
    {
        get => this.TimeOfDays.First(t => t.TimeRange.Contains(this.CurrentTime));
    }


#if UNITY_EDITOR

    [SerializeField]
    private bool _forceFullIntensity = false;
    private bool ForceFullIntensity { get => _forceFullIntensity; set => _forceFullIntensity = value; }

#endif

    /// <summary>
    /// A value between 0 and 1 which shows how far into the cycle the current time is.
    /// </summary>
    public float ElapsedTimePercentage { get => this.CurrentTime / this.SecondsPerCycle; }

    private void Start()
    {
        this.CurrentTime = this.StartTime;
    }

    private void Update()
    {
        this.UpdateTime();
    }

    private void UpdateTime()
    {
        this.CurrentTime += Time.deltaTime;

        if (this.CurrentTime >= this.SecondsPerCycle)
            this.CurrentTime -= this.SecondsPerCycle;

        this.UpdateGlobalLighting();
    }

    private void UpdateGlobalLighting()
    {
#if UNITY_EDITOR

        if (this.ForceFullIntensity)
        {
            this.GlobalLight.intensity = 1;

            return;
        }

#endif

        this.GlobalLight.intensity = this.GlobalLightIntensity.Evaluate(this.ElapsedTimePercentage);
    }

    [Button("Set Time")]
    private void SetTimeDebug(float newCurrentTime)
    {
        this.CurrentTime = newCurrentTime;

        this.UpdateTime();
    }

    private void TimeOfDays_Add()
    {
        this.TimeOfDays.Add(new TimeOfDay
        {
            TimeRange = new Range(this.SecondsPerCycle, this.SecondsPerCycle + 60)
        });
    }
}
