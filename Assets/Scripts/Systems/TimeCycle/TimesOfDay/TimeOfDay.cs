﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TimeOfDay
{
    [SerializeField]
    [Required]
    private string _name;
    public string Name { get => _name; set => _name = value; }

    [SerializeField]
    [Required]
    private Range _timeRange = new Range(0, 1);
    public Range TimeRange { get => _timeRange; set => _timeRange = value; }
}
