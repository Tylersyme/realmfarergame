﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConditionChangedEventArgs
{
    public Condition Condition { get; set; }

    public bool IsFulfilled { get; set; }

    public ConditionChangedCause ConditionChangedCause { get; set; }

    public ConditionChangedEventArgs(Condition condition, bool isFulfilled)
    {
        this.Condition = condition;
        this.IsFulfilled = isFulfilled;
    }
}
