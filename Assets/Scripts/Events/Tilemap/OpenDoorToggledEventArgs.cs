﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenDoorToggledEventArgs
{
    public bool WasOpened { get; set; }
}
