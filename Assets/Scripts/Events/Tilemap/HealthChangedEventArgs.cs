﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthChangedEventArgs
{
    public float MaxHealth { get; set; }

    public float OriginalHealth { get; set; }

    public float CurrentHealth { get; set; }

    public HealthChangeCause HealthChangeCause { get; set; }

    public bool HasHealth { get => this.CurrentHealth <= 0.0f; }
}
