﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DespawnEventArgs
{
    public DespawnCause DespawnCause { get; set; }
}
