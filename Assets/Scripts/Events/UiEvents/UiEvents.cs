﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleInventoryEventArgs
{
    /// <summary>
    /// The inventory container being opened.
    /// </summary>
    public EntityContainer EntityContainer { get; set; }
}

public class PreToggleInventoryEventArgs
{
    /// <summary>
    /// Whether the inventory is being toggled on or off.
    /// </summary>
    public bool IsOpening { get; set; }

    /// <summary>
    /// The inventory container being opened.
    /// </summary>
    public EntityContainer EntityContainer { get; set; }
}
