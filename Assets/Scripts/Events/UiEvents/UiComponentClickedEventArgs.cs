﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UiComponentClickedEventArgs
{
    public UiComponent ClickedUiComponent { get; set; }
}
