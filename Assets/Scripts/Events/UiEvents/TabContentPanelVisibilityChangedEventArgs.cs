﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TabContentPanelVisibilityChangedEventArgs
{
    public bool IsVisible { get; set; }
}
