﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectedHotbarSlotChangedEventArgs
{
    public int SelectedSlotPosition { get; set; }

    public Item SelectedItem { get; set; }
}
