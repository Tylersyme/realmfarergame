﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryChangedEventArgs
{
    public Inventory Inventory { get; set; }
}
