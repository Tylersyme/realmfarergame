﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractionAdjectiveEventArgs
{
    public Interaction Interaction { get; set; }

    public InteractionAdjective InteractionAdjective { get; set; }

    public IList<InteractionAdjectiveType> AllAdjectiveTypes { get; set; }

    public InteractionAdjectiveEventArgs()
    {
        this.AllAdjectiveTypes = new List<InteractionAdjectiveType>();
    }
}
