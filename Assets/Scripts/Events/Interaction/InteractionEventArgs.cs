﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractionEventArgs
{
    public Interaction Interaction { get; set; }

    public InteractionResponse InteractionResponse { get; set; }
}
