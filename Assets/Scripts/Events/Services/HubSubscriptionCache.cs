﻿using Easy.MessageHub;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HubSubscriptionCache : IHubSubscriptionCache
{
    private IDictionary<Guid, IMessageHub> SubscriptionTokenCache { get; set; }

    public HubSubscriptionCache()
    {
        this.SubscriptionTokenCache = new Dictionary<Guid, IMessageHub>();
    }

    public void Subscribe<T>(IMessageHub messageHub, Action<T> callback)
    {
        var token = messageHub.Subscribe(callback);

        AddSubscriptionToken(token, messageHub);
    }

    public void AddSubscriptionToken(Guid token, IMessageHub messageHub)
    {
        this.SubscriptionTokenCache.Add(token, messageHub);
    }

    public void UnsubscribeAll()
    {
        foreach (var keyValue in this.SubscriptionTokenCache)
            keyValue.Value.Unsubscribe(keyValue.Key);

        this.SubscriptionTokenCache.Clear();
    }
}
