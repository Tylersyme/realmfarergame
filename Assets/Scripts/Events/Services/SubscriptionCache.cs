﻿using Sirenix.Utilities;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SubscriptionCache : ISubscriptionCache
{
    private IList<ISubscription> Subscriptions { get; set; } = new List<ISubscription>();

    public ISubscriptionCache Subscribe<TEventArgs>( 
        EventHandler<TEventArgs> eventHandler,
        Action<EventHandler<TEventArgs>> subscription, 
        Action<EventHandler<TEventArgs>> unsubscription)
    {
        subscription.Invoke(eventHandler);

        this.Subscriptions.Add(new Subscription<TEventArgs>(unsubscription, eventHandler));

        return this;
    }

    public void UnsubscribeAll() =>
        this.Subscriptions.ForEach(s => s.Unsubscribe());

    private interface ISubscription
    {
        void Unsubscribe();
    }

    private class Subscription<TEventArgs> : ISubscription
    {
        private Action<EventHandler<TEventArgs>> Unsubscription;

        private EventHandler<TEventArgs> EventHandler { get; set; }

        public Subscription(Action<EventHandler<TEventArgs>> unsubscription, EventHandler<TEventArgs> eventHandler)
        {
            this.Unsubscription = unsubscription;
            this.EventHandler = eventHandler;
        }

        public void Unsubscribe() =>
            this.Unsubscription.Invoke(this.EventHandler);
    }
}
