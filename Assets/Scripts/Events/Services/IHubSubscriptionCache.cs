﻿using Easy.MessageHub;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IHubSubscriptionCache
{
    void Subscribe<T>(IMessageHub messageHub, Action<T> callback);
    void AddSubscriptionToken(Guid token, IMessageHub messageHub);
    void UnsubscribeAll();
}
