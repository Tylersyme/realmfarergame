﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ISubscriptionCache
{
    ISubscriptionCache Subscribe<TEventArgs>(
        EventHandler<TEventArgs> eventHandler,
        Action<EventHandler<TEventArgs>> subscription,
        Action<EventHandler<TEventArgs>> unsubscription);
    void UnsubscribeAll();
}
