﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TilemapTileChangedEventArgs
{
    public Vector3Int TileChangedTilemapPosition { get; set; }

    public bool ShouldUpdateTilemap { get; set; } = false;
}
