﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveGameEventArgs
{
    /// <summary>
    /// The name identifying the save. Will be used as the name of the save file.
    /// </summary>
    public string SaveName { get; set; }
}
