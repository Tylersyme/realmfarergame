﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NeedSatisfactionChangedEventArgs
{
    public NeedType NeedType { get; set; }

    public float PreviousSatisfaction { get; set; }

    public float NewSatisfaction { get; set; }
}
