﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RegionLoadingEventArgs
{
    public Vector2Int RegionPosition { get; set; }
}
