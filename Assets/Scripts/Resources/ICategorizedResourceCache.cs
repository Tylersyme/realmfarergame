﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ICategorizedResourceCache<TValue> : IResourceCache<TValue>
{
    TValue GetResource(string key, string category);
    ICollection<TValue> GetResources(string category);
    void LoadResource(string key, string category, TValue value);
}
