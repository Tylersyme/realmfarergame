﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public sealed class ResourceTable
{
    public ResourceTable()
    {
        this.ResourceInfos = new List<ResourceInfo>();
    }

    public string FileName { get; set; }

    public string RelativePath { get; set; }

    public List<ResourceInfo> ResourceInfos { get; set; }
}
