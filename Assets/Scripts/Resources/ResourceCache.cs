﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ResourceCache<TValue> : IResourceCache<TValue>
{
    private IDictionary<string, TValue> Resources { get; set; }

    public ResourceCache()
    {
        this.Resources = new Dictionary<string, TValue>();
    }

    public TValue GetResource(string key)
    {
        var resource = this.Resources[key];

        if (resource == null)
            throw new System.NullReferenceException($"Error getting resource: No resource with key \'{key}\' could be found.");

        return resource;
    }

    public void LoadResource(string key, TValue value)
    {
        // Ensure no duplicate key is created
        if (this.Resources.Any(r => r.Key == key))
            throw new System.Exception($"Error loading resource: A resource with key \'{key}\' already exists.");

        this.Resources.Add(key, value);
    }

    public void Clear()
    {
        this.Resources.Clear();
    }
}
