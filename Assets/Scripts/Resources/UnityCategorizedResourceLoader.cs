﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnityCategorizedResourceLoader : IUnityCategorizedResourceLoader
{
    public void LoadIntoCache<T>(string resourceName, string directoryPath, string category, ICategorizedResourceCache<T> resourceCache)
        where T : UnityEngine.Object
    {
        var resource = Resources.Load<T>(System.IO.Path.Combine(directoryPath, resourceName));

        resourceCache.LoadResource(resourceName, category, resource);
    }
}
