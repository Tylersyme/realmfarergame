﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IResourceLoader
{
    void LoadIntoCache<T>(string resourceName, string directoryPath, IResourceCache<T> resourceCache);

    void LoadIntoCache<T>(string resourcePath, IResourceCache<T> resourceCache);

    void LoadIntoCategorizedCache<T>(string resourceName, string directoryPath, string category, ICategorizedResourceCache<T> resourceCache);

    void LoadIntoCategorizedCache<T>(string resourcePath, string category, ICategorizedResourceCache<T> resourceCache);
}
