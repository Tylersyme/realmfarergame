﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicMaterials
{
    private const string BASIC_MATERIALS_DIRECTORY = @"Materials\Basic\";

    public static readonly Material UNLIT = Resources.Load<Material>(BASIC_MATERIALS_DIRECTORY + @"Unlit");
}
