﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class DatabaseRecord<TRecord> where TRecord : UnityEngine.Object
{
    [SerializeField]
    private short _assetId;
    public short AssetId { get => _assetId; set => _assetId = value; }

    [SerializeField]
    private short _databaseId;
    public short DatabaseId { get => _databaseId; set => _databaseId = value; }

    public DatabaseRecord() { }

    public DatabaseRecord(short assetId, short databaseId)
    {
        this.AssetId = assetId;
        this.DatabaseId = databaseId;
    }

    public override bool Equals(object obj)
    {
        if (!(obj is DatabaseRecord<TRecord>))
            return false;

        var other = (DatabaseRecord<TRecord>)obj;

        return this.AssetId == other.AssetId &&
               this.DatabaseId == other.DatabaseId;
    }

    public static bool operator==(DatabaseRecord<TRecord> lhs, DatabaseRecord<TRecord> rhs)
    {
        return lhs.Equals(rhs);
    }

    public static bool operator!=(DatabaseRecord<TRecord> lhs, DatabaseRecord<TRecord> rhs)
    {
        return !lhs.Equals(rhs);
    }

    public override int GetHashCode()
    {
        var hash = 17;

        hash = hash * 23 + this.AssetId.GetHashCode();

        hash = hash * 23 + this.DatabaseId.GetHashCode();

        return hash;
    }
}
