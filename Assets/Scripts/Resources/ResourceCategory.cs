﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResourceCategory
{
    public static readonly ResourceCategory TILE = new ResourceCategory("Tile");
    public static readonly ResourceCategory FOLIAGE = new ResourceCategory("Foliage");
    public static readonly ResourceCategory TREES = new ResourceCategory("Trees");

    public string Name { get; set; }

    private ResourceCategory(string name)
    {
        this.Name = name;
    }

    public ResourceCategory Combine(ResourceCategory resourceCategory)
    {
        return new ResourceCategory($"{this.Name}.{resourceCategory.Name}");
    }
}
