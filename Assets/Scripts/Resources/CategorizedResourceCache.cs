﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CategorizedResourceCache<TValue> : ICategorizedResourceCache<TValue>
{
    private IDictionary<string, IDictionary<string, TValue>> Resources { get; set; }

    public CategorizedResourceCache()
    {
        this.Resources = new Dictionary<string, IDictionary<string, TValue>>();
    }

    public TValue GetResource(string key, string category)
    {
        var resource = this.Resources[category].FirstOrDefault(r => r.Key == key).Value;

        if (resource == null)
            throw new System.NullReferenceException($"Error getting resource: No resource with key \'{key}\' in category \'{category}\' could be found.");

        return resource;
    }

    public ICollection<TValue> GetResources(string category)
    {
        return this.Resources[category].Values;
    }

    public TValue GetResource(string key)
    {
        var resource = this.Resources
            .SelectMany(r => r.Value)
            .FirstOrDefault(r => r.Key == key)
            .Value;

        if (resource == null)
            throw new System.NullReferenceException($"Error getting resource: No resource with key \'{key}\' could be found.");

        return resource;
    }

    public void LoadResource(string key, string category, TValue value)
    {
        // Create category if it doesn't exist
        if (!this.Resources.Any(r => r.Key == category))
            this.Resources.Add(category, new Dictionary<string, TValue>());
        // Ensure no duplicate key is created
        else if (this.Resources[category].Any(r => r.Key == key))
            throw new System.Exception($"Error loading resource: A resource with key \'{key}\' already exists in category \'{category}\'.");

        this.Resources[category].Add(key, value);
    }

    public void Clear()
    {
        this.Resources.Clear();
    }

    [Obsolete]
    public void LoadResource(string key, TValue value)
    {
        LoadResource(key, string.Empty, value);
    }
}
