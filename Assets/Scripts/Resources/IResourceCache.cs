﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IResourceCache<TValue>
{
    void LoadResource(string key, TValue value);
    TValue GetResource(string key);
    void Clear();
}
