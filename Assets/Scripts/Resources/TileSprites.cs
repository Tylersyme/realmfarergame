﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileSprites
{
    private const string TILE_SPRITES_DIRECTORY = @"Sprites\Tiles\";

    public static readonly Sprite DIRT = Resources.Load<Sprite>(TILE_SPRITES_DIRECTORY + @"DirtTile");
}
