﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class TextureGroupRecords
{
    public static readonly DatabaseRecord<BitmaskingTextureGroup> GRASSLETS_DECORATION_GROUP = 
        new DatabaseRecord<BitmaskingTextureGroup>(13046, TextureGroupDatabaseIds.TILE_TEXTURE_GROUP_DATABASE);
}

public static class TextureGroupDatabaseIds
{
    public const short TILE_TEXTURE_GROUP_DATABASE = 22705;
}
