﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class PrefabRecords
{
    public static class Trees
    {
        public static readonly DatabaseRecord<PrefabContainer> OAK_TREE = 
            new DatabaseRecord<PrefabContainer>(24896, PrefabDatabaseIds.FOLIAGE_PREFAB_DATABASE);

        public static readonly DatabaseRecord<PrefabContainer> POPLAR_TREE = 
            new DatabaseRecord<PrefabContainer>(7210, PrefabDatabaseIds.FOLIAGE_PREFAB_DATABASE);
    }
}

public static class PrefabDatabaseIds
{
    public const short FOLIAGE_PREFAB_DATABASE = 17584;
}
