﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ShaderRecords
{
    public static readonly DatabaseRecord<ShaderContainer> TILE_BLEND =
        new DatabaseRecord<ShaderContainer>(14397, ShaderDatabaseIds.TILE_SHADER_DATABASE);
}

public static class ShaderDatabaseIds
{
    public const short TILE_SHADER_DATABASE = 26580;
}
