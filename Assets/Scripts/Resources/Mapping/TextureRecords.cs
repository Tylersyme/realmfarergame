﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Stores the records of individual textures as records which contain the asset id and database id
/// </summary>
public static class TextureRecords
{
    public static class Tiles
    {
        public static readonly DatabaseRecord<TextureContainer> DIRT_TILE = 
            new DatabaseRecord<TextureContainer>(20417, TextureDatabaseIds.TILE_DATABASE);

        public static readonly DatabaseRecord<TextureContainer> STONE_TILE = 
            new DatabaseRecord<TextureContainer>(30622, TextureDatabaseIds.TILE_DATABASE);

        public static readonly DatabaseRecord<TextureContainer> FOREST_GRASS_TILE_00 = 
            new DatabaseRecord<TextureContainer>(27418, TextureDatabaseIds.TILE_DATABASE);

        public static readonly DatabaseRecord<TextureContainer> FOREST_GRASS_SMALL_BRUSH_00 =
            new DatabaseRecord<TextureContainer>(1686, TextureDatabaseIds.TILE_DATABASE);

        public static readonly DatabaseRecord<TextureContainer> FOREST_GRASS_SMALL_BRUSH_01 =
            new DatabaseRecord<TextureContainer>(32109, TextureDatabaseIds.TILE_DATABASE);

        public static readonly DatabaseRecord<TextureContainer> SHALLOW_WATER_TILE =
            new DatabaseRecord<TextureContainer>(7694, TextureDatabaseIds.TILE_DATABASE);
    }

    public static class Decorations
    {
        public static readonly DatabaseRecord<TextureContainer> GRASSLETS_00 =
            new DatabaseRecord<TextureContainer>(6306, TextureDatabaseIds.DECORATION_DATABASE);
    }
}

public static class TextureDatabaseIds
{
    public const short TILE_DATABASE = 15612;
    public const short DECORATION_DATABASE = 13766;
}
