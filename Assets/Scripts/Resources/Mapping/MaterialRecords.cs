﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class MaterialRecords
{
    public static readonly DatabaseRecord<MaterialContainer> FOREST_GRASS_TILE =
        new DatabaseRecord<MaterialContainer>(13440, MaterialDatabaseIds.MATERIAL_DATABASE);

    public static readonly DatabaseRecord<MaterialContainer> MOUNTAIN_TILE =
        new DatabaseRecord<MaterialContainer>(16325, MaterialDatabaseIds.MATERIAL_DATABASE);

    public static readonly DatabaseRecord<MaterialContainer> SHALLOW_WATER_TILE =
        new DatabaseRecord<MaterialContainer>(32232, MaterialDatabaseIds.MATERIAL_DATABASE);

    public static readonly DatabaseRecord<MaterialContainer> WOOD_TILE =
    new DatabaseRecord<MaterialContainer>(22093, MaterialDatabaseIds.MATERIAL_DATABASE);

    public static readonly DatabaseRecord<MaterialContainer> STANDARD_TILEMAP_DECORATION =
        new DatabaseRecord<MaterialContainer>(17712, MaterialDatabaseIds.MATERIAL_DATABASE);
}

public static class MaterialDatabaseIds
{
    public const short MATERIAL_DATABASE = 25958;
}
