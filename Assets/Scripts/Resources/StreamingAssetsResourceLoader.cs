﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;

public class StreamingAssetsResourceLoader : IResourceLoader
{
    public void LoadIntoCache<T>(string resourceName, string directoryPath, IResourceCache<T> resourceCache)
    {
        var resource = DataSerializer.Instance.Deserialize<T>(resourceName, directoryPath);

        resourceCache.LoadResource(Path.GetFileNameWithoutExtension(resourceName), resource);
    }

    public void LoadIntoCache<T>(string resourcePath, IResourceCache<T> resourceCache)
    {
        var fileNameAndDirectory = IOExtended.SplitFileNameAndDirectory(resourcePath);

        LoadIntoCache(fileNameAndDirectory.Item1, fileNameAndDirectory.Item2, resourceCache);
    }

    public void LoadIntoCategorizedCache<T>(string resourceName, string directoryPath, string category, ICategorizedResourceCache<T> resourceCache)
    {
        var resource = DataSerializer.Instance.Deserialize<T>(resourceName, directoryPath);

        resourceCache.LoadResource(Path.GetFileNameWithoutExtension(resourceName), category, resource);
    }

    public void LoadIntoCategorizedCache<T>(string resourcePath, string category, ICategorizedResourceCache<T> resourceCache)
    {
        var fileNameAndDirectory = IOExtended.SplitFileNameAndDirectory(resourcePath);

        LoadIntoCategorizedCache(fileNameAndDirectory.Item1, fileNameAndDirectory.Item2, category, resourceCache);
    }
}
