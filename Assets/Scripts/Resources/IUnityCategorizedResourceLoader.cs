﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IUnityCategorizedResourceLoader
{
    void LoadIntoCache<T>(string resourceName, string directoryPath, string category, ICategorizedResourceCache<T> resourceCache)
        where T : UnityEngine.Object;
}
