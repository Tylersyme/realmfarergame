﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public sealed class ResourceInfo
{
    public string UniqueIdentifier { get; set; }

    public string ResourceName { get; set; }

    public string ResourceCategory { get; set; }

    public string RelativePath { get; set; }
}
