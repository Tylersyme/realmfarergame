﻿using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class WorldManager : MonoBehaviour
{
    private static WorldManager _instance;
    public static WorldManager Instance
    {
        get
        {
            if (_instance == null)
                _instance = UnityEngine.Object.FindObjectOfType<WorldManager>();

            return _instance;
        }
    }

    [SerializeField]
    private TilemapRenderer _tilemapRenderer;
    public TilemapRenderer TilemapRenderer { get => _tilemapRenderer; set => _tilemapRenderer = value; }

    [SerializeField]
    private string _seedValue = "Seed";
    public string SeedValue { get => _seedValue; set => _seedValue = value; }
    public Seed WorldSeed { get => new Seed(this.SeedValue); }

    [SerializeField]
    private Vector2Int _worldDimensions = new Vector2Int(100, 100);
    public Vector2Int WorldSize { get => _worldDimensions; set => _worldDimensions = value; }

    /// <summary>
    /// Defines the width and height of each region.
    /// Depth is variable.
    /// </summary>
    [SerializeField]
    private Vector2Int _regionSize = new Vector2Int(200, 200);
    public Vector2Int RegionSize { get => _regionSize; set => _regionSize = value; }

    [SerializeField]
    private Vector2Int _currentRegionPosition;
    public Vector2Int CurrentRegionPosition { get => _currentRegionPosition; set => _currentRegionPosition = value; }

    public World World { get; set; }

    public Region CurrentRegion { get; set; }

    public Tilemap CurrentTilemap { get => this.CurrentRegion.Tilemap; }

    [SerializeField]
    private ForestGenerationParameters _forestGenerationParameters;
    public ForestGenerationParameters ForestGenerationParameters { get => _forestGenerationParameters; set => _forestGenerationParameters = value; }

    [SerializeField]
    private NoiseMapParameters _heightMapParameters;
    public NoiseMapParameters HeightMapParameters { get => _heightMapParameters; set => _heightMapParameters = value; }

    [SerializeField]
    private NoiseMapParameters _temperatureMapParameters;
    public NoiseMapParameters TemperatureMapParameters { get => _temperatureMapParameters; set => _temperatureMapParameters = value; }

    [SerializeField]
    private NoiseMapParameters _precipitationMapParameters;
    public NoiseMapParameters PrecipitationMapParameters { get => _precipitationMapParameters; set => _precipitationMapParameters = value; }

    public IWorldGenerator WorldGenerator { get; private set; }
    public IWorldBuilder WorldBuilder { get; private set; }

    private void Start()
    {
        this.WorldGenerator = new WorldGenerator();
        this.WorldBuilder = new WorldBuilder();

        if (this.TilemapRenderer == null)
            this.TilemapRenderer = FindObjectOfType<TilemapRenderer>();
    }

    private void Awake()
    {
        _instance = this;
    }

    public void LoadRegion(Vector2Int regionPosition)
    {
        if (this.World == null)
            throw new System.Exception("Error loading region: The world is null.");

        var region = this.World.GetRegion(regionPosition);

        if (!region.IsGenerated)
            throw new System.Exception($"Error loading region: The region at {regionPosition} has not been generated.");

        this.CurrentRegion = region;

        this.CurrentRegionPosition = regionPosition;
    }

    public void RenderCurrentRegion()
    {
        if (this.CurrentRegion == null)
            throw new System.Exception("Error rendering tilemap: The current region is null.");

        this.TilemapRenderer.LoadTilemap(this.CurrentTilemap);
        this.TilemapRenderer.Render();
    }

    [Button(ButtonSizes.Medium)]
    [DisableInEditorMode]
    public void GenerateRegion()
    {
        // Do nothing if a world was not generated
        if (this.World == null)
            return;

        this.HeightMapParameters.Dimensions = this.RegionSize;

        TilemapManager.Instance.DestroyTilemap();

        // Generate and load the region
        this.World.GenerateRegion(this.CurrentRegionPosition, this.HeightMapParameters);

        LoadRegion(this.CurrentRegionPosition);

        RenderCurrentRegion();
    }

    [Button(ButtonSizes.Medium)]
    [DisableInEditorMode]
    public void GenerateWorld()
    {
        var worldParameters = this.WorldBuilder
            .NewWorld(this.WorldSeed, this.WorldSize.x, this.WorldSize.y, this.RegionSize.x, this.RegionSize.y)
            .WithHeightParameters(this.HeightMapParameters)
            .WithTemperatureParameters(this.TemperatureMapParameters)
            .WithPrecipitationParameters(this.PrecipitationMapParameters)
            .Build();

        this.WorldSize = this.WorldSize;
        this.RegionSize = this.RegionSize;

        var world = this.WorldGenerator.GenerateWorld(worldParameters);

        this.World = world;
    }
}