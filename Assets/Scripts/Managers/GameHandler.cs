﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameHandler : MonoBehaviour
{
    private static GameHandler _instance;
    public static GameHandler Instance
    {
        get
        {
            if (_instance == null)
                _instance = Object.FindObjectOfType<GameHandler>();

            return _instance;
        }
    }

    private IHubSubscriptionCache _subscriptionCache;
    private ISaveService _saveService;

    private void Start()
    {
        _subscriptionCache = new HubSubscriptionCache();
        _saveService = new SaveService();

        Subscribe();
    }

    private void OnDestroy() => _subscriptionCache.UnsubscribeAll();

    private void OnSaveGame(SaveGameEventArgs eventArgs)
    {
        _saveService.Save(eventArgs.SaveName);
    }

    private void Subscribe()
    {
        _subscriptionCache.Subscribe<SaveGameEventArgs>(EventManager.Instance.SystemEventHub, OnSaveGame);
    }

    private void Awake()
    {
        _instance = this;
    }

#if UNITY_EDITOR

    [Button(ButtonSizes.Medium)]
    [DisableInEditorMode]
    private void SaveGame()
    {
        EventManager.Instance.SystemEventHub.Publish(new SaveGameEventArgs { SaveName = this.SaveGameName });
    }

    [Button(ButtonSizes.Medium)]
    [DisableInEditorMode]
    private void LoadGame()
    {
        _saveService.Load(this.SelectedSaveGameName);
    }

    [SerializeField]
    [LabelText("Save As")]
    private string _saveGameName = "WildernessSave";
    public string SaveGameName { get => _saveGameName; set => _saveGameName = value; }

    [ValueDropdown(nameof(SaveGameNames), SortDropdownItems = true)]
    [ShowInInspector]
    [LabelText("Load Save")]
    [SerializeField]
    private string _selectedSaveGameName;
    public string SelectedSaveGameName { get => _selectedSaveGameName; set => _selectedSaveGameName = value; }

    public List<string> SaveGameNames { get => (new SaveService()).GetSavedGameNames(); }

#endif
}