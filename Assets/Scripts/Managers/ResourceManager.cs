﻿using MyLib.Shared.Database;
using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

public class ResourceManager : MonoBehaviour
{
    private static ResourceManager _instance;
    public static ResourceManager Instance
    {
        get
        {
            if (_instance == null)
                _instance = Object.FindObjectOfType<ResourceManager>();

            return _instance;
        }
    }

    [SerializeField]
    private MainDatabaseCollection _mainDatabaseCollection;
    public MainDatabaseCollection MainDatabaseCollection { get => _mainDatabaseCollection; set => _mainDatabaseCollection = value; }

    private IUnityCategorizedResourceLoader _unityResourceLoader;
    private IResourceLoader _streamingAssetsResourceLoader;

    [SerializeField]
    [ShowIf(nameof(_showDatabaseIds))]
    private List<DatabaseIdDisplay> _databaseIds = new List<DatabaseIdDisplay>();

    [SerializeField]
    private bool _showDatabaseIds = false;

    private void Awake()
    {
        _unityResourceLoader = new UnityCategorizedResourceLoader();
        _streamingAssetsResourceLoader = new StreamingAssetsResourceLoader();

        _instance = this;
    }

    public T GetRecord<T>(DatabaseRecord<T> databaseRecord) where T : UnityEngine.Object
    {
        try
        {
            return (T)GetDatabase(databaseRecord.DatabaseId).DatabaseData.GetAsset(databaseRecord.AssetId).AssetObject;
        }
        catch (System.Exception)
        {
            Debug.LogError($"Error getting record from database: Database Id {databaseRecord.DatabaseId}, Asset Id {databaseRecord.AssetId}");

            throw;
        }
    }

    public List<T> GetRecords<T>(IEnumerable<DatabaseRecord<T>> databaseRecords) where T : UnityEngine.Object
    {
        return databaseRecords
            .Select(dr => GetRecord(dr))
            .ToList();
    }

    public List<T> GetAllDatabaseResources<T>(short databaseId) where T : UnityEngine.Object
    {
        var records = new List<T>();

        for (var idx = 0; idx < MainDatabaseCollection.GetDatabase(databaseId).DatabaseData.NumOfEntries; idx++)
        {
            var record = (T)MainDatabaseCollection.GetDatabase(databaseId).DatabaseData.GetAssetAtIndex(idx).AssetObject;

            records.Add(record);
        }

        return records;
    }

    private IDatabaseFile GetDatabase(short databaseId)
    {
        return global::MainDatabaseCollection.GetDatabase(databaseId);
    }

    private ResourceTable DeserializeResourceTable(ResourceInfo resourceInfo)
    {
        return DataSerializer.Instance.Deserialize<ResourceTable>(resourceInfo);
    }

    private void LoadResourceTable<T>(ResourceTable resourceTable, ICategorizedResourceCache<T> resourceCache)
        where T : UnityEngine.Object
    {
        resourceTable.ResourceInfos.ForEach(ri => LoadResourceInfo(ri, resourceCache));
    }

    private void LoadResourceInfo<T>(ResourceInfo resourceInfo, ICategorizedResourceCache<T> resourceCache)
        where T : UnityEngine.Object
    {
        _unityResourceLoader.LoadIntoCache(
            resourceInfo.UniqueIdentifier, 
            resourceInfo.RelativePath, 
            resourceInfo.ResourceCategory, 
            resourceCache);
    }

    [Button(ButtonSizes.Medium)]
    public void UpdateDatabaseIds()
    {
        _databaseIds = this.MainDatabaseCollection.visibleFiles
            .Cast<IDatabaseFile>()
            .Select(d => new DatabaseIdDisplay { DatabaseId = d.ID16, DatabaseName = d.DatabaseData.GetType().Name })
            .ToList();

        _showDatabaseIds = true;
    }
}

[System.Serializable]
public class DatabaseIdDisplay
{
    [SerializeField]
    private string _databaseName;
    public string DatabaseName { get => _databaseName; set => _databaseName = value; }

    [SerializeField]
    private short _databaseId;
    public short DatabaseId { get => _databaseId; set => _databaseId = value; }
}
