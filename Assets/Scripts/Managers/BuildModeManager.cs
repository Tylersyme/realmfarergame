﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class BuildModeManager : MonoBehaviour
{
    private static BuildModeManager _instance;
    public static BuildModeManager Instance
    {
        get
        {
            if (_instance == null)
                _instance = Object.FindObjectOfType<BuildModeManager>();

            return _instance;
        }
    }

    public DirectionType SelectedItemCurrentDirectionType { get; set; }

    private Item SelectedItem { get; set; }

    private IHubSubscriptionCache _hubSubscriptionCache;
    private IItemSkillService _itemSkillService;
    private IItemPlacementService _itemPlacementService;

    private void Awake()
    {
        _instance = this;

        _hubSubscriptionCache = new HubSubscriptionCache();
        _itemSkillService = new ItemSkillService();
        _itemPlacementService = new ItemPlacementService();

        this.Subscribe();
    }

    private void Update()
    {
        if (InputManager.Instance.IsActionActive(InputAction.ROTATE_SELECTED_ITEM))
            this.HandleRotateSelectedItemInput();
    }

    private void HandleRotateSelectedItemInput()
    {
        if (!_itemPlacementService.IsRotateable(this.SelectedItem))
            return;

        var rotationType = _itemPlacementService.GetRotationType(this.SelectedItem);

        var nextDirectionType = this.GetNextDirection(rotationType);

        this.SetSelectedItemDirection(nextDirectionType);
    }

    private void OnDestroy() =>
        _hubSubscriptionCache.UnsubscribeAll();

    protected void OnSelectedHotbarSlotChanged(SelectedHotbarSlotChangedEventArgs eventArgs)
    {
        if (this.SelectedItem == eventArgs.SelectedItem)
            return;

        this.SelectedItem = eventArgs.SelectedItem;

        if (this.SelectedItem == null || !_itemPlacementService.IsPlaceable(this.SelectedItem))
        {
            VisualGizmoManager.Instance.DisableGhostItem();

            return;
        }

        if (_itemPlacementService.IsRotateable(this.SelectedItem))
        {
            var rotationType = _itemPlacementService.GetRotationType(this.SelectedItem);

            // Set the current rotation to the default of the newly selected item
            this.SelectedItemCurrentDirectionType = this.GetDefaultDirectionOfRotationType(rotationType);

            this.SetSelectedItemDirection(this.SelectedItemCurrentDirectionType);
        }
        else
        {
            var placeSelfSkill = _itemSkillService.GetSkill<PlaceSelfSkill>(this.SelectedItem);

            var spriteRenderer = placeSelfSkill.PlacedItemPrefab.GetComponentInChildren<SpriteRenderer>();

            // Uses the object's sprite renderer to determine the sprite and offset
            VisualGizmoManager.Instance.EnableGhostItem(new OffsetSprite
            {
                Sprite = spriteRenderer.sprite,
                Offset = spriteRenderer.gameObject.transform.position,
            },
            placeSelfSkill.SnapToTile);
        }
    }

    private void SetSelectedItemDirection(DirectionType directionType)
    {
        this.SelectedItemCurrentDirectionType = directionType;

        var placeSelfSkill = _itemSkillService.GetSkill<PlaceSelfSkill>(this.SelectedItem);

        var offsetSprite = _itemPlacementService.GetOffsetSpriteAtDirection(this.SelectedItem, directionType);

        VisualGizmoManager.Instance.EnableGhostItem(offsetSprite, placeSelfSkill.SnapToTile);
    }

    private DirectionType GetDefaultDirectionOfRotationType(RotationType rotationType) =>
        rotationType == RotationType.EastWest
            ? DirectionType.East
            : DirectionType.South;

    private DirectionType GetNextDirection(RotationType rotationType)
    {
        var directions = Direction.GetDirectionsOfRotationType(rotationType);

        var currentDirectionIndex = directions
            .ToList()
            .FindIndex(d => d.Type == this.SelectedItemCurrentDirectionType);

        // Loop back to the first item if the last item is currently being used
        if (currentDirectionIndex == directions.Count - 1)
            return directions.First().Type;

        return directions[currentDirectionIndex + 1].Type;
    }

    private void Subscribe()
    {
        _hubSubscriptionCache.Subscribe<SelectedHotbarSlotChangedEventArgs>(
            EventManager.Instance.UiEventHub, this.OnSelectedHotbarSlotChanged);
    }
}
