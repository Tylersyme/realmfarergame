﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoiseGeneratorPreviewManager : MonoBehaviour
{
    private static NoiseGeneratorPreviewManager _instance;
    public static NoiseGeneratorPreviewManager Instance
    {
        get
        {
            if (_instance == null)
                _instance = Object.FindObjectOfType<NoiseGeneratorPreviewManager>();

            return _instance;
        }
    }

    public NoiseGeneratorParameters NoiseGeneratorParameters { get; set; }

    public bool HasApplyChangesBeenClicked { get; set; } = false;

    // Stores the new parameters created by the editor window
    private NoiseGeneratorParameters AppliedChanges { get; set; }

    public void StoreChanges(NoiseGeneratorParameters noiseGeneratorParameters)
    {
        this.AppliedChanges = noiseGeneratorParameters;
    }

    public NoiseGeneratorParameters GetChanges()
    {
        var changes = this.AppliedChanges;

        this.AppliedChanges = null;

        return changes;
    }

    public bool HasChanges()
    {
        return this.AppliedChanges != null;
    }

    private void Awake()
    {
        _instance = this;
    }
}
