﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MappingDatabase : MonoBehaviour
{
    private static MappingDatabase _instance;
    public static MappingDatabase Instance
    {
        get
        {
            if (_instance == null)
                _instance = Object.FindObjectOfType<MappingDatabase>();

            return _instance;
        }
    }

    [SerializeField]
    [Required]
    private TileMappings _tileMappings;
    public TileMappings TileMappings { get => _tileMappings; set => _tileMappings = value; }

    [SerializeField]
    [Required]
    private ItemMappings _itemMappings;
    public ItemMappings ItemMappings { get => _itemMappings; set => _itemMappings = value; }

    [SerializeField]
    [Required]
    private CraftingRecipeDefinitionDatabase _craftingRecipeDefinitions;
    public CraftingRecipeDefinitionDatabase CraftingRecipeDefinitions { get => _craftingRecipeDefinitions; set => _craftingRecipeDefinitions = value; }

    private void Awake() =>
        _instance = this;
}
