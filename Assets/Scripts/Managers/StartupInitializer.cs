﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartupInitializer : MonoBehaviour
{
    private static StartupInitializer _instance;
    public static StartupInitializer Instance
    {
        get
        {
            if (_instance == null)
                _instance = Object.FindObjectOfType<StartupInitializer>();

            return _instance;
        }
    }

    private void Awake()
    {
        _instance = this;

        AutoMapperConfig.ConfigureMappings();
        EventManager.Instance.InitializeEventHubs();
    }
}
