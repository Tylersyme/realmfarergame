﻿using System.Collections;
using System.Collections.Generic;
using Easy.MessageHub;
using UnityEngine;

public class EventManager : MonoBehaviour
{
    private static EventManager _instance;
    public static EventManager Instance
    {
        get
        {
            if (_instance == null)
                _instance = Object.FindObjectOfType<EventManager>();

            return _instance;
        }
    }

    public IMessageHub UiEventHub { get; private set; }

    public IMessageHub SystemEventHub { get; private set; }

    public IMessageHub TilemapEventHub { get; private set; }

    public void InitializeEventHubs()
    {
        this.UiEventHub = new MessageHub();
        this.SystemEventHub = new MessageHub();
        this.TilemapEventHub = new MessageHub();
    }

    private void Awake()
    {
        _instance = this;
    }
}
