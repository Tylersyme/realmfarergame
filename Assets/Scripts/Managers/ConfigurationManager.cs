﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConfigurationManager : MonoBehaviour
{
    private static ConfigurationManager _instance;
    public static ConfigurationManager Instance
    {
        get
        {
            if (_instance == null)
                _instance = Object.FindObjectOfType<ConfigurationManager>();

            return _instance;
        }
    }

    public static string CONFIGURATION_PATH;

    public void LoadConfigurationData()
    {
    }

    private void Awake()
    {
        CONFIGURATION_PATH = $"{Application.dataPath}/Config";

        LoadConfigurationData();

        _instance = this;
    }
}
