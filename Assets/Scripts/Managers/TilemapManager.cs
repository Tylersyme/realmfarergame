﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TilemapManager : MonoBehaviour
{
    private static TilemapManager _instance;
    public static TilemapManager Instance
    {
        get
        {
            if (_instance == null)
                _instance = Object.FindObjectOfType<TilemapManager>();

            return _instance;
        }
    }

    [SerializeField]
    private TilemapRenderer _tilemapRenderer;
    public TilemapRenderer TilemapRenderer { get => _tilemapRenderer; set => _tilemapRenderer = value; }

    [SerializeField]
    private Grid grid;
    public Grid Grid { get => grid; set => grid = value; }

    /// <summary>
    /// Represents the number of "units" of space a single tile will take up.
    /// </summary>
    [InfoBox("The tile size must match up to the pixels per unit which is used for each sprite. To calculate the correct tile size: (2 * TilePixelSize) / PPU.")]
    [SerializeField]
    private Vector2 _tileSize = new Vector2(0.5f, 0.5f);
    public Vector2 TileSize { get => _tileSize; set => _tileSize = value; }

    public Vector3 TileHalfSize { get => this.TileSize * 0.5f; }

    [SerializeField]
    private Vector2Int _tilePixelSize = new Vector2Int(32, 32);
    public Vector2Int TilePixelSize { get => _tilePixelSize; set => _tilePixelSize = value; }

    [SerializeField]
    private Vector2Int _tilesPerMesh = new Vector2Int(10, 10);
    public Vector2Int TilesPerMesh { get => _tilesPerMesh; set => _tilesPerMesh = value; }

    public List<TilemapObject> TilemapObjects { get; set; }

    /// <summary>
    /// Creates the given tilemap object at the center of the tile at the given position.
    /// </summary>
    /// <param name="tilemapObject"></param>
    /// <param name="position"></param>
    public void CreateTilemapObjectAtTilePosition(GameObject gameObject, Vector3Int position)
    {
        var tilemapObject = gameObject.GetComponent<TilemapObject>();

        if (tilemapObject == null)
            throw new System.Exception("Error creating tilemap object: The game object does not have component \'TilemapObject\'");

        var localPosition = GetTileCenterPoint(position);

        var createdGameObject = Object.Instantiate(gameObject, localPosition, Quaternion.identity);

        this.TilemapObjects.Add(createdGameObject.GetComponent<TilemapObject>());
    }

    public void DestroyTilemap()
    {
        this.TilemapRenderer.DestroyQuads();

        this.TilemapObjects.ForEach(to => Object.Destroy(to.gameObject));
        this.TilemapObjects.Clear();
    }

    private Vector3 GetTileCenterPoint(Vector3Int position)
    {
        return GetTileBottomLeftPoint(position) + this.TileHalfSize;
    }

    private Vector3 GetTileBottomLeftPoint(Vector3Int position)
    {
        return position.AsVector3().Multiply(this.TileSize.AsVector3()) + this.TilemapRenderer.gameObject.GetParent().transform.position;
    }

    private Vector2Int? GetCellPositionAt(Vector2 worldPosition)
    {
        if (!IsInTilemapBounds(worldPosition))
            return null;

        var bottomLeftPoint = this.TilemapRenderer.gameObject.transform.position.AsVector2();
        var localPosition = worldPosition - bottomLeftPoint;


        return null;
    }

    public bool IsInTilemapBounds(Vector2 worldPosition)
    {
        var bottomLeftPoint = this.TilemapRenderer.gameObject.transform.position.AsVector2();
        var localPosition = worldPosition - bottomLeftPoint;

        return !localPosition.HasNegative();
    }

    private void Start()
    {
        if (this.TilemapRenderer == null)
            this.TilemapRenderer = FindObjectOfType<TilemapRenderer>();

        if (this.Grid == null)
            this.Grid = FindObjectOfType<Grid>();

        this.TilemapObjects = new List<TilemapObject>();
    }

    private void Awake()
    {
        _instance = this;
    }
}
