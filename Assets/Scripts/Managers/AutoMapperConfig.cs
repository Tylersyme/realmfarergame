﻿using AutoMapper;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class AutoMapperConfig
{
    public static IMapper ComponentSerializationMapper;
    public static IMapper ViewModelMapper;

    public static void ConfigureMappings()
    {
        ComponentSerializationMapper = new MapperConfiguration(cfg =>
        {
            cfg.AddProfile<FadeOnCoveringProfile>();
        })
        .CreateMapper();

        ViewModelMapper = new MapperConfiguration(cfg =>
        {
            cfg.AddProfile<InventoryViewModelProfile>();
            cfg.AddProfile<ItemHolderViewModelProfile>();
            cfg.AddProfile<ItemViewModelProfile>();
        })
        .CreateMapper();
    }
}
