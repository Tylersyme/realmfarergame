﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VisualGizmoManager : MonoBehaviour
{
    private static VisualGizmoManager _instance;
    public static VisualGizmoManager Instance
    {
        get
        {
            if (_instance == null)
                _instance = Object.FindObjectOfType<VisualGizmoManager>();

            return _instance;
        }
    }

    [SerializeField]
    [Required]
    private GameObject _ghostItemPrefab;
    public GameObject GhostItemPrefab { get => _ghostItemPrefab; set => _ghostItemPrefab = value; }

    private GameObject GhostItem { get; set; }

    private IGameObjectBuilder _gameObjectBuilder;

    private void Awake()
    {
        _instance = this;

        _gameObjectBuilder = new GameObjectBuilder();
    }

    private void Start()
    {
        this.GhostItem = _gameObjectBuilder
            .CreateFromPrefab(this.GhostItemPrefab)
            .WithEnabled(false)
            .Build();
    }

    public void EnableGhostItem(OffsetSprite offsetSprite, bool snapToTile)
    {
        _gameObjectBuilder
            .UseExistingGameObject(this.GhostItem)
            .WithEnabled(true)
            .ModifyComponent<SpriteRenderer>()
                .WithValue(x => x.sprite, offsetSprite.Sprite)
            .ModifyComponent<GhostItem>()
                .WithValue(x => x.SnapToTile, snapToTile)
                .WithValue(x => x.PositionOffset, offsetSprite.Offset);

        // Prevents item from noticeably teleporting to the cursor
        this.GhostItem
            .GetComponent<GhostItem>()
            .UpdatePosition();
    }

    public void DisableGhostItem() =>
        this.GhostItem.DisableSelf();
}
