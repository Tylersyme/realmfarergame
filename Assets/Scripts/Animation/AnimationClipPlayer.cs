﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class AnimationClipPlayer
{
    [SerializeField]
    [Required]
    private Animator _animator;
    public Animator Animator { get => _animator; set => _animator = value; }

    [SerializeField]
    [Required]
    private AnimationClip _animation;
    public AnimationClip Animation { get => _animation; set => _animation = value; }

    public void Play() =>
        this.Animator.Play(this.Animation.name);
}
