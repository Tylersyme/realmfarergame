﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

public class PlaceableBehavior : SerializedMonoBehaviour
{
    [SerializeField]
    private TileFootprint _tileFootprint;
    public TileFootprint TileFootprint { get => _tileFootprint; set => _tileFootprint = value; }

    [SerializeField]
    [TitleGroup("Rotation")]
    private bool _canRotate = false;
    public bool CanRotate { get => _canRotate; set => _canRotate = value; }

    [SerializeField]
    [ShowIf(nameof(CanRotate))]
    [TitleGroup("Rotation")]
    private RotationType _rotationType = RotationType.Cardinal;
    public RotationType RotationType { get => _rotationType; set => _rotationType = value; }

    [SerializeField]
    [ShowIf(nameof(ShowNorthSouthRotationAnimations))]
    [TitleGroup("Rotation")]
    private List<AnimationClipPlayer> _northRotationAnimations = new List<AnimationClipPlayer>();
    public List<AnimationClipPlayer> NorthRotationAnimations { get => _northRotationAnimations; set => _northRotationAnimations = value; }

    [SerializeField]
    [ShowIf(nameof(ShowNorthSouthRotationAnimations))]
    [TitleGroup("Rotation")]
    private List<AnimationClipPlayer> _southRotationAnimations = new List<AnimationClipPlayer>();
    public List<AnimationClipPlayer> SouthRotationAnimations { get => _southRotationAnimations; set => _southRotationAnimations = value; }

    [SerializeField]
    [ShowIf(nameof(ShowEastWestRotationAnimations))]
    [TitleGroup("Rotation")]
    private List<AnimationClipPlayer> _westRotationAnimations = new List<AnimationClipPlayer>();
    public List<AnimationClipPlayer> WestRotationAnimations { get => _westRotationAnimations; set => _westRotationAnimations = value; }

    [SerializeField]
    [ShowIf(nameof(ShowEastWestRotationAnimations))]
    [TitleGroup("Rotation")]
    private List<AnimationClipPlayer> _eastRotationAnimations = new List<AnimationClipPlayer>();
    public List<AnimationClipPlayer> EastRotationAnimations { get => _eastRotationAnimations; set => _eastRotationAnimations = value; }

    [SerializeField]
    [HideInInspector]
    private Direction _facingDirection = Direction.SOUTH;
    public Direction FacingDirection { get => _facingDirection; set => _facingDirection = value; }

    /// <summary>
    /// Stores the sprite renderer's sprite and its position based on the corresponding direction.
    /// This is used to preview what the placeable object would look like if it were to be rotated.
    /// </summary>
    [SerializeField]
    [TitleGroup("Info")]
    [ShowIf(nameof(CanRotate))]
    [ReadOnly]
    [DictionaryDrawerSettings(DisplayMode = DictionaryDisplayOptions.ExpandedFoldout)]
    private IDictionary<DirectionType, OffsetSprite> _rotationOffsetSprites = new Dictionary<DirectionType, OffsetSprite>();

    private bool ShowNorthSouthRotationAnimations 
    { 
        get => this.CanRotate && (this.RotationType == RotationType.Cardinal || this.RotationType == RotationType.NorthSouth);
    }

    private bool ShowEastWestRotationAnimations
    {
        get => this.CanRotate && (this.RotationType == RotationType.Cardinal || this.RotationType == RotationType.EastWest);
    }

    public List<AnimationClipPlayer> GetAnimationClipPlayersOfDirection(DirectionType directionType)
    {
        if (directionType == DirectionType.South)
            return this.SouthRotationAnimations;
        else if (directionType == DirectionType.North)
            return this.NorthRotationAnimations;
        else if (directionType == DirectionType.West)
            return this.WestRotationAnimations;
        else if (directionType == DirectionType.East)
            return this.EastRotationAnimations;
        else
            throw new System.Exception($"Error setting rotation direction: The direction type {directionType} is not supported.");
    }
    
    [Button]
    [TitleGroup("Actions")]
    [ShowIf(nameof(CanRotate))]
    public void SetDirection(DirectionType directionType)
    {
        var animationClipPlayers = this.GetAnimationClipPlayersOfDirection(directionType);

        this.PlayRotationAnimations(animationClipPlayers);
    }

    public OffsetSprite GetOffsetSpriteAtDirection(DirectionType directionType) =>
        _rotationOffsetSprites[directionType];

    private void PlayRotationAnimations(IList<AnimationClipPlayer> animationClipPlayers) =>
        animationClipPlayers
            .ToList()
            .ForEach(acp => acp.Play());

#if UNITY_EDITOR

    [Button]
    [TitleGroup("Actions")]
    [ShowIf(nameof(CanRotate))]
    [HideInPlayMode]
    private void StoreRotationAnimationOffsetSprites()
    {
        foreach (var directionType in Direction.XY_CARDINAL_DIRECTIONS.Select(d => d.Type))
        {
            var animationClip = this
                .GetAnimationClipPlayersOfDirection(directionType)
                .First()
                .Animation;

            var spriteRendererPositionX = EditorCurveBinding.FloatCurve(
                string.Empty,
                typeof(Transform),
                "m_LocalPosition.x");

            var spriteRendererPositionY = EditorCurveBinding.FloatCurve(
                string.Empty,
                typeof(Transform),
                "m_LocalPosition.y");

            var xPosition = AnimationUtility
                .GetEditorCurve(animationClip, spriteRendererPositionX)
                .Evaluate(0.0f);

            var yPosition = AnimationUtility
                .GetEditorCurve(animationClip, spriteRendererPositionY)
                .Evaluate(0.0f);

            var sprite = animationClip.GetFirstSprite();

            if (_rotationOffsetSprites == null)
                _rotationOffsetSprites = new Dictionary<DirectionType, OffsetSprite>();

            _rotationOffsetSprites.Remove(directionType);
            _rotationOffsetSprites.Add(directionType, new OffsetSprite
            {
                Offset = new Vector2(xPosition, yPosition),
                Sprite = sprite,
            });

            EditorUtility.SetDirty(this.gameObject);
        }
    }

    private void Update_FacingDirectionValue(string facingDirectionName) =>
        this.FacingDirection = Direction.ParseByName(facingDirectionName);

#endif
}

public enum RotationType
{
    NorthSouth,
    EastWest,
    Cardinal,
}

