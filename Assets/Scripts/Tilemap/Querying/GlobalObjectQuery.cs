﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class GlobalObjectQuery : IObjectQuery
{
    public abstract IList<GameObject> Query();
}
