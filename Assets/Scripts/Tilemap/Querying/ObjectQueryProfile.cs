﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[System.Serializable]
public class ObjectQueryProfile
{
    [SerializeField]
    [Required]
    private List<IObjectQuery> _objectQueries = new List<IObjectQuery>();
    public List<IObjectQuery> ObjectQueries { get => _objectQueries; set => _objectQueries = value; }

    public List<GameObject> QueryAll(Vector2 boundedObjectCenterPoint, IList<GameObject> toIgnore) =>
        this.ObjectQueries
            .SelectMany(oq =>
            {
                if (oq is BoundedObjectQuery boundedObjectQuery)
                    return boundedObjectQuery.QueryWithIgnored(boundedObjectCenterPoint, toIgnore);
                else if (oq is GlobalObjectQuery globalObjectQuery)
                    return globalObjectQuery.Query();

                throw new System.Exception(
                    $"Error querying all in profile: The object query of type {oq.GetType().ToString()} is not supported.");
            })
            .Distinct()
            .ToList();
}
