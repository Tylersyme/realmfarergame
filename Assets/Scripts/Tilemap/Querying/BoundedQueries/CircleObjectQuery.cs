﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[System.Serializable]
public class CircleObjectQuery : BoundedObjectQuery
{
    [SerializeField]
    [MinValue(0.0f)]
    private float _radius = 1.0f;
    public float Radius { get => _radius; set => _radius = value; }

    [SerializeField]
    private bool _targetClosest = false;
    public bool TargetClosest { get => _targetClosest; set => _targetClosest = value; }

    public override IList<GameObject> Query(Vector2 centerPoint) =>
        this.QueryWithIgnored(centerPoint, new List<GameObject>());

    public override IList<GameObject> QueryWithIgnored(Vector2 centerPoint, IList<GameObject> toIgnore)
    {
        var gameObjects = Physics2D
            .OverlapCircleAll(centerPoint, this.Radius)
            .Select(c => c.gameObject.GetRootParent())
            .Where(go => !toIgnore.Contains(go))
            .Distinct()
            .ToList();

        if (this.TargetClosest)
        {
            var closestGameObject = gameObjects.MinBy(go => Vector2.Distance(go.transform.position, centerPoint));

            return new List<GameObject> { closestGameObject };
        }
        else
        {
            return gameObjects;
        }
    }
}
