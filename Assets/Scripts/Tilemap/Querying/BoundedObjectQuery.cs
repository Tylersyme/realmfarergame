﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public abstract class BoundedObjectQuery : IObjectQuery
{
    public abstract IList<GameObject> Query(Vector2 centerPoint);

    public abstract IList<GameObject> QueryWithIgnored(Vector2 centerPoint, IList<GameObject> toIgnore);
}
