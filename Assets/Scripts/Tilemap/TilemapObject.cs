﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Represents an object which belongs to the tilemap
/// </summary>
public class TilemapObject : MonoBehaviour
{
    public Grid Grid { get => TilemapManager.Instance.Grid; }

    private void Start()
    {
        UpdateSortingLayer();
    }

    public void CenterOnCell(Vector3Int position)
    {
        var worldPosition = this.Grid
            .GetCellWorldCenterPoint(position.AsVector2Int())
            .AsVector3()
            .SetZ(-position.z);

        this.gameObject.transform.position = worldPosition;
    }

    public Vector2Int GetCellPosition()
    {
        var cellPosition = this.Grid.GetCellAt(this.gameObject.transform.position);

        if (!cellPosition.HasValue)
            throw new System.NullReferenceException($"Error getting tilemap object cell position: The object is out of bounds.");

        return cellPosition.Value;
    }

    public Vector2Int GetRelativePosition(Direction direction, int amount)
    {
        return GetCellPosition() + (direction.RelativeDirection.AsVector2Int() * amount);
    }

    public bool IsIsBounds()
    {
        return this.Grid.GetCellAt(this.gameObject.transform.position).HasValue;
    }

    private void UpdateSortingLayer()
    {
        var spriteRenderer = GetSpriteRenderer();

        if (spriteRenderer != null)
            spriteRenderer.sortingLayerName = SortingLayers.TILEMAP_OBJECTS;
    }

    private SpriteRenderer GetSpriteRenderer()
    {
        return this.GetComponentInChildren<SpriteRenderer>();
    }
}
