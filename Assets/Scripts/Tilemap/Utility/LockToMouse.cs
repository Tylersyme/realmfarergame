﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LockToMouse : MonoBehaviour
{
    private void Update()
    {
        this.UpdatePosition();
    }

    public void UpdatePosition() =>
        this.transform.position = Camera.main.MouseWorldPosition();
}
