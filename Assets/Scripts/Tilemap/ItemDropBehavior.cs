﻿using Sirenix.OdinInspector;
using Sirenix.Serialization;
using Sirenix.Utilities;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ItemDropBehavior : SerializedMonoBehaviour
{
    [OdinSerialize]
    [Required]
    private List<ConditionalItemDrop> _conditionalItemDrops = new List<ConditionalItemDrop>();
    private List<ConditionalItemDrop> ConditionalItemDrops { get => _conditionalItemDrops; set => _conditionalItemDrops = value; }

    private ISubscriptionCache _subscriptionCache;
    private IItemSpawnService _itemSpawnService;

    private void Awake()
    {
        _subscriptionCache = new SubscriptionCache();
        _itemSpawnService = new ItemSpawnService();

        this.ConditionalItemDrops.ForEach(c => c.Condition.Subscribe());

        this.Subscribe();
    }

    private void OnDestroy()
    {
        this.ConditionalItemDrops.ForEach(c => c.Condition.Unsubscribe());

        _subscriptionCache.UnsubscribeAll();
    }

    protected void OnConditionChanged(object sender, ConditionChangedEventArgs eventArgs)
    {
        if (!eventArgs.IsFulfilled)
            return;

        this.SpawnItemDrop(eventArgs.Condition);
    }

    private void SpawnItemDrop(Condition condition)
    {
        this.ConditionalItemDrops.First(cid => cid.Condition == condition).ItemDrops.ForEach(i =>
        {
            _itemSpawnService.SpawnDroppedItemFromTemplate(i.ItemTemplate, i.StackSize, this.gameObject.transform.position);
        });
    }

    private void Subscribe() =>
        this.ConditionalItemDrops.ForEach(cid =>
        {
            _subscriptionCache.Subscribe<ConditionChangedEventArgs>(
                this.OnConditionChanged,
                handler => cid.Condition.ConditionChangedEvent += handler,
                handler => cid.Condition.ConditionChangedEvent -= handler);
        });

    [System.Serializable]
    private class ConditionalItemDrop
    {
        [SerializeField]
        [Required]
        private Condition _condition;
        public Condition Condition { get => _condition; set => _condition = value; }

        [SerializeField]
        [Required]
        private List<ItemDrop> _itemDrops = new List<ItemDrop>();
        public List<ItemDrop> ItemDrops { get => _itemDrops; set => _itemDrops = value; }
    }
}

