﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Represents a 3 dimensional grid of cells
/// </summary>
/// <typeparam name="T"></typeparam>
[System.Serializable]
public class Cellmap<T> where T : new()
{
    [SerializeField]
    private Vector3Int _size;
    public Vector3Int Size { get => _size; set => _size = value; }

    public int Width { get => this.Size.x; }
    public int Height { get => this.Size.y; }
    public int Depth { get => this.Size.z; }

    [SerializeField]
    private List<List<List<T>>> _map;
    public List<List<List<T>>> Map { get => _map; set => _map = value; }

    public Cellmap() { }

    public Cellmap(int width, int height, int depth)
        : this()
    {
        this.Map = new List<List<List<T>>>();

        this.Size = new Vector3Int(width, height, depth);

        Initialize();
    }

    public Cellmap(Vector3Int dimensions)
        : this(dimensions.x, dimensions.y, dimensions.z)
    {
    }

    private void Initialize()
    {
        // Fill out entire cellmap with empty cells
        for (var x = 0; x < this.Width; x++)
        {
            this.Map.Add(new List<List<T>>());
            for (var y = 0; y < this.Height; y++)
            {
                this.Map[x].Add(new List<T>());
                for (var z = 0; z < this.Depth; z++)
                {
                    this.Map[x][y].Add(new T());
                }
            }
        }
    }

    public T GetElementAt(Vector3Int position)
    {
        return this.Map[position.x][position.y][position.z];
    }

    public bool HasElementAt(Vector3Int position)
    {
        return GetElementAt(position) != null;
    }

    public void SetElement(Vector3Int position, T element)
    {
        this.Map[position.x][position.y][position.z] = element;
    }

    public bool IsInBounds(Vector3Int position)
    {
        return MathExtended.IsBetweenInclusive(position.x, 0, this.Width - 1) &&
               MathExtended.IsBetweenInclusive(position.y, 0, this.Height - 1) &&
               MathExtended.IsBetweenInclusive(position.z, 0, this.Depth - 1);
    }
}
