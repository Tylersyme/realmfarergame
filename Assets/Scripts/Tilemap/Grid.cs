﻿using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grid : MonoBehaviour
{
    [SerializeField]
    [OnValueChanged(nameof(GridChanged))]
    private float _cellSize;
    public float CellSize { get => _cellSize; set => _cellSize = value; }

    [SerializeField]
    [OnValueChanged(nameof(GridChanged))]
    private Vector2Int _dimensions = new Vector2Int(1, 1);
    public Vector2Int Dimensions { get => _dimensions; set => _dimensions = value; }

    public Vector2 Size { get => this.Dimensions.Multiply(this.CellSize); }
    public float Width { get => this.Size.x; }
    public float HalfWidth { get => this.Size.x * 0.5f; }
    public float Height { get => this.Size.y; }
    public float HalfHeight { get => this.Size.y * 0.5f; }

    public Vector2 CenterPoint { get => this.gameObject.transform.position + this.Size.Multiply(0.5f).AsVector3(); }

    public Vector2 TopLeftCorner { get => this.CenterPoint + new Vector2(-this.HalfWidth, this.HalfHeight); }
    public Vector2 BottomLeftCorner { get => this.CenterPoint + new Vector2(-this.HalfWidth, -this.HalfHeight); }
    public Vector2 TopRightCorner { get => this.CenterPoint + new Vector2(this.HalfWidth, this.HalfHeight); }
    public Vector2 BottomRightCorner { get => this.CenterPoint + new Vector2(this.HalfWidth, -this.HalfHeight); }
    public float Left { get => this.CenterPoint.x - this.HalfWidth; }
    public float Right { get => this.CenterPoint.x + this.HalfWidth; }
    public float Top { get => this.CenterPoint.y + this.HalfHeight; }
    public float Bottom { get => this.CenterPoint.y - this.HalfHeight; }


    private void GridChanged()
    {
        _dimensions.x = Mathf.Max(this.Dimensions.x, 1);
        _dimensions.y = Mathf.Max(this.Dimensions.y, 1);
    }

    public bool IsPointInBounds(Vector2 worldPosition)
    {
        return worldPosition.x >= this.Left && worldPosition.x <= this.Right &&
               worldPosition.y >= this.Bottom && worldPosition.y <= this.Top;
    }

    /// <summary>
    /// Returns the relative position of the point from the bottom left corner of the grid.
    /// </summary>
    /// <param name="worldPosition"></param>
    /// <returns></returns>
    private Vector2 RelativePosition(Vector2 worldPosition)
    {
        return worldPosition - this.BottomLeftCorner;
    }

    /// <summary>
    /// Return the centerpoint of a cell at the given position.
    /// </summary>
    /// <param name="position"></param>
    /// <returns></returns>
    public Vector2 GetCellWorldCenterPoint(Vector2Int position)
    {
        return new Vector2((this.CellSize * position.x) + (this.CellSize * 0.5f) + this.BottomLeftCorner.x,
                           (this.CellSize * position.y) + (this.CellSize * 0.5f) + this.BottomLeftCorner.y);
    }

    /// <summary>
    /// Returns the cell position at the provided world position.
    /// If the world position is out of bounds, this returns null.
    /// </summary>
    /// <param name="worldPosition"></param>
    /// <returns></returns>
    public Vector2Int? GetCellAt(Vector2 worldPosition)
    {
        if (!IsPointInBounds(worldPosition))
            return null;

        var relativePosition = RelativePosition(worldPosition);

        var xPosition = (int)(relativePosition.x / this.CellSize);
        var yPosition = (int)(relativePosition.y / this.CellSize);

        return new Vector2Int(xPosition, yPosition);
    }
}
