﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreeModule : MonoBehaviour
{
    private IInteractionSubscriber _interactionSubscriber;

    private void Awake()
    {
        _interactionSubscriber = new InteractionSubscriber(this.gameObject);
    }

    private void OnDestroy() => 
        _interactionSubscriber.UnsubscribeAll();
}