﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class HealthChangeCause
{
    public abstract HealthChangedCauseType Type { get; }
}
