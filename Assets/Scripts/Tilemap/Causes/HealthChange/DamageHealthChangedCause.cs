﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageHealthChangedCause : HealthChangeCause
{
    public override HealthChangedCauseType Type { get => HealthChangedCauseType.Damage; }

    public DamageCause DamageCause { get; set; }
}
