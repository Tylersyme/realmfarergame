﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ConditionChangedCause
{
    public abstract ConditionType Type { get; }
}
