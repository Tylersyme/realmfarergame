﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthInThresholdConditionChangedCause : ConditionChangedCause
{
    public override ConditionType Type { get => ConditionType.HealthInThreshold; }

    public HealthChangeCause HealthChangeCause { get; set; }
}
