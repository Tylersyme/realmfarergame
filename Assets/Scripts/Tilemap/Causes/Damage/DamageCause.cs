﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class DamageCause
{
    public abstract DamageCauseType Type { get; }
}
