﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemInteractionDamageCause : DamageCause
{
    public override DamageCauseType Type { get => DamageCauseType.ItemInteraction; }

    public Item DamageCauser { get; set; }
}
