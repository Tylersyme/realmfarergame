﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConditionChangedDespawnCause : DespawnCause
{
    public override DespawnCauseType Type { get => DespawnCauseType.ConditionChanged; }

    public ConditionChangedCause ConditionChangedCause { get; set; }
}
