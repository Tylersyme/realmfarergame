﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class DespawnCause
{
    public abstract DespawnCauseType Type { get; }
}
