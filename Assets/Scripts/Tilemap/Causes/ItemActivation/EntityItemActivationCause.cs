﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntityItemActivationCause : ItemActivationCause
{
    public override ItemActivationCauseType Type { get => ItemActivationCauseType.Entity; }

    // TODO: Make and use EntityBehavior instead of its controller
    public EntityController EntityController { get; set; }
}
