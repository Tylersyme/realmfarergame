﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ItemActivationCause
{
    public abstract ItemActivationCauseType Type { get; }
}
