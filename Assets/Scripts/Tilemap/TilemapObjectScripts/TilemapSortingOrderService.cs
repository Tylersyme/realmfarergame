﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TilemapSortingOrderService : ISortingOrderService
{
    public int GetSortingOrder(Vector2 worldPosition)
    {
        return (int)(worldPosition.y * -100);
    }
}
