﻿using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// Used to fade a sprite which is obscuring another important sprite.
/// </summary>
[RequireComponent(typeof(Collider2D))]
public class FadeOnCovering : MonoBehaviour, ISerializedComponent
{
    [SerializeField]
    private string _uniqueSerializationId = $"FadeOnCovering:{Guid.NewGuid()}";
    public string UniqueSerializationId { get => _uniqueSerializationId; }

    private bool IsFaded { get; set; } = false;

    /// <summary>
    /// Tracks the number of colliders which have entered but have not exited.
    /// </summary>
    private int TotalCollidersNotExited { get; set; } = 0;

    private bool AllCollidersExited => this.TotalCollidersNotExited == 0;

    [SerializeField]
    private float _secondsToFade = 0.25f;
    public float SecondsToFade { get => _secondsToFade; set => _secondsToFade = value; }

    [SerializeField]
    [Range(0.0f, 1.0f)]
    private float _fadeAlpha = 0.4f;
    public float FadeAlpha { get => _fadeAlpha; set => _fadeAlpha = value; }

    private Coroutine FadeOutCoroutine { get; set; }
    private Coroutine FadeInCoroutine { get; set; }

    private IComponentSerializerService<FadeOnCovering, FadeOnCoveringData> _componentSerializerService =
        new ComponentSerializerService<FadeOnCovering, FadeOnCoveringData>();

    private IComponentSerializationSubscriber<FadeOnCovering, FadeOnCoveringData> _componentSerializationSubscriber;

    private void Awake()
    {
        _componentSerializationSubscriber = new ComponentSerializationSubscriber<FadeOnCovering, FadeOnCoveringData>(this);

        Subscribe();
    }

    private void OnDestroy()
    {
        Unsubscribe();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        ++this.TotalCollidersNotExited;

        if (!this.IsFaded)
        {
            if (this.FadeInCoroutine != null)
                StopCoroutine(this.FadeInCoroutine);

            this.FadeOutCoroutine = StartCoroutine(FadeOut());
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        --this.TotalCollidersNotExited;

        if (this.AllCollidersExited && this.IsFaded)
        {
            if (this.FadeOutCoroutine != null)
                StopCoroutine(this.FadeOutCoroutine);

            this.FadeInCoroutine = StartCoroutine(FadeIn());
        }
    }

    private IEnumerator FadeOut()
    {
        this.IsFaded = true;

        var spriteRenderer = GetSpriteRenderer();

        while (spriteRenderer.color.a > this.FadeAlpha)
        {
            var interval = ((1.0f / this.SecondsToFade) * Time.fixedDeltaTime);

            spriteRenderer.color = spriteRenderer.color.SetAlpha(Mathf.Clamp(spriteRenderer.color.a - interval, this.FadeAlpha, 1.0f));

            yield return null;
        }

        spriteRenderer.color.SetAlpha(this.FadeAlpha);
    }

    private IEnumerator FadeIn()
    {
        this.IsFaded = false;

        var spriteRenderer = GetSpriteRenderer();

        while (spriteRenderer.color.a < 1.0f)
        {
            var interval = ((1.0f / this.SecondsToFade) * Time.fixedDeltaTime);

            spriteRenderer.color = spriteRenderer.color.SetAlpha(Mathf.Clamp(spriteRenderer.color.a + interval, 0.0f, 1.0f));

            yield return null;
        }

        spriteRenderer.color.SetAlpha(1.0f);
    }

    public SpriteRenderer GetSpriteRenderer()
    {
        return this.gameObject.GetComponentInSelfOrParent<SpriteRenderer>();
    }

    private void Subscribe()
    {
        _componentSerializationSubscriber.SubscribeAsSerializable();
    }

    private void Unsubscribe()
    {
        _componentSerializationSubscriber.UnsubscribeAsSerializable();
    }
}
