﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(TilemapObject))]
public class TileRepresentation : MonoBehaviour
{
    [SerializeField]
    private BoxCollider2D _primaryCollider;
    public BoxCollider2D PrimaryCollider { get => _primaryCollider; set => _primaryCollider = value; }

    public Tile Tile { get; set; }

    private void Start()
    {
        if (this.PrimaryCollider != null)
            UpdateColliderSize();
    }

    /// <summary>
    /// Adjusts collider size to be the size of a tile.
    /// </summary>
    private void UpdateColliderSize()
    {
        this.PrimaryCollider.size = TilemapManager.Instance.TileSize;
    }
}
