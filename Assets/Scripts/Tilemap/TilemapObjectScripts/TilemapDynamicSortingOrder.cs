﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This class is designed to determine the sorting order of a tilemap object based upon
/// its current position and will update as its position changes.
/// </summary>
[RequireComponent(typeof(Renderer))]
public class TilemapDynamicSortingOrder : MonoBehaviour
{
    private Renderer CachedRenderer { get; set; }

    [SerializeField]
    private bool _useParentPosition = true;
    public bool UseParentPosition { get => _useParentPosition; set => _useParentPosition = value; }

    [SerializeField]
    private List<ParticleSystemRenderer> _particleSystemRenderers = new List<ParticleSystemRenderer>();
    public List<ParticleSystemRenderer> ParticleSystemRenderers { get => _particleSystemRenderers; set => _particleSystemRenderers = value; }

    private ISortingOrderService _sortingOrderService;

    private void Awake()
    {
        _sortingOrderService = new TilemapSortingOrderService();
    }

    private void Start()
    {
        this.CachedRenderer = GetRenderer();
    }

    private void Update() =>
        this.UpdateSortingOrder();

    private void UpdateSortingOrder()
    {
        var position = this.UseParentPosition
            ? this.gameObject.GetParent().gameObject.transform.position
            : this.gameObject.transform.position;

        int sortingOrder = _sortingOrderService.GetSortingOrder(position);

        this.CachedRenderer.sortingOrder = sortingOrder;

        this.ParticleSystemRenderers.ForEach(psr => psr.sortingOrder = sortingOrder);
    }

    public Renderer GetRenderer()
    {
        return this.gameObject.GetComponent<Renderer>();
    }
}
