﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This class is designed to determine the sorting order of a tilemap object based upon
/// its initial starting position. This will only occur once and should only be used
/// for objects which do not change positions.
/// </summary>
[RequireComponent(typeof(Renderer))]
public class TilemapStaticSortingOrder : MonoBehaviour
{
    [SerializeField]
    private bool _useParentPosition = true;
    public bool UseParentPosition { get => _useParentPosition; set => _useParentPosition = value; }

    [SerializeField]
    private List<ParticleSystemRenderer> _particleSystemRenderers = new List<ParticleSystemRenderer>();
    public List<ParticleSystemRenderer> ParticleSystemRenderers { get => _particleSystemRenderers; set => _particleSystemRenderers = value; }

    /// <summary>
    /// Stores the original sorting order of all renderers related to the object so that this order is
    /// preserved when the object is placed in the tilemap.
    /// </summary>
    private IDictionary<Renderer, int> RelativeSortingOrders { get; set; } = new Dictionary<Renderer, int>();

    private ISortingOrderService _sortingOrderService;

    private void Awake()
    {
        _sortingOrderService = new TilemapSortingOrderService();

        this.AddRelativeSortingOrderRenderer(this.GetRenderer());

        this.ParticleSystemRenderers.ForEach(psr => this.AddRelativeSortingOrderRenderer(psr));
    }

    private void Start()
    {
        var position = this.UseParentPosition
            ? this.gameObject.GetParent().gameObject.transform.position
            : this.gameObject.transform.position;

        int sortingOrder = _sortingOrderService.GetSortingOrder(position);

        this.GetRenderer().sortingOrder = sortingOrder + this.RelativeSortingOrders[this.GetRenderer()];

        this.ParticleSystemRenderers.ForEach(psr => psr.sortingOrder = sortingOrder + this.RelativeSortingOrders[psr]);
    }

    private void AddRelativeSortingOrderRenderer(Renderer renderer) =>
        this.RelativeSortingOrders.Add(renderer, renderer.sortingOrder);

    public Renderer GetRenderer()
    {
        return this.gameObject.GetComponent<Renderer>();
    }
}
