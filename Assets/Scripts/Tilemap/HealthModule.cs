﻿using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthModule : MonoBehaviour
{
    [SerializeField]
    private float _maxHealth = 1.0f;
    public float MaxHealth { get => _maxHealth; set => _maxHealth = value; }

    [SerializeField]
    private float _currentHealth = 1.0f;
    public float CurrentHealth { get => _currentHealth; private set => _currentHealth = value; }

    public event EventHandler<HealthChangedEventArgs> HealthChangedEvent;

    [Button]
    public float SetCurrentHealth(float health, HealthChangeCause healthChangeCause)
    {
        var originalHealth = this.CurrentHealth;

        this.CurrentHealth = this.GetClampedHealth(health);

        this.HealthChangedEvent?.Invoke(this, new HealthChangedEventArgs
        {
            OriginalHealth = originalHealth,
            CurrentHealth = this.CurrentHealth,
            MaxHealth = this.MaxHealth,
            HealthChangeCause = healthChangeCause,
        });

        return this.CurrentHealth;
    }

    public float ChangeCurrentHealth(float change, HealthChangeCause healthChangeCause) =>
        this.SetCurrentHealth(this.CurrentHealth + change, healthChangeCause);

    private float GetClampedHealth(float health) =>
        Mathf.Clamp(health, 0.0f, this.MaxHealth);
}
