﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemPickupBehavior : MonoBehaviour
{
    [SerializeField]
    [Required]
    private ItemModule _itemModule;
    public ItemModule ItemModule { get => _itemModule; set => _itemModule = value; }

    private IInteractionSubscriber _interactionSubscriber;

    private void Awake()
    {
        _interactionSubscriber = new InteractionSubscriber(this.gameObject)
            .WithDefaultInteraction(InteractionType.PickupIntoInventory);

        _interactionSubscriber.SubscribeToInteractionType(InteractionType.PickupIntoInventory, this.OnItemPickup);
    }

    protected void OnItemPickup(object sender, InteractionEventArgs eventArgs)
    {
        eventArgs.InteractionResponse = new PickupIntoInventoryInteractionResponse
        {
            InteractionType = InteractionType.PickupIntoInventory,
            WasSuccessful = true,
            Item = this.ItemModule.Item,
        };

        this.gameObject
            .GetRootParent()
            .DestroySelf();
    }
}
