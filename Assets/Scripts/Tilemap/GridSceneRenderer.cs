﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[RequireComponent(typeof(Grid))]
public class GridSceneRenderer : MonoBehaviour
{
    [SerializeField]
    private Color _gridColor = Color.black;
    public Color GridColor { get => _gridColor; set => _gridColor = value; }

    [SerializeField]
    private Color _highlightedCellColor = Color.blue;
    public Color HighlightedCellColor { get => _highlightedCellColor; set => _highlightedCellColor = value; }

    [SerializeField]
    private Material _linesMaterial;
    public Material LinesMaterial { get => _linesMaterial; set => _linesMaterial = value; }

    [SerializeField]
    private bool _renderGrid = true;
    public bool RenderGrid { get => _renderGrid; set => _renderGrid = value; }

    private List<Vector2Int> _highlightedCells = new List<Vector2Int>();
    public List<Vector2Int> HighlightedCells { get => _highlightedCells; set => _highlightedCells = value; }

    private void OnDrawGizmos()
    {
        if (this.RenderGrid)
            RenderLines();
    }

    public void EditorUpdate(Vector2 mouseWorldPosition)
    {
        var grid = GetGrid();

        var hoveredCell = grid.GetCellAt(mouseWorldPosition);
        this.HighlightedCells.Clear();

        if (!hoveredCell.HasValue)
            return;

        AddHighlightedCell(hoveredCell.Value);
    }

    public void AddHighlightedCell(Vector2Int position)
    {
        if (this.HighlightedCells.Any(p => p.IsEquivalent(position)))
            return;

        this.HighlightedCells.Add(position);
    }

    private void RenderLines()
    {
        GL.PushMatrix();

        var grid = GetGrid();

        for (var x = 0; x < grid.Dimensions.x + 1; x++)
        {
            var offset = x * grid.CellSize;
            DrawVerticalLine(grid.BottomLeftCorner.AddX(offset), grid.Height);
        }

        for (var y = 0; y < grid.Dimensions.y + 1; y++)
        {
            var offset = y * grid.CellSize;
            DrawHorizontalLine(grid.BottomLeftCorner.AddY(offset), grid.Width);
        }

        GL.PopMatrix();
    }

    private void DrawVerticalLine(Vector3 position, float height)
    {
        GL.Begin(GL.LINES);
        this.LinesMaterial.SetPass(0);
        GL.Color(this.GridColor);
        GL.Vertex(position);
        GL.Vertex(position.ChangeY(height));
        GL.End();
    }

    private void DrawHorizontalLine(Vector3 position, float width)
    {
        GL.Begin(GL.LINES);
        this.LinesMaterial.SetPass(0);
        GL.Color(this.GridColor);
        GL.Vertex(position);
        GL.Vertex(position.ChangeX(width));
        GL.End();
    }

    public Grid GetGrid()
    {
        return this.gameObject.GetComponent<Grid>();
    }
}
