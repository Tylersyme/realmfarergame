﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[System.Serializable]
public class DespawnByCondition : DespawnCondition
{
    [SerializeField]
    private IList<DespawnCauseType> _despawnCauseTypes = new List<DespawnCauseType>();
    private IList<DespawnCauseType> DespawnCauseTypes { get => _despawnCauseTypes; set => _despawnCauseTypes = value; }

    [SerializeField]
    [ShowIf(nameof(IsFilteringByConditionChanged))]
    private bool _filterByConditionType = false;
    public bool FilterByConditionType { get => _filterByConditionType; set => _filterByConditionType = value; }

    [SerializeField]
    [ShowIf(nameof(FilterByConditionType))]
    private IList<ConditionType> conditionTypes = new List<ConditionType>();
    private IList<ConditionType> ConditionTypes { get => conditionTypes; set => conditionTypes = value; }

    private bool IsFilteringByConditionChanged { get => this.DespawnCauseTypes.Contains(DespawnCauseType.ConditionChanged); }

    protected override void OnDespawn(object sender, DespawnEventArgs eventArgs)
    {
        if (!this.DespawnCauseTypes.Contains(eventArgs.DespawnCause.Type))
            return;

        if (this.FilterByConditionType && !this.GetFilterByConditionType((ConditionChangedDespawnCause)eventArgs.DespawnCause))
            return;

        base.OnDespawn(sender, eventArgs);
    }

    private bool GetFilterByConditionType(ConditionChangedDespawnCause despawnCause) =>
        this.ConditionTypes.Contains(despawnCause.ConditionChangedCause.Type);
}
