﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public abstract class Condition
{
    public event EventHandler<ConditionChangedEventArgs> ConditionChangedEvent;

    public bool IsFulfilled { get; private set; } = false;

    public abstract ConditionType Type { get; }

    public virtual void Subscribe() { }

    public virtual void Unsubscribe() { }

    protected void UpdateFulfillment(bool isFulfilled, ConditionChangedCause conditionChangedCause = null)
    {
        if (this.IsFulfilled == isFulfilled)
            return;

        this.IsFulfilled = isFulfilled;

        this.ConditionChangedEvent?.Invoke(this, new ConditionChangedEventArgs(this, this.IsFulfilled)
        {
            ConditionChangedCause = conditionChangedCause,
        });
    }
}
