﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class HealthInThresholdCondition : Condition
{
    [SerializeField]
    [Required]
    private HealthModule _healthModule;
    public HealthModule HealthModule { get => _healthModule; set => _healthModule = value; }

    [SerializeField]
    [MinMaxSlider(0.0f, nameof(MaxHealth), true)]
    private Vector2 _healthThreshold;
    public Vector2 HealthThreshold { get => _healthThreshold; set => _healthThreshold = value; }

    private float MaxHealth { get => this.HealthModule?.MaxHealth ?? 0.0f; }

    public override ConditionType Type { get => ConditionType.HealthInThreshold; }

    private ISubscriptionCache _subscriptionCache;

    protected void OnHealthChanged(object sender, HealthChangedEventArgs eventArgs)
    {
        var currentHealth = eventArgs.CurrentHealth;

        var isInThreshold = this.IsInThreshold(currentHealth);

        base.UpdateFulfillment(isInThreshold, new HealthInThresholdConditionChangedCause
        {
            HealthChangeCause = eventArgs.HealthChangeCause,
        });
    }

    private bool IsInThreshold(float health) =>
        health >= this.HealthThreshold.x && health < this.HealthThreshold.y;

    public override void Unsubscribe() =>
        _subscriptionCache.UnsubscribeAll();

    public override void Subscribe()
    {
        _subscriptionCache = new SubscriptionCache();

        _subscriptionCache.Subscribe<HealthChangedEventArgs>(
            this.OnHealthChanged,
            handler => this.HealthModule.HealthChangedEvent += handler,
            handler => this.HealthModule.HealthChangedEvent -= handler);
    }
}
