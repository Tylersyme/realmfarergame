﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class DespawnCondition : Condition
{
    public override ConditionType Type { get => ConditionType.Despawn; }

    [SerializeField]
    [Required]
    private DespawnBehavior _despawnBehavior;
    public DespawnBehavior DespawnBehavior { get => _despawnBehavior; set => _despawnBehavior = value; }

    private ISubscriptionCache _subscriptionCache;

    protected virtual void OnDespawn(object sender, DespawnEventArgs eventArgs) =>
        base.UpdateFulfillment(true);

    public override void Unsubscribe() =>
        _subscriptionCache.UnsubscribeAll();

    public override void Subscribe()
    {
        _subscriptionCache = new SubscriptionCache();

        _subscriptionCache.Subscribe<DespawnEventArgs>(
            this.OnDespawn,
            handler => this.DespawnBehavior.DespawnEvent += handler,
            handler => this.DespawnBehavior.DespawnEvent -= handler);
    }
}
