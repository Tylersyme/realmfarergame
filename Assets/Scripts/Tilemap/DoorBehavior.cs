﻿using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorBehavior : MonoBehaviour
{
    [SerializeField]
    private List<Collider2D> _colliders = new List<Collider2D>();
    public List<Collider2D> Colliders { get => _colliders; set => _colliders = value; }

    [SerializeField]
    private bool _isOpen = false;
    public bool IsOpen { get => _isOpen; private set => _isOpen = value; }

    public event EventHandler<OpenDoorToggledEventArgs> OpenDoorToggledEvent;

    private IInteractionSubscriber _interactionSubscriber;

    private void Awake()
    {
        _interactionSubscriber = new InteractionSubscriber(this.gameObject)
            .WithDefaultInteraction(InteractionType.OpenDoorToggle);

        _interactionSubscriber.SubscribeToInteractionType(InteractionType.OpenDoorToggle, this.OnDoorToggled);
    }

    private void OnDestroy() =>
        _interactionSubscriber.UnsubscribeAll();

    public void SetOpen(bool isOpen)
    {
        if (this.IsOpen == isOpen)
            return;

        this.IsOpen = isOpen;

        this.UpdateColliders(!isOpen);

        this.OpenDoorToggledEvent?.Invoke(this, new OpenDoorToggledEventArgs
        {
            WasOpened = this.IsOpen,
        });
    }

    public void ToggleOpen() =>
        this.SetOpen(!this.IsOpen);

    private void UpdateColliders(bool isActive) =>
        this.Colliders.ForEach(c => c.isTrigger = !isActive);

    protected void OnDoorToggled(object sender, InteractionEventArgs interactionEventArgs)
    {
        this.ToggleOpen();

        interactionEventArgs.InteractionResponse = new OpenDoorInteractionResponse
        {
            InteractionType = InteractionType.None,
            WasSuccessful = true,
            WasOpened = this.IsOpen,
        };
    }
}
