﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(EntityController))]
[RequireComponent(typeof(ContainerModule))]
public class EntityInteractionListener : MonoBehaviour
{
    private IInteractionSubscriber _interactionSubscriber;
    private ITilemapModificationService _tilemapModificationService;
    private ITilemapMouseDataService _tilemapMouseDataService;

    private IItemSpawnService _itemSpawnService;
    private IItemPlacementService _itemPlacementService;

    private void Awake()
    {
        _interactionSubscriber = new InteractionSubscriber(this.gameObject);
        _tilemapModificationService = new TilemapModificationService();
        _tilemapMouseDataService = new TilemapMouseDataService();
        _itemSpawnService = new ItemSpawnService();
        _itemPlacementService = new ItemPlacementService();

        _interactionSubscriber.SubscribeToInteractionAdjective(InteractionAdjectiveType.PlacesTile, this.OnPlaceTile);
        _interactionSubscriber.SubscribeToInteractionAdjective(InteractionAdjectiveType.DropsItem, this.OnDropItem);
        _interactionSubscriber.SubscribeToInteractionAdjective(InteractionAdjectiveType.PlacesItem, this.OnPlaceItem);
    }

    private void OnDestroy() =>
        _interactionSubscriber.UnsubscribeAll();

    protected void OnPlaceTile(object sender, InteractionAdjectiveEventArgs eventArgs)
    {
        if (eventArgs.Interaction.Type != InteractionType.Item)
            return;

        var itemInteraction = (ItemInteraction)eventArgs.Interaction;

        if (itemInteraction.ItemActivationCause.Type != ItemActivationCauseType.Entity)
            return;

        var entityItemActivationCause = (EntityItemActivationCause)itemInteraction.ItemActivationCause;

        if (entityItemActivationCause.EntityController != this.GetEntityController())
            return;

        var placeTileInteractionAdjective = (PlaceTileInteractionAdjective)eventArgs.InteractionAdjective;

        var tileTypeToPlace = placeTileInteractionAdjective.TileTypeToPlace;
        var hoveredTilePosition = _tilemapMouseDataService.GetHoveredTilePosition();

        if (_tilemapModificationService.CanPlaceTileAt(hoveredTilePosition))
            _tilemapModificationService.PlaceTileFromType(tileTypeToPlace, hoveredTilePosition);
    }

    protected void OnDropItem(object sender, InteractionAdjectiveEventArgs eventArgs)
    {
        if (eventArgs.Interaction.Type != InteractionType.Item)
            return;

        var itemInteraction = (ItemInteraction)eventArgs.Interaction;

        if (itemInteraction.ItemActivationCause.Type != ItemActivationCauseType.Entity)
            return;

        var entityItemActivationCause = (EntityItemActivationCause)itemInteraction.ItemActivationCause;

        if (entityItemActivationCause.EntityController != this.GetEntityController())
            return;

        var dropItemInteractionAdjective = (DropItemInteractionAdjective)eventArgs.InteractionAdjective;

        var droppedItem = this
            .GetContainerModule().Container.Inventory
            .SplitItem(dropItemInteractionAdjective.DroppedItem, 1);

        _itemSpawnService.SpawnDroppedItem(
            droppedItem,
            this.gameObject.transform.position);

        EventManager.Instance.UiEventHub.Publish(new InventoryChangedEventArgs
        {
            Inventory = this.GetContainerModule().Container.Inventory,
        });
    }

    protected void OnPlaceItem(object sender, InteractionAdjectiveEventArgs eventArgs)
    {
        if (eventArgs.Interaction.Type != InteractionType.Item)
            return;

        var itemInteraction = (ItemInteraction)eventArgs.Interaction;

        if (itemInteraction.ItemActivationCause.Type != ItemActivationCauseType.Entity)
            return;

        var entityItemActivationCause = (EntityItemActivationCause)itemInteraction.ItemActivationCause;

        if (entityItemActivationCause.EntityController != this.GetEntityController())
            return;

        var placeItemInteractionAdjective = (PlaceItemInteractionAdjective)eventArgs.InteractionAdjective;

        var placedItem = this
            .GetContainerModule().Container.Inventory
            .SplitItem(placeItemInteractionAdjective.PlacedItem, 1);

        if (!_itemPlacementService.IsPlaceable(placedItem))
            throw new System.Exception($"Error placing item: The item of type {placedItem.Type} is not placeable.");

        if (_itemPlacementService.IsRotateable(placedItem))
            _itemSpawnService.SpawnPlacedItemWithDirection(
                placedItem,
                placeItemInteractionAdjective.PlaceItemPrefab,
                placeItemInteractionAdjective.Position,
                BuildModeManager.Instance.SelectedItemCurrentDirectionType);
        else
            _itemSpawnService.SpawnPlacedItem(
                placedItem,
                placeItemInteractionAdjective.PlaceItemPrefab,
                placeItemInteractionAdjective.Position);

        EventManager.Instance.UiEventHub.Publish(new InventoryChangedEventArgs
        {
            Inventory = this.GetContainerModule().Container.Inventory,
        });
    }

    public EntityController GetEntityController() =>
        this.gameObject.GetComponent<EntityController>();

    public ContainerModule GetContainerModule() =>
        this.gameObject.GetComponent<ContainerModule>();
}
