﻿using Sirenix.OdinInspector;
using Sirenix.Utilities;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DespawnBehavior : SerializedMonoBehaviour
{
    [SerializeField]
    private IList<Condition> _despawnConditions = new List<Condition>();
    public IList<Condition> DespawnConditions { get => _despawnConditions; }

    private ISubscriptionCache _subscriptionCache;

    public event EventHandler<DespawnEventArgs> DespawnEvent;

    protected void OnConditionChanged(object sender, ConditionChangedEventArgs eventArgs)
    {
        if (!eventArgs.IsFulfilled)
            return;

        this.Despawn(new ConditionChangedDespawnCause
        {
            ConditionChangedCause = eventArgs.ConditionChangedCause,
        });
    }

    private void Awake()
    {
        _subscriptionCache = new SubscriptionCache();

        this.DespawnConditions.ForEach(c => c.Subscribe());

        this.Subscribe();
    }

    private void OnDestroy()
    {
        this.DespawnConditions.ForEach(c => c.Unsubscribe());

        _subscriptionCache.UnsubscribeAll();
    }

    public void Despawn(DespawnCause despawnCause)
    {
        this.DespawnEvent?.Invoke(this, new DespawnEventArgs
        {
            DespawnCause = despawnCause,
        });

        this.gameObject.DestroySelf();
    }

    private void Subscribe() =>
        this.DespawnConditions.ForEach(c =>
        {
            _subscriptionCache.Subscribe<ConditionChangedEventArgs>(
                this.OnConditionChanged,
                handler => c.ConditionChangedEvent += handler,
                handler => c.ConditionChangedEvent -= handler);
        });
}
