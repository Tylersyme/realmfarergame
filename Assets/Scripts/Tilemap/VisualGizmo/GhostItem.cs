﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostItem : MonoBehaviour
{
    public bool SnapToTile { get; set; }

    public Vector2 PositionOffset { get; set; } = Vector2.zero;

    private ITilemapMouseDataService _tilemapMouseDataService;

    private void Awake()
    {
        _tilemapMouseDataService = new TilemapMouseDataService();
    }

    private void Update()
    {
        this.UpdatePosition();
    }

    public void UpdatePosition()
    {
        var position = this.PositionOffset;

        if (this.SnapToTile)
            position = position.Add(_tilemapMouseDataService.GetHoveredTileCenterWorldPosition());
        else
            position = position.Add(Camera.main.MouseWorldPosition());

        this.gameObject.transform.position = position;
    }
}
