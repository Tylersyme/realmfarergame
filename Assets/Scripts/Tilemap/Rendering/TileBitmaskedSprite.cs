﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[RequireComponent(typeof(TileRepresentation))]
public class TileBitmaskedSprite : MonoBehaviour
{
    [SerializeField]
    [Required]
    private BitmaskingSpriteGroup _bitmaskingSpriteGroup;
    public BitmaskingSpriteGroup BitmaskingSpriteGroup { get => _bitmaskingSpriteGroup; set => _bitmaskingSpriteGroup = value; }

    [SerializeField]
    [Required]
    private SpriteRenderer _spriteRenderer;
    public SpriteRenderer SpriteRenderer { get => _spriteRenderer; set => _spriteRenderer = value; }

    private Vector2Int TilePosition { get; set; }

    private ITileBitmaskService _tileBitmaskService;
    private ITilemapPointService _tilemapPointService;

    private void Awake()
    {
        _tileBitmaskService = new TileBitmaskService();
        _tilemapPointService = new TilemapPointService();
    }

    private void Start()
    {
        this.TilePosition = _tilemapPointService.GetTilePositionAt(this.gameObject.transform.position);

        this.UpdateSpriteWithIgnoredCascadePosition(new Vector2Int[] { }, true);
    }

    public void UpdateSprite() =>
        this.UpdateSpriteWithIgnoredCascadePosition(new Vector2Int[] { }, false);

    public void UpdateSpriteWithIgnoredCascadePosition(IEnumerable<Vector2Int> ignoredCascadePositions) =>
        this.UpdateSpriteWithIgnoredCascadePosition(ignoredCascadePositions, false);

    private void UpdateSpriteWithIgnoredCascadePosition(IEnumerable<Vector2Int> ignoredCascadePositions, bool forceCascade)
    {
        var tile = this.GetTileRepresentation().Tile;

        var sprite = _tileBitmaskService.GetBitmaskSpriteFromTileType(
            tile.Type,
            this.TilePosition,
            this.BitmaskingSpriteGroup);

        if (!forceCascade && sprite == this.SpriteRenderer.sprite)
            return;

        this.SpriteRenderer.sprite = sprite;

        var tilePositions = _tileBitmaskService
            .GetBitmaskAffectedTilePositions(
                this.TilePosition,
                this.BitmaskingSpriteGroup.BitmaskAdjacencyMode.ToAdjacencyType())
            .Where(p => !ignoredCascadePositions.Any(ip => ip.AsVector3Int().SetZ(1).IsEquivalent(p)))
            .ToList();

        ignoredCascadePositions = ignoredCascadePositions.Concat(new Vector2Int[] { this.TilePosition });

        var tileRepresentations = WorldManager.Instance.CurrentTilemap
            .GetTiles(tilePositions)
            .Select(t => t.TileRepresentation)
            .ToList();

        foreach (var tileRepresentation in tileRepresentations)
            tileRepresentation.gameObject
                .GetComponent<TileBitmaskedSprite>()
                .UpdateSpriteWithIgnoredCascadePosition(ignoredCascadePositions);
    }

    public TileRepresentation GetTileRepresentation() =>
        this.gameObject.GetComponent<TileRepresentation>();
}
