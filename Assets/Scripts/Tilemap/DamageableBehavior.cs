﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(HealthModule))]
public class DamageableBehavior : MonoBehaviour
{
    [SerializeField]
    [Required]
    private DamageImpactProfile _damageImpactProfile;
    public DamageImpactProfile DamageImpactProfile { get => _damageImpactProfile; set => _damageImpactProfile = value; }

    private IInteractionSubscriber _interactionSubscriber;

    private void Awake()
    {
        _interactionSubscriber = new InteractionSubscriber(this.gameObject);

        _interactionSubscriber.SubscribeToInteractionAdjective(InteractionAdjectiveType.Damages, this.OnDamaged);
    }

    private void OnDestroy() => 
        _interactionSubscriber.UnsubscribeAll();

    protected void OnDamaged(object sender, InteractionAdjectiveEventArgs eventArgs)
    {
        var interactionType = eventArgs.Interaction.Type;

        if (interactionType != InteractionType.Item)
            return;

        var itemInteraction = (ItemInteraction)eventArgs.Interaction;
        var damagesInteractionAdjective = (DamagesInteractionAdjective)eventArgs.InteractionAdjective;

        var damageImpact = this.DamageImpactProfile.GetDamageImpactOfType(damagesInteractionAdjective.Damage.Type);

        var damageDealt = damageImpact.DamageModifier * damagesInteractionAdjective.Damage.Amount;

        var damageCause = new ItemInteractionDamageCause
        {
            DamageCauser = itemInteraction.ItemUsed,
        };

        this
            .GetHealthModule()
            .ChangeCurrentHealth(-damageDealt, new DamageHealthChangedCause
            {
                DamageCause = damageCause,
            });
    }

    public HealthModule GetHealthModule() =>
        base.gameObject.GetComponent<HealthModule>();
}
