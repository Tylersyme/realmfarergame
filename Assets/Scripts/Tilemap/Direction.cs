﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using UnityEngine;

[System.Serializable]
public class Direction
{
    // Cardinal
    public static readonly Direction NORTH = new Direction(DirectionType.North, "North", 0, 1, 0, 0.0f, true, new Axis[] { Axis.Y }, new Direction[] { NORTH });
    public static readonly Direction SOUTH = new Direction(DirectionType.South, "South", 0, -1, 0, 180.0f, true, new Axis[] { Axis.Y }, new Direction[] { SOUTH });
    public static readonly Direction EAST = new Direction(DirectionType.East, "East", 1, 0, 0, 270.0f, true, new Axis[] { Axis.X }, new Direction[] { EAST });
    public static readonly Direction WEST = new Direction(DirectionType.West, "West", -1, 0, 0, 90.0f, true, new Axis[] { Axis.X }, new Direction[] { WEST });
    public static readonly Direction UP = new Direction(DirectionType.Up, "Up", 0, 0, 1, 0.0f, true, new Axis[] { Axis.Z }, new Direction[] { UP });
    public static readonly Direction DOWN = new Direction(DirectionType.Down, "Down", 0, 0, -1, 180.0f, true, new Axis[] { Axis.Z }, new Direction[] { DOWN });

    // Diagonal
    public static readonly Direction NORTH_EAST = 
        new Direction(DirectionType.NorthEast, "North East", 1, 1, 0, 315.0f, false, new Axis[] { Axis.Y, Axis.X }, new Direction[] { NORTH, EAST });
    public static readonly Direction NORTH_WEST = 
        new Direction(DirectionType.NorthWest, "North West", -1, 1, 0, 45.0f, false, new Axis[] { Axis.Y, Axis.X }, new Direction[] { NORTH, WEST });
    public static readonly Direction SOUTH_EAST = 
        new Direction(DirectionType.SouthEast, "South East", 1, -1, 0, 225.0f, false, new Axis[] { Axis.Y, Axis.X }, new Direction[] { SOUTH, EAST });
    public static readonly Direction SOUTH_WEST = 
        new Direction(DirectionType.SouthWest, "South West", -1, -1, 0, 135.0f, false, new Axis[] { Axis.Y, Axis.X }, new Direction[] { SOUTH, WEST });

    public static readonly ReadOnlyCollection<Direction> XY_DIRECTIONS =
        new ReadOnlyCollection<Direction>(new Direction[] { NORTH, SOUTH, EAST, WEST, NORTH_EAST, NORTH_WEST, SOUTH_EAST, SOUTH_WEST });

    public static readonly ReadOnlyCollection<Direction> XY_CARDINAL_DIRECTIONS =
        new ReadOnlyCollection<Direction>(new Direction[] { NORTH, SOUTH, EAST, WEST });

    public static readonly ReadOnlyCollection<Direction> XY_DIAGONAL_DIRECTIONS =
        new ReadOnlyCollection<Direction>(new Direction[] { NORTH_EAST, NORTH_WEST, SOUTH_EAST, SOUTH_WEST });

    private static readonly IDictionary<RotationType, IList<Direction>> ROTATION_TYPE_DIRECTIONS =
        new Dictionary<RotationType, IList<Direction>>
        {
            [RotationType.NorthSouth] = new List<Direction> { SOUTH, NORTH },
            [RotationType.EastWest] = new List<Direction> { WEST, EAST },
            [RotationType.Cardinal] = new List<Direction> { SOUTH, WEST, NORTH, EAST },
        };

    public DirectionType Type { get; private set; }
    public Vector3 RelativeDirection { get; private set; }
    public bool IsCardinal { get; private set; }
    public bool IsDiagonal { get => !this.IsCardinal; }
    public Axis[] Axes { get; private set; }
    public string Name { get; private set; }
    public float AngleInDegrees { get; private set; }

    /// <summary>
    /// The directions which compose this direction. 
    /// e.g. NORTH_EAST is composed of NORTH and EAST.
    /// e.g. NORTH is composed of NORTH.
    /// </summary>
    public Direction[] CompositeDirections { get; private set; }

    /// <summary>
    /// Get the directions which correspond to the adjacency type.
    /// NORTH, SOUTH, EAST and WEST are considered cardinal.
    /// NORTH_EAST, NORTH_WEST, SOUTH_EAST and SOUTH_WEST are considered diagonal.
    /// </summary>
    /// <param name="adjacencyType"></param>
    /// <returns></returns>
    public static ReadOnlyCollection<Direction> GetAdjacencyDirections(AdjacencyType adjacencyType)
    {
        if (adjacencyType == AdjacencyType.CARDINAL)
            return XY_CARDINAL_DIRECTIONS;
        else if (adjacencyType == AdjacencyType.DIAGONAL)
            return XY_DIAGONAL_DIRECTIONS;
        else if (adjacencyType == AdjacencyType.FULL)
            return XY_DIRECTIONS;

        throw new NotImplementedException($"Error getting direction from adjacency type: The adjacency type {adjacencyType} is not supported.");
    }

    public static Direction GetApproximateDirectionFromRotation(Quaternion rotation)
    {
        var angle = rotation.eulerAngles;

        if (MathExtended.IsBetweenInclusive(angle.z, 45.0f, 135.0f))
            return Direction.WEST;
        else if (MathExtended.IsBetweenInclusive(angle.z, 135.0f, 225.0f))
            return Direction.SOUTH;
        else if (MathExtended.IsBetweenInclusive(angle.z, 225.0f, 315.0f))
            return Direction.EAST;
        else
            return Direction.NORTH;
    }

    /// <summary>
    /// Returns the direction from the first point to the second point.
    /// </summary>
    /// <param name="first"></param>
    /// <param name="second"></param>
    /// <returns></returns>
    public static Direction GetDirectionBetweenPoints(Vector2Int first, Vector2Int second)
    {
        var difference = second - first;

        if (difference.x == 0 && difference.y > 0)
            return Direction.NORTH;
        else if (difference.x == 0 && difference.y < 0)
            return Direction.SOUTH;
        else if (difference.x > 0 && difference.y == 0)
            return Direction.EAST;
        else if (difference.x < 0 && difference.y == 0)
            return Direction.WEST;
        else if (difference.x > 0 && difference.y > 0)
            return Direction.NORTH_EAST;
        else if (difference.x < 0 && difference.y > 0)
            return Direction.NORTH_WEST;
        else if (difference.x > 0 && difference.y < 0)
            return Direction.SOUTH_EAST;
        else if (difference.x < 0 && difference.y < 0)
            return Direction.SOUTH_WEST;

        throw new System.NotImplementedException(
            $"Error getting direction between two points: No direction could be found between {first} and {second}.");
    }

    public static IList<Direction> GetDirectionsOfRotationType(RotationType rotationType) =>
        ROTATION_TYPE_DIRECTIONS[rotationType];

    public static Direction ParseByName(string directionName) =>
        XY_DIRECTIONS.First(d => d.Name == directionName);

    public static Direction ParseByType(DirectionType directionType) =>
        XY_DIRECTIONS.First(d => d.Type == directionType);

    private Direction(
        DirectionType directionType,
        string name, 
        float x, float y, float z, 
        float angleInDegrees, 
        bool isOrthogonal, 
        Axis[] axes, 
        Direction[] compositeDirections)
    {
        this.Type = directionType;
        this.Name = name;
        this.RelativeDirection = new Vector3(x, y, z);
        this.AngleInDegrees = angleInDegrees;
        this.IsCardinal = isOrthogonal;
        this.Axes = axes;
        this.CompositeDirections = compositeDirections;
    }

    public Direction GetOpposite()
    {
        if (this == Direction.NORTH)
            return Direction.SOUTH;
        else if (this == Direction.SOUTH)
            return Direction.NORTH;
        else if (this == Direction.EAST)
            return Direction.WEST;
        else if (this == Direction.WEST)
            return Direction.EAST;

        throw new NotImplementedException();
    }

    public float GetRelativeAxisDirection(Axis axis)
    {
        if (axis == Axis.X)
            return this.RelativeDirection.x;
        else if (axis == Axis.Y)
            return this.RelativeDirection.y;
        else if (axis == Axis.Z)
            return this.RelativeDirection.z;

        throw new System.NotImplementedException($"Error getting relative axis direction: The axis {axis} is not supported.");
    }
}

public enum DirectionType
{
    North,
    South,
    East,
    West,
    NorthWest,
    NorthEast,
    SouthWest,
    SouthEast,
    Up,
    Down,
}

public enum Axis
{
    X,
    Y,
    Z
}

public enum AdjacencyType
{
    /// <summary>
    /// North, South, East and West.
    /// </summary>
    CARDINAL,

    /// <summary>
    /// North East, North West, South East, South West.
    /// </summary>
    DIAGONAL,

    /// <summary>
    /// All Directions.
    /// </summary>
    FULL,
}