﻿using Sirenix.OdinInspector;
using Sirenix.Serialization;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemModule : SerializedMonoBehaviour
{
    [OdinSerialize]
    private Item _item;
    public Item Item { get => _item; set => _item = value; }
}
