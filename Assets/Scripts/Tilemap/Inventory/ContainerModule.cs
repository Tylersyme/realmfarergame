﻿using Sirenix.OdinInspector;
using Sirenix.Serialization;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// TODO: Create EntityContainerModule
public class ContainerModule : SerializedMonoBehaviour
{
    [OdinSerialize]
    private EntityContainer _container = new EntityContainer();
    public EntityContainer Container { get => _container; set => _container = value; }
}
