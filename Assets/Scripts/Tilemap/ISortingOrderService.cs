﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ISortingOrderService
{
    int GetSortingOrder(Vector2 worldPosition);
}
