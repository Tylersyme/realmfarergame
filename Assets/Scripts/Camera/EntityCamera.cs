﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class EntityCamera : MonoBehaviour
{
    [SerializeField]
    private GameObject _objectToFollow;
    public GameObject ObjectToFollow
    {
        get { return _objectToFollow; }
        set { _objectToFollow = value; }
    }

    [SerializeField]
    private Vector2 _offset;
    public Vector2 Offset
    {
        get { return _offset; }
        set { _offset = value; }
    }

    private void Start()
    {
        CenterOnGameObject();
    }

    private void Update()
    {
        CenterOnGameObject();
    }

    private void CenterOnGameObject()
    {
        this.transform.position = this.ObjectToFollow.transform.position
            .Add(this.Offset)
            .SetZ(this.transform.position.z);
    }

    public Camera GetCamera()
    {
        return this.GetComponent<Camera>();
    }
}
