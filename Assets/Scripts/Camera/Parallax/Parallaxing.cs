﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class Parallaxing : MonoBehaviour
{
    [SerializeField]
    private List<ParallaxLayer> _parllaxLayers = new List<ParallaxLayer>();
    public List<ParallaxLayer> ParallaxLayers
    {
        get { return _parllaxLayers; }
    }

    private Vector3 _previousPosition;

    private void Start()
    {
        _previousPosition = this.transform.position;
    }

    private void Update()
    {
        UpdateParallaxLayers();

        _previousPosition = this.transform.position;
    }

    private void UpdateParallaxLayers()
    {
        foreach (var parallaxLayer in this.ParallaxLayers)
            UpdateParallaxLayerGameObjects(parallaxLayer);
    }

    public void AddToParallaxLayer(GameObject gameObject, Vector2 speedRatio)
    {
        var parallaxLayer = this.ParallaxLayers.FirstOrDefault(p => p.SpeedRatio == speedRatio);
        if (parallaxLayer == null)
        {
            parallaxLayer = new ParallaxLayer(speedRatio);
            this.ParallaxLayers.Add(parallaxLayer);
        }
        
        parallaxLayer.GameObjects.Add(gameObject);
    }

    /// <summary>
    /// Adjusts the position of the game objects in a parallax layer.
    /// </summary>
    private void UpdateParallaxLayerGameObjects(ParallaxLayer parallaxLayer)
    {
        var changeInPosition = GetChangeInPosition();

        foreach (var gameObject in parallaxLayer.GameObjects)
            gameObject.transform.position = GetParallaxPosition(
                gameObject.transform.position, 
                parallaxLayer.SpeedRatio, 
                changeInPosition);
    }

    /// <summary>
    /// Calculates the new position of the game object given its parallaxing properties
    /// and the movement of the camera.
    /// </summary>
    /// <param name="originalGameObjectPosition"></param>
    /// <param name="speedRatio"></param>
    /// <param name="cameraPositionDifference"></param>
    /// <returns></returns>
    private Vector2 GetParallaxPosition(Vector2 originalGameObjectPosition, Vector2 speedRatio, Vector2 cameraPositionDifference)
    {
        var speedRatioAdjustment = new Vector2(1 - speedRatio.x, 1 - speedRatio.y);
        var positionAdjustment = cameraPositionDifference.Multiply(speedRatioAdjustment);
        Debug.Log("obj = " + speedRatioAdjustment.x + "," + speedRatioAdjustment.y);
        Debug.Log("cam = " + cameraPositionDifference.x + "," + cameraPositionDifference.y);

        return originalGameObjectPosition.Add(positionAdjustment);
    }

    private Vector2 GetChangeInPosition()
    {
        return this.transform.position - _previousPosition;
    }

    public Camera GetCamera()
    {
        return this.GetComponent<Camera>();
    }
}
