﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ParallaxLayer
{
    [SerializeField]
    private Vector2 _speedRatio = Vector2.one;
    public Vector2 SpeedRatio
    {
        get { return _speedRatio; }
        set { _speedRatio = value; }
    }

    [SerializeField]
    private List<GameObject> _gameObjects = new List<GameObject>();
    public List<GameObject> GameObjects
    {
        get { return _gameObjects; }
    }

    public ParallaxLayer(Vector2 speedRatio)
    {
        this.SpeedRatio = speedRatio;
    }
}
