﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TilemapPointService : ITilemapPointService
{
    private const string HOVERED_TILE_POSITION_CACHE_ID = "HoveredTilePosition";

    private readonly IFrameCache _frameCache;

    public TilemapPointService()
    {
        _frameCache = new FrameCache();
    }

    public Vector2Int GetTilePositionAt(Vector2 position)
    {
        if (_frameCache.IsCached(HOVERED_TILE_POSITION_CACHE_ID))
            return _frameCache.GetCachedData<Vector2Int>(HOVERED_TILE_POSITION_CACHE_ID);

        var tileSize = TilemapManager.Instance.TileSize;

        var relativeToTilemapPosition = this.GetPositionRelativeToTilemap(position);

        var hoveredTilePosition = relativeToTilemapPosition
            .Divide(tileSize)
            .AsVector2Int();

        _frameCache.Cache(HOVERED_TILE_POSITION_CACHE_ID, hoveredTilePosition);

        return hoveredTilePosition;
    }

    public Vector2 GetTileCenterWorldPosition(Vector2Int tilePosition)
    {
        var tileSize = TilemapManager.Instance.TileSize;

        var tilemapBottomLeftPosition = this.GetTilemapRendererBottomLeftCorner();

        return tilePosition
            .AsVector2()
            .Add(Vector2.one)
            .Multiply(tileSize)
            .Subtract(tileSize.Multiply(0.5f))
            .Add(tilemapBottomLeftPosition);
    }

    public Vector2 GetPositionRelativeToTilemap(Vector2 position)
    {
        var tilemapBottomLeftPosition = this.GetTilemapRendererBottomLeftCorner();

        return position.Subtract(tilemapBottomLeftPosition);
    }

    private Vector2 GetTilemapRendererBottomLeftCorner() =>
        TilemapManager.Instance.TilemapRenderer.gameObject.transform.position.AsVector2();
}
