﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ITilemapModificationService
{
    void PlaceTileFromType(TileType tileType, Vector2Int tilemapPosition);
    bool CanPlaceTileAt(Vector2Int tilemapPosition);
}
