﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ITilemapPointService
{
    Vector2Int GetTilePositionAt(Vector2 position);
    Vector2 GetTileCenterWorldPosition(Vector2Int tilePosition);
    Vector2 GetPositionRelativeToTilemap(Vector2 position);
}
