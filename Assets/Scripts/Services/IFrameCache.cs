﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IFrameCache
{
    void Cache(string dataId, object data);
    T GetCachedData<T>(string dataId);
    void TryGetCachedData<T>(string dataId, out T data) where T : struct;
    bool IsCached(string dataId);
}
