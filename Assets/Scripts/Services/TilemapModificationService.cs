﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TilemapModificationService : ITilemapModificationService
{
    private ITilemapObjectRepresentationService _tilemapObjectRepresentationService;

    public TilemapModificationService()
    {
        _tilemapObjectRepresentationService = new TilemapObjectRepresentationService();
    }

    public void PlaceTileFromType(TileType tileType, Vector2Int tilemapPosition)
    {
        var tilemap = WorldManager.Instance.CurrentTilemap;

        var tileHeight = tilemap.GetTilesAlongZ(tilemapPosition).Count;

        var tilePosition = tilemapPosition
            .AsVector3Int()
            .SetZ(tileHeight);

        var tile = tilemap.SetTile(tileType, tilePosition);

        // Placing a tile removes any decorations so that they don't cover the new tileS
        this.RemoveDecorationsAt(tilemapPosition);

        if (_tilemapObjectRepresentationService.TileNeedsObjectRepresentation(tilemap, tile, tilePosition))
            _tilemapObjectRepresentationService.InstantiateTilemapObjectRepresentationAt(tilemap, tilePosition);

        EventManager.Instance.TilemapEventHub.Publish(new TilemapTileChangedEventArgs
        {
            TileChangedTilemapPosition = tilePosition,
            ShouldUpdateTilemap = true,
        });
    }

    private void RemoveDecorationsAt(Vector2Int tilemapPosition)
    {
        var tilemap = WorldManager.Instance.CurrentTilemap;

        tilemap.RemoveDecorationTilesAt(tilemapPosition);
    }

    public bool CanPlaceTileAt(Vector2Int tilemapPosition)
    {
        var tilemap = WorldManager.Instance.CurrentTilemap;

        if (!tilemap.IsInBounds(tilemapPosition.AsVector3Int()))
            return false;

        var tileHeight = tilemap.GetTilesAlongZ(tilemapPosition).Count;

        return tileHeight < tilemap.Size.z;
    }
}
