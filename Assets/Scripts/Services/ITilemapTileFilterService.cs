﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ITilemapTileFilterService
{
    IList<Vector3Int> GetAdjacentTilesOfType(Tilemap tilemap, Vector3Int tilePosition, TileType tileType);
}
