﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using UnityEngine;

public class GameObjectBuilder : 
    IGameObjectBuilder, 
    IGameObjectBuilderNonCreateable 
{
    protected GameObject _gameObject;

    public IGameObjectBuilderNonCreateable Create(string name)
    {
        _gameObject = new GameObject(name);

        return this;
    }

    public IGameObjectBuilderNonCreateable CreateFromPrefab(GameObject prefab)
    {
        _gameObject = UnityEngine.Object.Instantiate(prefab);

        return this;
    }
    public IGameObjectBuilderNonCreateable UseExistingGameObject(GameObject existingGameObject)
    {
        _gameObject = existingGameObject;

        return this;
    }

    public IGameObjectBuilderNonCreateable WithPosition(Vector3 position)
    {
        _gameObject.transform.position = position;

        return this;
    }

    public IGameObjectBuilderNonCreateable WithRotation(Quaternion rotation)
    {
        _gameObject.transform.rotation = rotation;

        return this;
    }

    public IGameObjectBuilderNonCreateable WithEnabled(bool isEnabled)
    {
        _gameObject.SetActive(isEnabled);

        return this;
    }

    public IGameObjectBuilderComponentValueInitializer<TComponent> WithComponent<TComponent>() where TComponent : Component
    {
        this.AddRequiredComponents<TComponent>();

        var component = _gameObject.AddComponent<TComponent>();

        return new GameObjectBuilderInternal<TComponent>(_gameObject, component);
    }

    public IGameObjectBuilderNonCreateable WithComponent(Type componentType)
    {
        _gameObject.AddComponent(componentType);

        return this;
    }

    public IGameObjectBuilderComponentValueInitializer<TComponent> WithComponentIf<TComponent>(bool shouldAddComponent) 
        where TComponent : Component
    {
        if (!shouldAddComponent)
            return new GameObjectBuilderInternal<TComponent>(_gameObject, null);

        return this.WithComponent<TComponent>();
    }

    public IGameObjectBuilderNonCreateable WithComponentIf(Type componentType, bool shouldAddComponent)
    {
        if (!shouldAddComponent)
            return this;

        return this.WithComponent(componentType);
    }

    public IGameObjectBuilderComponentValueInitializer<TComponent> ModifyComponent<TComponent>() where TComponent : Component
    {
        var component = _gameObject.GetComponent<TComponent>();

        return new GameObjectBuilderInternal<TComponent>(_gameObject, component);
    }

    public IGameObjectBuilderNonCreateable WithEmptyChild(string name)
    {
        var child = new GameObject(name);

        child.SetParent(_gameObject);

        return this;
    }

    public IGameObjectBuilderNonCreateable ModifyImmediateChildByComponent<TComponent>(
        Action<IGameObjectBuilderNonCreateable> childBuilder) 
        where TComponent : Component
    {
        var child = _gameObject
            .GetImmediateChildren()
            .First(c => c.HasComponent<TComponent>());

        childBuilder.Invoke(new GameObjectBuilder()
            .UseExistingGameObject(child));

        return this;
    }

    public IGameObjectBuilderNonCreateable WithChild(
        string name, 
        Func<IGameObjectBuilderNonCreateable, GameObject> childBuilder)
    {
        var child = childBuilder.Invoke(new GameObjectBuilder()
            .Create(name));

        child.SetParent(_gameObject);

        return this;
    }

    public IGameObjectBuilderNonCreateable WithChildIf(
        string name, 
        bool shouldAddChild, 
        Func<IGameObjectBuilderNonCreateable, GameObject> childBuilder)
    {
        if (!shouldAddChild)
            return this;

        return this.WithChild(name, childBuilder);
    }

    public GameObject Build()
    {
        var gameObject = _gameObject;

        _gameObject = null;

        return gameObject;
    }

    public class GameObjectBuilderInternal<TComponent> :
        GameObjectBuilder,
        IGameObjectBuilderComponentValueInitializer<TComponent>
        where TComponent : Component
    {
        private TComponent _currentComponent;

        public GameObjectBuilderInternal(GameObject existingGameObject, TComponent component)
        {
            _gameObject = existingGameObject;

            _currentComponent = component;
        }

        public IGameObjectBuilderComponentValueInitializer<TComponent> WithValue<TProperty>(
            Expression<Func<TComponent, TProperty>> propertySelector, TProperty value)
        {
            if (_currentComponent == null)
                return this;

            var propertyInfo = ((MemberExpression)propertySelector.Body).Member as PropertyInfo;

            if (propertyInfo == null)
                throw new ArgumentException("The lambda expression 'property' should point to a valid Property");

            propertyInfo.SetValue(_currentComponent, value);

            return this;
        }

        public IGameObjectBuilderComponentValueInitializer<TComponent> WithValueIf<TProperty>(
            Expression<Func<TComponent, TProperty>> propertySelector, 
            TProperty value,
            bool shouldValueBeSet)
        {
            if (!shouldValueBeSet)
                return this;

            return this.WithValue(propertySelector, value);
        }
    }

    private void AddRequiredComponents<TComponent>() where TComponent : Component =>
        ComponentExtensions
            .GetRequiredComponents<TComponent>()
            .ToList()
            .ForEach(t =>
            {
                if (!_gameObject.HasComponent(t) && !t.IsAbstract)
                    _gameObject.AddComponent(t);
            });
}

public interface IGameObjectBuilderNonCreateable : IGameObjectBuilderNonBuildable
{
    GameObject Build();
}

public interface IGameObjectBuilderNonBuildable
{
    IGameObjectBuilderNonCreateable WithPosition(Vector3 position);

    IGameObjectBuilderNonCreateable WithRotation(Quaternion rotation);

    IGameObjectBuilderNonCreateable WithEnabled(bool isEnabled);

    IGameObjectBuilderNonCreateable WithEmptyChild(string name);

    IGameObjectBuilderNonCreateable WithChild(
        string name,
        Func<IGameObjectBuilderNonCreateable, GameObject> childBuilder);

    IGameObjectBuilderNonCreateable WithChildIf(
        string name, 
        bool shouldAddChild, 
        Func<IGameObjectBuilderNonCreateable, GameObject> childBuilder);

    IGameObjectBuilderNonCreateable ModifyImmediateChildByComponent<TComponent>(
        Action<IGameObjectBuilderNonCreateable> childBuilder)
        where TComponent : Component;

    IGameObjectBuilderComponentValueInitializer<TComponent> WithComponent<TComponent>() where TComponent : Component;

    IGameObjectBuilderNonCreateable WithComponent(Type componentType);

    IGameObjectBuilderComponentValueInitializer<TComponent> WithComponentIf<TComponent>(bool shouldAddComponent) where TComponent : Component;

    IGameObjectBuilderNonCreateable WithComponentIf(Type componentType, bool shouldAddComponent);

    IGameObjectBuilderComponentValueInitializer<TComponent> ModifyComponent<TComponent>() where TComponent : Component;
}

public interface IGameObjectBuilderComponentValueInitializer<TComponent> 
    : IGameObjectBuilderNonCreateable 
    where TComponent : Component
{
    IGameObjectBuilderComponentValueInitializer<TComponent> WithValue<TProperty>(
            Expression<Func<TComponent, TProperty>> propertySelector, 
            TProperty value);

    IGameObjectBuilderComponentValueInitializer<TComponent> WithValueIf<TProperty>(
        Expression<Func<TComponent, TProperty>> propertySelector,
        TProperty value,
        bool shouldValueBeSet);
}
