﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ITilemapMouseDataService
{
    Vector2Int GetHoveredTilePosition();
    Vector2 GetHoveredTileCenterWorldPosition();
    Vector2 GetMousePositionRelativeToTilemap();
}
