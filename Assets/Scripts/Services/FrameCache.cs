﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class FrameCache : IFrameCache
{
    private long LatestFrameCount { get; set; }

    private IDictionary<long, IDictionary<string, object>> _cache;

    public FrameCache()
    {
        _cache = new Dictionary<long, IDictionary<string, object>>();
    }

    public void Cache(string dataId, object data)
    {
        this.EnsureCacheIsUpdated();

        _cache[this.LatestFrameCount].Add(dataId, data);
    }

    public T GetCachedData<T>(string dataId) =>
        (T) _cache[Time.frameCount][dataId];

    public void TryGetCachedData<T>(string dataId, out T data) where T : struct
    {
        if (!this.IsCached(dataId))
        {
            data = default(T);

            return;
        }

        data = (T)_cache[Time.frameCount][dataId];
    }

    public bool IsCached(string dataId) =>
        _cache.ContainsKey(Time.frameCount)
            ? _cache[Time.frameCount].ContainsKey(dataId)
            : false;

    private void EnsureCacheIsUpdated()
    {
        if (this.LatestFrameCount == Time.frameCount)
            return;

        this.LatestFrameCount = Time.frameCount;
        _cache.Clear();
        _cache.Add(this.LatestFrameCount, new Dictionary<string, object>());
    }
}
