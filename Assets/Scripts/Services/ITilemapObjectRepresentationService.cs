﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ITilemapObjectRepresentationService
{
    bool TileNeedsObjectRepresentation(Tilemap tilemap, Tile tile, Vector3Int tilemapPosition);
    void InstantiateTilemapObjectRepresentationAt(Tilemap tilemap, Vector3Int tilemapPosition);
}
