﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IGameObjectBuilder : IGameObjectBuilderNonBuildable
{
    IGameObjectBuilderNonCreateable Create(string name);

    IGameObjectBuilderNonCreateable CreateFromPrefab(GameObject prefab);

    IGameObjectBuilderNonCreateable UseExistingGameObject(GameObject existingGameObject);
}
