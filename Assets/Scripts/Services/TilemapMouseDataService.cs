﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TilemapMouseDataService : ITilemapMouseDataService
{
    private readonly ITilemapPointService _tilemapPointService;

    public TilemapMouseDataService()
    {
        _tilemapPointService = new TilemapPointService();
    }

    public Vector2Int GetHoveredTilePosition()
    {
        var mouseWorldPosition = Camera.main.MouseWorldPosition();

        return _tilemapPointService.GetTilePositionAt(mouseWorldPosition);
    }

    public Vector2 GetHoveredTileCenterWorldPosition()
    {
        var hoveredTilePosition = this.GetHoveredTilePosition();

        return _tilemapPointService.GetTileCenterWorldPosition(hoveredTilePosition);
    }

    public Vector2 GetMousePositionRelativeToTilemap()
    {
        var mouseWorldPosition = Camera.main.MouseWorldPosition();
            
        return _tilemapPointService.GetPositionRelativeToTilemap(mouseWorldPosition);
    }
}
