﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TilemapObjectRepresentationService : ITilemapObjectRepresentationService
{
    private ITilemapTileFilterService _tilemapTileFilterService;
    private IGameObjectBuilder _gameObjectBuilder;
    private ITilemapPointService _tilemapPointService;

    public TilemapObjectRepresentationService()
    {
        _tilemapTileFilterService = new TilemapTileFilterService();
        _gameObjectBuilder = new GameObjectBuilder();
        _tilemapPointService = new TilemapPointService();
    }

    public bool TileNeedsObjectRepresentation(Tilemap tilemap, Tile tile, Vector3Int tilemapPosition)
    {
        if (!tile.Template.UseTilemapObjectRepresentation)
            return false;

        if (!tile.Template.UseTilemapObjectOnEdgeOnly)
            return true;

        var totalAdjacentTilesOfSameType = _tilemapTileFilterService
            .GetAdjacentTilesOfType(tilemap, tilemapPosition, tile.Type)
            .Count;

        // A tile whic is not surrounded on all sides by other tiles of the same type needs a tilemap object
        return totalAdjacentTilesOfSameType < 4;
    }

    public void InstantiateTilemapObjectRepresentationAt(Tilemap tilemap, Vector3Int tilePosition)
    {
        var tile = tilemap.GetTileAt(tilePosition);

        var worldPosition = _tilemapPointService.GetTileCenterWorldPosition(tilePosition.AsVector2Int());

        var tileRepresentationObject = _gameObjectBuilder
            .CreateFromPrefab(tile.Template.TilemapObjectPrefab)
            .WithPosition(worldPosition)
            .ModifyComponent<TileRepresentation>()
                .WithValue(x => x.Tile, tile)
            .Build();

        var tileRepresentation = tileRepresentationObject.GetComponent<TileRepresentation>();

        tile.TileRepresentation = tileRepresentation;
    }
}
