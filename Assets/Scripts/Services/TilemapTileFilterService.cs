﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TilemapTileFilterService : ITilemapTileFilterService
{
    public IList<Vector3Int> GetAdjacentTilesOfType(Tilemap tilemap, Vector3Int tilemapPosition, TileType tileType) =>
        tilemap.GetAdjacentPositionsWhere(
            tilemapPosition,
            AdjacencyType.CARDINAL,
            (tileCell, _, __, ___) =>
            {
                return tileCell.Tile != null && tileCell.Tile.Type == tileType;
            });
}
