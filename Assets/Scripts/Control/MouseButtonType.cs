﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum MouseButtonType
{
    LEFT = 0,
    RIGHT = 1,
    MIDDLE = 2,
}
