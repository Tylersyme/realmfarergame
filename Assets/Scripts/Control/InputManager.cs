﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    private static InputManager _instance;
    public static InputManager Instance
    {
        get
        {
            if (_instance == null)
                _instance = UnityEngine.Object.FindObjectOfType<InputManager>();

            return _instance;
        }
    }

    private InputBindingDefinition InputBindingDefinition { get; set; }

    // Tracks the last time that the mouse button was pressed
    private IDictionary<MouseButtonType, float> TimeWhenPressed { get; set; }

    private void Start()
    {
        this.InputBindingDefinition = new InputBindingDefinition();
        this.TimeWhenPressed = new Dictionary<MouseButtonType, float>();

        var playerInputBindingGroup = new InputBindingGroup(InputBindingGroupCategory.PLAYER);

        playerInputBindingGroup.AddInputBinding(InputAction.MOVE_NORTH, KeyCode.W, ActionActiveCondition.KEY_HELD_DOWN);
        playerInputBindingGroup.AddInputBinding(InputAction.MOVE_SOUTH, KeyCode.S, ActionActiveCondition.KEY_HELD_DOWN);
        playerInputBindingGroup.AddInputBinding(InputAction.MOVE_EAST, KeyCode.D, ActionActiveCondition.KEY_HELD_DOWN);
        playerInputBindingGroup.AddInputBinding(InputAction.MOVE_WEST, KeyCode.A, ActionActiveCondition.KEY_HELD_DOWN);
        playerInputBindingGroup.AddInputBinding(InputAction.DEFAULT_INTERACT_CLOSEST_OBJECT, KeyCode.E);
        playerInputBindingGroup.AddInputBinding(InputAction.TOGGLE_INVENTORY, KeyCode.Tab);

        playerInputBindingGroup.AddInputBinding(InputAction.CHANGE_SELECTED_SLOT, new KeyCode[] 
        {
            KeyCode.Alpha0,
            KeyCode.Alpha1,
            KeyCode.Alpha2,
            KeyCode.Alpha3,
            KeyCode.Alpha4,
            KeyCode.Alpha5,
            KeyCode.Alpha6,
            KeyCode.Alpha7,
            KeyCode.Alpha8,
            KeyCode.Alpha9,
        });

        playerInputBindingGroup.AddInputBinding(InputAction.DROP_SELECTED_ITEM, KeyCode.Q);
        playerInputBindingGroup.AddInputBinding(InputAction.ROTATE_SELECTED_ITEM, KeyCode.R);

        this.InputBindingDefinition.AddInputBindingGroup(playerInputBindingGroup);

        this.InputBindingDefinition.SetCurrentInputBindingGroup(InputBindingGroupCategory.PLAYER);
    }

    private void Update()
    {
        foreach (var mouseButtonType in Enum.GetValues(typeof(MouseButtonType)))
        {
            if (Input.GetMouseButtonDown((int)mouseButtonType))
            {
                if (!this.HasClicked((MouseButtonType)mouseButtonType))
                    this.MouseButtonClicked((MouseButtonType)mouseButtonType);
            }
            else
            {
                this.MouseButtonReleased((MouseButtonType)mouseButtonType);
            }
        }
    }

    public bool IsActionActive(InputAction inputAction) =>
        this.GetActionData(inputAction) != null;

    public InputActionData GetActionData(InputAction inputAction) =>
        this.InputBindingDefinition.GetActionData(inputAction);

    /// <summary>
    /// Whether the mouse was clicked on this frame.
    /// </summary>
    /// <param name="mouseButtonType"></param>
    /// <returns></returns>
    public bool HasClicked(MouseButtonType mouseButtonType) =>
        this.IsMouseButtonActive(mouseButtonType) &&
        this.TimeWhenPressed[mouseButtonType] == Time.time;

    public bool IsMouseButtonActive(MouseButtonType mouseButtonType) =>
        this.TimeWhenPressed.Keys.Contains(mouseButtonType);

    public void DisableAction(InputAction inputAction)
    {
        this.InputBindingDefinition.CurrentInputBindingGroup.DisableInputAction(inputAction);
    }

    public void DisableActions(IEnumerable<InputAction> inputActions)
    {
        foreach (var inputAction in inputActions)
            DisableAction(inputAction);
    }

    public void EnableAction(InputAction inputAction)
    {
        this.InputBindingDefinition.CurrentInputBindingGroup.EnableInputAction(inputAction);
    }

    public void EnableActions(IEnumerable<InputAction> inputActions)
    {
        foreach (var inputAction in inputActions)
            EnableAction(inputAction);
    }

    private void MouseButtonClicked(MouseButtonType mouseButtonType) =>
        this.TimeWhenPressed.Add(mouseButtonType, Time.time);

    private void MouseButtonReleased(MouseButtonType mouseButtonType) =>
        this.TimeWhenPressed.Remove(mouseButtonType);

    private void Awake()
    {
        _instance = this;
    }
}
