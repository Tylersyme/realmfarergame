﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputAction
{
    private static short InputActionIdIteration = 0;

    public static readonly InputAction MOVE_NORTH = new InputAction("Move Forward");
    public static readonly InputAction MOVE_SOUTH = new InputAction("Move Backward");
    public static readonly InputAction MOVE_EAST = new InputAction("Move Right");
    public static readonly InputAction MOVE_WEST = new InputAction("Move Left");
    public static readonly InputAction DEFAULT_INTERACT_CLOSEST_OBJECT = new InputAction("Default Interact Closest Object");
    public static readonly InputAction TOGGLE_INVENTORY = new InputAction("Open Inventory");
    public static readonly InputAction CHANGE_SELECTED_SLOT = new InputAction("Change Selected Slot");
    public static readonly InputAction DROP_SELECTED_ITEM = new InputAction("Drop Selected Item");
    public static readonly InputAction ROTATE_SELECTED_ITEM = new InputAction("Rotate Selected Item");

    public short Id { get; private set; }
    public string Name { get; private set; }

    private InputAction(string name)
    {
        this.Id = InputActionIdIteration++;
        this.Name = name;
    }
}
