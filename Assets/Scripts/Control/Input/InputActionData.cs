﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputActionData
{
    public KeyCode KeyPressed { get; set; }

    public InputActionData(KeyCode keyPressed)
    {
        this.KeyPressed = keyPressed;
    }
}
