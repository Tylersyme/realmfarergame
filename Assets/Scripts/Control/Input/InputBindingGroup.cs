﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Defines a map of individual input actions to any input bindings that can cause that action to occur.
/// </summary>
public class InputBindingGroup
{
    public InputBindingGroup(InputBindingGroupCategory inputBindingGroupCategory)
    {
        this.InputActionMap = new Dictionary<short, List<InputBinding>>();

        this.InputBindingGroupCategory = InputBindingGroupCategory;
    }

    /// <summary>
    /// Maps input bindings by the unique id of the input action they represents.
    /// This is done for fast reads.
    /// </summary>
    private IDictionary<short, List<InputBinding>> InputActionMap { get; set; }

    public InputBindingGroupCategory InputBindingGroupCategory { get; private set; }

    public List<InputBinding> GetInputBindings(InputAction inputAction)
    {
        return this.InputActionMap[inputAction.Id];
    }

    public void AddInputBinding(
            InputAction inputAction,
            KeyCode[] keyCodes,
            ActionActiveCondition actionActiveCondition = ActionActiveCondition.KEY_DOWN)
    {
        foreach (var keyCode in keyCodes)
            this.AddInputBinding(inputAction, keyCode, actionActiveCondition);
    }

    public void AddInputBinding(
        InputAction inputAction, 
        KeyCode keyCode, 
        ActionActiveCondition actionActiveCondition = ActionActiveCondition.KEY_DOWN)
    {
        var inputBinding = new InputBinding(inputAction, keyCode, actionActiveCondition);

        if (!this.InputActionMap.ContainsKey(inputBinding.InputAction.Id))
            this.InputActionMap.Add(inputBinding.InputAction.Id, new List<InputBinding>());

        this.InputActionMap[inputBinding.InputAction.Id].Add(inputBinding);
    }

    public void DisableInputAction(InputAction inputAction)
    {
        this.InputActionMap[inputAction.Id].ForEach((inputBinding) =>
        {
            inputBinding.IsEnabled = false;
        });
    }

    public void EnableInputAction(InputAction inputAction)
    {
        this.InputActionMap[inputAction.Id].ForEach((inputBinding) =>
        {
            inputBinding.IsEnabled = true;
        });
    }
}

public enum InputBindingGroupCategory
{
    PLAYER,
}
