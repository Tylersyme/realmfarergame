﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class InputBindingDefinition
{
    public InputBindingDefinition()
    {
        this.InputBindingGroups = new List<InputBindingGroup>();
    }

    private List<InputBindingGroup> InputBindingGroups { get; set; }

    public InputBindingGroup CurrentInputBindingGroup { get; set; }

    /// <summary>
    /// Determines which input binding group is currently active.
    /// </summary>
    public InputBindingGroupCategory CurrentInputBindingGroupCategory { get; private set; }

    public void AddInputBindingGroup(InputBindingGroup inputBindingGroup)
    {
        this.InputBindingGroups.Add(inputBindingGroup);
    }

    public void SetCurrentInputBindingGroup(InputBindingGroupCategory inputBindingGroupCategory)
    {
        this.CurrentInputBindingGroup = this.InputBindingGroups.First(i => i.InputBindingGroupCategory == inputBindingGroupCategory);
    }

    public InputActionData GetActionData(InputAction inputAction)
    {
        var inputBindings = this.CurrentInputBindingGroup.GetInputBindings(inputAction)
            .Where(i => i.IsEnabled);

        // Check each binding to see if the corresponding key is currently active
        foreach(var inputBinding in inputBindings)
        {
            if (inputBinding.ActionActiveCondition == ActionActiveCondition.KEY_DOWN)
            {
                if (!Input.GetKeyDown(inputBinding.KeyCode))
                    continue;
            }
            else if (inputBinding.ActionActiveCondition == ActionActiveCondition.KEY_HELD_DOWN)
            {
                if (!Input.GetKey(inputBinding.KeyCode))
                    continue;
            }
            else if (inputBinding.ActionActiveCondition == ActionActiveCondition.KEY_UP)
            {
                if (!Input.GetKeyUp(inputBinding.KeyCode))
                    continue;
            }
            else
            {
                throw new System.Exception($"Error getting active input action: The input action {inputAction.Name} has a binding with an unsupported action active condition {inputBinding.ActionActiveCondition}.");
            }

            return new InputActionData(inputBinding.KeyCode);
        }

        return null;
    }
}
