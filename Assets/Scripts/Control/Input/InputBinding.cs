﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputBinding
{
    public InputBinding(InputAction inputAction, KeyCode keyCode, ActionActiveCondition actionActiveType = ActionActiveCondition.KEY_DOWN)
    {
        this.InputAction = inputAction;
        this.KeyCode = keyCode;
        this.ActionActiveCondition = actionActiveType;
        this.IsEnabled = true;
    }

    public InputAction InputAction { get; set; }

    public KeyCode KeyCode { get; set; }

    /// <summary>
    /// Determines the condition when this input binding is considered active.
    /// </summary>
    public ActionActiveCondition ActionActiveCondition { get; set; }

    /// <summary>
    /// Whether this input binding can be active.
    /// </summary>
    public bool IsEnabled { get; set; }
}

public enum ActionActiveCondition
{
    // The input action will be considered active on the first frame when held down
    KEY_DOWN,
    // The input action will be considered active on the first frame when released
    KEY_UP,
    // The input action will be considered active when held down
    KEY_HELD_DOWN,
}
