﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class InputActionSubgroup
{
    public static readonly InputAction[] MOVEMENT = new InputAction[]
    {
        InputAction.MOVE_NORTH,
        InputAction.MOVE_SOUTH,
        InputAction.MOVE_EAST,
        InputAction.MOVE_WEST,
    };
}
