﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[System.Serializable]
public class Tilemap
{
    public Vector3Int BottomLeft { get => Vector3Int.zero; }
    public Vector3Int TopRight { get => new Vector3Int(this.Size.x - 1, this.Size.y - 1, this.Size.z - 1); }

    [SerializeField]
    private Cellmap<TileCell> _map;
    public Cellmap<TileCell> Map { get => _map; set => _map = value; }

    public Vector3Int Size { get => this.Map.Size; }

    private readonly ITileFactory _tileFactory;

    public Tilemap()
    {
        _tileFactory = new TileFactory();
    }

    public Tilemap(Vector2Int size)
        : this()
    {
        this.Map = new Cellmap<TileCell>(size.x, size.y, 2);
    }

    public Tile SetTile(TileType tileType, Vector3Int position)
    {
        var tile = _tileFactory.CreateFromType(tileType);

        this.Map.GetElementAt(position).Tile = tile;

        return tile;
    }

    public Tile GetTileAt(Vector3Int position)
    {
        return this.Map.GetElementAt(position).Tile;
    }

    public Tile GetTile(int x, int y, int z)
    {
        return GetTileAt(new Vector3Int(x, y, z));
    }

    public TileCell GetTileCell(Vector3Int position)
    {
        return this.Map.GetElementAt(position);
    }

    public TileCell GetTileCell(int x, int y, int z)
    {
        return GetTileCell(new Vector3Int(x, y, z));
    }

    public List<Tile> GetTiles(IEnumerable<Vector3Int> tilePositions)
    {
        return tilePositions
            .Select(p => this.GetTileAt(p))
            .ToList();
    }

    public bool HasTile(Vector3Int position)
    {
        return this.Map.GetElementAt(position).Tile != null;
    }

    public void AddDecorationTile(TileType tileType, Vector2Int position)
    {
        var decorationTile = _tileFactory.CreateFromType(tileType);

        this.GetDecorationTilesAt(position).Add(decorationTile);
    }

    public void AddDecorationTile(TileCreationDefinition tileCreationDefinition, Vector2Int position)
    {
        var decorationTile = _tileFactory.Create(tileCreationDefinition);

        this.GetDecorationTilesAt(position).Add(decorationTile);
    }

    public void RemoveDecorationTilesAt(int x, int y) =>
        this.RemoveDecorationTilesAt(new Vector2Int(x, y));

    public void RemoveDecorationTilesAt(Vector2Int position) =>
        this.GetDecorationTilesAt(position).Clear();

    public IList<Tile> GetDecorationTilesAt(int x, int y) =>
        this.GetDecorationTilesAt(new Vector2Int(x, y));

    public IList<Tile> GetDecorationTilesAt(Vector2Int position) =>
        this.Map.GetElementAt(position.AsVector3Int().SetZ(0)).DecorationTiles;

    /// <summary>
    /// Returns the positions adjacent to the given tile position.
    /// </summary>
    /// <param name="tilePosition"></param>
    /// <param name="includeOutOfBounds"></param>
    /// <returns></returns>
    public List<Vector3Int> GetAdjacentTileCellPositions(Vector3Int tilePosition, bool includeOutOfBounds = false)
    {
        var adjacentPositions = new List<Vector3Int>();
        foreach (var direction in Direction.XY_DIRECTIONS)
        {
            var adjacentPosition = tilePosition + direction.RelativeDirection.AsVector3Int();

            // Skip adjacent positions which are out of bounds
            if (!IsInBounds(adjacentPosition) && !includeOutOfBounds)
                continue;

            adjacentPositions.Add(adjacentPosition);
        }

        return adjacentPositions;
    }

    /// <summary>
    /// Returns directions leading out from the origin tile position towards adjacent tiles which meet the provided predicate.
    /// </summary>
    /// <param name="tilePosition"></param>
    /// <param name="adjacencyType"></param>
    /// <param name="predicate"></param>
    /// <param name="includeOutOfBounds"></param>
    /// <returns></returns>
    public List<Direction> GetAdjacentTileCellDirectionsWhere(
        Vector3Int tilePosition,
        AdjacencyType adjacencyType,
        Func<TileCell, int, int, int, bool> predicate,
        bool includeOutOfBounds = false)
    {
        var adjacentDirections = new List<Direction>();
        foreach (var direction in Direction.GetAdjacencyDirections(adjacencyType))
        {
            var adjacentPosition = tilePosition + direction.RelativeDirection.AsVector3Int();

            // Skip adjacent positions which are out of bounds
            if (!IsInBounds(adjacentPosition) && !includeOutOfBounds)
                continue;

            var tileCell = GetTileCell(adjacentPosition);
            if (!predicate(tileCell, adjacentPosition.x, adjacentPosition.y, adjacentPosition.z))
                continue;

            adjacentDirections.Add(direction);
        }

        return adjacentDirections;
    }

    public List<Vector3Int> GetAdjacentPositionsWhere(
        Vector3Int tilePosition, 
        AdjacencyType adjacencyType, 
        Func<TileCell, int, int, int, bool> predicate,
        bool includeOutOfBounds = false)
    {
        var adjecentPositions = new List<Vector3Int>();
        foreach (var direction in Direction.GetAdjacencyDirections(adjacencyType))
        {
            var adjacentPosition = tilePosition + direction.RelativeDirection.AsVector3Int();

            // Skip adjacent positions which are out of bounds
            if (!IsInBounds(adjacentPosition) && !includeOutOfBounds)
                continue;

            var tileCell = IsInBounds(adjacentPosition) ? GetTileCell(adjacentPosition) : null;
            if (!predicate(tileCell, adjacentPosition.x, adjacentPosition.y, adjacentPosition.z))
                continue;

            adjecentPositions.Add(adjacentPosition);
        }

        return adjecentPositions;
    }

    /// <summary>
    /// Gets all tiles along the z axis at the given x and y axes.
    /// </summary>
    /// <param name="position"></param>
    /// <returns></returns>
    public List<Tile> GetTilesAlongZ(Vector2Int position)
    {
        var tiles = new List<Tile>();

        for (var z = 0; z < this.Map.Depth; z++)
        {
            var tilePosition = new Vector3Int(position.x, position.y, z);
            if (!this.HasTile(tilePosition))
                break;

            var tile = GetTileAt(new Vector3Int(position.x, position.y, z));

            tiles.Add(tile);
        }

        return tiles;
    }

    public void Fill(TileType tileType, Vector3Int bottomLeftBound, Vector3Int size)
    {
        var topRightBound = bottomLeftBound + size.Change(-1);

        for (var x = bottomLeftBound.x; x < size.x; x++)
        {
            for (var y = bottomLeftBound.y; y < size.y; y++)
            {
                for (var z = bottomLeftBound.z; z < size.z; z++)
                {
                    SetTile(tileType, new Vector3Int(x, y, z));
                }
            }
        }
    }

    public void Fill2D(TileType tileType, Vector2Int bottomLeft, Vector2Int size, int depth)
    {
        Fill(tileType, bottomLeft.AsVector3Int().SetZ(depth), size.AsVector3Int().SetZ(depth + 1));
    }

    public void ForEachTileCell(Action<TileCell, int, int, int> action)
    {
        for (var x = 0; x < this.Size.x; x++)
        {
            for (var y = 0; y < this.Size.y; y++)
            {
                for (var z = 0; z < this.Size.z; z++)
                {
                    action.Invoke(this.Map.GetElementAt(new Vector3Int(x, y, z)), x, y, z);
                }
            }
        }
    }

    public void ForEachTile(Action<Tile, int, int, int> action)
    {
        ForEachTile(this.BottomLeft, this.Size, action);
    }

    public void ForEachTile(Vector3Int bottomLeftBound, Vector3Int size, System.Action<Tile, int, int, int> action)
    {
        IsInBoundsOrThrow(bottomLeftBound, "Error looping over tilemap: The bottom left bound {0} is out of bounds.");
        IsInBoundsOrThrow(
            bottomLeftBound + (size.ChangeX(-1).ChangeY(-1)), 
            "Error looping over tilemap: The top right bound {0} is out of bounds.");

        for (var x = bottomLeftBound.x; x < size.x; x++)
        {
            for (var y = bottomLeftBound.y; y < size.y; y++)
            {
                for (var z = bottomLeftBound.z; z < size.z; z++)
                {
                    var tile = GetTile(x, y, z);

                    if (tile == null)
                        continue;

                    action.Invoke(tile, x, y, z);
                }
            }
        }
    }

    public List<Vector3Int> PositionsWhere(Func<Tile, int, int, int, bool> predicate)
    {
        var positions = new List<Vector3Int>();

        ForEachTile((tile, x, y, z) =>
        {
            if (predicate.Invoke(tile, x, y, z))
                positions.Add(new Vector3Int(x, y, z));
        });

        return positions;
    }

    public List<Vector3Int> PositionsWhere(
        Vector3Int bottomLeftBound,
        Vector3Int size, 
        Func<Tile, int, int, int, bool> predicate)
    {
        var positions = new List<Vector3Int>();

        ForEachTile(bottomLeftBound, size, (tile, x, y, z) =>
        {
            if (predicate.Invoke(tile, x, y, z))
                positions.Add(new Vector3Int(x, y, z));
        });

        return positions;
    }

    /// <summary>
    /// Returns whether the given position is within the bounds of the map.
    /// Note: There is no bound to the Z axis
    /// </summary>
    /// <param name="position"></param>
    /// <returns></returns>
    public bool IsInBounds(Vector3Int position)
    {
        return MathExtended.IsBetweenInclusive(position.x, 0, this.Size.x - 1) &&
               MathExtended.IsBetweenInclusive(position.y, 0, this.Size.y - 1) &&
               position.z >= 0;
    }

    public void IsInBoundsOrThrow(Vector3Int position, string errorMessageFormat)
    {
        if (!IsInBounds(position))
            throw new System.ArgumentOutOfRangeException(string.Format(errorMessageFormat, position));
    }
}
