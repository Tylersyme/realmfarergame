﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Seed
{
    [SerializeField]
    private string _originalValue;
    public string OriginalValue { get => _originalValue; set => _originalValue = value; }

    private int HashedValue
    {
        get => Mathf.Abs(this.OriginalValue.GetHashCode());
    }

    private System.Random _randomGenerator;
    public System.Random RandomGenerator
    {
        get
        {
            if (_randomGenerator == null)
                _randomGenerator = new System.Random(this.HashedValue);

            return _randomGenerator;
        }
    }

    // Used for serialization
    public Seed() { }

    public Seed(string value)
    {
        this.OriginalValue = value;
    }

    public int NextInteger()
    {
        return this.RandomGenerator.Next(int.MaxValue);
    }

    public int NextInteger(int max)
    {
        return this.RandomGenerator.Next(max);
    }
}
