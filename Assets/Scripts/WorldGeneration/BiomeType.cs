﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BiomeType
{
    public static readonly BiomeType TUNDRA = new BiomeType(
        "Tundra", 
        ColorExtensions.CreateColorNotNormalized(218, 226, 232));

    public static readonly BiomeType TAIGA = new BiomeType(
        "Taiga", 
        ColorExtensions.CreateColorNotNormalized(79, 135, 97));

    public static readonly BiomeType TEMPERATE_FOREST = new BiomeType(
        "Temperate Forest", 
        ColorExtensions.CreateColorNotNormalized(49, 102, 38));

    public static readonly BiomeType DESERT = new BiomeType(
        "Desert", 
        ColorExtensions.CreateColorNotNormalized(170, 135, 52));

    public static readonly BiomeType LAKE = new BiomeType(
        "Lake", 
        ColorExtensions.CreateColorNotNormalized(27, 62, 119));

    public static readonly BiomeType FROZEN_LAKE = new BiomeType(
        "Frozen Lake", 
        ColorExtensions.CreateColorNotNormalized(189, 209, 255));

    public string Name { get; private set; }
    public Color IdentifyingColor { get; private set; }
    private BiomeType(string name, Color identifyingColor)
    {
        this.Name = name;
        this.IdentifyingColor = identifyingColor;
    }
}
