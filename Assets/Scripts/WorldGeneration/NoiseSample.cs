﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class NoiseSample
{
    [SerializeField]
    private float _value;
    public float Value
    {
        get => _value;
        set => _value = value;
    }

    public NoiseSample() { }

    public NoiseSample(float value)
    {
        this.Value = value;
    }

    public static NoiseSample operator +(NoiseSample lhs, NoiseSample rhs)
    {
        return new NoiseSample(lhs.Value + rhs.Value);
    }

    public static NoiseSample operator *(NoiseSample lhs, NoiseSample rhs)
    {
        return new NoiseSample(lhs.Value * rhs.Value);
    }

    public static NoiseSample operator *(NoiseSample lhs, float rhs)
    {
        return new NoiseSample(lhs.Value * rhs);
    }

    public static bool operator <(NoiseSample lhs, NoiseSample rhs)
    {
        return lhs.Value < rhs.Value;
    }

    public static bool operator >(NoiseSample lhs, NoiseSample rhs)
    {
        return lhs.Value > rhs.Value;
    }

    public static bool operator <(NoiseSample lhs, float rhs)
    {
        return lhs.Value < rhs;
    }

    public static bool operator >(NoiseSample lhs, float rhs)
    {
        return lhs.Value > rhs;
    }
}
