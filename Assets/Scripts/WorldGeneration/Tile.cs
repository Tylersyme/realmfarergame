﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Represents a tile within a tilemap. Most data about a tile is stored within its tile type
/// but any other data specific to a tile will be stored in the tile separately.
/// </summary>
[System.Serializable]
public class Tile
{
    /// <summary>
    /// Stores the dynamic material which will be rendered atop the tile as a visual decoration.
    /// </summary>
    [SerializeField]
    protected DynamicMaterialTexture _dynamicMaterialTexture;
    public DynamicMaterialTexture DynamicMaterialTexture { get => _dynamicMaterialTexture; set => _dynamicMaterialTexture = value; }

    public virtual TileTemplate Template { get; private set; }

    public int TextureIndex { get; set; } = 0;

    public TileType Type { get => this.Template.TileType; }

    public TileRepresentation TileRepresentation { get; set; }

    public Tile()
    {
    }

    public Tile(TileTemplate tileTemplate)
    {
        this.Template = tileTemplate;
    }

    public void UpdateDynamicTexture(Texture2D texture) =>
        this.DynamicMaterialTexture.Texture = texture;
}
