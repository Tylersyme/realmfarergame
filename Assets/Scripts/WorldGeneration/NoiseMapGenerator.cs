﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoiseMapGenerator
{
    public NoiseMap GenerateNoiseMap(FastNoise noiseGenerator, int width, int height, int offsetX = 0, int offsetY = 0)
    {
        var minValue = float.MaxValue;
        var maxValue = float.MinValue;

        var noiseSamples = new List<List<NoiseSample>>();
        for (var x = offsetX; x < width + offsetX; x++)
        {
            noiseSamples.Add(new List<NoiseSample>());
            for (var y = offsetY; y < height + offsetY; y++)
            {
                var value = noiseGenerator.GetNoise(x, y);

                if (value < minValue)
                    minValue = value;
                if (value > maxValue)
                    maxValue = value;

                noiseSamples[x].Add(new NoiseSample(value));
            }
        }

        var noiseMap = new NoiseMap(noiseSamples, width, height, minValue, maxValue);

        return noiseMap;
    }
}
