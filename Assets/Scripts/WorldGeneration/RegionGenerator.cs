﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class RegionGenerator : IRegionGenerator
{
    public static Region CreatedUnloadedRegion(Biome biome, float height, float temperature, float precipitation, Vector2Int size)
    {
        var region = new Region(
            biome,
            height,
            temperature,
            precipitation,
            size);

        return region;
    }

    protected readonly NoiseMapGenerator _noiseMapGenerator;
    protected readonly INoiseService _noiseService;
    protected readonly ITilemapObjectRepresentationService _tilemapObjectRepresentationService;
    protected readonly IDistributionPlacementService _distributionPlacementService;

    /// <summary>
    /// Stores the conditions necessary for the each decoration tile type to be placed on the tilemap.
    /// </summary>
    private IDictionary<TileCreationDefinition, Predicate<Tile>> DecorationTilePlacementConditions { get; set; }

    protected RegionGenerator()
    {
        _noiseMapGenerator = new NoiseMapGenerator();
        _noiseService = new NoiseService();
        _tilemapObjectRepresentationService = new TilemapObjectRepresentationService();
        _distributionPlacementService = new DistributionPlacementService();

        this.DecorationTilePlacementConditions = new Dictionary<TileCreationDefinition, Predicate<Tile>>();
    }

    /// <summary>
    /// The region currently being constructed
    /// </summary>
    protected Tilemap Tilemap { get; set; }

    public virtual Region GenerateRegion(Region unloadedRegion, RegionParameters regionParameters)
    {
        if (unloadedRegion.Tilemap != null)
            throw new System.Exception("Error generating unloaded region: The provided region's tilemap must be null.");

        // Tilemap generation pipeline
        var generatedTilemap = GenerateStartingTilemap(unloadedRegion, regionParameters);
        this.AddTiles(generatedTilemap, regionParameters);
        this.AddTileObjectRepresentations(generatedTilemap, regionParameters);
        this.AddDecorationTiles(generatedTilemap, regionParameters);
        this.AddTilemapObjects(generatedTilemap, regionParameters);
        this.AddItems(generatedTilemap, regionParameters);

        // Load reagion's tilemap
        unloadedRegion.Tilemap = generatedTilemap;

        // Tilemap size and region size should be equivalent
        if (!unloadedRegion.Tilemap.Size.AsVector2Int().IsEquivalent(unloadedRegion.Size))
            throw new System.Exception($"Error generating unloaded region: The region size {unloadedRegion.Size} is not the same as tilemap size {unloadedRegion.Tilemap.Size}.");

        return unloadedRegion;
    }

    protected virtual Tilemap GenerateStartingTilemap(Region region, RegionParameters regionParameters)
    {
        regionParameters.HeightMapParameters.Seed = region.Seed;

        return new Tilemap(region.Size);
    }

    protected virtual void AddTiles(Tilemap tilemap, RegionParameters regionParameters)
    {
        var regionGenerationParameters = regionParameters.RegionGenerationParameters;

       tilemap.Fill2D(regionGenerationParameters.BaseTileType, tilemap.BottomLeft.AsVector2Int(), tilemap.Size.AsVector2Int(), 0);

        var heightMapParameters = regionParameters.HeightMapParameters;

        var noiseGenerator = _noiseService.BuildFromParameters(heightMapParameters);

        var localHeightMap = _noiseMapGenerator.GenerateNoiseMap(
            noiseGenerator, 
            heightMapParameters.Dimensions.x, 
            heightMapParameters.Dimensions.y)
            .Normalize();

        var adjustedHeight = GetAdjustedHeight(regionParameters.Height, regionParameters.RegionGenerationParameters);
        localHeightMap.ForEach((sample, x, y) =>
        {
            if (sample > 1 - adjustedHeight)
                tilemap.SetTile(regionGenerationParameters.MountainTileType, new Vector3Int(x, y, 1));

            if (sample < adjustedHeight * .5f)
                tilemap.SetTile(regionGenerationParameters.WaterTileType, new Vector3Int(x, y, 0));
        });
    }

    protected virtual void AddTileObjectRepresentations(Tilemap tilemap, RegionParameters regionParameters)
    {
        tilemap.ForEachTile((tile, x, y, z) =>
        {
            var tilemapPosition = new Vector3Int(x, y, z);

            if (!_tilemapObjectRepresentationService.TileNeedsObjectRepresentation(tilemap, tile, tilemapPosition))
                return;

            _tilemapObjectRepresentationService.InstantiateTilemapObjectRepresentationAt(tilemap, tilemapPosition);
        });
    }

    protected virtual void AddDecorationTiles(Tilemap tilemap, RegionParameters regionParameters) =>
        _distributionPlacementService.PlaceDecorationTiles(
            tilemap,
            regionParameters.Seed,
            regionParameters.RegionGenerationParameters.DecorationDistributions);

    protected virtual void AddTilemapObjects(Tilemap tilemap, RegionParameters regionParameters) =>
        _distributionPlacementService.PlaceTilemapObjects(
            tilemap,
            regionParameters.Seed,
            regionParameters.RegionGenerationParameters.ObjectDistributions);

    protected virtual void AddItems(Tilemap tilemap, RegionParameters regionParameters) =>
        _distributionPlacementService.PlaceItems(
            tilemap,
            regionParameters.Seed,
            regionParameters.RegionGenerationParameters.ItemDistributions);

    protected float GetAdjustedHeight(float regionStartingHeight, RegionGenerationParameters regionGenerationParameters)
    {
        return Mathf.Clamp(
            regionStartingHeight * regionGenerationParameters.HeightModifier, 
            regionGenerationParameters.HeightClamp.x, 
            regionGenerationParameters.HeightClamp.y);
    }

    protected void AddDecorationTileCondition(TileCreationDefinition tileCreationDefinition, Predicate<Tile> condition) =>
        this.DecorationTilePlacementConditions.Add(tileCreationDefinition, condition);
}
