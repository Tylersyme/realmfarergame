﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldParameters
{
    public Seed Seed { get; set; }
    public Vector2Int WorldSize { get; set; }
    public Vector2Int RegionSize { get; set; }
    public Climate Climate { get; set; }

    public NoiseMapParameters HeightParameters { get; set; }
    public NoiseMapParameters TemperatureParameters { get; set; }
    public NoiseMapParameters PrecipitationParameters { get; set; }

    public WorldParameters()
    {
        this.Climate = new BalancedClimate();
    }
}
