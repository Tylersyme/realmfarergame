﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This is responsible for designing the world parameters, such as average precipitation.
/// A world generator is used to generate the world given the parameters produced by a world builder.
/// </summary>
public interface IWorldBuilder
{
    IWorldBuilder NewWorld(Seed seed, int width, int height, int regionWidth, int regionHeight);
    IWorldBuilder WithHeightParameters(NoiseMapParameters heightMapParameters);
    IWorldBuilder WithTemperatureParameters(NoiseMapParameters temperatureMapParameters);
    IWorldBuilder WithPrecipitationParameters(NoiseMapParameters precipitationMapParameters);

    WorldParameters Build();
}
