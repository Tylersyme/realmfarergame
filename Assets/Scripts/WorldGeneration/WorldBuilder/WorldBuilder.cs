﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldBuilder : IWorldBuilder
{
    private WorldParameters WorldParameters { get; set; }

    public IWorldBuilder NewWorld(Seed seed, int width, int height, int regionWidth, int regionHeight)
    {
        this.WorldParameters = new WorldParameters
        {
            Seed = seed,
            WorldSize = new Vector2Int(width, height),
            RegionSize = new Vector2Int(regionWidth, regionHeight),
        };

        return this;
    }

    public IWorldBuilder WithClimate(Climate climate)
    {
        this.WorldParameters.Climate = climate;

        return this;
    }

    public IWorldBuilder WithHeightParameters(NoiseMapParameters heightMapParameters)
    {
        this.WorldParameters.HeightParameters = heightMapParameters;
        this.WorldParameters.HeightParameters.Seed = this.WorldParameters.Seed;
        this.WorldParameters.HeightParameters.Dimensions = this.WorldParameters.WorldSize;

        return this;
    }

    public IWorldBuilder WithTemperatureParameters(NoiseMapParameters temperatureMapParameters)
    {
        this.WorldParameters.TemperatureParameters = temperatureMapParameters;
        this.WorldParameters.TemperatureParameters.Seed = this.WorldParameters.Seed;
        this.WorldParameters.TemperatureParameters.Dimensions = this.WorldParameters.WorldSize;

        return this;
    }

    public IWorldBuilder WithPrecipitationParameters(NoiseMapParameters precipitationMapParameters)
    {
        this.WorldParameters.PrecipitationParameters = precipitationMapParameters;
        this.WorldParameters.PrecipitationParameters.Seed = this.WorldParameters.Seed;
        this.WorldParameters.PrecipitationParameters.Dimensions = this.WorldParameters.WorldSize;

        return this;
    }

    public WorldParameters Build()
    {
        return this.WorldParameters;
    }
}
