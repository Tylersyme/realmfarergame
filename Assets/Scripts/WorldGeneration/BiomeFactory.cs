﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BiomeFactory : IBiomeFactory
{
    public Biome Create(BiomeType biomeType)
    {
        if (biomeType == BiomeType.TUNDRA)
            return new Tundra();
        else if (biomeType == BiomeType.DESERT)
            return new Desert();
        else if (biomeType == BiomeType.TAIGA)
            return new Taiga();
        else if (biomeType == BiomeType.TEMPERATE_FOREST)
            return new TemperateForest();
        else if (biomeType == BiomeType.LAKE)
            return new Lake();
        else if (biomeType == BiomeType.FROZEN_LAKE)
            return new FrozenLake();

        throw new System.Exception($"Biome creation error: The biome type \"{biomeType.Name}\" has not been defined.");
    }
}
