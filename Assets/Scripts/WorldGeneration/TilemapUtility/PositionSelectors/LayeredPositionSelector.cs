﻿using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[System.Serializable]
public class LayeredPositionSelector : PositionSelector, ILayeredPositionSelector
{
    [SerializeField]
    [ListDrawerSettings(CustomAddFunction = nameof(PositionSelectorLayerAdd), ListElementLabelName = nameof(PositionSelectorLayer.LabelName))]
    private List<PositionSelectorLayer> _positionSelectorLayers = new List<PositionSelectorLayer>();
    public List<PositionSelectorLayer> PositionSelectorLayers { get => _positionSelectorLayers; set => _positionSelectorLayers = value; }

    [System.Serializable]
    public class PositionSelectorLayer
    {
        [SerializeField]
        private IPositionSelector _positionSelector;
        public IPositionSelector PositionSelector { get => _positionSelector; set => _positionSelector = value; }

        [SerializeField]
        [Tooltip("Whether this position selector will add to the selection or subtract from it.")]
        private bool _isAdditive = true;
        public bool IsAdditive { get => _isAdditive; set => _isAdditive = value; }

        #region OdinInspector

        public string LabelName { get => this.PositionSelector?.GetType()?.Name ?? string.Empty; }

        #endregion
    }

    public override List<Vector3Int> Select(
        Tilemap tilemap, 
        Seed seed, 
        Vector3Int bottomLeftBound, 
        Vector3Int size, 
        Predicate<Tile> isSelectable)
    {
        var selectedPositions = new List<Vector3Int>();

        foreach (var positionSelectorLayer in this.PositionSelectorLayers.OrderByDescending(psl => psl.IsAdditive))
        {
            var positions = positionSelectorLayer.PositionSelector.Select(tilemap, seed, bottomLeftBound, size, isSelectable);

            if (positionSelectorLayer.IsAdditive)
                selectedPositions.AddRange(positions);
            else
                selectedPositions = selectedPositions
                    .Except(positions, new Vector3IntComparer())
                    .ToList();
        }

        return selectedPositions;
    }

    #region OdinInspector

    private void PositionSelectorLayerAdd()
    {
        this.PositionSelectorLayers.Add(new PositionSelectorLayer());
    }

    #endregion
}
