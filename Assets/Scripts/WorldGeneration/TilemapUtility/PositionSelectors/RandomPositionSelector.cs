﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[System.Serializable]
public class RandomPositionSelector : PositionSelector
{
    [SerializeField]
    [Range(0.0f, 1.0f)]
    private float _selectionChance;
    public float SelectionChance { get => _selectionChance; set => _selectionChance = value; }

    public override List<Vector3Int> Select(
        Tilemap tilemap,
        Seed seed,
        Vector3Int bottomLeftBound, 
        Vector3Int size, 
        Predicate<Tile> isSelectable)
    {
        var positions = tilemap.PositionsWhere((tile, x, y, _) =>
        {
            UnityEngine.Random.InitState(seed.NextInteger());

            return UnityEngine.Random.value > (1 - this.SelectionChance) &&
                   tilemap
                       .GetTilesAlongZ(new Vector2Int(x, y))
                       .All(t => isSelectable(t));
        });

        return positions;
    }
}
