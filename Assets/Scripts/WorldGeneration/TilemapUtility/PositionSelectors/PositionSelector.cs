﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public abstract class PositionSelector : IPositionSelector
{
    public abstract List<Vector3Int> Select(
        Tilemap tilemap, 
        Seed seed,
        Vector3Int bottomLeftBound, 
        Vector3Int size, 
        Predicate<Tile> isSelectable);
}
