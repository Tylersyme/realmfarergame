﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPositionSelector
{
    List<Vector3Int> Select(
        Tilemap tilemap, 
        Seed seed, 
        Vector3Int bottomLeftBound, 
        Vector3Int size, 
        Predicate<Tile> isSelectable);
}
