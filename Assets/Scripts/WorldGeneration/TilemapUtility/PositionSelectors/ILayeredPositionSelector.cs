﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Defines position selectors which are applied atop one another to produce a more complex selection.
/// </summary>
public interface ILayeredPositionSelector
{
}
