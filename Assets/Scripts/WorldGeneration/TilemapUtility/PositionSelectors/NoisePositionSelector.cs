﻿using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[System.Serializable]
public class NoisePositionSelector : PositionSelector
{
    [SerializeField]
    [Range(0.0f, 1.0f)]
    [OnValueChanged(nameof(EnsureMinLessThanOrEqualMax))]
    private float _minNormalizedValue;
    public float MinNormalizedValue { get => _minNormalizedValue; set => _minNormalizedValue = value; }

    [SerializeField]
    [Range(0.0f, 1.0f)]
    [OnValueChanged(nameof(EnsureMinLessThanOrEqualMax))]
    private float _maxNormalizedValue;
    public float MaxNormalizedValue { get => _maxNormalizedValue; set => _maxNormalizedValue = value; }

    [SerializeField]
    private NoiseGeneratorParameters _noiseGeneratorParameters = NoiseGeneratorParameters.DefaultSetup;
    public NoiseGeneratorParameters NoiseGeneratorParameters { get => _noiseGeneratorParameters; set => _noiseGeneratorParameters = value; }

    private INoiseService _noiseService;
    private INoiseService NoiseService
    {
        // This is necessary so that odin can serialize without having to manually populate this field
        get
        {
            if (_noiseService == null)
                _noiseService = new NoiseService();

            return _noiseService;
        }
    }

    private NoiseMapGenerator _noiseMapGenerator;
    private NoiseMapGenerator NoiseMapGenerator
    {
        // This is necessary so that odin can serialize without having to manually populate this field
        get
        {
            if (_noiseMapGenerator == null)
                _noiseMapGenerator = new NoiseMapGenerator();

            return _noiseMapGenerator;
        }
    }

    public override List<Vector3Int> Select(
        Tilemap tilemap,
        Seed seed,
        Vector3Int bottomLeftBound, 
        Vector3Int size, 
        Predicate<Tile> isSelectable)
    {
        this.NoiseGeneratorParameters.Seed = seed;

        var noiseGenerator = NoiseService.BuildFromParameters(this.NoiseGeneratorParameters);

        var noiseMap = NoiseMapGenerator
            .GenerateNoiseMap(noiseGenerator, size.x, size.y, bottomLeftBound.x, bottomLeftBound.y)
            .Normalize();

        return noiseMap.PositionsWhere((noiseSample, x, y) =>
        {
            return IsNoiseSampleInThreshold(noiseSample) &&
                   tilemap
                       .GetTilesAlongZ(new Vector2Int(x, y))
                       .All(new Func<Tile, bool>(isSelectable));
        })
        .Select(v => v.AsVector3Int().SetZ(bottomLeftBound.z))
        .ToList();
    }

    private bool IsNoiseSampleInThreshold(NoiseSample noiseSample)
    {
        return noiseSample.Value >= this.MinNormalizedValue &&
               noiseSample.Value <= this.MaxNormalizedValue;
    }

    #region OdinInspector

    private void EnsureMinLessThanOrEqualMax()
    {
        if (this.MinNormalizedValue <= this.MaxNormalizedValue)
            return;

        this.MinNormalizedValue = this.MaxNormalizedValue;
    }

    #endregion
}
