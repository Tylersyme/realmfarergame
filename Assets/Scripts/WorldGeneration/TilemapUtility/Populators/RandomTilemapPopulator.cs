﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[System.Serializable]
public class RandomTilemapPopulator<T> : TilemapPopulator<T>
{
    [SerializeField]
    private RandomPool<T> _placeables;
    public RandomPool<T> Placeables { get => _placeables; set => _placeables = value; }

    public RandomTilemapPopulator()
    {
        this.Placeables = new RandomPool<T>();
    }

    public void AddPlaceable(T placeable, float weight)
    {
        this.Placeables.AddElement(placeable, weight);
    }

    public override IDictionary<Vector3Int, T> Populate(
        Tilemap tilemap, 
        List<Vector3Int> positions, 
        IDictionary<T, Predicate<Tile>> populationConditions)
    {
        var populated = new Dictionary<Vector3Int, T>();

        foreach (var position in positions)
        {
            var randomElement = default(T);

            var foundElement = TryGetRandomElement(tilemap, position, populationConditions, out randomElement);

            if (!foundElement)
                continue;

            populated.Add(position, randomElement);
        }

        return populated;
    }

    /// <summary>
    /// Will select a random element to populate the position. If an element fails its condition it will be
    /// removed from consideration and a new random element from the pool will be selected.
    /// </summary>
    /// <param name="tilemap"></param>
    /// <param name="position"></param>
    /// <param name="populationConditions"></param>
    /// <param name="selectedElement"></param>
    /// <returns></returns>
    private bool TryGetRandomElement(
        Tilemap tilemap, 
        Vector3Int position, 
        IDictionary<T, Predicate<Tile>> populationConditions, 
        out T selectedElement)
    {
        var placeables = this.Placeables;

        while (!placeables.IsEmpty())
        {
            var randomElement = placeables.GetElement();

            var populationCondition = GetPopulationCondition(randomElement, populationConditions);
            var canPopulatePosition = populationCondition == null ||
                                      tilemap.GetTilesAlongZ(position.AsVector2Int()).All(t => populationCondition.Invoke(t));

            if (!canPopulatePosition)
            {
                placeables = placeables.Exclude(randomElement);

                continue;
            }

            selectedElement = randomElement;

            return true;
        }

        selectedElement = default(T);

        return false;
    }
}
