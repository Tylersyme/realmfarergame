﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ITilemapPopulator<T>
{
    IDictionary<Vector3Int, T> Populate(
        Tilemap tilemap,
        List<Vector3Int> positions, 
        IDictionary<T, Predicate<Tile>> populationConditions);
}
