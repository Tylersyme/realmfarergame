﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[System.Serializable]
public class SingularTilemapPopulator<T> : TilemapPopulator<T>
{
    [SerializeField]
    private T _element;
    public T Element { get => _element; set => _element = value; }

    public override IDictionary<Vector3Int, T> Populate(
        Tilemap tilemap,
        List<Vector3Int> positions, 
        IDictionary<T, Predicate<Tile>> populationConditions)
    {
        var populated = new Dictionary<Vector3Int, T>();

        var elementPopulationCondition = GetPopulationCondition(this.Element, populationConditions);

        foreach (var position in positions)
        {
            var canPopulatePosition = 
                elementPopulationCondition == null ||
                tilemap.GetTilesAlongZ(position.AsVector2Int()).All(t => elementPopulationCondition.Invoke(t));

            if (!canPopulatePosition)
                continue;

            populated.Add(position, this.Element);
        }

        return populated;
    }
}
