﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public abstract class TilemapPopulator<T> : ITilemapPopulator<T>
{
    public abstract IDictionary<Vector3Int, T> Populate(
        Tilemap tilemap,
        List<Vector3Int> positions, 
        IDictionary<T, Predicate<Tile>> populationConditions);

    protected Predicate<Tile> GetPopulationCondition(T element, IDictionary<T, Predicate<Tile>> populationConditions)
    {
        var populationCondition = default(Predicate<Tile>);

        populationConditions.TryGetValue(element, out populationCondition);

        return populationCondition;
    }
}
