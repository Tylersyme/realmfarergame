﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RegionParameters
{
    public World World { get; set; }
    public Seed Seed { get; set; }
    public Biome Biome { get; set; }
    public float Height { get; set; }
    public float Temperature { get; set; }
    public float Precipitation { get; set; }

    public NoiseMapParameters HeightMapParameters { get; set; }

    /// <summary>
    /// Contains all info regarding how various elements of the region will be generated based upon the biome of this region.
    /// </summary>
    public RegionGenerationParameters RegionGenerationParameters { get; set; }
}
