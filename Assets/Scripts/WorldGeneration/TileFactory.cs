﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileFactory : ITileFactory
{
    public Tile CreateFromType(TileType tileType)
    {
        var tile = new Tile(MappingDatabase.Instance.TileMappings.TileTemplates[tileType]);
        this.SetDynamicMaterialTexture(tile);

        return tile;
    }

    public Tile Create(TileCreationDefinition tileCreationDefinition)
    {
        var tile = this.CreateFromType(tileCreationDefinition.Template.TileType);

        if (tile.Template.RenderData.HasVarietyTextures)
        {
            var textureIndex = tileCreationDefinition.RandomizeVarietyTextureIndex
                ? Random.Range(0, tile.Template.RenderData.VarietyTextureGroup.Textures.Count)
                : tileCreationDefinition.VarietyTextureIndex;

            tile.UpdateDynamicTexture(tile.Template.RenderData.VarietyTextureGroup.GetTextureByIndex(textureIndex));

            tile.TextureIndex = tileCreationDefinition.VarietyTextureIndex;
        }

        return tile;
    }

    private void SetDynamicMaterialTexture(Tile tile)
    {
        tile.DynamicMaterialTexture = new DynamicMaterialTexture
        {
            MaterialRecord = tile.Template.RenderData.BaseMaterial.DatabaseRecord,
            Texture = tile.Template.RenderData.BaseTexture,
        };
    }
}
