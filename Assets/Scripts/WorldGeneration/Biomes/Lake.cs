﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lake : Biome
{
    public Lake() 
        : base(BiomeType.LAKE)
    {
    }
}
