﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Taiga : Biome
{
    public Taiga()
        : base(BiomeType.TAIGA)
    {
    }
}
