﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TemperateForest : Biome
{
    public TemperateForest() 
        : base(BiomeType.TEMPERATE_FOREST)
    {
    }
}
