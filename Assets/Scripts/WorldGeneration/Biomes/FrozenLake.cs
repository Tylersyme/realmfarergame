﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FrozenLake : Biome
{
    public FrozenLake() 
        : base(BiomeType.FROZEN_LAKE)
    {
    }
}
