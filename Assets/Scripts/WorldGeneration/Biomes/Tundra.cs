﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tundra : Biome
{
    public Tundra()
        : base(BiomeType.TUNDRA)
    {
    }
}
