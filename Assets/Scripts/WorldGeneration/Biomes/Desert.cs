﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Desert : Biome
{
    public Desert() 
        : base(BiomeType.DESERT)
    {
    }
}
