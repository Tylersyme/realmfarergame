﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDistributionPlacementService
{
    void PlaceTilemapObjects(Tilemap tilemap, Seed seed, List<DistributionParameters<GameObject>> objectDistributions);

    void PlaceDecorationTiles(Tilemap tilemap, Seed seed, List<DistributionParameters<TileCreationDefinition>> objectDistributions);

    void PlaceItems(Tilemap tilemap, Seed seed, List<DistributionParameters<ItemTemplate>> objectDistributions);
}
