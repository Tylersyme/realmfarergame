﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ITileBitmaskService
{
    Texture2D GetBitmaskTextureFromTile(Tile tile, Vector2Int tilePosition);

    Texture2D GetBitmaskTextureFromDecorationTile(Tile decorationTile, Vector2Int tilePosition);

    Texture2D GetBitmaskTextureFromTileType(
        TileType tileType,
        Vector2Int tilePosition,
        BitmaskingTextureGroup bitmaskingTextureGroup);

    Sprite GetBitmaskSpriteFromTileType(
        TileType tileType,
        Vector2Int tilePosition,
        BitmaskingSpriteGroup bitmaskingSpriteGroup);

    IList<Vector3Int> GetBitmaskAffectedTilePositions(Vector2Int tilePosition, AdjacencyType adjacencyType);
}
