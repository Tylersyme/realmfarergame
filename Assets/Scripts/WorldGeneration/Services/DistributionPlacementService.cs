﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DistributionPlacementService : IDistributionPlacementService
{
    private readonly IItemSpawnService _itemSpawnService;
    private readonly ITilemapPointService _tilemapPointService;

    public DistributionPlacementService()
    {
        _itemSpawnService = new ItemSpawnService();
        _tilemapPointService = new TilemapPointService();
    }

    public void PlaceTilemapObjects(Tilemap tilemap, Seed seed, List<DistributionParameters<GameObject>> objectDistributions) =>
        this.Distribute(
            tilemap,
            seed,
            objectDistributions,
            Vector3Int.zero.SetZ(0),
            tilemap.TopRight.SetZ(1),
            (t) => t?.Type != TileType.Stone &&
                   t?.Type != TileType.ShallowWater,
            (value, position) => TilemapManager.Instance.CreateTilemapObjectAtTilePosition(value, position));

    public void PlaceDecorationTiles(Tilemap tilemap, Seed seed, List<DistributionParameters<TileCreationDefinition>> objectDistributions) =>
        this.Distribute(
            tilemap,
            seed,
            objectDistributions,
            Vector3Int.zero.SetZ(1),
            tilemap.Size.SetZ(1),
            (t) => t?.Type != TileType.Stone &&
                   t?.Type != TileType.ShallowWater &&
                   t?.Type != TileType.ForestDirt,
            (value, position) => tilemap.AddDecorationTile(value, position.AsVector2Int()));

    public void PlaceItems(Tilemap tilemap, Seed seed, List<DistributionParameters<ItemTemplate>> objectDistributions) =>
        this.Distribute(
            tilemap,
            seed,
            objectDistributions,
            Vector3Int.zero.SetZ(1),
            tilemap.Size.SetZ(1),
            (t) => t?.Type != TileType.Stone &&
                   t?.Type != TileType.ShallowWater,
            (value, position) => _itemSpawnService.SpawnDroppedItemFromTemplate(
                value, 
                1, 
                _tilemapPointService.GetTileCenterWorldPosition(position.AsVector2Int())));

    private void Distribute<T>(
        Tilemap tilemap,
        Seed seed,
        List<DistributionParameters<T>> distributions,
        Vector3Int bottomLeftBound,
        Vector3Int size,
        Predicate<Tile> isSelectable,
        Action<T, Vector3Int> populateAction)
    {
        foreach (var distributionParameters in distributions)
        {
            if (distributionParameters.IsDisabled)
                continue;

            var positionSelector = distributionParameters.PositionSelector;

            var selectedPositions = positionSelector.Select(
                tilemap,
                seed,
                bottomLeftBound,
                size,
                isSelectable);

            var populated = distributionParameters.TilemapPopulator.Populate(
                tilemap,
                selectedPositions,
                new Dictionary<T, Predicate<Tile>>());

            foreach (var entry in populated)
                populateAction.Invoke(entry.Value, entry.Key);
        }
    }
}
