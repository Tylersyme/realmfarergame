﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class TileBitmaskService : ITileBitmaskService
{
    private readonly IBitmaskService _bitmaskService;

    public TileBitmaskService()
    {
        _bitmaskService = new BitmaskService();
    }

    public Texture2D GetBitmaskTextureFromTile(Tile tile, Vector2Int tilePosition) =>
        this.GetBitmaskTextureFromTileType(tile.Type, tilePosition, tile.Template.RenderData.BitmaskTextureGroup);

    public Texture2D GetBitmaskTextureFromDecorationTile(Tile decorationTile, Vector2Int tilePosition) =>
        this.GetBitmaskTexture(
            tilePosition,
            decorationTile.Template.RenderData.BitmaskTextureGroup,
            (tileCell, _, __, ___) => tileCell.DecorationTiles.Any(dt => dt.Type == decorationTile.Type));

    public Texture2D GetBitmaskTextureFromTileType(
        TileType tileType, 
        Vector2Int tilePosition, 
        BitmaskingTextureGroup bitmaskingTextureGroup)
    {
        return this.GetBitmaskTexture(
            tilePosition, 
            bitmaskingTextureGroup, 
            (tileCell, _, __, ___) => tileCell.Tile != null && tileCell.Tile.Type == tileType);
    }

    public Sprite GetBitmaskSpriteFromTileType(
        TileType tileType,
        Vector2Int tilePosition,
        BitmaskingSpriteGroup bitmaskingSpriteGroup)
    {
        return this.GetBitmaskSprite(
            tilePosition,
            bitmaskingSpriteGroup,
            (tileCell, _, __, ___) => tileCell.Tile != null && tileCell.Tile.Type == tileType);
    }

    public IList<Vector3Int> GetBitmaskAffectedTilePositions(Vector2Int tilePosition, AdjacencyType adjacencyType)
    {
        var tilemap = WorldManager.Instance.CurrentTilemap;

        var tileType = tilemap.GetTileAt(tilePosition.AsVector3Int().SetZ(1)).Type;

        var adjacentPositionOfSameTileType = tilemap
            .GetAdjacentPositionsWhere(
                // TODO: Only tiles on z = 1 can be tiled, change this to loop over all tiles on along z
                tilePosition.AsVector3Int().SetZ(1),
                adjacencyType,
                (tileCell, _, __, ___) => tileCell.Tile != null && tileCell.Tile.Type == tileType)
            .ToList();

        return adjacentPositionOfSameTileType;
    }

    private Texture2D GetBitmaskTexture(
        Vector2Int tilePosition,
        BitmaskingTextureGroup bitmaskingTextureGroup,
        Func<TileCell, int, int, int, bool> predicate)
    {
        var tilemap = WorldManager.Instance.CurrentTilemap;

        var adjacencyMode = bitmaskingTextureGroup.BitmaskAdjacencyMode;

        var adjacentDirectionsOfSameTileType = tilemap
            .GetAdjacentTileCellDirectionsWhere(
                // TODO: Only tiles on z = 1 can be tiled, change this to loop over all tiles on along z
                tilePosition.AsVector3Int().SetZ(1),
                adjacencyMode.ToAdjacencyType(),
                predicate);

        var textureIndex = _bitmaskService.GetBitmaskValue(
            adjacentDirectionsOfSameTileType,
            adjacencyMode);

        return bitmaskingTextureGroup.GetTextureByIndex(textureIndex);
    }

    private Sprite GetBitmaskSprite(
        Vector2Int tilePosition,
        BitmaskingSpriteGroup bitmaskingSpriteGroup,
        Func<TileCell, int, int, int, bool> predicate)
    {
        var tilemap = WorldManager.Instance.CurrentTilemap;

        var adjacencyMode = bitmaskingSpriteGroup.BitmaskAdjacencyMode;

        var adjacentDirectionsOfSameTileType = tilemap
            .GetAdjacentTileCellDirectionsWhere(
                // TODO: Only tiles on z = 1 can be tiled, change this to loop over all tiles on along z
                tilePosition.AsVector3Int().SetZ(1),
                adjacencyMode.ToAdjacencyType(),
                predicate);

        var textureIndex = _bitmaskService.GetBitmaskValue(
            adjacentDirectionsOfSameTileType,
            adjacencyMode);

        return bitmaskingSpriteGroup.GetSpriteByIndex(textureIndex);
    }
}
