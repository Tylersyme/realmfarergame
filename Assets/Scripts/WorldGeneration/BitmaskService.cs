﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// This service is used to determine how sprites should be chosen when adjacent to other sprites.
/// </summary>
public class BitmaskService : IBitmaskService
{
    private static readonly IDictionary<Direction, int> DIRECTION_EIGHT_SIDED_BITMASK_VALUES = new Dictionary<Direction, int>
    {
        [Direction.NORTH_WEST] = 1,
        [Direction.NORTH] = 2,
        [Direction.NORTH_EAST] = 4,
        [Direction.WEST] = 8,
        [Direction.EAST] = 16,
        [Direction.SOUTH_WEST] = 32,
        [Direction.SOUTH] = 64,
        [Direction.SOUTH_EAST] = 128,
    };

    private static readonly IDictionary<Direction, int> DIRECTION_FOUR_SIDED_BITMASK_VALUES = new Dictionary<Direction, int>
    {
        [Direction.NORTH] = 1,
        [Direction.WEST] = 2,
        [Direction.EAST] = 4,
        [Direction.SOUTH] = 8,
    };

    /// <summary>
    /// Contains the mapping from bitmask sum values to a linear list of values.
    /// </summary>
    private static readonly IDictionary<int, int> RAW_SUM_TO_INDEX = new Dictionary<int, int>
    {
        [2] = 1, [8] = 2, [10] = 3, [11] = 4, [16] = 5, [18] = 6, [22] = 7, [24] = 8, [26] = 9, [27] = 10, [30] = 11, [31] = 12, [64] = 13,
        [66] = 14, [72] = 15, [74] = 16, [75] = 17, [80] = 18, [82] = 19, [86] = 20, [88] = 21, [90] = 22, [91] = 23, [94] = 24, [95] = 25,
        [104] = 26, [106] = 27, [107] = 28, [120] = 29, [122] = 30, [123] = 31, [126] = 32, [127] = 33, [208] = 34, [210] = 35, [214] = 36,
        [216] = 37, [218] = 38, [219] = 39, [222] = 40, [223] = 41, [248] = 42, [250] = 43, [251] = 44, [254] = 45, [255] = 46, [0] = 47,
    };

    public int GetBitmaskValue(BitmaskAdjacencyMode bitmaskAdjacencyMode, List<Direction> directions)
    {
        if (bitmaskAdjacencyMode == BitmaskAdjacencyMode.FOUR_SIDED)
            return GetFourSidedBitmaskValue(directions);
        else if (bitmaskAdjacencyMode == BitmaskAdjacencyMode.EIGHT_SIDED)
            return GetEightSidedBitmaskValue(directions);

        throw new System.Exception($"Error getting bitmask value: The adjacency mode {bitmaskAdjacencyMode} is not supported.");
    }

    public int GetBitmaskValue(List<Direction> directions, BitmaskAdjacencyMode bitmaskAdjacencyMode)
    {
        if (bitmaskAdjacencyMode == BitmaskAdjacencyMode.FOUR_SIDED)
            return GetFourSidedBitmaskValue(directions);
        else
            return GetEightSidedBitmaskValue(directions);
    }

    public int GetEightSidedBitmaskValue(List<Direction> directions)
    {
        directions = ExcludeIsolatedCorners(directions);

        var sum = 0;

        foreach (var direction in directions)
            sum += DIRECTION_EIGHT_SIDED_BITMASK_VALUES[direction];

        return RawSumToIndex(sum);
    }

    public int GetFourSidedBitmaskValue(List<Direction> directions)
    {
        var sum = 0;

        foreach (var direction in directions)
            sum += DIRECTION_FOUR_SIDED_BITMASK_VALUES[direction];

        return sum;
    }

    private int RawSumToIndex(int sum)
    {
        return RAW_SUM_TO_INDEX[sum] - 1;
    }

    /// <summary>
    /// This removes corner directions if they are not also surrounded on both sides by its cardinal direction.
    /// For example, NORTH_WEST would be removed if either NORTH or WEST was not also present.
    /// </summary>
    /// <param name="directions"></param>
    /// <returns></returns>
    private List<Direction> ExcludeIsolatedCorners(List<Direction> directions)
    {
        var noIsolatedCorners = new List<Direction>();

        foreach (var direction in directions)
        {
            if (direction.IsCardinal)
            {
                noIsolatedCorners.Add(direction);
                continue;
            }

            if (direction.CompositeDirections.All(d => directions.Contains(d)))
            {
                noIsolatedCorners.Add(direction);
                continue;
            }
        }

        return noIsolatedCorners;
    }
}

public enum BitmaskAdjacencyMode
{
    NONE,
    FOUR_SIDED,
    EIGHT_SIDED,
}
