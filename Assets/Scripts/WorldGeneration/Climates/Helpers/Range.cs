﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Range
{
    [SerializeField]
    private float _start;
    public float Start { get => _start; private set => _start = value; }

    [SerializeField]
    private float _end;
    public float End { get => _end; private set => _end = value; }

    [ShowInInspector]
    public float Length
    {
        get => this.End - this.Start;
    }

    public Range(float start, float end)
    {
        if (end < start)
            throw new System.Exception($"Error creating range: End [{end}] must be greater than Start [{start}]");

        this.Start = start;
        this.End = end;
    }

    /// <summary>
    /// Creates a new rect with this range being the x axis and the other being the y axis.
    /// </summary>
    /// <param name="heightRange"></param>
    /// <returns></returns>
    public Rect ToRect(Range heightRange)
    {
        var position = new Vector2(this.Start, heightRange.Start);
        var size = new Vector2(this.Length, heightRange.Length);

        return new Rect(position, size);
    }

    /// <summary>
    /// Start is inclusive.
    /// End is exclusive.
    /// </summary>
    /// <param name="value"></param>
    /// <returns></returns>
    public bool Contains(float value)
    {
        return value >= this.Start && value <= this.End;
    }

    /// <summary>
    /// Returns the closest distance to the given value from either the Start or End points.
    /// </summary>
    /// <param name="value"></param>
    /// <returns></returns>
    public float DistanceTo(float value)
    {
        var startDistance = Mathf.Abs(this.Start - value);
        var endDistance = Mathf.Abs(this.End - value);

        return Mathf.Min(startDistance, endDistance);
    }
}
