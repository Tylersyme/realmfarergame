﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BiomeRegion
{
    public BiomeType BiomeType { get; private set; }
    public Rect Region { get; private set; }
    public Range HeightRange { get; private set; } = new Range(0.0f, 1.0f);

    public BiomeRegion(BiomeType biomeType, Rect region, Range heightRange)
        : this(biomeType, region)
    {
        this.HeightRange = heightRange;
    }

    public BiomeRegion(BiomeType biomeType, Rect region)
    {
        if (region.x < 0 || region.x > 1.0f ||
            region.y < 0 || region.y > 1.0f)
            throw new ArgumentException("Error creating biome region: The provided rect's top left corner must be within 1.0 x 1.0");

        if (region.xMax > 1.0f ||
            region.xMin < 0.0f)
            throw new ArgumentException("Error creating biome region: The provided rect must be within 1.0 x 1.0.");

        this.BiomeType = biomeType;
        this.Region = region;
    }

    public bool HasParameters(float temperature, float precipitation, float height)
    {
        if (!this.Region.ContainsInclusive(new Vector2(temperature, precipitation)))
            return false;

        if (!this.HeightRange.Contains(height))
            return false;

        return true;
    }
}
