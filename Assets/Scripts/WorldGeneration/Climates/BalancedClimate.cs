﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class BalancedClimate : Climate
{
    public BalancedClimate() : base()
    {
        DefineBiomeRegions();
    }

    protected override void DefineBiomeRegions()
    {
        this.AddBiomeRegion(BiomeType.TAIGA, new Range(0, .3f), new Range(0.3f, 1.0f), this.LandRange);
        this.AddBiomeRegion(BiomeType.TEMPERATE_FOREST, new Range(.3f, 1f), new Range(0.3f, 1.0f), this.LandRange);
        this.AddBiomeRegion(BiomeType.TUNDRA, new Range(0, 0.25f), new Range(0, 0.3f), this.LandRange);
        this.AddBiomeRegion(BiomeType.DESERT, new Range(0.25f, 1.0f), new Range(0, 0.3f), this.LandRange);

        this.AddBiomeRegion(BiomeType.LAKE, new Range(0.15f, 1.0f), new Range(0.0f, 1.0f), this.WaterLevelRange);
        this.AddBiomeRegion(BiomeType.FROZEN_LAKE, new Range(0.0f, 0.15f), new Range(0.0f, 1.0f), this.WaterLevelRange);
    }
}
