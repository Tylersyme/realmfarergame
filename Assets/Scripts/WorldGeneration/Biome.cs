﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public abstract class Biome
{
    [SerializeField]
    private BiomeType _type;
    public BiomeType Type { get => _type; protected set => _type = value; }

    public Color IdentifyingColor { get => this.Type.IdentifyingColor; }
    public string Name { get => this.Type.Name; }

    public Biome() { }

    protected Biome(BiomeType type)
    {
        this.Type = type;
    }
}
