﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Stores any data related to a cell within a tilemap.
/// </summary>
[System.Serializable]
public class TileCell
{
    public TileCell()
    {
        this.DecorationTiles = new List<Tile>();
    }

    /// <summary>
    /// Represents the tile stored at this location.
    /// </summary>
    [SerializeField]
    private Tile _tile;
    public Tile Tile { get => _tile; set => _tile = value; }

    [SerializeField]
    private IList<Tile> _decorationTiles;
    public IList<Tile> DecorationTiles { get => _decorationTiles; set => _decorationTiles = value; }
}
