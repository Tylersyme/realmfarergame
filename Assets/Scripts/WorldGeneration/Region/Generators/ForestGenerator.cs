﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ForestGenerator : RegionGenerator
{
    public ForestGenerator()
    {
        //AddDecorationTileCondition(DecorationTileType.ForestGrasslets, (t) => t.Type == TileType.ForestGrass);
    }

    public override Region GenerateRegion(Region emptyRegion, RegionParameters regionParameters)
    {
        var baseRegion = base.GenerateRegion(emptyRegion, regionParameters);

        //var generationParameters = (ForestGenerationParameters)regionParameters.RegionGenerationParameters;

        //foreach (var distributionParameters in generationParameters.ObjectDistributions)
        //{
        //    var g = distributionParameters.PositionSelector;

        //    var selectedPositions = g.Select(
        //        baseRegion.Tilemap,
        //        regionParameters.Seed,
        //        Vector3Int.zero.SetZ(1),
        //        baseRegion.Tilemap.TopRight.SetZ(1),
        //        (t) => t?.Type != TileType.Stone &&
        //               t?.Type != TileType.ShallowWater);

        //    var populated = distributionParameters.TilemapPopulator.Populate(
        //        baseRegion.Tilemap, 
        //        selectedPositions, 
        //        new Dictionary<GameObject, Predicate<Tile>>());

        //    foreach (var entry in populated)
        //        TilemapManager.Instance.CreateTilemapObjectAtTilePosition(entry.Value, entry.Key);
        //}

        return baseRegion;
    }
}
