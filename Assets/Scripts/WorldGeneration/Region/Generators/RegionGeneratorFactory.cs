﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RegionGeneratorFactory : IRegionGeneratorFactory
{
    public IRegionGenerator Create(BiomeType biomeType)
    {
        if (biomeType == BiomeType.TAIGA)
            return new ForestGenerator();

        return new ForestGenerator();
    }
}
