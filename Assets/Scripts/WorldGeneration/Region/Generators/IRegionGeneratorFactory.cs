﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IRegionGeneratorFactory
{
    IRegionGenerator Create(BiomeType biomeType);
}
