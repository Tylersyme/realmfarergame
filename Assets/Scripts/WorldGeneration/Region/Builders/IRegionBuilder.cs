﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IRegionBuilder<T> where T : IRegionBuilder<T>
{
    T NewRegion(Seed seed, World world, Biome biome, float height, float temperature, float precipitation);

    RegionParameters Build();
}
