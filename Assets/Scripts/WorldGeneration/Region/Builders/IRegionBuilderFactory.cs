﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IRegionBuilderFactory
{
    T Create<T>(BiomeType biomeType) where T : IRegionBuilder<T>;
}
