﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RegionBuilderFactory : IRegionBuilderFactory
{
    public T Create<T>(BiomeType biomeType) where T : IRegionBuilder<T>
    {
        if (biomeType == BiomeType.TAIGA)
            return (T)(object)new TaigaBuilder();

        throw new System.Exception($"Region builder creation error: The biome type \"{biomeType.Name}\" has not been defined.");
    }
}
