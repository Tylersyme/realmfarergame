﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// TODO: Are region builder necessary?
public abstract class RegionBuilder<T> : IRegionBuilder<T> where T : RegionBuilder<T>
{
    protected RegionParameters RegionParameters { get; set; }
    protected BiomeType BiomeType { get; set; }

    protected RegionBuilder(BiomeType biomeType)
    {
        this.BiomeType = biomeType;
    }

    public virtual T NewRegion(Seed seed, World world, Biome biome, float height, float temperature, float precipitation)
    {
        this.RegionParameters = new RegionParameters
        {
            Seed = seed,
            Biome = biome,
            Height = height,
            Temperature = temperature,
            Precipitation = precipitation,
            World = world,
            // TODO: Dynamically determine region generation parameters
            RegionGenerationParameters = WorldManager.Instance.ForestGenerationParameters,
        };

        return (T)this;
    }

    public RegionParameters Build()
    {
        return this.RegionParameters;
    }
}
