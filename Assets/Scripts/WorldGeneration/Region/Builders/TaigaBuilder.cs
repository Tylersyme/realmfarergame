﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TaigaBuilder : RegionBuilder<TaigaBuilder>
{
    public TaigaBuilder() : base(BiomeType.TAIGA)
    {
    }
}
