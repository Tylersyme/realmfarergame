﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Grid3D<T> where T : class
{
    private OffsetList<OffsetList<OffsetList<T>>> Grid { get; set; } 
        = new OffsetList<OffsetList<OffsetList<T>>>();

    public Grid3D<T> AddElement(T element, Vector3Int position)
    {
        if (HasElementAt(position))
            throw new ArgumentException("An element at position " + position + " already exists.");

        var absX = Math.Abs(position.x);
        var absY = Math.Abs(position.y);
        var absZ = Math.Abs(position.z);
        var indexX = 0;
        var indexY = 0;
        var indexZ = 0;

        if (position.x < 0)
        {
            var willUpdate = this.Grid.WillUpdateOffset(position.x);

            var difX = willUpdate
                ? absX - this.Grid.Offset
                : 0;

            this.Grid.AttemptUpdateOffset(position.x);

            indexX = this.Grid.Offset + position.x;

            for (var x = 0; x < difX; x++)
                this.Grid.Insert(0, new OffsetList<OffsetList<T>>());
        }
        else
        {
            indexX = position.x + this.Grid.Offset;
        }

        if (position.y < 0)
        {
            var willUpdate = this.Grid[indexX].WillUpdateOffset(position.y);

            var difY = willUpdate
                ? absY - this.Grid[indexX].Offset
                : 0;

            this.Grid[indexX].AttemptUpdateOffset(position.y);

            indexY = this.Grid[indexX].Offset + position.y;

            for (var y = 0; y < difY; y++)
                this.Grid[indexX].Insert(0, new OffsetList<T>());
        }
        else
        {
            try
            {
                indexY = position.y + this.Grid[indexX].Offset;
            }
            catch (ArgumentOutOfRangeException)
            {
                indexY = position.y;
            }
        }

        if (position.z < 0)
        {
            var grid = this.Grid[indexX][indexY];
            var willUpdate = grid.WillUpdateOffset(position.z);

            var difZ = willUpdate
                ? absZ - grid.Offset
                : 0;

            grid.AttemptUpdateOffset(position.z);

            indexZ = grid.Offset + position.z;

            for (var z = 0; z < difZ; z++)
                grid.Insert(0, default(T));
        }
        else
        {
            try
            {
                indexZ = position.z + this.Grid[indexX][indexY].Offset;
            }
            catch (ArgumentOutOfRangeException)
            {
                indexZ = position.z;
            }
        }

        //EnsureCapacity(new Vector3Int(indexX, indexY, indexZ));

        this.Grid[indexX][indexY][indexZ] = element;

        return this;
    }

    public T RemoveElement(Vector3Int position)
    {
        var element = this.Grid[position.x][position.y][position.z];
        this.Grid[position.x][position.y][position.z] = default(T);

        return element;
    }

    public Grid3D<T> SetElement(T element, Vector3Int position)
    {
        RemoveElement(position);
        AddElement(element, position);

        return this;
    }

    public T ElementAt(Vector3Int pos)
    {
        if (!HasElementAt(pos))
            return default(T);

        var indexPos = GetIndexPos(pos);
        return this.Grid[indexPos.x][indexPos.y][indexPos.z];
    }

    public T ElementAt(int x, int y, int z)
    {
        return ElementAt(new Vector3Int(x, y, z));
    }

    public bool HasElementAt(Vector3Int pos)
    {
        try
        {
            var indexPos = GetIndexPos(pos);
            return this.IsInBounds(pos) &&
                   this.Grid[indexPos.x]
                            [indexPos.y]
                            [indexPos.z] != null;
        }
        catch (ArgumentOutOfRangeException)
        {
            return false;
        }
    }

    public bool IsInBounds(Vector3Int pos)
    {
        try
        {
            var indexPos = GetIndexPos(pos);
            return this.Grid.Count - 1 >= indexPos.x &&
                   this.Grid[indexPos.x].Count - 1 >= indexPos.y &&
                   this.Grid[indexPos.x][indexPos.y].Count - 1 >= indexPos.z;
        }
        catch (ArgumentOutOfRangeException)
        {
            return false;
        }
    }

    private int GetIndexX(int x)
    {
        return x + GetOffsetX();
    }
    private int GetOffsetX()
    {
        return this.Grid.Offset;
    }

    private int GetIndexY(int x, int y)
    {
        return y + GetOffsetY(x);
    }
    private int GetOffsetY(int x)
    {
        return this.Grid[GetIndexX(x)].Offset;
    }

    private int GetIndexZ(int x, int y, int z)
    {
        return z + GetOffsetZ(x, y);
    }
    private int GetOffsetZ(int x, int y)
    {
        return this.Grid[GetIndexX(x)][GetIndexY(x, y)].Offset;
    }

    private Vector3Int GetIndexPos(Vector3Int pos)
    {
        return new Vector3Int(
            GetIndexX(pos.x),
            GetIndexY(pos.x, pos.y),
            GetIndexZ(pos.x, pos.y, pos.z));
    }

    /// <summary>
    /// Expands the grid until it is large enough to fully contain the given position
    /// on the x, y, and z axes.
    /// </summary>
    /// <param name="pos"></param>
    public void EnsureCapacity(Vector3Int pos)
    {
        for (int x = 0; x < pos.x + 1; x++)
        {
            if (this.Grid.Count - 1 < x)
                this.Grid.Add(new OffsetList<OffsetList<T>>());
            for (int y = 0; y < pos.y + 1; y++)
            {
                if (this.Grid.ElementAt(x).Count - 1 < y)
                    this.Grid.ElementAt(x).Add(new OffsetList<T>());
                for (int z = 0; z < pos.z + 1; z++)
                {
                    if (this.Grid.ElementAt(x).ElementAt(y).Count - 1 < z)
                        this.Grid.ElementAt(x).ElementAt(y).Add(default(T));
                }
            }
        }
    }

}
