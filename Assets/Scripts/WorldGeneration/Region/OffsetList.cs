﻿using System;
using System.Collections.Generic;

public class OffsetList<T> : List<T>
{
    public int Offset { get; set; }

    public bool WillUpdateOffset(int num)
    {
        return Math.Abs(num) > this.Offset;
    }

    public int AttemptUpdateOffset(int num)
    {
        var willIncrease = WillUpdateOffset(num);
        if (willIncrease)
            Offset = Math.Abs(num);

        return this.Offset;
    }
}
