﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Region
{
    [SerializeField]
    private Biome _biome;
    public Biome Biome { get => _biome; private set => _biome = value; }

    [SerializeField]
    private float _height;
    public float Height { get => _height; private set => _height = value; }

    [SerializeField]
    private float _temperature;
    public float Temperature { get => _temperature; private set => _temperature = value; }

    [SerializeField]
    private float _precipitation;
    public float Precipitation { get => _precipitation; private set => _precipitation = value; }

    [SerializeField]
    private Vector2Int _size;
    public Vector2Int Size { get => _size; private set => _size = value; }

    [SerializeField]
    private Tilemap _tilemap;
    public Tilemap Tilemap { get => _tilemap; set => _tilemap = value; }

    public bool IsGenerated { get => this.Tilemap != null; }

    public Seed Seed { get => WorldManager.Instance.World.Seed; }

    public Region() { }

    public Region(Biome biome, float height, float temperature, float precipitation, Vector2Int size)
    {
        this.Biome = biome;
        this.Height = height;
        this.Temperature = temperature;
        this.Precipitation = precipitation;
        this.Size = size;
    }
}
