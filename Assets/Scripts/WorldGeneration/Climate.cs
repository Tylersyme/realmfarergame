﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// A climate defines which biomes should be assigned to regions depending upon temperature and 
/// precipitation.
/// </summary>
[System.Serializable]
public abstract class Climate
{
    private List<BiomeRegion> BiomeRegions { get; set; }

    private float WaterLevel { get; set; }
    protected Range LandRange { get => new Range(this.WaterLevel, 1.0f); }
    protected Range WaterLevelRange { get => new Range(0.0f, this.WaterLevel); }

    public Climate(float waterLevel = 0.25f)
    {
        this.BiomeRegions = new List<BiomeRegion>();
        this.WaterLevel = waterLevel;
    }

    protected abstract void DefineBiomeRegions();

    public BiomeType GetBiomeType(float temperature, float precipitation, float height)
    {
        var biomeType = this.BiomeRegions.SingleOrDefault(br => br.HasParameters(temperature, precipitation, height))?.BiomeType
            ?? throw new NullReferenceException($"Error selectin biome type: No biome region could be found for - " +
                                                $"Temp={temperature}, Prec={precipitation}, Height={height}.");

        return biomeType;
    }

    private bool IsAboveSeaLevel(BiomeRegion biomeRegion)
    {
        return biomeRegion.HeightRange.Start >= this.WaterLevel;
    }

    protected void AddBiomeRegion(BiomeType biomeType, Range temperature, Range precipitation, Range height)
    {
        var region = temperature.ToRect(precipitation);

        this.BiomeRegions.Add(new BiomeRegion(biomeType, region, height));
    }

    protected void AddBiomeRegion(BiomeType biomeType, Range temperature, Range precipitation)
    {
        var region = temperature.ToRect(precipitation);

        this.BiomeRegions.Add(new BiomeRegion(biomeType, region));
    }

    public Texture2D GetBiomeDistributionTexture(int width, int height)
    {
        var texture2D = new Texture2D(width, height);

        foreach (var biomeRegion in this.BiomeRegions.Where(br => IsAboveSeaLevel(br)))
        {
            var rect = biomeRegion.Region;
            var pixelWidth = (int)(width * rect.width);
            var pixelHeight = (int)(height * rect.height);
            var pixelWidthStart = (int)(width * rect.xMin);
            var pixelHeightStart = (int)(height * rect.yMin);

            for (var x = 0; x < pixelWidth; x++)
                for (var y = 0; y < pixelHeight; y++)
                    texture2D.SetPixel(pixelWidthStart + x, pixelHeightStart + y, biomeRegion.BiomeType.IdentifyingColor);
        }

        texture2D.Apply();

        return texture2D;
    }
}
