﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix;
using Sirenix.OdinInspector;

/// <summary>
/// A distribution describes the parameters for how something should be distributed over a space.
/// </summary>
[System.Serializable]
public class DistributionParameters<T>
{
    [Title("Name")]
    [Tooltip("Helpful identifier for inspector use only")]
    [SerializeField]
    private string _labelName = "Distribution";
    public string LabelName { get => _labelName; set => _labelName = value; }

    [SerializeField]
    private bool _isDisabled = false;
    public bool IsDisabled { get => _isDisabled; set => _isDisabled = value; }

    [SerializeField]
    private IPositionSelector _positionSelector;
    public IPositionSelector PositionSelector { get => _positionSelector; }

    [Title("Tilemap Populator")]
    [SerializeField]
    private ITilemapPopulator<T> _tilemapPopulator;
    public ITilemapPopulator<T> TilemapPopulator { get => _tilemapPopulator; set => _tilemapPopulator = value; }
}
