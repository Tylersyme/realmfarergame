﻿using Sirenix.OdinInspector;
using Sirenix.Serialization;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Generation profiles are used to provide the generation parameters to region generators.
/// </summary>
public abstract class RegionGenerationParameters : SerializedScriptableObject
{
    public virtual BiomeType BiomeType { get; }

    [SerializeField]
    [BoxGroup("Tiles")]
    [Tooltip("The tile type which will cover the entire tilemap as its ground")]
    private TileType _baseTileType = TileType.ForestGrass;
    public TileType BaseTileType { get => _baseTileType; set => _baseTileType = value; }

    [SerializeField]
    [BoxGroup("Tiles")]
    private TileType _mountainTileType = TileType.Stone;
    public TileType MountainTileType { get => _mountainTileType; set => _mountainTileType = value; }

    [SerializeField]
    [BoxGroup("Tiles")]
    private TileType _waterTileType = TileType.ShallowWater;
    public TileType WaterTileType { get => _waterTileType; set => _waterTileType = value; }

    /// <summary>
    /// The world will determine a region's average height which then determines how much mountain will
    /// be placed in that region. That height will be modified by this value (through multiplication)
    /// which is used to prevent too much mountain.
    /// </summary>
    [SerializeField]
    [BoxGroup("Height Map")]
    private float _heightModifier = 1.0f;
    public float HeightModifier { get => _heightModifier; set => _heightModifier = value; }

    /// <summary>
    /// The final height threshold calculation for mountains will be clamped by this value.
    /// </summary>
    [SerializeField]
    [BoxGroup("Height Map")]
    [MinMaxSlider(0.0f, 1.0f, true)]
    private Vector2 _heightClamp = new Vector2(0.0f, 1.0f);
    public Vector2 HeightClamp { get => _heightClamp; set => _heightClamp = value; }

    [OdinSerialize]
    [ListDrawerSettings(
        DraggableItems = false, 
        Expanded = false, 
        ListElementLabelName = nameof(DistributionParameters<GameObject>.LabelName))]
    private List<DistributionParameters<GameObject>> _objectDistributions = new List<DistributionParameters<GameObject>>();
    public List<DistributionParameters<GameObject>> ObjectDistributions { get => _objectDistributions; set => _objectDistributions = value; }

    [OdinSerialize]
    [ListDrawerSettings(
        DraggableItems = false, 
        Expanded = false, 
        ListElementLabelName = nameof(DistributionParameters<GameObject>.LabelName))]
    private List<DistributionParameters<ItemTemplate>> _itemDistributions = new List<DistributionParameters<ItemTemplate>>();
    public List<DistributionParameters<ItemTemplate>> ItemDistributions { get => _itemDistributions; set => _itemDistributions = value; }

    [OdinSerialize]
    [ListDrawerSettings(
        DraggableItems = false,
        Expanded = false,
        ListElementLabelName = nameof(DistributionParameters<TileCreationDefinition>.LabelName))]
    private List<DistributionParameters<TileCreationDefinition>> _decorationDistributions;
    public List<DistributionParameters<TileCreationDefinition>> DecorationDistributions
    {
        get => _decorationDistributions;
        set => _decorationDistributions = value;
    }
}
