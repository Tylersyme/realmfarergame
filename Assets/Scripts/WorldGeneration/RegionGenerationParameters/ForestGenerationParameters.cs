﻿using Sirenix.OdinInspector;
using Sirenix.Serialization;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
[CreateAssetMenu(fileName = "ForestGenerationParameters", menuName = "GenerationParameters/Forest")]
public class ForestGenerationParameters : RegionGenerationParameters
{
    public override BiomeType BiomeType { get => BiomeType.TAIGA; }
}
