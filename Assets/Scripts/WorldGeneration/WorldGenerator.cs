﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldGenerator : IWorldGenerator
{
    public NoiseMapGenerator NoiseMapGenerator { get; private set; }

    private readonly INoiseService _noiseService;
    private readonly IBiomeFactory _biomeFactory;
    private readonly IRegionBuilderFactory _regionBuilderFactory;
    private readonly IRegionGeneratorFactory _regionGeneratorFactory;

    public WorldGenerator()
    {
        _noiseService = new NoiseService();
        _biomeFactory = new BiomeFactory();
        _regionGeneratorFactory = new RegionGeneratorFactory();

        this.NoiseMapGenerator = new NoiseMapGenerator();
    }

    public World GenerateWorld(WorldParameters worldParameters)
    {
        var seed = worldParameters.Seed;
        var width = worldParameters.WorldSize.x;
        var height = worldParameters.WorldSize.y;
        var climate = worldParameters.Climate;
        var heightParameters = worldParameters.HeightParameters;
        var temperatureParameters = worldParameters.TemperatureParameters;
        var precipitationParameters = worldParameters.PrecipitationParameters;

        var world = new World(seed, width, height, climate);

        var heightMap = GenerateHeightMap(heightParameters);
        var temperatureMap = GenerateTemperatureMap(temperatureParameters);
        var precipitationMap = GeneratePrecipitationMap(precipitationParameters); 

        var regions = GenerateEmptyRegions(heightMap, temperatureMap, precipitationMap, world);
        world.SetRegions(regions);

        world.HeightMap = heightMap;
        world.TemperatureMap = temperatureMap;
        world.PrecipitationMap = precipitationMap;

        return world;
    }

    /// <summary>
    /// Generates a 2D list of empty regions whose tiles have not be deteremined.
    /// Empty regions are used because the tiles for a region will only be loaded when needed.
    /// </summary>
    /// <param name="heightMap"></param>
    /// <param name="temperatureMap"></param>
    /// <param name="precipitatonMap"></param>
    /// <param name="world"></param>
    /// <returns></returns>
    private List<List<Region>> GenerateEmptyRegions(
        NoiseMap heightMap, 
        NoiseMap temperatureMap, 
        NoiseMap precipitatonMap, 
        World world)
    {
        var regions = new List<List<Region>>();

        for (var x = 0; x < world.Size.x; x++)
        {
            regions.Add(new List<Region>());
            for (var y = 0; y < world.Size.y; y++)
            {
                var heightValue = heightMap.ValueAt(x, y);
                var temperatureValue = temperatureMap.ValueAt(x, y);
                var precipitationValue = precipitatonMap.ValueAt(x, y);
                var biome = _biomeFactory.Create(world.Climate.GetBiomeType(temperatureValue, precipitationValue, heightValue));

                // An unloaded region has not had its individual tiles generated
                var unloadedRegion = RegionGenerator.CreatedUnloadedRegion(
                    biome,
                    heightValue,
                    temperatureValue,
                    precipitationValue,
                    WorldManager.Instance.RegionSize);

                regions[x].Add(unloadedRegion);
            }
        }

        return regions;
    }
    
    private NoiseMap GenerateHeightMap(NoiseMapParameters noiseMapParameters)
    {
        var width = noiseMapParameters.Dimensions.x;
        var height = noiseMapParameters.Dimensions.y;

        var noiseGenerator = _noiseService.BuildFromParameters(noiseMapParameters);

        var heightMap = this.NoiseMapGenerator
            .GenerateNoiseMap(noiseGenerator, width, height)
            .Normalize();

        return heightMap;
    }

    private NoiseMap GenerateTemperatureMap(NoiseMapParameters noiseMapParameters)
    {
        var width = noiseMapParameters.Dimensions.x;
        var height = noiseMapParameters.Dimensions.y;

        var noiseGenerator = _noiseService.BuildFromParameters(noiseMapParameters);

        var temperatureMap = this.NoiseMapGenerator
            .GenerateNoiseMap(noiseGenerator, width, height)
            .Normalize();

        return temperatureMap;
    }

    private NoiseMap GeneratePrecipitationMap(NoiseMapParameters noiseMapParameters)
    {
        var width = noiseMapParameters.Dimensions.x;
        var height = noiseMapParameters.Dimensions.y;

        var noiseGenerator = _noiseService.BuildFromParameters(noiseMapParameters);

        var precipitationMap = this.NoiseMapGenerator
            .GenerateNoiseMap(noiseGenerator, width, height)
            .Normalize();

        return precipitationMap;
    }

}
