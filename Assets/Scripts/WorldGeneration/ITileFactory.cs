﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ITileFactory
{
    Tile Create(TileCreationDefinition tileCreationDefinition);
    Tile CreateFromType(TileType tileType);
}
