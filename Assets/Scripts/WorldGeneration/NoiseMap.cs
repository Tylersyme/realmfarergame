﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class NoiseMap
{
    [SerializeField]
    private List<List<NoiseSample>> _map;
    public List<List<NoiseSample>> Map { get => _map; set => _map = value; }

    [SerializeField]
    private int _width;
    public int Width { get => _width; set => _width = value; }

    [SerializeField]
    private int _height;
    public int Height { get => _height; set => _height = value; }

    [SerializeField]
    private float _minValue;
    public float MinValue { get => _minValue; set => _minValue = value; }

    [SerializeField]
    private float _maxValue;
    public float MaxValue { get => _maxValue; set => _maxValue = value; }

    public NoiseMap() { }

    public NoiseMap(List<List<NoiseSample>> samples, int width, int height, float minValue, float maxValue)
    {
        this.Map = samples;
        this.Width = width;
        this.Height = height;
        this.MinValue = minValue;
        this.MaxValue = maxValue;
    }

    public NoiseMap(int width, int height, float initialValue = 0.0f)
    {
        this.Map = new List<List<NoiseSample>>();

        for (var x = 0; x < this.Width; x++)
        {
            this.Map.Add(new List<NoiseSample>());
            for (var y = 0; y < this.Height; y++)
            {
                this.Map[x].Add(new NoiseSample(initialValue));
            }
        }
    }

    public NoiseMap Normalize()
    {
        var copy = GetNoiseMapCopy();

        var minValue = float.MaxValue;
        var maxValue = float.MinValue;

        for (var x = 0; x < this.Width; x++)
        {
            for (var y = 0; y < this.Height; y++)
            {
                var normalizedValue = Mathf.InverseLerp(this.MinValue, this.MaxValue, copy.Map[x][y].Value);
                copy.Map[x][y].Value = normalizedValue;

                if (normalizedValue < minValue)
                    minValue = normalizedValue;
                if (normalizedValue > maxValue)
                    maxValue = normalizedValue;
            }
        }

        copy.UpdateMinMaxValue(minValue, maxValue);

        return copy;
    }

    public NoiseMap Modify(System.Func<NoiseSample, NoiseSample, float> func, NoiseMap other)
    {
        var copy = GetNoiseMapCopy();

        for (var x = 0; x < this.Width; x++)
            for (var y = 0; y < this.Height; y++)
                copy.Map[x][y].Value = func.Invoke(this.Map[x][y], other.Map[x][y]);

        return copy;
    }

    public NoiseMap Modify(System.Func<NoiseSample, NoiseSample, float> func, NoiseSample value)
    {
        var copy = GetNoiseMapCopy();

        for (var x = 0; x < this.Width; x++)
            for (var y = 0; y < this.Height; y++)
                copy.Map[x][y].Value = func.Invoke(this.Map[x][y], value);

        return copy;
    }

    public NoiseMap SplitCombine(NoiseMap other, float splitCutoff)
    {
        splitCutoff = Mathf.Clamp01(splitCutoff);

        return Modify((originalValue, otherValue) => Mathf.Clamp01(originalValue.Value + (otherValue.Value - splitCutoff)), other);
    }

    public NoiseMap AverageCombine(NoiseMap other)
    {
        return Modify((originalValue, otherValue) => Mathf.Clamp01((originalValue.Value + otherValue.Value) * 0.5f), other);
    }

    public NoiseMap DrawTowards(float point, float percentage)
    {
        point = Mathf.Clamp01(point);

        var resultFunction = new System.Func<NoiseSample, NoiseSample, float>(
            (originalValue, otherValue) =>
            {
                var distance = Mathf.Abs(originalValue.Value - otherValue.Value);

                // Draw value towards point by given percentage
                if (originalValue.Value < otherValue.Value)
                    return Mathf.Clamp01(originalValue.Value + (distance * percentage));
                else if (originalValue.Value > otherValue.Value)
                    return Mathf.Clamp01(originalValue.Value - (distance * percentage));
                else
                    return originalValue.Value;
            });

        return Modify(resultFunction, new NoiseSample(point));
    }

    public NoiseMap Add(NoiseMap other)
    {
        return Modify((originalValue, otherValue) => Mathf.Clamp01(originalValue.Value + otherValue.Value), other);
    }

    public NoiseMap Add(float value)
    {
        return Modify((originalValue, otherValue) => Mathf.Clamp01(originalValue.Value + otherValue.Value), new NoiseSample(value));
    }

    public NoiseMap Subtract(NoiseMap other)
    {
        return Modify((originalValue, otherValue) => Mathf.Clamp01(originalValue.Value - otherValue.Value), other);
    }

    public NoiseMap Subtract(float value)
    {
        return Modify((originalValue, otherValue) => Mathf.Clamp01(originalValue.Value - otherValue.Value), new NoiseSample(value));
    }

    public NoiseMap Multiply(NoiseMap other)
    {
        return Modify((originalValue, otherValue) => Mathf.Clamp01(originalValue.Value * otherValue.Value), other);
    }

    public NoiseMap Multiply(float value)
    {
        return Modify((originalValue, otherValue) => Mathf.Clamp01(originalValue.Value * otherValue.Value), new NoiseSample(value));
    }

    public float ValueAt(int x, int y)
    {
        return this.Map[x][y].Value;
    }

    public void ForEach(Action<NoiseSample, int, int> action)
    {
        for (var x = 0; x < this.Width; x++)
        {
            for (var y = 0; y < this.Height; y++)
            {
                action.Invoke(this.Map[x][y], x, y);
            }
        }
    }

    public List<Vector2Int> PositionsWhere(Func<NoiseSample, int, int, bool> predicate)
    {
        var positions = new List<Vector2Int>();

        ForEach((noiseSample, x, y) =>
        {
            if (predicate.Invoke(noiseSample, x, y))
                positions.Add(new Vector2Int(x, y));
        });

        return positions;
    }

    public float CalculateMinValue()
    {
        var minValue = float.MaxValue;

        ForEach((noiseSample, _, __) =>
        {
            if (noiseSample.Value < minValue)
                minValue = noiseSample.Value;
        });

        return minValue;
    }

    public float CalculateMaxValue()
    {
        var maxValue = float.MinValue;

        ForEach((noiseSample, _, __) =>
        {
            if (noiseSample.Value > maxValue)
                maxValue = noiseSample.Value;
        });

        return maxValue;
    }

    public void UpdateMinMaxValue(float min, float max)
    {
        this.MinValue = min;
        this.MaxValue = max;
    }

    public Texture2D ToTexture()
    {
        return ToTexture(this.Width, this.Height);
    }

    public Texture2D ToTexture(int width, int height)
    {
        var texture = new Texture2D(width, height);

        for (var x = 0; x < this.Width; x++)
        {
            for (var y = 0; y < this.Height; y++)
            {
                var sampleValue = this.Map[x][y].Value;
                var color = new Color(1 - sampleValue, 1 - sampleValue, 1 - sampleValue);
                texture.SetPixel(x, y, color);
            }
        }

        texture.filterMode = FilterMode.Point;
        texture.Apply();

        return texture;
    }

    private NoiseMap GetNoiseMapCopy()
    {
        return new NoiseMap(new List<List<NoiseSample>>(this.Map), this.Width, this.Height, this.MinValue, this.MaxValue);
    }
}
