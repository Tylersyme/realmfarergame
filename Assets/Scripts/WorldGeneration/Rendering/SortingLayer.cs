﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class SortingLayers
{
    public const string TILEMAP_TILES = "TilemapTiles";
    public const string TILEMAP_DECORATIONS = "TilemapDecorations";
    public const string TILEMAP_OBJECTS = "TilemapObjects";
}
