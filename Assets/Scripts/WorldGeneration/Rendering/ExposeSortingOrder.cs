﻿
using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshRenderer))]
public class ExposeSortingOrder : MonoBehaviour
{
    // This script is made for use in the editor only and has no bearing on gameplay
    #if UNITY_EDITOR

    [SerializeField]
    [ReadOnly]
    private int _sortingOrder;

    private void Update()
    {
        _sortingOrder = GetMeshRenderer().sortingOrder;
    }

    private MeshRenderer GetMeshRenderer()
    {
        return this.gameObject.GetComponent<MeshRenderer>();
    }

    #endif
}
