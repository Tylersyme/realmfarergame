﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DynamicMaterialTextureComparer : IEqualityComparer<DynamicMaterialTexture>
{
    public bool Equals(DynamicMaterialTexture x, DynamicMaterialTexture y)
    {
        return x.Texture.imageContentsHash.CompareTo(y.Texture.imageContentsHash) == 0;
    }

    public int GetHashCode(DynamicMaterialTexture obj)
    {
        int hash = 17;

        hash = hash * 23 + obj.MaterialRecord.GetHashCode();

        //hash = hash * 23 + obj.Texture.imageContentsHash.GetHashCode();

        return hash;
    }
}
