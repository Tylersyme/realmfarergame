﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IMeshService
{
    (GameObject, MeshRenderer, MeshFilter) CreateMeshObject(
        Mesh mesh,
        Material material,
        int sortingOrder = 0,
        string gameObjectName = "MeshObject",
        Transform parentTransform = null);
}
