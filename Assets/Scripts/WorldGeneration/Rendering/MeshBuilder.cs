﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class MeshBuilder
{
    public List<Vector3> Vertices { get; private set; }

    public List<int> Triangles { get; private set; }

    public List<Color> Colors { get; private set; }

    public List<Vector2> Uvs { get; private set; }

    public Mesh Mesh { get; private set; }

    public bool Use32BitVertexCount { get; private set; } = false;

    public short SortingOrder { get; private set; } = 0;

    public MeshBuilder()
    {
        this.Vertices = new List<Vector3>();
        this.Triangles = new List<int>();
        this.Colors = new List<Color>();
        this.Uvs = new List<Vector2>();
        this.Mesh = new Mesh();
    }

    public void AddTriangle(int vertexIndex, int a, int b, int c)
    {
        this.Triangles.Add(vertexIndex + a);
        this.Triangles.Add(vertexIndex + b);
        this.Triangles.Add(vertexIndex + c);
    }

    public void NewMesh()
    {
        Object.Destroy(this.Mesh);

        this.Mesh = new Mesh();
    }

    public void Clear()
    {
        this.Vertices.Clear();
        this.Triangles.Clear();
        this.Colors.Clear();

        NewMesh();
    }

    public MeshBuilder SetUse32BitVertexCount()
    {
        this.Use32BitVertexCount = true;

        return this;
    }

    public MeshBuilder SetSortingOrder(short sortingOrder)
    {
        this.SortingOrder = sortingOrder;

        return this;
    }
    
    /// <summary>
    /// Calculates Uvs with the assumption that the mesh is not stretching the texture.
    /// </summary>
    /// <returns></returns>
    protected Vector2[] GenerateUvs(List<Vector3> vertices)
    {
        var uvs = new Vector2[0];

        var xMax = vertices
            .Select(v => v.x)
            .Max();

        var yMax = vertices
            .Select(v => v.y)
            .Max();

        //for (var vertexIdx = 0; vertexIdx < vertices.Count; vertexIdx++)
        //{
        //    var vertex = vertices[vertexIdx];

        //    uvs[vertexIdx] = new Vector2(vertex.x / (float)xMax, vertex.y / (float)yMax);

        //    //uvs[vertexIdx] = new Vector2(123980, -21380);
        //}

        //uvs[0] = new Vector2(0, 0);

        return uvs;
    }

    public Mesh Build()
    {
        if (this.Use32BitVertexCount)
            this.Mesh.indexFormat = UnityEngine.Rendering.IndexFormat.UInt32;

        this.Mesh.SetVertices(this.Vertices);
        this.Mesh.SetTriangles(this.Triangles, 0);
        this.Mesh.uv = this.Uvs.ToArray();

        if (this.Colors.Any())
            this.Mesh.SetColors(this.Colors);

        return this.Mesh;
    }
}
