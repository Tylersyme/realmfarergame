﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class TileChunkRenderer : MonoBehaviour
{
    [SerializeField]
    private Vector2Int _bottomLeft;
    public Vector2Int BottomLeft { get => _bottomLeft; set => _bottomLeft = value; }

    public Vector2Int TopRight { get => this.BottomLeft + this.ChunkTileSize; }

    /// <summary>
    /// Represents the dimensions of the tiles this renderer is responsible for.
    /// </summary>
    [SerializeField]
    private Vector2Int _chunkTileSize;
    public Vector2Int ChunkTileSize { get => _chunkTileSize; set => _chunkTileSize = value; }

    public bool WillCombineTileMeshes { get; set; } = true;

    private Vector2 ChunkSize { get => this.TileSize * this.ChunkTileSize; }

    private Vector2Int TilePixelSize { get => TilemapManager.Instance.TilePixelSize; }

    private Vector2 TileSize { get => TilemapManager.Instance.TileSize; }

    private Vector2Int TextureSize { get => this.TilePixelSize * this.ChunkTileSize; }

    private IMeshService _meshService;
    private ITileBitmaskService _tileBitmaskService;

    private void Awake()
    {
        _meshService = new MeshService();
        _tileBitmaskService = new TileBitmaskService();
    }

    public void Render(Tilemap tilemap)
    {
        // Destroys any existing mesh renderers which will be replaced with the ones created here
        this.DestroyMeshRenderers();

        // Dynamic material textures of the same type will all be combined into the same mesh
        var tileMeshBuilders = new Dictionary<DynamicMaterialTexture, TileMeshBuilder>(new DynamicMaterialTextureComparer());

        for (var x = this.BottomLeft.x; x < this.TopRight.x; x++)
        {
            for (var y = this.BottomLeft.y; y < this.TopRight.y; y++)
            {
                var tilePosition = new Vector2Int(x, y);

                var tiles = tilemap.GetTilesAlongZ(tilePosition);

                for (var z = 0; z < tiles.Count; z++)
                {
                    var tile = tiles[z];

                    var tileTemplate = tile.Template;

                    // Optionally set the texture based upon its neighbors
                    if (tile.Template.RenderData.HasBitmaskTextureGroup)
                    {
                        var bitmaskTexture = _tileBitmaskService.GetBitmaskTextureFromTile(tile, tilePosition);

                        tile.UpdateDynamicTexture(bitmaskTexture);
                    }

                    var meshBuilder = default(TileMeshBuilder);

                    tileMeshBuilders.TryGetValue(tile.DynamicMaterialTexture, out meshBuilder);

                    // Add new mesh builder if one does not exist for this tile type
                    if (meshBuilder == null)
                    {
                        meshBuilder = new TileMeshBuilder();

                        // Use 32 bit index to increase max vertex count
                        meshBuilder.SetUse32BitVertexCount();

                        meshBuilder.SetSortingOrder((short)z);

                        tileMeshBuilders.Add(tile.DynamicMaterialTexture, meshBuilder);
                    }

                    // The tile's position relative to this chunk's bottom left corner tile position
                    // This way the first tile will always have a relative position of 0,0
                    var tileRelativePosition = new Vector2Int(x - this.BottomLeft.x, y - this.BottomLeft.y);

                    var tileLocalPosition = tileRelativePosition * this.TileSize;

                    var textureSize = tile.DynamicMaterialTexture.Texture.GetPixelSize();

                    // Create the quad mesh of the tile consisting of four corner vertices and two triangles
                    // The material will be overlaid on this mesh to render the tile's texture
                    meshBuilder.AddTileQuadMesh(tileLocalPosition, this.TileSize, textureSize, new Vector2Int(x, y));
                }
            }
        }

        foreach (var keyValue in tileMeshBuilders)
        {
            var tileMesh = keyValue.Value.Build();

            // Set the material of the decoration mesh to the generated material
            var tileMaterial = keyValue.Key.CreateMaterial();

            (var meshObject, var meshRenderer, var meshFilter) = _meshService.CreateMeshObject(
                tileMesh,
                tileMaterial,
                keyValue.Value.SortingOrder,
                "TileMesh",
                this.transform);

            meshObject.transform.localPosition = new Vector3(-this.TileSize.x, -this.TileSize.y, this.transform.position.z + 1);

            meshRenderer.sortingLayerName = SortingLayers.TILEMAP_TILES;
        }

        GenerateDecorations(tilemap);
    }

    private void GenerateDecorations(Tilemap tilemap)
    {
        // Dynamic material textures of the same type will all be combined into the same mesh
        var decorationMeshBuilders = new Dictionary<DynamicMaterialTexture, TileMeshBuilder>(new DynamicMaterialTextureComparer());

        for (var x = this.BottomLeft.x; x < this.TopRight.x; x++)
        {
            for (var y = this.BottomLeft.y; y < this.TopRight.y; y++)
            {
                var decorationTilePosition = new Vector3Int(x, y, 0);

                // TODO: Currently all decorations are assumed to be at z = 0. Remove this assumption
                var decorationTiles = tilemap.GetDecorationTilesAt(x, y);

                foreach (var decorationTile in decorationTiles)
                {
                    var decorationTileTemplate = decorationTile.Template;

                    // Optionally set the texture based upon its neighbors
                    if (decorationTile.Template.RenderData.HasBitmaskTextureGroup)
                    {
                        var bitmaskTexture = _tileBitmaskService.GetBitmaskTextureFromDecorationTile(
                            decorationTile, 
                            decorationTilePosition.AsVector2Int());

                        decorationTile.UpdateDynamicTexture(bitmaskTexture);
                    }

                    var decorationMeshBuilder = default(TileMeshBuilder);

                    decorationMeshBuilders.TryGetValue(decorationTile.DynamicMaterialTexture, out decorationMeshBuilder);

                    // Add new mesh builder if one does not exist for this decoration type
                    if (decorationMeshBuilder == null)
                    {
                        decorationMeshBuilder = new TileMeshBuilder();

                        // Use 32 bit index to increase max vertex count
                        decorationMeshBuilder.SetUse32BitVertexCount();

                        decorationMeshBuilders.Add(decorationTile.DynamicMaterialTexture, decorationMeshBuilder);
                    }

                    // The tile's position relative to this chunk's bottom left corner tile position
                    // This way the first tile will always have a relative position of 0,0
                    var tileRelativePosition = new Vector2Int(x - this.BottomLeft.x, y - this.BottomLeft.y);

                    var tileLocalPosition = tileRelativePosition * this.TileSize;

                    var textureSize = decorationTileTemplate.RenderData.BaseTexture.GetPixelSize();

                    decorationMeshBuilder.AddTileQuadMesh(tileLocalPosition, this.TileSize, textureSize, new Vector2Int(x, y));
                }
            }
        }

        foreach (var keyValue in decorationMeshBuilders)
        {
            var decorationMesh = keyValue.Value.Build();

            // Set the material of the decoration mesh to the generated material
            var decorationMaterial = keyValue.Key.CreateMaterial();

            (var meshObject, var meshRenderer, var meshFilter) = _meshService.CreateMeshObject(
                decorationMesh, 
                decorationMaterial, 
                -20000, 
                "DecorationMesh", 
                this.transform);

            meshObject.transform.localPosition = new Vector3(-this.TileSize.x, -this.TileSize.y, this.transform.position.z);

            meshRenderer.sortingLayerName = SortingLayers.TILEMAP_DECORATIONS;
        }
    }

    private class OrderedCombineInstances
    {
        public List<CombineInstance> CombineInstances { get; set; }
        public int SortingOrder { get; set; }
    }

    private void CombineTileMeshes()
    {
        var tileMeshFilters = this.transform.GetComponentsInChildren<MeshFilter>();
        var tileMeshRenderers = this.transform.GetComponentsInChildren<MeshRenderer>();

        var combinedTileMeshObject = new GameObject("TileMesh");

        combinedTileMeshObject.transform.SetParent(this.transform);

        var combinedTileMeshRenderer = combinedTileMeshObject.AddComponent<MeshRenderer>();
        combinedTileMeshObject.AddComponent<ExposeSortingOrder>();

        // This mesh filter will contain the combined meshes
        var combinedTileMeshFilter = combinedTileMeshObject.AddComponent<MeshFilter>();

        // Create a submesh combine instance list for each material
        var submeshCombineInstances = new Dictionary<Material, OrderedCombineInstances>();
        foreach (var meshRenderer in tileMeshRenderers)
        {
            if (submeshCombineInstances.ContainsKey(meshRenderer.sharedMaterial))
                continue;

            submeshCombineInstances.Add(
                meshRenderer.sharedMaterial, 
                new OrderedCombineInstances
                {
                    CombineInstances = new List<CombineInstance>(),
                    SortingOrder = meshRenderer.sortingOrder
                });
        }

        // Combine each mesh of the same material into its corresponding submesh
        foreach (var tileMeshFilter in tileMeshFilters)
        {
            var filterRenderer = tileMeshFilter.gameObject.GetComponent<MeshRenderer>();

            var combineInstance = new CombineInstance();

            combineInstance.mesh = tileMeshFilter.sharedMesh;
            combineInstance.subMeshIndex = 0;
            combineInstance.transform = tileMeshFilter.transform.localToWorldMatrix;

            // Add the new combine instance to the list of the combine instances that make up the submesh
            // for that material
            submeshCombineInstances[filterRenderer.sharedMaterial].CombineInstances.Add(combineInstance);

            Object.Destroy(tileMeshFilter.gameObject);
        }

        var submeshes = new List<Mesh>();
        foreach (var entry in submeshCombineInstances.OrderBy(c => c.Value.SortingOrder))
        {
            var submesh = new Mesh();
            submesh.indexFormat = UnityEngine.Rendering.IndexFormat.UInt32;

            var combineInstances = entry.Value.CombineInstances;
            submesh.CombineMeshes(combineInstances.ToArray(), true);

            submeshes.Add(submesh);
        }

        var fullMeshCombineInstances = new List<CombineInstance>();
        for (var submeshIdx = 0; submeshIdx < submeshes.Count; submeshIdx++)
        {
            var fullMeshCombineInstance = new CombineInstance();

            fullMeshCombineInstance.mesh = submeshes[submeshIdx];
            fullMeshCombineInstance.subMeshIndex = 0;
            fullMeshCombineInstance.transform = Matrix4x4.identity;

            fullMeshCombineInstances.Add(fullMeshCombineInstance);
        }

        var fullMesh = new Mesh();
        fullMesh.indexFormat = UnityEngine.Rendering.IndexFormat.UInt32;
        fullMesh.CombineMeshes(fullMeshCombineInstances.ToArray(), false);

        combinedTileMeshFilter.sharedMesh = fullMesh;

        combinedTileMeshRenderer.sharedMaterials = submeshCombineInstances
            .OrderBy(e => e.Value.SortingOrder)
            .Select(e => e.Key)
            .ToArray();

        // Sorting order actually has a min/max value of a short
        combinedTileMeshRenderer.sortingOrder = short.MinValue;
    }

    private void DestroyMeshRenderers() =>
        this.gameObject
            .GetChildren()
            .ForEach(go => go.DestroySelf());

    private Renderer GetRenderer()
    {
        return this.gameObject.GetComponent<Renderer>();
    }
}
