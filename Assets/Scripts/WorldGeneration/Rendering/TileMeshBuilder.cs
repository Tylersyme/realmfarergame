﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class TileMeshBuilder : MeshBuilder
{
    public void AddTileQuadMesh(Vector2 tileLocalPosition, Vector2 tileSize, Vector2Int textureSize, Vector2Int tilemapPosition)
    {
        var vertexIndex = this.Vertices.Count;

        // Create the four vertices to represent the four corners of the tile mesh
        this.Vertices.Add(new Vector3(tileLocalPosition.x, tileLocalPosition.y));
        this.Vertices.Add(new Vector3(tileLocalPosition.x, tileLocalPosition.y + tileSize.y));
        this.Vertices.Add(new Vector3(tileLocalPosition.x + tileSize.x, tileLocalPosition.y + tileSize.y));
        this.Vertices.Add(new Vector3(tileLocalPosition.x + tileSize.x, tileLocalPosition.y));

        var uvs = GetTileQuadUvs(textureSize, tilemapPosition);

        this.Uvs.AddRange(uvs);

        // Color each corner vertex as white which means the texture will not be tinted
        this.Colors.Add(Color.white);
        this.Colors.Add(Color.white);
        this.Colors.Add(Color.white);
        this.Colors.Add(Color.white);

        // Create triangles which slice the mesh into two pieces
        this.AddTriangle(vertexIndex, 0, 1, 2);
        this.AddTriangle(vertexIndex, 0, 2, 3);
    }

    /// <summary>
    /// Allows textures which are larger than a tile to be overlaid atop tile quad meshes in the correct order.
    /// </summary>
    /// <param name="textureSize"></param>
    /// <param name="tilemapPosition"></param>
    /// <returns></returns>
    private List<Vector2> GetTileQuadUvs(Vector2Int textureSize, Vector2Int tilemapPosition)
    {
        var uvs = new List<Vector2>();

        // The number of times the texture may be sliced into tile sized squares across the x and y axes
        var slicedTextureCountX = (float)(textureSize.x / TilemapManager.Instance.TilePixelSize.x);
        var slicedTextureCountY = (float)(textureSize.y / TilemapManager.Instance.TilePixelSize.y);

        // A texture whose size is twice the size of a tile will be broken up into a percentage of 1 / 2 = 0.5
        // which is the uv coordinate that will be used to determine which slice of the texture is used
        var textureSliceUvPercentageX = 1 / slicedTextureCountX;
        var textureSliceUvPercentageY = 1 / slicedTextureCountY;

        var xTilePosition = tilemapPosition.x % slicedTextureCountX;
        var yTilePosition = tilemapPosition.y % slicedTextureCountY;

        var bottomLeftUv = new Vector2(xTilePosition / slicedTextureCountX, yTilePosition / slicedTextureCountY);

        // Bottom Left
        uvs.Add(new Vector2(bottomLeftUv.x, bottomLeftUv.y));
        // Top Left
        uvs.Add(new Vector2(bottomLeftUv.x, bottomLeftUv.y + textureSliceUvPercentageY));
        // Top Right
        uvs.Add(new Vector2(bottomLeftUv.x + textureSliceUvPercentageX, bottomLeftUv.y + textureSliceUvPercentageY));
        // Bottom Right
        uvs.Add(new Vector2(bottomLeftUv.x + textureSliceUvPercentageX, bottomLeftUv.y));

        return uvs;
    }

    public void AddEdgeBlendMesh(
        Vector2 tileWorldPosition,
        Vector2 tileSize,
        Direction blendDirection, 
        float blendIntensity,
        List<int> noColorVertices)
    {
        var vertexCount = this.Vertices.Count;
        var tileHalfSize = tileSize * 0.5f;

        // Offset the blend mesh so that it overlaps the tile that is being blended into
        var blendMeshWorldPosition = tileWorldPosition + (blendDirection.RelativeDirection * tileSize);

        var v0 = new Vector3(blendMeshWorldPosition.x, blendMeshWorldPosition.y); // 0
        var v1 = new Vector3(blendMeshWorldPosition.x + tileHalfSize.x, blendMeshWorldPosition.y); // 1
        var v2 = new Vector3(blendMeshWorldPosition.x + tileSize.x, blendMeshWorldPosition.y); // 2
        var v3 = new Vector3(blendMeshWorldPosition.x, blendMeshWorldPosition.y + tileHalfSize.y); // 3
        var v4 = new Vector3(blendMeshWorldPosition.x + tileHalfSize.x, blendMeshWorldPosition.y + tileHalfSize.y); // 4
        var v5 = new Vector3(blendMeshWorldPosition.x + tileSize.x, blendMeshWorldPosition.y + tileHalfSize.y); // 5
        var v6 = new Vector3(blendMeshWorldPosition.x, blendMeshWorldPosition.y + tileSize.y); // 6
        var v7 = new Vector3(blendMeshWorldPosition.x + tileHalfSize.x, blendMeshWorldPosition.y + tileSize.y); // 7
        var v8 = new Vector3(blendMeshWorldPosition.x + tileSize.x, blendMeshWorldPosition.y + tileSize.y); // 8

        var intensityVertexDistance = blendIntensity * tileSize;

        if (blendDirection == Direction.NORTH)
        {
            v3 = v3.Add(new Vector3(0, -tileHalfSize.y + intensityVertexDistance.y));
            v4 = v4.Add(new Vector3(0, -tileHalfSize.y + intensityVertexDistance.y));
            v5 = v5.Add(new Vector3(0, -tileHalfSize.y + intensityVertexDistance.y));
        }
        else if (blendDirection == Direction.SOUTH)
        {
            v3 = v3.Add(new Vector3(0, tileHalfSize.y - intensityVertexDistance.y));
            v4 = v4.Add(new Vector3(0, tileHalfSize.y - intensityVertexDistance.y));
            v5 = v5.Add(new Vector3(0, tileHalfSize.y - intensityVertexDistance.y));
        }
        else if (blendDirection == Direction.EAST)
        {
            v1 = v1.Add(new Vector3(-tileHalfSize.x + intensityVertexDistance.x, 0));
            v4 = v4.Add(new Vector3(-tileHalfSize.x + intensityVertexDistance.x, 0));
            v7 = v7.Add(new Vector3(-tileHalfSize.x + intensityVertexDistance.x, 0));
        }
        else if (blendDirection == Direction.WEST)
        {
            v1 = v1.Add(new Vector3(tileHalfSize.x - intensityVertexDistance.x, 0));
            v4 = v4.Add(new Vector3(tileHalfSize.x - intensityVertexDistance.x, 0));
            v7 = v7.Add(new Vector3(tileHalfSize.x - intensityVertexDistance.x, 0));
        }
        else if (blendDirection == Direction.NORTH_EAST)
        {
            v1 = v1.Add(new Vector3(-tileHalfSize.x + intensityVertexDistance.x, 0));
            v3 = v3.Add(new Vector3(0, -tileHalfSize.y + intensityVertexDistance.y));
            v4 = v4.Add(new Vector3(-tileHalfSize.x + intensityVertexDistance.x, -tileHalfSize.y + intensityVertexDistance.y));
        }
        else if (blendDirection == Direction.NORTH_WEST)
        {
            v1 = v1.Add(new Vector3(tileHalfSize.x - intensityVertexDistance.x, 0));
            v4 = v4.Add(new Vector3(tileHalfSize.x - intensityVertexDistance.x, -tileHalfSize.y + intensityVertexDistance.y));
            v5 = v5.Add(new Vector3(0, -tileHalfSize.y + intensityVertexDistance.y));
        }
        else if (blendDirection == Direction.SOUTH_EAST)
        {
            v3 = v3.Add(new Vector3(0, tileHalfSize.y - intensityVertexDistance.y));
            v4 = v4.Add(new Vector3(-tileHalfSize.x + intensityVertexDistance.x, tileHalfSize.y - intensityVertexDistance.y));
            v7 = v7.Add(new Vector3(-tileHalfSize.x + intensityVertexDistance.x, 0));
        }
        else if (blendDirection == Direction.SOUTH_WEST)
        {
            v4 = v4.Add(new Vector3(tileHalfSize.x - intensityVertexDistance.x, tileHalfSize.y - intensityVertexDistance.y));
            v5 = v5.Add(new Vector3(0, tileHalfSize.y - intensityVertexDistance.y));
            v7 = v7.Add(new Vector3(tileHalfSize.x - intensityVertexDistance.x, 0));
        }

        this.Vertices.Add(v0);
        this.Vertices.Add(v1);
        this.Vertices.Add(v2);
        this.Vertices.Add(v3);
        this.Vertices.Add(v4);
        this.Vertices.Add(v5);
        this.Vertices.Add(v6);
        this.Vertices.Add(v7);
        this.Vertices.Add(v8);

        //var uvs = base.GenerateUvs(this.Vertices.GetRange(this.Vertices.Count - 9, 9));
        //this.Uvs.AddRange(uvs);

        if (blendDirection.IsDiagonal)
        {
            this.AddTriangle(vertexCount, 2, 1, 4);
            this.AddTriangle(vertexCount, 2, 4, 5);

            this.AddTriangle(vertexCount, 0, 3, 4);
            this.AddTriangle(vertexCount, 0, 4, 1);
        }
        else
        {
            this.AddTriangle(vertexCount, 2, 1, 5);
            this.AddTriangle(vertexCount, 1, 4, 5);

            this.AddTriangle(vertexCount, 1, 0, 3);
            this.AddTriangle(vertexCount, 3, 4, 1);
        }

        this.AddTriangle(vertexCount, 6, 4, 3);
        this.AddTriangle(vertexCount, 6, 7, 4);
        this.AddTriangle(vertexCount, 4, 7, 8);
        this.AddTriangle(vertexCount, 4, 8, 5);

        var vertexColors = GetEdgeBlendColorsFromDirection(blendDirection);

        if (noColorVertices == null || !noColorVertices.Any())
        {
            this.Colors.AddRange(vertexColors);
        }
        else
        {
            for (var vertexColorIdx = 0; vertexColorIdx < vertexColors.Length; vertexColorIdx++)
            {
                if (noColorVertices.Contains(vertexColorIdx))
                    this.Colors.Add(Color.clear);
                else
                    this.Colors.Add(vertexColors[vertexColorIdx]);
            }
        }
    }

    public List<int> GetEdgeIntensityVertices(Direction blendDirection)
    {
        var vertices = new List<int>();

        if (blendDirection == Direction.NORTH)
        {
            vertices.Add(3);
            vertices.Add(4);
            vertices.Add(5);
        }
        else if (blendDirection == Direction.SOUTH)
        {
            vertices.Add(3);
            vertices.Add(4);
            vertices.Add(5);
        }
        else if (blendDirection == Direction.EAST)
        {
            vertices.Add(1);
            vertices.Add(4);
            vertices.Add(7);
        }
        else if (blendDirection == Direction.WEST)
        {
            vertices.Add(1);
            vertices.Add(4);
            vertices.Add(7);
        }
        else if (blendDirection == Direction.NORTH_EAST)
        {
            vertices.Add(4);
        }
        else if (blendDirection == Direction.NORTH_WEST)
        {
            vertices.Add(4);
        }
        else if (blendDirection == Direction.SOUTH_EAST)
        {
            vertices.Add(4);
        }
        else if (blendDirection == Direction.SOUTH_WEST)
        {
            vertices.Add(4);
        }

        return vertices;
    }

    public List<int> GetEdgeBlendVerticesToColor(Direction blendDirection)
    {
        var vertices = new List<int>();

        if (blendDirection == Direction.NORTH)
        {
            vertices.Add(0);
            vertices.Add(1);
            vertices.Add(2);
        }
        else if (blendDirection == Direction.SOUTH)
        {
            vertices.Add(6);
            vertices.Add(7);
            vertices.Add(8);
        }
        else if (blendDirection == Direction.EAST)
        {
            vertices.Add(0);
            vertices.Add(3);
            vertices.Add(6);
        }
        else if (blendDirection == Direction.WEST)
        {
            vertices.Add(2);
            vertices.Add(5);
            vertices.Add(8);
        }
        else if (blendDirection == Direction.NORTH_EAST)
        {
            vertices.Add(0);
        }
        else if (blendDirection == Direction.NORTH_WEST)
        {
            vertices.Add(2);
        }
        else if (blendDirection == Direction.SOUTH_EAST)
        {
            vertices.Add(6);
        }
        else if (blendDirection == Direction.SOUTH_WEST)
        {
            vertices.Add(8);
        }

        return vertices;
    }

    public Color[] GetEdgeBlendColorsFromDirection(Direction blendDirection)
    {
        var colors = new Color[9] 
        {
            Color.clear,
            Color.clear,
            Color.clear,
            Color.clear,
            Color.clear,
            Color.clear,
            Color.clear,
            Color.clear,
            Color.clear,
        };

        var vertexIndices = GetEdgeBlendVerticesToColor(blendDirection);

        foreach (var vertexIndex in vertexIndices)
            colors[vertexIndex] = Color.white;

        return colors;
    }
}
