﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Represents a texture which will be dynamically applied to the material at runtime.
/// </summary>
[System.Serializable]
public class DynamicMaterialTexture
{
    /// <summary>
    /// The record to the material whose texture will be set to the texture.
    /// </summary>
    [SerializeField]
    private DatabaseRecord<MaterialContainer> _materialRecord;
    public DatabaseRecord<MaterialContainer> MaterialRecord { get => _materialRecord; set => _materialRecord = value; }

    /// <summary>
    /// The record to the texture which will be applied to the material.
    /// </summary>
    [SerializeField]
    private Texture2D _texture;
    public Texture2D Texture { get => _texture; set => _texture = value; }

    public DynamicMaterialTexture() { }

    /// <summary>
    /// Returns a new material based upon the material record with its main texture being set to the texture.
    /// </summary>
    /// <returns></returns>
    public Material CreateMaterial()
    {
        // Copy the material so that a texture can be applied
        var material = new Material(ResourceManager.Instance.GetRecord(this.MaterialRecord).Material);

        material.mainTexture = this.Texture;

        return material;
    }
}
