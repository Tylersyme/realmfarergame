﻿using Sirenix.OdinInspector;
using Sirenix.Serialization;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// Stores all information regarding how a tile should be rendered.
/// </summary>
[System.Serializable]
public class TileRenderData
{
    [SerializeField]
    [InfoBox("This material record should contain no texture. Use BaseTextureRecord instead.")]
    [Required]
    private MaterialContainer _baseMaterial;
    public MaterialContainer BaseMaterial { get => _baseMaterial; }

    [SerializeField]
    [ShowIf(nameof(ShowBaseTexture))]
    private Texture2D _baseTexture;
    public Texture2D BaseTexture
    {
        get
        {
            if (this.HasBitmaskTextureGroup)
                return this.BitmaskTextureGroup.Textures.FirstOrDefault();
            else if (this.HasVarietyTextures)
                return this.VarietyTextureGroup.GetTextureByIndex(0);

            return _baseTexture;
        }
    }

    [SerializeField]
    [OnValueChanged(nameof(HasBitmaskTextureGroup_Changed))]
    private bool _hasBitmaskTextureGroup;
    public bool HasBitmaskTextureGroup { get => _hasBitmaskTextureGroup; private set => _hasBitmaskTextureGroup = value; }

    /// <summary>
    /// Whether the tile has interchangeable textures for variety.
    /// </summary>
    [SerializeField]
    private bool _hasVarietyTextures = false;
    public bool HasVarietyTextures { get => _hasVarietyTextures; set => _hasVarietyTextures = value; }

    [SerializeField]
    [ShowIf(nameof(HasBitmaskTextureGroup))]
    private BitmaskAdjacencyMode _bitmaskAdjacencyMode = BitmaskAdjacencyMode.NONE;
    public BitmaskAdjacencyMode BitmaskAdjacencyMode { get => _bitmaskAdjacencyMode; private set => _bitmaskAdjacencyMode = value; }

    [SerializeField]
    private bool _useLargerTexture = false;
    public bool UseLargerTexture { get => _useLargerTexture; set => _useLargerTexture = value; }

    [SerializeField]
    [ShowIf(nameof(UseLargerTexture))]
    private Vector2Int _textureSize;
    public Vector2Int TextureSize { get => _textureSize; set => _textureSize = value; }

    /// <summary>
    /// Contains the other versions of the same tile whose edges conform to the adjacent tiles.
    /// </summary>
    [SerializeField]
    [ShowIf(nameof(HasBitmaskTextureGroup))]
    private BitmaskingTextureGroup _bitmaskTextureGroup;
    public BitmaskingTextureGroup BitmaskTextureGroup { get => _bitmaskTextureGroup; private set => _bitmaskTextureGroup = value; }

    [SerializeField]
    [Required]
    [ShowIf(nameof(HasVarietyTextures))]
    private VarietyTextureGroup _varietyTextureGroup;
    public VarietyTextureGroup VarietyTextureGroup { get => _varietyTextureGroup; set => _varietyTextureGroup = value; }

#if UNITY_EDITOR

    private bool ShowBaseTexture { get => !this.HasBitmaskTextureGroup && !this.HasVarietyTextures; }

    private void HasBitmaskTextureGroup_Changed(bool value)
    {
        if (!value)
            this.BitmaskAdjacencyMode = BitmaskAdjacencyMode.NONE;
        else
            this.BitmaskAdjacencyMode = BitmaskAdjacencyMode.FOUR_SIDED;
    }

#endif
}
