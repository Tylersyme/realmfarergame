﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Represents a group of textures which will be assigned dynamically to the material
/// when the tilemap is being constructed. This simply acts as a mapping from a
/// texture to the material it will be assigned to.
/// </summary>
public class TextureMaterialGroup
{
    public List<DatabaseRecord<TextureContainer>> Textures { get; set; }

    public DatabaseRecord<MaterialContainer> MaterialRecord { get; set; }
}
