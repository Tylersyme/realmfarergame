﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshService : IMeshService
{
    public (GameObject, MeshRenderer, MeshFilter) CreateMeshObject(
        Mesh mesh, 
        Material material, 
        int sortingOrder = 0, 
        string gameObjectName = "MeshObject",
        Transform parentTransform = null)
    {
        var meshObject = new GameObject(gameObjectName);

        if (parentTransform != null)
            meshObject.transform.SetParent(parentTransform);

        var meshFilter = meshObject.AddComponent<MeshFilter>();
        meshFilter.mesh = mesh;

        var meshRenderer = meshObject.AddComponent<MeshRenderer>();

        // Set the material of the decoration mesh to the generated material
        meshRenderer.material = material;

        meshRenderer.sortingOrder = sortingOrder;

        return (meshObject, meshRenderer, meshFilter);
    }
}
