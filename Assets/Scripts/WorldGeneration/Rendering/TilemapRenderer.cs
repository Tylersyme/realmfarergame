﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class TilemapRenderer : MonoBehaviour
{
    [SerializeField]
    private bool _combineTileMeshes = true;
    public bool CombineTileMeshes { get => _combineTileMeshes; set => _combineTileMeshes = value; }

    private Vector2 TileSize { get => TilemapManager.Instance.TileSize; }

    private Vector2Int TilesPerMesh { get => TilemapManager.Instance.TilesPerMesh; }

    private Tilemap Tilemap { get; set; }

    private List<List<GameObject>> Quads { get; set; }

    private IHubSubscriptionCache _hubSubscriptionCache;
    private ITilemapTileFilterService _tilemapTileFilterService;

    private void Awake()
    {
        _hubSubscriptionCache = new HubSubscriptionCache();
        _tilemapTileFilterService = new TilemapTileFilterService();

        this.Subscribe();
    }

    private void OnDestroy() =>
        _hubSubscriptionCache.UnsubscribeAll();

    private Vector2 QuadScale
    {
        get 
        {
            var width = this.TileSize.x * this.TilesPerMesh.x;
            var height = this.TileSize.y * this.TilesPerMesh.y;

            return new Vector2(width, height);
        }
    }

    public void LoadTilemap(Tilemap tilemap)
    {
        this.Tilemap = tilemap;

        DestroyQuads();
        this.Quads = CreateQuads();
    }

    public void Render()
    {
        if (this.Tilemap == null)
            throw new System.NullReferenceException("Error rendering tilemap: The tilemap is null. Use LoadTilemap() before rendering.");

        RenderQuads();
    }

    private void UpdateAtTilemapPosition(Vector3Int tilePosition)
    {
        var quadContainingTilePosition = this.GetQuadAtTilemapPosition(tilePosition.AsVector2Int());

        var tile = this.Tilemap.GetTileAt(tilePosition);

        // If the tile is placed on the edge of a chunk, it may cause the adjacent tiles in the
        // adjacent chunks to change as well
        var adjacentTilemapPositions = _tilemapTileFilterService
            .GetAdjacentTilesOfType(this.Tilemap, tilePosition, tile.Type);

        var quadsToUpdate = adjacentTilemapPositions
            .Select(p => this.GetQuadAtTilemapPosition(p.AsVector2Int()))
            .Append(quadContainingTilePosition)
            .Distinct()
            .ToList();

        quadsToUpdate.ForEach(q => q
            .GetComponent<TileChunkRenderer>()
            .Render(this.Tilemap));
    }

    private void RenderQuads()
    {
        foreach (var quadRow in this.Quads)
        {
            foreach (var tileChunkRenderer in quadRow.Select(go => go.GetComponent<TileChunkRenderer>()))
            {
                tileChunkRenderer.Render(this.Tilemap);
            }
        }
    }

    private List<List<GameObject>> CreateQuads()
    {
        var quads = new List<List<GameObject>>();

        var quadDimensions = GetTilemapQuadDimensions();

        for (var x = 0; x < quadDimensions.x; x++)
        {
            quads.Add(new List<GameObject>());
            for (var y = 0; y < quadDimensions.y; y++)
            {
                var chunkRendererObject = new GameObject("ChunkRenderer");
                chunkRendererObject.SetParent(this.gameObject);

                // Add a tile chunk renderer
                var tileChunkRenderer = chunkRendererObject.AddComponent<TileChunkRenderer>();
                tileChunkRenderer.BottomLeft = new Vector2Int(this.TilesPerMesh.x * x, this.TilesPerMesh.y * y);
                tileChunkRenderer.ChunkTileSize = this.TilesPerMesh;
                tileChunkRenderer.WillCombineTileMeshes = this.CombineTileMeshes;

                chunkRendererObject.transform.localScale = this.QuadScale;
                chunkRendererObject.transform.localPosition = new Vector3(
                    (this.QuadScale.x * this.TileSize.x) + (this.QuadScale.x * x), 
                    (this.QuadScale.y * this.TileSize.y) + (this.QuadScale.y * y),
                    chunkRendererObject.transform.position.z);

                quads[x].Add(chunkRendererObject);
            }
        }

        return quads;
    }

    public void DestroyQuads()
    {
        if (this.Quads == null)
            return;

        foreach (var quadRow in this.Quads)
        {
            foreach (var quad in quadRow)
            {
                Destroy(quad);
            }
        }
    }

    private GameObject GetQuadAtTilemapPosition(Vector2Int tilemapPosition)
    {
        var quadPosition = tilemapPosition
            .AsVector2()
            .Divide(this.TilesPerMesh.AsVector2())
            .AsVector2Int();

        return this.Quads[quadPosition.x][quadPosition.y];
    }

    private Vector2Int GetTilemapQuadDimensions()
    {
        var x = Mathf.CeilToInt(this.Tilemap.Size.AsVector2().x / this.TilesPerMesh.AsVector2().x);
        var y = Mathf.CeilToInt(this.Tilemap.Size.AsVector2().y / this.TilesPerMesh.AsVector2().y);

        return new Vector2Int(x, y);
    }

    private void Subscribe()
    {
        _hubSubscriptionCache.Subscribe<TilemapTileChangedEventArgs>(EventManager.Instance.TilemapEventHub, this.OnTilemapChanged);
    }

    protected void OnTilemapChanged(TilemapTileChangedEventArgs eventArgs)
    {
        if (!eventArgs.ShouldUpdateTilemap)
            return;

        this.UpdateAtTilemapPosition(eventArgs.TileChangedTilemapPosition);
    }
}
