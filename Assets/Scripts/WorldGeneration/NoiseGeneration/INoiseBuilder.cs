﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface INoiseBuilder<T> where T : INoiseBuilder<T>
{
    T WithFrequency(float frequency);
    T WithSeed(int seed);
    T WithInterpolation(FastNoise.Interp interpolation);
    FastNoise Build();
}
