﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IFractalNoiseBuilder<T> : INoiseBuilder<T> where T : INoiseBuilder<T>
{
    IFractalNoiseBuilder<T> WithOctaves(int octaves);
    IFractalNoiseBuilder<T> WithLacunarity(float lacunarity);
    IFractalNoiseBuilder<T> WithGain(float gain);
    IFractalNoiseBuilder<T> WithFractalType(FastNoise.FractalType fractalType);
}
