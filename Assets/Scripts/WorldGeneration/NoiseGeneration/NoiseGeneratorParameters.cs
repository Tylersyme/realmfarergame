﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using static FastNoise;

/// <summary>
/// Represents all parameters necessary to specify a noise generator using fast noise
/// </summary>
[System.Serializable]
public class NoiseGeneratorParameters
{
    public static NoiseGeneratorParameters DefaultSetup { get; private set; } = new NoiseGeneratorParameters
    {
        NoiseType = NoiseType.PerlinFractal,
        Frequency = 0.1f,
        Octaves = 1,
        Gain = 1,
        Lacunarity = 1,
    };

    [SerializeField]
    [HideInInspector]
    private Seed _seed;
    public Seed Seed { get => _seed; set => _seed = value; }

    [SerializeField]
    private NoiseType _noiseType;
    public NoiseType NoiseType { get => _noiseType; set => _noiseType = value; }

    [SerializeField]
    private float _frequency;
    public float Frequency { get => _frequency; set => _frequency = value; }

    [SerializeField]
    private int _octaves;
    public int Octaves { get => _octaves; set => _octaves = value; }

    [SerializeField]
    private float _gain;
    public float Gain { get => _gain; set => _gain = value; }

    [SerializeField]
    private float _lacunarity;
    public float Lacunarity { get => _lacunarity; set => _lacunarity = value; }

#if UNITY_EDITOR

    [Button(ButtonSizes.Medium)]
    private void Preview()
    {
        // Set parameters for the preview window
        NoiseGeneratorPreviewManager.Instance.NoiseGeneratorParameters = this;

        NoiseGeneratorPreviewWindow.OpenWindow();
        EditorWindow.GetWindow<NoiseGeneratorPreviewWindow>().NoiseMapPreviewClosedEvent += OnNoiseGeneratorPreviewWindowClosed;
    }

    protected void OnNoiseGeneratorPreviewWindowClosed(object sender, NoiseMapPreviewClosedEventArgs eventArgs)
    {
        EditorWindow.GetWindow<NoiseGeneratorPreviewWindow>().NoiseMapPreviewClosedEvent -= OnNoiseGeneratorPreviewWindowClosed;

        // TODO: HasApplyChangesBeenClicked was made to fix a bug which would reset all values to default even if the button
        // had never been pressed. Find a better solution.
        if (!NoiseGeneratorPreviewManager.Instance.HasApplyChangesBeenClicked ||
            !NoiseGeneratorPreviewManager.Instance.HasChanges())
            return;

        var changes = NoiseGeneratorPreviewManager.Instance.GetChanges();

        this.NoiseType = changes.NoiseType;
        this.Frequency = changes.Frequency;
        this.Octaves = changes.Octaves;
        this.Gain = changes.Gain;
        this.Lacunarity = changes.Lacunarity;
    }

#endif
}
