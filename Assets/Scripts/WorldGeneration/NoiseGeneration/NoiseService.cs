﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static FastNoise;

public class NoiseService : INoiseService
{
    public INoiseBuilder<NoiseBuilder> CreatePerlinNoiseGenerator()
    {
        return new NoiseBuilder(NoiseType.Perlin);
    }

    public IFractalNoiseBuilder<FractalNoiseBuilder> CreatePerlinNoiseFractalGenerator()
    {
        return new FractalNoiseBuilder(NoiseType.PerlinFractal);
    }

    public FastNoise BuildFromParameters(NoiseGeneratorParameters noiseParameters)
    {
        if (noiseParameters.NoiseType == NoiseType.PerlinFractal)
        {
            return CreatePerlinNoiseFractalGenerator()
                .WithSeed(noiseParameters.Seed.NextInteger())
                .WithFrequency(noiseParameters.Frequency)
                .WithOctaves(noiseParameters.Octaves)
                .WithLacunarity(noiseParameters.Lacunarity)
                .WithGain(noiseParameters.Gain)
                .Build();
        }

        throw new System.NotImplementedException($"Error building noise generator from parameters: The noise type {noiseParameters.NoiseType} is not supported.");
    }
}

public abstract class NoiseBuilderBase<T> : INoiseBuilder<T> where T : NoiseBuilderBase<T>
{
    // General settings that apply to all types of noise
    private NoiseType NoiseType { get; set; }
    private float Frequency { get; set; } = 0.5f;
    private int Seed { get; set; } = 1337;
    private Interp Interpolation { get; set; } = Interp.Quintic;

    public NoiseBuilderBase(NoiseType noiseType)
    {
        this.NoiseType = noiseType;
    }

    public T WithFrequency(float frequency)
    {
        this.Frequency = frequency;

        return (T)this;
    }

    public T WithSeed(int seed)
    {
        this.Seed = seed;

        return (T)this;
    }

    public T WithInterpolation(Interp interpolation)
    {
        this.Interpolation = interpolation;

        return (T)this;
    }

    public virtual FastNoise Build()
    {
        var noiseGenerator = new FastNoise();

        noiseGenerator.SetNoiseType(this.NoiseType);
        noiseGenerator.SetSeed(this.Seed);
        noiseGenerator.SetFrequency(this.Frequency);
        noiseGenerator.SetInterp(this.Interpolation);

        return noiseGenerator;
    }
}

public class NoiseBuilder : NoiseBuilderBase<NoiseBuilder>
{
    public NoiseBuilder(NoiseType noiseType) : base(noiseType)
    {
    }
}

public class FractalNoiseBuilder : NoiseBuilderBase<FractalNoiseBuilder>, IFractalNoiseBuilder<FractalNoiseBuilder>
{
    // Fractal settings
    private int Octaves { get; set; } = 2;
    private float Lacunarity { get; set; } = 2.0f;
    private float Gain { get; set; } = 0.5f;
    private FractalType FractalType { get; set; } = FractalType.FBM;

    public FractalNoiseBuilder(NoiseType noiseType) : base(noiseType)
    {
    }

    public override FastNoise Build()
    {
        var noiseGenerator = base.Build();

        noiseGenerator.SetFractalOctaves(this.Octaves);
        noiseGenerator.SetFractalLacunarity(this.Lacunarity);
        noiseGenerator.SetFractalGain(this.Gain);
        noiseGenerator.SetFractalType(this.FractalType);

        return noiseGenerator;
    }

    public IFractalNoiseBuilder<FractalNoiseBuilder> WithOctaves(int octaves)
    {
        this.Octaves = octaves;

        return this;
    }

    public IFractalNoiseBuilder<FractalNoiseBuilder> WithLacunarity(float lacunarity)
    {
        this.Lacunarity = lacunarity;

        return this;
    }

    public IFractalNoiseBuilder<FractalNoiseBuilder> WithGain(float gain)
    {
        this.Gain = gain;

        return this;
    }

    public IFractalNoiseBuilder<FractalNoiseBuilder> WithFractalType(FractalType fractalType)
    {
        this.FractalType = FractalType;

        return this;
    }
}


    //public NoiseSample GenerateNoise(
    //    float scale,
    //    float xPosition, float yPosition,
    //    float frequency,
    //    float width, float height,
    //    Vector2 offset)
    //{
    //    var x = ((xPosition - (width / 2)) / scale) * frequency + offset.x;
    //    var y = ((yPosition - (height / 2)) / scale) * frequency + offset.y;

    //    // Will have a range of -1 to 1
    //    var sample = Mathf.PerlinNoise(x, y) * 2 -1;

    //    return new NoiseSample(sample);
    //}

    //public NoiseSample GenerateNoise(
    //    float scale,
    //    float xPosition, float yPosition,
    //    float frequency,
    //    float width, float height,
    //    Vector2 offset)
    //{
    //    var x = ((xPosition - (width / 2)) / scale) * frequency + offset.x;
    //    var y = ((yPosition - (height / 2)) / scale) * frequency + offset.y;

    //    // Will have a range of -1 to 1
    //    var sample = Mathf.PerlinNoise(x, y) * 2 - 1;

    //    return new NoiseSample(sample);
    //}

    //public List<List<NoiseSample>> GenerateNoise2D(
    //    float scale,
    //    float width, float height,
    //    int octaves,
    //    float persistance, float lacunarity,
    //    Vector2[] octaveOffsets,
    //    float offsetX = 0.0f,
    //    float offsetY = 0.0f)
    //{
    //    if (scale <= 0)
    //        scale = float.Epsilon;

    //    var offset = new Vector2(offsetX, offsetY);

    //    var samples = new List<List<NoiseSample>>();

    //    float maxNoiseHeight = float.MinValue;
    //    float minNoiseHeight = float.MaxValue;

    //    for (var x = 0; x < width; x++)
    //    {
    //        samples.Add(new List<NoiseSample>());
    //        for (var y = 0; y < height; y++)
    //        {
    //            var amplitude = 1.0f;
    //            var frequency = 1.0f;

    //            var sample = new NoiseSample(0.0f);
    //            for (var octaveIdx = 0; octaveIdx < octaves; octaveIdx++)
    //            {
    //                sample += GenerateNoise(scale, x, y, frequency, width, height, octaveOffsets[octaveIdx] + offset) * amplitude;

    //                amplitude *= persistance;
    //                frequency *= lacunarity;
    //            }

    //            if (sample > maxNoiseHeight)
    //                maxNoiseHeight = sample.Value;
    //            else if (sample < minNoiseHeight)
    //                minNoiseHeight = sample.Value;

    //            samples[x].Add(sample);
    //        }
    //    }

    //    Normalize(samples, (int)width, (int)height, maxNoiseHeight, minNoiseHeight);

    //    return samples;
    //}

    //public List<List<NoiseSample>> GenerateNoise2D(
    //    float scale,
    //    float width, float height,
    //    int octaves,
    //    float persistance, float lacunarity,
    //    Seed seed,
    //    float offsetX = 0.0f,
    //    float offsetY = 0.0f)
    //{
    //    var octaveOffsets = new Vector2[octaves];
    //    for (var octaveIdx = 0; octaveIdx < octaves; octaveIdx++)
    //        octaveOffsets[octaveIdx] = new Vector2(seed.NextFloat(), seed.NextFloat());

    //    return GenerateNoise2D(scale, width, height, octaves, persistance, lacunarity, octaveOffsets, offsetX, offsetY);
    //}

    //private void Normalize(List<List<NoiseSample>> samples, int width, int height, float maxNoiseHeight, float minNoiseHeight)
    //{
    //    for (var x = 0; x < width; x++)
    //        for (var y = 0; y < height; y++)
    //            samples[x][y].Value = Mathf.InverseLerp(minNoiseHeight, maxNoiseHeight, samples[x][y].Value);
    //}