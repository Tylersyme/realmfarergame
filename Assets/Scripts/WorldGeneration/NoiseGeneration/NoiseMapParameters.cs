﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// In addition to the parameters for creating the noise generator this also contains the
/// relevant information to create a 2D noise map from that generator.
/// </summary>
[System.Serializable]
public class NoiseMapParameters : NoiseGeneratorParameters
{
    [SerializeField]
    private Vector2Int _dimensions;
    public Vector2Int Dimensions { get => _dimensions; set => _dimensions = value; }
}
