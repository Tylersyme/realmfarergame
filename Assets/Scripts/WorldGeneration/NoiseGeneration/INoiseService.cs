﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface INoiseService
{
    IFractalNoiseBuilder<FractalNoiseBuilder> CreatePerlinNoiseFractalGenerator();
    FastNoise BuildFromParameters(NoiseGeneratorParameters noiseParameters);
}
