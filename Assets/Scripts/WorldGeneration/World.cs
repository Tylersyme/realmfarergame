﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[System.Serializable]
public class World
{
    [SerializeField]
    private List<List<Region>> _regions;
    public List<List<Region>> Regions { get => _regions; set => _regions = value; }

    public Climate Climate { get; set; }

    [SerializeField]
    private Seed _seed;
    public Seed Seed { get => _seed; set => _seed = value; }

    [SerializeField]
    private Vector2Int _size;
    public Vector2Int Size { get => _size; set => _size = value; }

    [SerializeField]
    private NoiseMap _heightMap;
    public NoiseMap HeightMap { get => _heightMap; set => _heightMap = value; }

    [SerializeField]
    private NoiseMap _temperatureMap;
    public NoiseMap TemperatureMap { get => _temperatureMap; set => _temperatureMap = value; }

    [SerializeField]
    private NoiseMap _precipitationMap;
    public NoiseMap PrecipitationMap { get => _precipitationMap; set => _precipitationMap = value; }

    private readonly IRegionGeneratorFactory _regionGeneratorFactory;

    public World()
    {
        _regionGeneratorFactory = new RegionGeneratorFactory();
    }

    public World(Seed seed, int width, int height, Climate climate)
        : this()
    {
        this.Regions = new List<List<Region>>();
        this.Seed = seed;
        this.Size = new Vector2Int(width, height);
        this.Climate = climate;
    }

    /// <summary>
    /// Creates a new region through procedural generation.
    /// </summary>
    /// <param name="position"></param>
    /// <param name="heightMapParameters"></param>
    /// <returns>The generated region.</returns>
    public Region GenerateRegion(Vector2Int position, NoiseMapParameters heightMapParameters)
    {
        var region = GetRegion(position);

        if (region.IsGenerated)
            throw new System.Exception($"Error generation region: The region at {position} has already been generated.");

        var biomeType = region.Biome.Type;

        heightMapParameters.Seed = this.Seed;

        var regionGenerator = _regionGeneratorFactory.Create(biomeType);

        var regionParameters = new RegionParameters
        {
            Seed = this.Seed,
            Height = region.Height,
            HeightMapParameters = heightMapParameters,
            // TODO: Dynamically determine region generation parameters
            RegionGenerationParameters = WorldManager.Instance.ForestGenerationParameters,
        };

        regionGenerator.GenerateRegion(region, regionParameters);

        return region;
    }

    public void SetRegions(List<List<Region>> regions)
    {
        var regionsWidth = regions.Count;
        var regionsHeight = regions.First().Count;

        if (regionsWidth != this.Size.x || regionsHeight != this.Size.y)
            throw new System.Exception($"Error setting world regions: must have a width of [{this.Size.x},{this.Size.y}] but found [{regionsWidth},{regionsHeight}]");

        this.Regions = regions;
    }

    public Region GetRegion(Vector2Int position)
    {
        return this.Regions[position.x][position.y];
    }

    public Biome GetBiome(Vector2Int position)
    {
        return GetRegion(position).Biome;
    }

    public Texture2D ToBiomeTexture()
    {
        var texture = new Texture2D(this.Size.x, this.Size.y);

        for (var x = 0; x < this.Size.x; x++)
        {
            for (var y = 0; y < this.Size.y; y++)
            {
                var height = this.Regions[x][y].Height;
                var color = this.Regions[x][y].Biome.IdentifyingColor
                    .AdjustBrightness(height - 0.5f);
                texture.SetPixel(x, y, color);
            }
        }

        texture.Apply();

        return texture;
    }
}
