﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IBitmaskService
{
    int GetBitmaskValue(List<Direction> directions, BitmaskAdjacencyMode bitmaskAdjacencyMode);

    int GetEightSidedBitmaskValue(List<Direction> directions);

    int GetFourSidedBitmaskValue(List<Direction> directions);
}
