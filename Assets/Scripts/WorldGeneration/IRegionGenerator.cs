﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IRegionGenerator
{
    /// <summary>
    /// Generates a region which already exists.
    /// </summary>
    /// <param name="toModify"></param>
    /// <param name="regionParameters"></param>
    /// <returns></returns>
    Region GenerateRegion(Region emptyRegion, RegionParameters regionParameters);
}
