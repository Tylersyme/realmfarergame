﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TileCreationDefinition
{
    [SerializeField]
    [Required]
    private TileTemplate _template;
    public TileTemplate Template { get => _template; set => _template = value; }

    [SerializeField]
    [HideIf(nameof(RandomizeVarietyTextureIndex))]
    private int _varietyTextureIndex = 0;
    public int VarietyTextureIndex { get => _varietyTextureIndex; set => _varietyTextureIndex = value; }

    [SerializeField]
    private bool _randomizeVarietyTextureIndex = false;
    public bool RandomizeVarietyTextureIndex { get => _randomizeVarietyTextureIndex; set => _randomizeVarietyTextureIndex = value; }
}
