﻿using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[RequireComponent(typeof(MovementModule))]
public class EntityController : MonoBehaviour
{
    [SerializeField]
    private float _interactionRadius = 0.5f;
    public float InteractionRadius { get => _interactionRadius; set => _interactionRadius = value; }

    public Item SelectedItem { get => this.GetContainerModule().Container.SelectedItem; }

    private IInteractionInvoker _interactionInvoker;
    private IItemSkillService _itemSkillService;
    private PlayerItemSkillActivationStrategyManager _playerItemSkillDataStrategyManager;

    private void Awake()
    {
        _interactionInvoker = new InteractionInvoker();
        _itemSkillService = new ItemSkillService();
        _playerItemSkillDataStrategyManager = new PlayerItemSkillActivationStrategyManager();
    }

    private void Update()
    {
        var movementModule = this.GetMovementModule();

        if (InputManager.Instance.IsActionActive(InputAction.MOVE_NORTH))
            movementModule.SetVelocityY(movementModule.MaxVelocity);

        if (InputManager.Instance.IsActionActive(InputAction.MOVE_SOUTH))
            movementModule.SetVelocityY(-movementModule.MaxVelocity);

        if (InputManager.Instance.IsActionActive(InputAction.MOVE_EAST))
            movementModule.SetVelocityX(movementModule.MaxVelocity);

        if (InputManager.Instance.IsActionActive(InputAction.MOVE_WEST))
            movementModule.SetVelocityX(-movementModule.MaxVelocity);

        if (InputManager.Instance.IsActionActive(InputAction.DEFAULT_INTERACT_CLOSEST_OBJECT))
        {
            var containerModule = this.gameObject.GetComponent<ContainerModule>();

            var closestTilemapObject = this.GetClosestInteractableTilemapObject();

            if (closestTilemapObject == null)
                return;

            var interactionResponse = _interactionInvoker.Invoke<InteractionResponse>(closestTilemapObject.gameObject, new Interaction
            {
                IsDefaultInteraction = true,
            });

            if (interactionResponse != null)
            {
                var interactionType = interactionResponse.InteractionType;

                // Pickup Items
                if (interactionType == InteractionType.PickupIntoInventory &&
                    containerModule != null)
                {
                    var itemModule = closestTilemapObject.gameObject.GetComponent<ItemModule>();

                    this.PickupItem(itemModule, containerModule);
                }
            }
        }

        if (InputManager.Instance.IsActionActive(InputAction.TOGGLE_INVENTORY) && 
            this.gameObject.GetComponent<ContainerModule>() != null)
        {
            EventManager.Instance.UiEventHub.Publish(new ToggleInventoryEventArgs
            {
                EntityContainer = this.gameObject.GetComponent<ContainerModule>().Container,
            });
        }

        if (InputManager.Instance.IsActionActive(InputAction.DROP_SELECTED_ITEM))
        {
            if (this.SelectedItem == null)
                return;

            if (!_itemSkillService.ItemHasSkillOfType(this.SelectedItem, ItemSkillType.DropSelf))
                return;

            if (!_interactionInvoker.IsInteractable(this.gameObject.GetRootParent()))
                return;

            var itemSkillActivationData = _itemSkillService.ActivateItemSkillOfType(
                this.SelectedItem, 
                ItemSkillType.DropSelf, 
                new EntityItemActivationCause
                {
                    EntityController = this,
                },
                _playerItemSkillDataStrategyManager,
                this);

            _interactionInvoker.Invoke<InteractionResponse>(
                this.gameObject.GetRootParent(), 
                itemSkillActivationData.QueryableInteraction.Interaction);
        }

        if (InputManager.Instance.HasClicked(MouseButtonType.LEFT) ||
            InputManager.Instance.HasClicked(MouseButtonType.RIGHT))
        {
            if (this.SelectedItem == null)
                return;

            var currentPosition = this.gameObject.GetRootParent().transform.position;

            var itemSkillSlot = InputManager.Instance.HasClicked(MouseButtonType.LEFT)
                ? ItemSkillSlot.Primary
                : ItemSkillSlot.Secondary;

            var itemSkillActivationData = _itemSkillService.ActivateItemSkillSlot(
                this.SelectedItem,
                itemSkillSlot,
                new EntityItemActivationCause { EntityController = this, },
                _playerItemSkillDataStrategyManager,
                this);

            var queriedInteractableObjectData = itemSkillActivationData
                .Select(d =>
                {
                    var rootParent = this.gameObject.GetRootParent();

                    var toIgnore = new List<GameObject>();

                    // Add self to ignore list if needed
                    if (!d.ItemSkill.InteractsWithActivator)
                        toIgnore.Add(rootParent);

                    var interactableObjectData = new
                    {
                        Interaction = d.QueryableInteraction.Interaction,
                        Objects = d.QueryableInteraction.ObjectQueryProfile
                            .QueryAll(currentPosition, toIgnore)
                            .Where(go => _interactionInvoker.IsInteractable(go) && rootParent != go)
                            .ToList(),
                    };

                    if (d.ItemSkill.InteractsWithActivator && !interactableObjectData.Objects.Contains(rootParent))
                        interactableObjectData.Objects.Add(rootParent);

                    return interactableObjectData;
                });

            foreach (var interactableObjectData in queriedInteractableObjectData)
                foreach (var interactableObject in interactableObjectData.Objects)
                    _interactionInvoker.Invoke<InteractionResponse>(interactableObject, interactableObjectData.Interaction);
        }
    }

    private void PickupItem(ItemModule itemModule, ContainerModule containerModule)
    {
        var interactionResponse = _interactionInvoker.Invoke<PickupIntoInventoryInteractionResponse>(itemModule.gameObject, new Interaction
        {
            Type = InteractionType.PickupIntoInventory,
        });

        if (!interactionResponse.WasSuccessful)
            return;

        containerModule.Container.Inventory.AddItem(interactionResponse.Item);

        EventManager.Instance.UiEventHub.Publish(new InventoryChangedEventArgs
        {
            Inventory = containerModule.Container.Inventory,
        });
    }

    private TilemapObject GetClosestInteractableTilemapObject()
    {
        var objectColliders = Physics2D
            .OverlapCircleAll(this.gameObject.transform.position, this.InteractionRadius)
            .Where(c => c.gameObject.GetRootParent() != this.gameObject &&
                        c.gameObject
                            .GetRootParent()
                            .HasComponent<TilemapObject>());

        var closestTilemapObject = default(TilemapObject);
        var closestDistance = 0.0f;

        foreach (var collider in objectColliders)
        {
            var distance = Vector2.Distance(collider.gameObject.transform.position, this.gameObject.transform.position);

            if (!_interactionInvoker.IsInteractable(collider.gameObject.GetRootParent()))
                continue;

            if (closestTilemapObject == null || distance < closestDistance)
            {
                closestTilemapObject = collider.gameObject
                    .GetRootParent()
                    .GetComponent<TilemapObject>();

                closestDistance = distance;
            }
        }

        return closestTilemapObject;
    }

    public MovementModule GetMovementModule()
    {
        return this.gameObject.GetComponent<MovementModule>();
    }

    public ContainerModule GetContainerModule() =>
        this.gameObject.GetComponent<ContainerModule>();
}
