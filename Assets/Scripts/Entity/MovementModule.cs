﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class MovementModule : MonoBehaviour
{
    [SerializeField]
    private float _maxVelocity;
    public float MaxVelocity { get => _maxVelocity; set => _maxVelocity = value; }

    [SerializeField]
    private float _maxAcceleration;
    public float MaxAcceleration { get => _maxAcceleration; set => _maxAcceleration = value; }

    private void FixedUpdate()
    {
        
    }

    public void SetVelocity(Vector2 velocity)
    {
        velocity = Vector2.ClampMagnitude(velocity, this.MaxVelocity);

        GetRigidbody().velocity = velocity;
    }

    public void SetVelocityY(float y)
    {
        var velocity = GetVelocity().SetY(y);

        SetVelocity(velocity);
    }

    public void SetVelocityX(float x)
    {
        var velocity = GetVelocity().SetX(x);

        SetVelocity(velocity);
    }

    public void ChangeVelocity(Vector2 velocity)
    {
        var rigidBody = GetRigidbody();

        var resultantVelocity = rigidBody.velocity + velocity;

        SetVelocity(velocity);
    }

    public Vector2 GetVelocity()
    {
        return GetRigidbody().velocity;
    }

    private Rigidbody2D GetRigidbody()
    {
        return this.gameObject.GetComponent<Rigidbody2D>();
    }
}
