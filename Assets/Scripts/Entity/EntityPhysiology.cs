﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntityPhysiology : SerializedMonoBehaviour
{
    private const float PHYSIOLOGICAL_SYSTEM_UPDATE_INTERVAL = 1.0f;

    [SerializeField]
    private List<IPhysiologicalSystem> _physiologicalSystems = new List<IPhysiologicalSystem>();
    public List<IPhysiologicalSystem> PhysiologicalSystems { get => _physiologicalSystems; set => _physiologicalSystems = value; }

    private ITimeIntervalTracker _timeIntervalTracker;
    private ISubscriptionCache _subscriptionCache;
    private INeedsService _needsService;

    private void Awake()
    {
        _timeIntervalTracker = new TimeIntervalTracker(PHYSIOLOGICAL_SYSTEM_UPDATE_INTERVAL);
        _subscriptionCache = new SubscriptionCache();
        _needsService = new NeedsService(this.gameObject);

        this.Subscribe();
    }

    private void Update() =>
        _timeIntervalTracker.Update(Time.deltaTime);

    private void OnDestroy() =>
        _subscriptionCache.UnsubscribeAll();

    protected void OnTimeIntervalOccurred(object sender, TimeIntervalOccurredEventArgs eventArgs) =>
        this.UpdatePhysiologicalSystems(eventArgs.IntervalSeconds);

    private void UpdatePhysiologicalSystems(float elapsedSeconds) =>
        this.PhysiologicalSystems.ForEach(ps => ps.Update(elapsedSeconds, _needsService));

    private void Subscribe()
    {
        _subscriptionCache.Subscribe<TimeIntervalOccurredEventArgs>(
            this.OnTimeIntervalOccurred,
            handler => _timeIntervalTracker.TimeIntervalOccurredEvent += handler,
            handler => _timeIntervalTracker.TimeIntervalOccurredEvent -= handler);
    }
}
