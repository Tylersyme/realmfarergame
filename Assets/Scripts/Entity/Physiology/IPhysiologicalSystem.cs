﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPhysiologicalSystem
{
    void Update(float elapsedSeconds, INeedsService needsService);
}
