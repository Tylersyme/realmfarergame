﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class EnergyPhysiologicalSystem : IPhysiologicalSystem
{
    [SerializeField]
    private float _energyNeedSatisfactionChangePerSecond = -0.01f;
    public float EnergyNeedSatisfactionChangePerSecond { get => _energyNeedSatisfactionChangePerSecond; set => _energyNeedSatisfactionChangePerSecond = value; }

    public void Update(float elapsedSeconds, INeedsService needsService)
    {
        var energySatisfactionChange = this.GetEnergySatisfactionChange(elapsedSeconds);

        needsService.ChangeNeedSatisfaction(NeedType.Energy, energySatisfactionChange);
    }

    private float GetEnergySatisfactionChange(float elapsedSeconds) =>
        this.EnergyNeedSatisfactionChangePerSecond * elapsedSeconds;
}
