﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NeedsService : INeedsService
{
    private GameObject _gameObject;

    private NeedsModule _needsModule;

    public NeedsService(GameObject gameObject)
    {
        _gameObject = gameObject;

        _needsModule = this.GetNeedsModule();
    }

    public void ChangeNeedSatisfaction(NeedType needType, float amount) =>
        _needsModule.ChangeNeedSatisfaction(needType, amount);

    private NeedsModule GetNeedsModule() =>
        _gameObject
            .GetRootParent()
            .GetComponent<NeedsModule>();
}
