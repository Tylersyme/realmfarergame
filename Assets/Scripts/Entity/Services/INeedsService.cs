﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface INeedsService
{
    void ChangeNeedSatisfaction(NeedType needType, float amount);
}
