﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class NeedsModule : MonoBehaviour
{
    [SerializeField]
    private List<Need> _needs = new List<Need>();
    private List<Need> Needs { get => _needs; set => _needs = value; }

    public event EventHandler<NeedSatisfactionChangedEventArgs> NeedSatisfactionChangedEvent;

    public void ChangeNeedSatisfaction(NeedType needType, float amount)
    {
        var need = this.GetNeedOfType(needType);

        var needChangedEventArgs = new NeedSatisfactionChangedEventArgs
        {
            NeedType = needType,
            PreviousSatisfaction = need.Satisfaction,
        };

        need.Satisfaction += amount;

        needChangedEventArgs.NewSatisfaction = need.Satisfaction;

        this.NeedSatisfactionChangedEvent?.Invoke(this, needChangedEventArgs);
    }

    private Need GetNeedOfType(NeedType needType) =>
        this.Needs.First(n => n.Type == needType);

}
