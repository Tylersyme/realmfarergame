﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Need
{
    [SerializeField]
    private NeedType _type;
    public NeedType Type { get => _type; set => _type = value; }

    [SerializeField]
    [MinValue(0)]
    [MaxValue(1.0f)]
    private float _satisfaction = 1.0f;
    public float Satisfaction 
    { 
        get => _satisfaction; 
        set => _satisfaction = Mathf.Clamp(value, 0.0f, 1.0f); 
    }
}
