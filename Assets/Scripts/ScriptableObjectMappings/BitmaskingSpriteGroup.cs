﻿using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;

[CreateAssetMenu(fileName = "BitmaskingSpriteGroup", menuName = "Database/Bitmasking Sprite Group")]
public class BitmaskingSpriteGroup : SerializedScriptableObject
{
#if UNITY_EDITOR

    [SerializeField]
    private string _spriteDirectoryRelativePath = string.Empty;
    public string SpriteDirectoryRelativePath { get => _spriteDirectoryRelativePath; set => _spriteDirectoryRelativePath = value; }

    [SerializeField]
    private BitmaskAdjacencyMode _bitmaskAdjacencyMode = BitmaskAdjacencyMode.FOUR_SIDED;
    public BitmaskAdjacencyMode BitmaskAdjacencyMode { get => _bitmaskAdjacencyMode; private set => _bitmaskAdjacencyMode = value; }

    public bool ShowFourSidedBitmaskMethod
    {
        get => this.BitmaskAdjacencyMode == BitmaskAdjacencyMode.FOUR_SIDED;
    }

    [Button]
    [TitleGroup("Bitmasking Functions")]
    [PropertySpace(5)]
    private void FillBitmaskList(Sprite defaultSprite, bool clearExistingList)
    {
        if (clearExistingList)
            this.Sprites.Clear();

        if (this.BitmaskAdjacencyMode == BitmaskAdjacencyMode.FOUR_SIDED)
        {
            for (var idx = 0; idx < 16; idx++)
                this.Sprites.Add(defaultSprite);
        }
    }

    [Button("Set Bitmask Sprite")]
    [ShowIf(nameof(ShowFourSidedBitmaskMethod))]
    [TitleGroup("Bitmasking Functions")]
    [InfoBox("Bitmask directions refer to which sides have an adjacent sprite.")]
    [PropertySpace(5)]
    private void SetBitmaskSpriteFourSided(Sprite sprite, FourSidedBitmaskPosition bitmaskPosition) =>
        this.Sprites[(int)bitmaskPosition] = sprite;

    [Button]
    [InfoBox("The path is relative to the Sprites directory.")]
    private void FindAndUpdateByConvention()
    {
        if (string.IsNullOrWhiteSpace(this.SpriteDirectoryRelativePath))
            return;

        var directoryPath = Path.Combine("Assets", "Sprites", this.SpriteDirectoryRelativePath);

        var sprites = AssetDatabaseHelper.GetAssetsFromPath<Sprite>(directoryPath);

        sprites
            .Where(s => s.name.Contains("_"))
            .Select(s => 
            {
                if (Enum.TryParse<FourSidedBitmaskPosition>(s.name.Split('_')[1], true, out var result))
                    return new
                    {
                        FourSidedBitmaskPosition = result,
                        Sprite = s,
                    };

                return null;
            })
            .Where(s => s != null)
            .ToList()
            .ForEach(s => this.SetBitmaskSpriteFourSided(s.Sprite, s.FourSidedBitmaskPosition));
    }

    private enum FourSidedBitmaskPosition
    {
        Alone,
        North,
        West,
        NorthWest,
        East,
        NorthEast,
        WestEast,
        NorthWestEast,
        South,
        NorthSouth,
        SouthWest,
        NorthSouthWest,
        SouthEast,
        NorthSouthEast,
        SouthWestEast,
        NorthSouthEastWest,
    }

#endif

    [SerializeField]
    [ListDrawerSettings(ShowIndexLabels = true, NumberOfItemsPerPage = 16, HideAddButton = true, HideRemoveButton = true, Expanded = false)]
    private IList<Sprite> _sprites = new List<Sprite>();
    public IList<Sprite> Sprites { get => _sprites; set => _sprites = value; }

    public Sprite GetSpriteByIndex(int spriteIndex) =>
        this.Sprites[spriteIndex];
}
