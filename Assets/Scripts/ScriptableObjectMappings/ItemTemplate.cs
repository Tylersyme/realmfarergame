﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[System.Serializable]
[CreateAssetMenu(fileName = "ItemTemplate", menuName = "Database/Item Template")]
public class ItemTemplate : SerializedScriptableObject
{
    [SerializeField]
    private ItemType _type;
    public ItemType Type { get => _type; set => _type = value; }

    [SerializeField]
    [MinValue(1)]
    private int maxStackSize = 1;
    public int MaxStackSize { get => maxStackSize; set => maxStackSize = value; }

    [SerializeField]
    [Required]
    private Sprite _sprite;
    public Sprite Sprite { get => _sprite; set => _sprite = value; }

    /// <summary>
    /// The texture used to represent the item in UI.
    /// </summary>
    [SerializeField]
    [Required]
    private Texture2D _uiTexture;
    public Texture2D UiTexture { get => _uiTexture; set => _uiTexture = value; }

    /// <summary>
    /// The name of the item as appears in UI.
    /// </summary>
    [SerializeField]
    [Required]
    private string _uiName;
    public string UiName { get => _uiName; set => _uiName = value; }

    [SerializeField]
    [Required]
    private GameObject _droppedItemPrefab;
    public GameObject DroppedItemPrefab { get => _droppedItemPrefab; set => _droppedItemPrefab = value; }

    [SerializeField]
    [DictionaryDrawerSettings(DisplayMode = DictionaryDisplayOptions.CollapsedFoldout)]
    private Dictionary<ItemSkillSlot, IList<IItemSkill>> _slotSkills = new Dictionary<ItemSkillSlot, IList<IItemSkill>>();
    public Dictionary<ItemSkillSlot, IList<IItemSkill>> SlotSkills { get => _slotSkills; set => _slotSkills = value; }

    [SerializeField]
    private List<IItemSkill> _skills = new List<IItemSkill>();
    public List<IItemSkill> Skills { get => _skills; set => _skills = value; }

    public List<IItemSkill> AllSkills
    {
        get => this.SlotSkills.Values
            .SelectMany(l => l)
            .Concat(this.Skills)
            .ToList();
    }
}
