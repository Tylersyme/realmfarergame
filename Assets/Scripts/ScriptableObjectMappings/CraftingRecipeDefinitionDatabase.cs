﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "CraftingRecipeDefinitions", menuName = "Database/Crafting Recipe Definitions")]
public class CraftingRecipeDefinitionDatabase : SerializedScriptableObject
{
    [SerializeField]
    private IList<CraftingRecipeDefinition> _craftingRecipeDefinitions = new List<CraftingRecipeDefinition>();
    public IList<CraftingRecipeDefinition> CraftingRecipeDefinitions { get => _craftingRecipeDefinitions; set => _craftingRecipeDefinitions = value; }
}
