﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "VarietyTextureGroup", menuName = "Database/Variety Texture Group")]
public class VarietyTextureGroup : SerializedScriptableObject
{
    [SerializeField]
    private IList<Texture2D> _textures = new List<Texture2D>();
    public IList<Texture2D> Textures { get => _textures; set => _textures = value; }

    public Texture2D GetTextureByIndex(int index) =>
        this.Textures[index];
}
