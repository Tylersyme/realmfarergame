﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ItemMappings", menuName = "Database/Item Mappings")]
public class ItemMappings : SerializedScriptableObject
{
    [SerializeField]
    [DictionaryDrawerSettings(DisplayMode = DictionaryDisplayOptions.CollapsedFoldout)]
    private IDictionary<ItemType, ItemTemplate> _tileTemplates = new Dictionary<ItemType, ItemTemplate>();
    public IDictionary<ItemType, ItemTemplate> TileTemplates { get => _tileTemplates; set => _tileTemplates = value; }
}
