﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "TileMappings", menuName = "Database/Tile Mappings")]
public class TileMappings : SerializedScriptableObject
{
    [SerializeField]
    [DictionaryDrawerSettings(DisplayMode = DictionaryDisplayOptions.CollapsedFoldout)]
    private IDictionary<TileType, TileTemplate> _tileTemplates = new Dictionary<TileType, TileTemplate>();
    public IDictionary<TileType, TileTemplate> TileTemplates { get => _tileTemplates; set => _tileTemplates = value; }
}
