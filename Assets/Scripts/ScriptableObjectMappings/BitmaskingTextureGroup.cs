﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(fileName = "BitmaskingTextureGroup", menuName = "Database/Bitmasking Texture Group")]
public class BitmaskingTextureGroup : SerializedScriptableObject
{

#if UNITY_EDITOR

    [SerializeField]
    private BitmaskAdjacencyMode _bitmaskAdjacencyMode = BitmaskAdjacencyMode.FOUR_SIDED;
    public BitmaskAdjacencyMode BitmaskAdjacencyMode { get => _bitmaskAdjacencyMode; private set => _bitmaskAdjacencyMode = value; }

    public bool ShowFourSidedBitmaskMethod
    {
        get => this.BitmaskAdjacencyMode == BitmaskAdjacencyMode.FOUR_SIDED;
    }

    [Button]
    [TitleGroup("Bitmasking Functions")]
    [PropertySpace(5)]
    private void FillBitmaskList(Texture2D defaultTexture, bool clearExistingList)
    {
        if (clearExistingList)
            this.Textures.Clear();

        if (this.BitmaskAdjacencyMode == BitmaskAdjacencyMode.FOUR_SIDED)
        {
            for (var idx = 0; idx < 16; idx++)
                this.Textures.Add(defaultTexture);
        }
    }

    [Button("Set Bitmask Texture")]
    [ShowIf(nameof(ShowFourSidedBitmaskMethod))]
    [TitleGroup("Bitmasking Functions")]
    [InfoBox("Bitmask directions refer to which sides have an adjacent texture.")]
    [PropertySpace(5)]
    private void SetBitmaskTextureFourSided(Texture2D texture, FourSidedBitmaskPosition bitmaskPosition) =>
        this.Textures[(int)bitmaskPosition] = texture;

    private enum FourSidedBitmaskPosition
    {
        Alone,
        North,
        West,
        NorthWest,
        East,
        NorthEast,
        WestEast,
        NorthWestEast,
        South,
        NorthSouth,
        SouthWest,
        NorthSouthWest,
        SouthEast,
        NorthSouthEast,
        SouthWestEast,
        NorthSouthEastWest,
    }

#endif

    [SerializeField]
    [ListDrawerSettings(ShowIndexLabels = true, NumberOfItemsPerPage = 16, HideAddButton = true, HideRemoveButton = true, Expanded = false)]
    private IList<Texture2D> _textures = new List<Texture2D>();
    public IList<Texture2D> Textures { get => _textures; set => _textures = value; }

    public Texture2D GetTextureByIndex(int textureIndex) =>
        this.Textures[textureIndex];
}
