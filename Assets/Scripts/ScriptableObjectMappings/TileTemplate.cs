﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// A tile template is used to define the default configuration for one or more tiles.
/// </summary>
[System.Serializable]
[CreateAssetMenu(fileName = "TileTemplate", menuName = "Database/Tile Template")]
public class TileTemplate : SerializedScriptableObject
{
    [SerializeField]
    private TileType _tileType;
    public TileType TileType { get => _tileType; set => _tileType = value; }

    /// <summary>
    /// Whether tiles with this template should have a tilemap object be used to represent it in
    /// a tilemap. This does not affect the rendering of the tile.
    /// </summary>
    [SerializeField]
    [Title("Tilemap Object Representation")]
    private bool _useTilemapObject = false;
    public bool UseTilemapObjectRepresentation { get => _useTilemapObject; set => _useTilemapObject = value; }

    /// <summary>
    /// Whether the tilemap object representing a tile will be dynamically generated or will use a prefab.
    /// </summary>
    [SerializeField]
    [ShowIf(nameof(UseTilemapObjectRepresentation))]
    private bool _usePrefabTilemapObject = true;
    public bool UsePrefabTilemapObject { get => _usePrefabTilemapObject; set => _usePrefabTilemapObject = value; }

    [SerializeField]
    [ShowIf(nameof(UseTilemapObjectRepresentation))]
    [ShowIf(nameof(UsePrefabTilemapObject))]
    private GameObject _tilemapObjectPrefab;
    public GameObject TilemapObjectPrefab { get => _tilemapObjectPrefab; set => _tilemapObjectPrefab = value; }

    /// <summary>
    /// Whether the tilemap object representing a tile should only be created on the edges of a group of tiles.
    /// This will ignore inner tiles.
    /// </summary>
    [SerializeField]
    [ShowIf(nameof(UseTilemapObjectRepresentation))]
    private bool _useOnEdgeOnly = true;
    public bool UseTilemapObjectOnEdgeOnly { get => _useOnEdgeOnly; set => _useOnEdgeOnly = value; }

    [SerializeField]
    [Title("Rendering")]
    [Required]
    private TileRenderData _tileRenderData;
    public TileRenderData RenderData { get => _tileRenderData; set => _tileRenderData = value; }
}

