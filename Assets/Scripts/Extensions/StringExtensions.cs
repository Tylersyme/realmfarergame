﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;

public static class StringExtensions
{
    public static string StripAllWhitespace(this string str) =>
        Regex.Replace(str, @"\s+", "");
}
