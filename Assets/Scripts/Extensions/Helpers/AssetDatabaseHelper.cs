﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class AssetDatabaseHelper
{
    public static IList<T> GetAssetsFromPath<T>(string path) where T : UnityEngine.Object
    {
        var assetsFound = new List<T>();

        string[] filePaths = System.IO.Directory.GetFiles(path);

        if (filePaths != null && filePaths.Length > 0)
        {
            for (int i = 0; i < filePaths.Length; i++)
            {
                UnityEngine.Object obj = UnityEditor.AssetDatabase.LoadAssetAtPath(filePaths[i], typeof(T));
                if (obj is T asset)
                {
                    if (!assetsFound.Contains(asset))
                        assetsFound.Add(asset);
                }
            }
        }

        return assetsFound;
    }
}
