﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Vector2Extensions
{
    public static bool IsEquivalent(this Vector2 vector2, Vector2 other)
    {
        return vector2.x == other.x && vector2.y == other.y;
    }

    public static bool IsEquivalent(this Vector2Int vector2, Vector2Int other)
    {
        return vector2.x == other.x && vector2.y == other.y;
    }

    public static Vector2 Add(this Vector2 vector2, Vector2 other)
    {
        return new Vector2(vector2.x + other.x, vector2.y + other.y);
    }

    public static Vector2 AddX(this Vector2 vector2, float x)
    {
        return new Vector2(vector2.x + x, vector2.y);
    }

    public static Vector2 AddY(this Vector2 vector2, float y)
    {
        return new Vector2(vector2.x, vector2.y + y);
    }

    public static Vector2 Subtract(this Vector2 vector2, Vector2 other)
    {
        return new Vector2(vector2.x - other.x, vector2.y - other.y);
    }

    public static Vector2 Multiply(this Vector2 vector2, Vector2 toMultiply)
    {
        return new Vector2(vector2.x * toMultiply.x, vector2.y * toMultiply.y);
    }

    public static Vector2 Multiply(this Vector2 vector2, float amount)
    {
        return new Vector2(vector2.x * amount, vector2.y * amount);
    }

    public static Vector2 Multiply(this Vector2Int vector2, float amount)
    {
        return new Vector2(vector2.x * amount, vector2.y * amount);
    }

    public static Vector2 Divide(this Vector2 vector2, float amount)
    {
        return new Vector2(vector2.x / amount, vector2.y / amount);
    }

    public static Vector2 Divide(this Vector2 vector2, Vector2 toDivide) =>
        new Vector2(vector2.x / toDivide.x, vector2.y / toDivide.y);

    public static Vector2 SetX(this Vector2 vector2, float x)
    {
        return new Vector2(x, vector2.y);
    }

    public static Vector2 SetY(this Vector2 vector2, float y)
    {
        return new Vector2(vector2.x, y);
    }

    public static bool HasNegative(this Vector2 vector2)
    {
        return vector2.x < 0 || vector2.y < 0;
    }

    public static Vector2 SetMagnitude(this Vector2 vector2, float magnitude)
    {
        if (vector2.magnitude == 0)
            return Vector2.one.Multiply(magnitude);

        var magnitudeRatio = magnitude / vector2.magnitude;
        return vector2.Multiply(magnitudeRatio);
    }
 
    public static Vector2 ChangeMagnitude(this Vector2 vector2, float amount)
    {
        return vector2.SetMagnitude(vector2.magnitude + amount);
    }

    public static Vector2Int Add(this Vector2Int vector2, Vector2Int other)
    {
        return new Vector2Int(vector2.x + other.x, vector2.y + other.y);
    }

    public static Vector2Int Subtract(this Vector2Int vector2, Vector2Int other)
    {
        return new Vector2Int(vector2.x - other.x, vector2.y - other.y);
    }

    public static Vector2Int Multiply(this Vector2Int vector2, Vector2Int other)
    {
        return new Vector2Int(vector2.x * other.x, vector2.y * other.y);
    }

    public static Vector2Int ChangeX(this Vector2Int vector2, int amount)
    {
        return new Vector2Int(vector2.x + amount, vector2.y);
    }

    public static Vector2Int ChangeY(this Vector2Int vector2, int amount)
    {
        return new Vector2Int(vector2.x, vector2.y + amount);
    }

    public static Vector2Int WithX(this Vector2Int vector2, int x)
    {
        return new Vector2Int(x, vector2.y);
    }

    public static Vector2Int WithY(this Vector2Int vector2, int y)
    {
        return new Vector2Int(vector2.x, y);
    }

    public static Vector2Int AsVector2Int(this Vector2 vector2)
    {
        return new Vector2Int(Mathf.FloorToInt(vector2.x), Mathf.FloorToInt(vector2.y));
    }

    public static Vector3 AsVector3(this Vector2 vector2)
    {
        return new Vector3(vector2.x, vector2.y, 0);
    }

    public static Vector2 AsVector2(this Vector2Int vector2)
    {
        return new Vector2(vector2.x, vector2.y);
    }

    public static Vector3 AsVector3(this Vector2Int vector2)
    {
        return new Vector3(vector2.x, vector2.y, 0);
    }

    public static Vector3Int AsVector3Int(this Vector2Int vector2)
    {
        return new Vector3Int(vector2.x, vector2.y, 0);
    }

    public static Vector2 SetAngle(this Vector2 vector2, float degrees)
    {
        return DegreeToVector2(degrees).SetMagnitude(vector2.magnitude);
    }

    public static Vector2 RadianToVector2(float radian)
    {
        return new Vector2(Mathf.Cos(radian), Mathf.Sin(radian));
    }

    public static Vector2 RadianToVector2(float radian, float length)
    {
        return RadianToVector2(radian) * length;
    }

    public static Vector2 DegreeToVector2(float degree)
    {
        return RadianToVector2(degree * Mathf.Deg2Rad);
    }

    public static Vector2 DegreeToVector2(float degree, float length)
    {
        return RadianToVector2(degree * Mathf.Deg2Rad) * length;
    }
}
