﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class ComponentExtensions
{
    public static IList<Type> GetRequiredComponents<TComponent>() where TComponent : Component
    {
        return typeof(TComponent)
            .GetAttributesOfType<RequireComponent>()
            .SelectMany(a => new Type[] 
            { 
                a.m_Type0,  
                a.m_Type1,
                a.m_Type2,
            })
            .Where(t => t != null)
            .ToList();
    }
}
