﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using System;

#if UNITY_EDITOR
using UnityEditor;
#endif

public static class GameObjectExtensions
{
    public static void SetParent(this GameObject child, GameObject parent)
    {
        if (child.transform.GetType() == typeof(RectTransform))
            child.transform.AsRectTransform().SetParent(parent.transform);
        else
            child.transform.parent = parent.transform;
    }

    public static GameObject GetParent(this GameObject child)
    {
        return child.transform.parent?.gameObject;
    }

    public static GameObject GetRootParent(this GameObject child)
    {
        return child.transform.root.gameObject;
    }

    public static void DetachFromParent(this GameObject child)
    {
        child.transform.parent = null;
    }

    public static void DestroySelf(this GameObject gameObject)
    {
        UnityEngine.Object.Destroy(gameObject);
    }

    public static void DisableSelf(this GameObject gameObject) =>
        gameObject.SetActive(false);

    public static void EnableSelf(this GameObject gameObject) =>
        gameObject.SetActive(true);

    public static bool HasComponent<T>(this GameObject gameObject) where T : Component =>
        gameObject.GetComponent<T>() != null;

    public static bool HasComponent(this GameObject gameObject, Type type) =>
        gameObject.GetComponent(type) != null;

    public static T GetComponentInSelfOrParent<T>(this GameObject gameObject) where T : Component
    {
        if (gameObject.HasComponent<T>())
            return gameObject.GetComponent<T>();

        return gameObject
            .GetParent()
            .GetComponent<T>();
    }

    public static List<GameObject> GetChildren(this GameObject gameObject)
    {
        var childTransforms = gameObject.GetComponentsInChildren<Transform>();
        return childTransforms
            .Select(t => t.gameObject)
            .Where(go => go != gameObject)
            .ToList();
    }

    public static List<GameObject> GetImmediateChildren(this GameObject gameObject)
    {
        var childTransforms = gameObject.GetComponentsInChildren<Transform>();
        return childTransforms
            .Where(t => t.parent == gameObject.transform)
            .Select(t => t.gameObject)
            .ToList();
    }

    public static GameObject AddChild(this GameObject gameObject, string name = "Child Game Object")
    {
        var childGameObject = new GameObject(name);

        childGameObject.SetParent(gameObject);

        return childGameObject;
    }

#if UNITY_EDITOR
    public static bool IsPrefab(this GameObject gameObject)
    {
        return PrefabUtility.GetCorrespondingObjectFromSource(gameObject) == null;
    }
#endif
}
