﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class MathExtended
{
    public static bool IsBetweenInclusive(int value, int lowerBound, int upperBound)
    {
        return value >= lowerBound && value <= upperBound;
    }

    public static bool IsBetweenInclusive(float value, float lowerBound, float upperBound)
    {
        return value >= lowerBound && value <= upperBound;
    }

    public static bool IsBetweenExclusive(int value, int lowerBound, int upperBound)
    {
        return value > lowerBound && value < upperBound;
    }

    public static float Normalize(float value, float min, float max)
    {
        return (value - min) / (max - min);
    }
}
