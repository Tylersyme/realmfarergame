﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

public static class AnimationClipExtensions
{
    public static List<Sprite> GetSprites(this AnimationClip animationClip)
    {
        var sprites = new List<Sprite>();

        if (animationClip == null)
            return sprites;

        foreach (var binding in AnimationUtility.GetObjectReferenceCurveBindings(animationClip))
        {
            var keyframeSprites = AnimationUtility
                .GetObjectReferenceCurve(animationClip, binding)
                .Select(f => f.value)
                .OfType<Sprite>();

            sprites.AddRange(keyframeSprites);
        }

        return sprites;
    }

    public static Sprite GetFirstSprite(this AnimationClip animationClip)
    {
        foreach (var binding in AnimationUtility.GetObjectReferenceCurveBindings(animationClip))
        {
            var sprite = AnimationUtility
                .GetObjectReferenceCurve(animationClip, binding)
                .Select(f => f.value)
                .OfType<Sprite>()
                .FirstOrDefault();

            if (sprite == null)
                continue;

            return sprite;
        }

        return null;
    }
}
