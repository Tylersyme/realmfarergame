﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class AttributeHelper
{
    public static IList<TAttribute> GetAttributesOfType<TAttribute>(this Type classType) 
        where TAttribute : Attribute =>
        Attribute
            .GetCustomAttributes(classType, typeof(TAttribute))
            .Cast<TAttribute>()
            .ToList();
}
