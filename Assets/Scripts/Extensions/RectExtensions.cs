﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class RectExtensions
{
    public static bool ContainsInclusive(this Rect rect, Vector2 point)
    {
        return point.x >= rect.xMin && point.x <= rect.xMax &&
               point.y >= rect.yMin && point.y <= rect.yMax;
    }
}
