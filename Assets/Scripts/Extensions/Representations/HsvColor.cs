﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HsvColor
{
    public HsvColor(Color color)
    {
        var hue = default(float);
        var saturation = default(float);
        var value = default(float);

        Color.RGBToHSV(color, out hue, out saturation, out value);

        this.Hue = hue;
        this.Saturation = saturation;
        this.Value = value;
    }

    public float Hue { get; private set; }

    public float Saturation { get; private set; }

    public float Value { get; private set; }

    public HsvColor SetHue(float hue)
    {
        this.Hue = hue;

        return this;
    }

    public HsvColor SetSaturation(float saturation)
    {
        this.Saturation = saturation;

        return this;
    }

    public HsvColor SetValue(float value)
    {
        this.Value = value;

        return this;
    }

    public Color ToColor() =>
        Color.HSVToRGB(this.Hue, this.Saturation, this.Value);
}
