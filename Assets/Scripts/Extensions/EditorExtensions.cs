﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;

public static class EditorExtensions
{
    public static T TargetAs<T>(this Editor editor) where T : MonoBehaviour
    {
        return editor.target as T;
    }

    public static GameObject TargetGameobject(this Editor editor)
    {
        return (editor.target as MonoBehaviour)?.gameObject;
    }
}
#endif