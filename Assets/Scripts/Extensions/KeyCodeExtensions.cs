﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class KeyCodeExtensions
{
    public static int GetIntegerFromNumberKeycode(this KeyCode keyCode)
    {
        switch (keyCode)
        {
            case KeyCode.Alpha0:
                return 0;
            case KeyCode.Alpha1:
                return 1;
            case KeyCode.Alpha2:
                return 2;
            case KeyCode.Alpha3:
                return 3;
            case KeyCode.Alpha4:
                return 4;
            case KeyCode.Alpha5:
                return 5;
            case KeyCode.Alpha6:
                return 6;
            case KeyCode.Alpha7:
                return 7;
            case KeyCode.Alpha8:
                return 8;
            case KeyCode.Alpha9:
                return 9;
        }

        throw new System.ArgumentException($"Error getting integer from number keycode: The keycode {keyCode} does not represent a number.");
    }
}
