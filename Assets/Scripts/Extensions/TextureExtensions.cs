﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class TextureExtensions
{
    public static Vector2Int GetPixelSize(this Texture texture)
    {
        return new Vector2Int(texture.width, texture.height);
    }
}
