﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class CameraExtensions
{

    public static Vector2 OrthographicSize(this Camera camera)
    {
        var height = 2f * camera.orthographicSize;
        var width = height * camera.aspect;
        return new Vector2(width, height);
    }
    
    public static Vector2 CenterPoint(this Camera camera)
    {
        var cameraPosition = camera.gameObject.GetComponent<Transform>().position;
        return new Vector2(cameraPosition.x, cameraPosition.y);
    }

    public static Vector2 TopLeftPoint(this Camera camera)
    {
        var center = camera.CenterPoint();
        var size = camera.OrthographicSize();
        return new Vector2(center.x - (size.x / 2), center.y + (size.y / 2));
    }

    public static Vector2 TopRightPoint(this Camera camera)
    {
        var center = camera.CenterPoint();
        var size = camera.OrthographicSize();
        return new Vector2(center.x + (size.x / 2), center.y + (size.y / 2));
    }

    public static Vector2 BottomLeftPoint(this Camera camera)
    {
        var center = camera.CenterPoint();
        var size = camera.OrthographicSize();
        return new Vector2(center.x - (size.x / 2), center.y - (size.y / 2));
    }

    public static Vector2 BottomRightPoint(this Camera camera)
    {
        var center = camera.CenterPoint();
        var size = camera.OrthographicSize();
        return new Vector2(center.x + (size.x / 2), center.y - (size.y / 2));
    }

    public static float RightmostX(this Camera camera)
    {
        var center = camera.CenterPoint();
        var size = camera.OrthographicSize();
        return center.x + (size.x / 2);
    }

    public static float LeftmostX(this Camera camera)
    {
        var center = camera.CenterPoint();
        var size = camera.OrthographicSize();
        return center.x - (size.x / 2);
    }

    public static Vector2 MouseWorldPosition(this Camera camera) =>
        camera
            .ScreenToWorldPoint(Input.mousePosition)
            .AsVector2();
}
