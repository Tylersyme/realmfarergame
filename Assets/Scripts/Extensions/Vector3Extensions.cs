﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Vector3Extensions
{
    public static Vector3 SetX(this Vector3 vector3, float x)
    {
        return new Vector3(x, vector3.y, vector3.z);
    }

    public static Vector3 SetY(this Vector3 vector3, float y)
    {
        return new Vector3(vector3.x, y, vector3.z);
    }

    public static Vector3 SetZ(this Vector3 vector3, float z)
    {
        return new Vector3(vector3.x, vector3.y, z);
    }

    public static Vector3Int SetX(this Vector3Int vector3, int x)
    {
        return new Vector3Int(x, vector3.y, vector3.z);
    }

    public static Vector3Int SetY(this Vector3Int vector3, int y)
    {
        return new Vector3Int(vector3.x, y, vector3.z);
    }

    public static Vector3Int SetZ(this Vector3Int vector3, int z)
    {
        return new Vector3Int(vector3.x, vector3.y, z);
    }

    public static Vector3 ChangeX(this Vector3 vector3, float x)
    {
        return new Vector3(vector3.x + x, vector3.y, vector3.z);
    }

    public static Vector3 ChangeY(this Vector3 vector3, float y)
    {
        return new Vector3(vector3.x, vector3.y + y, vector3.z);
    }

    public static Vector3 ChangeZ(this Vector3 vector3, float z)
    {
        return new Vector3(vector3.x, vector3.y, vector3.z + z);
    }

    public static Vector3Int ChangeX(this Vector3Int vector3, int x)
    {
        return new Vector3Int(vector3.x + x, vector3.y, vector3.z);
    }

    public static Vector3Int ChangeY(this Vector3Int vector3, int y)
    {
        return new Vector3Int(vector3.x, vector3.y + y, vector3.z);
    }

    public static Vector3Int ChangeZ(this Vector3Int vector3, int z)
    {
        return new Vector3Int(vector3.x, vector3.y, vector3.z + z);
    }

    public static Vector3Int Change(this Vector3Int vector3, int value)
    {
        return new Vector3Int(vector3.x + value, vector3.y + value, vector3.z + value);
    }

    public static Vector3 Abs(this Vector3 vector3)
    {
        return new Vector3(Mathf.Abs(vector3.x), Mathf.Abs(vector3.y), Mathf.Abs(vector3.z));
    }

    public static Vector3 Add(this Vector3 vector3, Vector3 toAdd)
    {
        return new Vector3(vector3.x + toAdd.x, vector3.y + toAdd.y, vector3.z + toAdd.z);
    }

    public static Vector3 Subtract(this Vector3 vector3, Vector3 toSubtract)
    {
        return new Vector3(vector3.x - toSubtract.x, vector3.y - toSubtract.y, vector3.z - toSubtract.z);
    }

    public static Vector3 Multiply(this Vector3 vector3, float amount)
    {
        return new Vector3(vector3.x * amount, vector3.y * amount, vector3.z * amount);
    }

    public static Vector3 Multiply(this Vector3 vector3, Vector3 toMultiply)
    {
        return new Vector3(vector3.x * toMultiply.x, vector3.y * toMultiply.y, vector3.z * toMultiply.z);
    }

    public static Vector2 Divide(this Vector3 vector2, Vector2 toDivide) =>
        new Vector2(vector2.x * toDivide.x, vector2.y * toDivide.y);

    public static Vector2 AsVector2(this Vector3 vector3)
    {
        return new Vector2(vector3.x, vector3.y);
    }

    public static Vector3Int AsVector3Int(this Vector3 vector3)
    {
        return new Vector3Int((int)vector3.x, (int)vector3.y, (int)vector3.z);
    }

    public static Vector3 ShrinkBy(this Vector3 vector3, Vector3 other)
    {
        var x = vector3.x < 0 ? vector3.x + other.x : vector3.x - other.x;
        var y = vector3.y < 0 ? vector3.y + other.y : vector3.y - other.y;
        var z = vector3.z < 0 ? vector3.z + other.z : vector3.z - other.z;

        return new Vector3(x, y, z);
    }

    public static Vector3 Flip(this Vector3 vector3)
    {
        return vector3.Multiply(-1);
    }

    public static Vector3 SetMagnitude(this Vector3 vector3, float magnitude)
    {
        if (vector3.magnitude == 0)
            return Vector3.one.Multiply(magnitude);

        var magnitudeRatio = magnitude / vector3.magnitude;
        return vector3.Multiply(magnitudeRatio);
    }

    public static bool IsEquivalent(this Vector3 vector3, Vector3 other)
    {
        return vector3.x == other.x && 
               vector3.y == other.y &&
               vector3.z == other.z;
    }

    public static bool IsEquivalent(this Vector3Int vector3, Vector3Int other)
    {
        return vector3.x == other.x && 
               vector3.y == other.y &&
               vector3.z == other.z;
    }

    public static Vector2 AsVector2(this Vector3Int vector3)
    {
        return new Vector2(vector3.x, vector3.y);
    }

    public static Vector3 AsVector3(this Vector3Int vector3)
    {
        return new Vector3(vector3.x, vector3.y, vector3.z);
    }

    public static Vector2Int AsVector2Int(this Vector3 vector3)
    {
        return new Vector2Int((int)vector3.x, (int)vector3.y);
    }

    public static Vector2Int AsVector2Int(this Vector3Int vector3)
    {
        return new Vector2Int(vector3.x, vector3.y);
    }
}
