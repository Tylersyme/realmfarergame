﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public static class IOExtended
{
    public static IEnumerable<string> GetFilePathsRecursive(string path)
    {
        var queue = new Queue<string>();
        queue.Enqueue(path);
        while (queue.Count > 0)
        {
            path = queue.Dequeue();
            try
            {
                foreach (string subDir in Directory.GetDirectories(path))
                {
                    queue.Enqueue(subDir);
                }
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex);
            }
            string[] files = null;
            try
            {
                files = Directory.GetFiles(path);
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex);
            }
            if (files != null)
            {
                for (int i = 0; i < files.Length; i++)
                {
                    yield return files[i];
                }
            }
        }
    }

    public static string GetRelativeFromAbsolute(string path, string absolutePortion)
    {
        var relativePath = path;

        if (path.StartsWith(absolutePortion))
            relativePath = path.Substring(DataSerializer.RESOURCE_TABLES_ABSOLUTE_PATH.Length);

        if (relativePath.StartsWith("/") || relativePath.StartsWith(Path.DirectorySeparatorChar.ToString()))
            relativePath = relativePath.Substring(1);

        return relativePath;
    }

    public static Tuple<string, string> SplitFileNameAndDirectory(string path)
    {
        var fileName = Path.GetFileName(path);
        var relativePath = Path.GetDirectoryName(path);

        return new Tuple<string, string>(fileName, relativePath);
    }
}
