﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ObjectExtensions
{
    public static void SafeDestroyImmediate(this Object obj, GameObject gameObject)
    {
        if (Application.isPlaying)
            Object.Destroy(gameObject);
        else
            Object.DestroyImmediate(gameObject);
    }
}
