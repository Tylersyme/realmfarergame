﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class BoxCollider2DExtensions
{
   
    public static void FitToSprite(this BoxCollider2D collider2D)
    {
        var renderer = collider2D.GetComponentInParent<SpriteRenderer>();
        collider2D.FitToSprite(renderer);
    }

    public static void FitToSprite(this BoxCollider2D collider2D, SpriteRenderer renderer)
    {
        collider2D.size = renderer.size;
    }

    public static Vector2 CenterPoint(this BoxCollider2D collider2D)
    {
        return collider2D.transform.TransformPoint(collider2D.offset);
    }

    public static Vector2 TopLeftPoint(this BoxCollider2D collider2D)
    {
        return collider2D.CenterPoint().Add(new Vector2(-collider2D.size.x, collider2D.size.y));
    }

    public static Vector2 RightEdge(this BoxCollider2D collider2D)
    {
        return collider2D.CenterPoint().Add(new Vector2(collider2D.size.x, 0));
    }

    public static Vector2 TopEdge(this BoxCollider2D collider2D)
    {
        return collider2D.CenterPoint().Add(new Vector2(0, collider2D.size.y));
    }

    public static Vector2 BottomEdge(this BoxCollider2D collider2D)
    {
        return collider2D.CenterPoint().Add(new Vector2(0, -collider2D.size.y));
    }
}
