﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ColorExtensions
{
    public static Color SetRed(this Color color, float red) =>
        new Color(red, color.g, color.b, color.a);

    public static Color SetGreen(this Color color, float green) =>
        new Color(color.r, green, color.b, color.a);

    public static Color SetBlue(this Color color, float blue) =>
        new Color(color.r, color.g, blue, color.a);

    public static Color SetAlpha(this Color color, float alpha) =>
        new Color(color.r, color.g, color.b, alpha);

    public static HsvColor GetHsv(this Color color) =>
        new HsvColor(color);

    public static Color GetCopy(this Color color) =>
        new Color(color.r, color.g, color.b, color.a);

    public static Color SetValuePercentage(this Color color, float valuePercentage) =>
        color
            .GetHsv()
            .SetValue(valuePercentage)
            .ToColor();

    public static Color SetSaturationPercentage(this Color color, float saturationPercentage) =>
        color
            .GetHsv()
            .SetSaturation(saturationPercentage)
            .ToColor();

    public static Color CreateColorNotNormalized(float r, float g, float b, float a = 1.0f) =>
        new Color(r / 255f, g / 255f, b / 255f, a);

    public static Color AdjustBrightness(this Color color, float correctionFactor)
    {
        float red = ((float)color.r) * 255;
        float green = ((float)color.g) * 255;
        float blue = ((float)color.b) * 255;

        if (correctionFactor < 0)
        {
            correctionFactor = 1 + correctionFactor;
            red *= correctionFactor;
            green *= correctionFactor;
            blue *= correctionFactor;
        }
        else
        {
            red = (255 - red) * correctionFactor + red;
            green = (255 - green) * correctionFactor + green;
            blue = (255 - blue) * correctionFactor + blue;
        }

        return CreateColorNotNormalized(red, green, blue, color.a);
    }
}
