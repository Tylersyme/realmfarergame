﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class SpriteRendererExtensions
{

    public static Vector2 SetWidth(this SpriteRenderer renderer, float width)
    {
        renderer.size = new Vector2(width, renderer.size.y);

        return renderer.size;
    }

    public static Vector2 SetHeight(this SpriteRenderer renderer, float height)
    {
        renderer.size = new Vector2(renderer.size.x, height);

        return renderer.size;
    }

    public static Vector2 SetSize(this SpriteRenderer renderer, float width, float height)
    {
        renderer.size = new Vector2(width, height);

        return renderer.size;
    }

}
