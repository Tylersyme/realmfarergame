﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class DirectionExtensions
{
    public static AdjacencyType ToAdjacencyType(this BitmaskAdjacencyMode bitmaskAdjacencyMode)
    {
        if (bitmaskAdjacencyMode == BitmaskAdjacencyMode.FOUR_SIDED)
            return AdjacencyType.CARDINAL;
        else if (bitmaskAdjacencyMode == BitmaskAdjacencyMode.EIGHT_SIDED)
            return AdjacencyType.FULL;
        else if (bitmaskAdjacencyMode == BitmaskAdjacencyMode.NONE)
            throw new System.Exception($"Error converting bitmask adjacency mode to adjacency type: The bitmask mode {bitmaskAdjacencyMode} is not valid and cannot be converted.");

        throw new System.NotImplementedException($"Error converting bitmask adjacency mode to adjacency type: The bitmask mode {bitmaskAdjacencyMode} is supported.");
    }
}
