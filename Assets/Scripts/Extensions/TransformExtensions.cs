﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class TransformExtensions
{
    public static RectTransform AsRectTransform(this Transform transform)
    {
        return (RectTransform)transform;
    }
}
