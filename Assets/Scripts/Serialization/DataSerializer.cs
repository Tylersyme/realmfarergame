﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public sealed class DataSerializer
{
    public static DataSerializer Instance { get; } = new DataSerializer();

    public const string DEFAULT_FILE_EXTENSION = XmlSerializerService.DEFAULT_EXTENSION;

    public static string APPLICATION_DATA_PATH;
    public static string STREAMING_ASSETS_PATH;
    public static string RESOURCE_TABLES_ABSOLUTE_PATH;
    public static string RESOURCE_TABLES_RELATIVE_PATH;

    private ISerializer _serializer;

    public DataSerializer()
    {
        APPLICATION_DATA_PATH = $"{Application.dataPath}/Resources/Data";
        STREAMING_ASSETS_PATH = $"{Application.streamingAssetsPath}";
        RESOURCE_TABLES_RELATIVE_PATH = "ResourceTables";
        RESOURCE_TABLES_ABSOLUTE_PATH = $@"{STREAMING_ASSETS_PATH}\{RESOURCE_TABLES_RELATIVE_PATH}";

        _serializer = new XmlSerializerService(STREAMING_ASSETS_PATH);
    }

    public void Serialize(object toSerialize, string fileName, string relativePath)
    {
        _serializer.Serialize(toSerialize, fileName, relativePath);
    }

    public void Serialize(object toSerialize, string relativeFilePath)
    {
        var fileName = Path.GetFileName(relativeFilePath);
        var relativePath = Path.GetDirectoryName(relativeFilePath);

        Serialize(toSerialize, fileName, relativePath);
    }

    public T Deserialize<T>(string fileName, string relativePath)
    {
        return _serializer.Deserialize<T>(fileName, relativePath);
    }

    public T Deserialize<T>(string relativeFilePath)
    {
        var fileNameAndDirectory = IOExtended.SplitFileNameAndDirectory(relativeFilePath);
        var fileName = fileNameAndDirectory.Item1;
        var relativePath = fileNameAndDirectory.Item2;

        return Deserialize<T>(fileName, relativePath);
    }

    public T Deserialize<T>(ResourceInfo resourceInfo)
    {
        return _serializer.Deserialize<T>(resourceInfo.ResourceName, resourceInfo.RelativePath);
    }
}
