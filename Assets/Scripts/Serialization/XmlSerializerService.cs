﻿using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;
using System;
using UnityEngine;
using System.IO;

public class XmlSerializerService : ISerializer
{
    public const string DEFAULT_EXTENSION = ".xml";

    private string RootPath { get; set; }

    public XmlSerializerService(string rootPath)
    {
        this.RootPath = rootPath;
    }

    public void Serialize(object toSerialize, string fileName, string relativePath)
    {
        string absolutePath = GetAbsolutePath(fileName, relativePath);

        if (File.Exists(absolutePath))
            File.Delete(absolutePath);

        var serializer = GetSerializer(toSerialize.GetType());
        using (var stream = new FileStream(absolutePath, FileMode.OpenOrCreate, FileAccess.Write))
        {
            serializer.Serialize(stream, toSerialize);
        }
    }

    public T Deserialize<T>(string fileName, string relativePath)
    {
        string absolutePath = GetAbsolutePath(fileName, relativePath);

        var serializer = GetSerializer(typeof(T));
        using (var stream = new FileStream(absolutePath, FileMode.Open, FileAccess.Read))
        {
            var deserializedObject = (T)serializer.Deserialize(stream);
            return deserializedObject;
        }
    }

    private string GetAbsolutePath(string fileName, string relativePath)
    {
        // Add extension if it doesn't exist
        if (Path.GetExtension(fileName) != DEFAULT_EXTENSION)
            fileName = $"{fileName}{DEFAULT_EXTENSION}";

        return Path.Combine(this.RootPath, relativePath, fileName);
    }

    private XmlSerializer GetSerializer(Type type)
    {
        return new XmlSerializer(type);
    }
}
