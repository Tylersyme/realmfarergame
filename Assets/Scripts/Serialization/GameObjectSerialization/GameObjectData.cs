﻿using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GameObjectData
{
    /// <summary>
    /// Stores which prefab this data belongs to.
    /// </summary>
    [SerializeField]
    private PrefabContainer _prefabContainer;
    public PrefabContainer PrefabContainer { get => _prefabContainer; set => _prefabContainer = value; }

    /// <summary>
    /// Stores the data for each serialized component by a unique key.
    /// </summary>
    [HideInInspector]
    [SerializeField]
    private Dictionary<string, object> _componentData = new Dictionary<string, object>();
    private Dictionary<string, object> ComponentData { get => _componentData; set => _componentData = value; }

    public GameObjectData() { }

    public GameObjectData(PrefabContainer prefabRecord)
    {
        PrefabContainer = prefabRecord;
    }

    public void AddComponentData(string key, object componentData)
    {
        this.ComponentData.Add(key, componentData);
    }

    public T GetComponentData<T>(string key)
    {
        return (T)this.ComponentData[key];
    }

    public bool HasComponentData(string key)
    {
        return this.ComponentData.ContainsKey(key);
    }
}
