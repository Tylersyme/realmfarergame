﻿using Sirenix.OdinInspector;
using Sirenix.Serialization;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[RequireComponent(typeof(GameObjectDataModule))]
public class BuiltInComponentSerializer : MonoBehaviour
{
    [SerializeField]
    private List<SerializableTransform> _serializedTransforms = new List<SerializableTransform>();
    private List<SerializableTransform> SerializedTransforms { get => _serializedTransforms; set => _serializedTransforms = value; }

    private void Awake()
    {
        Subscribe();
    }

    private void OnDestroy()
    {
        Unsubscribe();
    }

    private void Subscribe()
    {
        var gameObjectDataModule = this.gameObject.GetComponent<GameObjectDataModule>();
        
        gameObjectDataModule.SerializeToGameObjectDataEvent += OnSerializeToGameObjectData;
        gameObjectDataModule.DeserializeFromGameObjectDataEvent += OnDeserializeFromGameObjectData;
    }

    private void Unsubscribe()
    {
        var gameObjectDataModule = this.gameObject.GetComponent<GameObjectDataModule>();

        gameObjectDataModule.SerializeToGameObjectDataEvent -= OnSerializeToGameObjectData;
        gameObjectDataModule.DeserializeFromGameObjectDataEvent -= OnDeserializeFromGameObjectData;
    }

    private void OnSerializeToGameObjectData(object sender, GameObjectData gameObjectData)
    {
        foreach (var serializableTransform in this.SerializedTransforms)
        {
            var transformData = GetTransformData(serializableTransform.Transform);

            gameObjectData.AddComponentData(serializableTransform.UniqueSerializationId, transformData);
        }
    }

    private void OnDeserializeFromGameObjectData(object sender, GameObjectData gameObjectData)
    {
        foreach (var serializableTransform in this.SerializedTransforms)
        {
            var transformData = gameObjectData.GetComponentData<TransformData>(serializableTransform.UniqueSerializationId);

            LoadTransformData(transformData, serializableTransform.Transform);
        }
    }

    private TransformData GetTransformData(Transform transform)
    {
        return new TransformData
        {
            LocalPosition = transform.localPosition,
            WorldPosition = transform.position,
            LocalRotation = transform.localRotation,
            WorldRotation = transform.rotation,
            LocalScale = transform.localScale,
        };
    }

    private void LoadTransformData(TransformData transformData, Transform transform)
    {
        transform.position = transformData.WorldPosition;
        transform.localPosition = transformData.LocalPosition;
        transform.rotation = transformData.WorldRotation;
        transform.localRotation = transformData.LocalRotation;
        transform.localScale = transformData.LocalScale;
    }

    #if UNITY_EDITOR

    [Button(ButtonSizes.Medium)]
    private void LoadAllTransforms()
    {
        foreach (var transform in this.gameObject.GetComponentsInChildren<Transform>())
        {
            if (this.SerializedTransforms.Any(st => st.Transform == transform))
                continue;

            this.SerializedTransforms.Add(new SerializableTransform
            {
                Transform = transform,
            });
        }
    }

    #endif

    [System.Serializable]
    private class SerializableTransform
    {
        public ISerializationIdGenerator _serializationIdGenerator { get; set; } = new SerializationIdGenerator();

        public SerializableTransform()
        {
            this.UniqueSerializationId = _serializationIdGenerator.GenerateId("Transform");
        }

        [SerializeField]
        [ChildGameObjectsOnly]
        private Transform _transform;
        public Transform Transform { get => _transform; set => _transform = value; }

        [SerializeField]
        [ReadOnly]
        private string _uniqueSerializationId;
        public string UniqueSerializationId { get => _uniqueSerializationId; private set => _uniqueSerializationId = value; }
    }
}
