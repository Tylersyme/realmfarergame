﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SerializationIdGenerator : ISerializationIdGenerator
{
    public string GenerateId(string humanReadableKey = null)
    {
        var id = humanReadableKey == null
            ? Guid
                .NewGuid()
                .ToString()
            : $"{humanReadableKey}:{Guid.NewGuid()}";

        return id;
    }
}
