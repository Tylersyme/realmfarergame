﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComponentSerializationSubscriber<TComponent, TData> 
    : IComponentSerializationSubscriber<TComponent, TData> where TComponent : Component, ISerializedComponent where TData : new()
{
    private readonly TComponent _component;

    private readonly IComponentSerializerService<TComponent, TData> _componentSerializerService;

    public ComponentSerializationSubscriber(TComponent component)
    {
        _component = component;
        _componentSerializerService = new ComponentSerializerService<TComponent, TData>();
    }

    public void SubscribeAsSerializable()
    {
        var gameObjectDataModule = _componentSerializerService.GetGameObjectDataModule(_component);

        if (gameObjectDataModule == null)
            return;

        gameObjectDataModule.SerializeToGameObjectDataEvent += OnSerializeToGameObjectData;
        gameObjectDataModule.DeserializeFromGameObjectDataEvent += OnDeserializeFromGameObjectData;
    }

    public void UnsubscribeAsSerializable()
    {
        var gameObjectDataModule = _componentSerializerService.GetGameObjectDataModule(_component);

        if (gameObjectDataModule == null)
            return;

        gameObjectDataModule.SerializeToGameObjectDataEvent -= OnSerializeToGameObjectData;
        gameObjectDataModule.DeserializeFromGameObjectDataEvent -= OnDeserializeFromGameObjectData;
    }

    private void OnSerializeToGameObjectData(object sender, GameObjectData gameObjectData)
    {
        _componentSerializerService.SaveDataInto(gameObjectData, _component);
    }

    private void OnDeserializeFromGameObjectData(object sender, GameObjectData gameObjectData)
    {
        _componentSerializerService.LoadDataIntoIfExists(gameObjectData, _component);
    }
}
