﻿using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameObjectDataModule : MonoBehaviour
{
    /// <summary>
    /// Used to let serialization know what prefab this game object is using.
    /// </summary>
    [SerializeField]
    [Required]
    private PrefabContainer _prefabContainer;
    public PrefabContainer PrefabContainer { get => _prefabContainer; set => _prefabContainer = value; }

    /// <summary>
    /// Subscribed to by components which have data that should be stored in GameObjectData.
    /// <see cref="GetGameObjectData"/>
    /// </summary>
    [NonSerialized]
    public EventHandler<GameObjectData> SerializeToGameObjectDataEvent;

    /// <summary>
    /// Subscribed to by components that should load their data from GameObjectData.
    /// </summary>
    [NonSerialized]
    public EventHandler<GameObjectData> DeserializeFromGameObjectDataEvent;

    /// <summary>
    /// Transfers a game object's data into a GameObjectData which is returned.
    /// </summary>
    /// <returns></returns>
    public GameObjectData GetGameObjectData()
    {
        var gameObjectData = new GameObjectData(this.PrefabContainer);

        this.SerializeToGameObjectDataEvent?.Invoke(this, gameObjectData);

        return gameObjectData;
    }

    public void LoadGameObjectData(GameObjectData gameObjectData)
    {
        this.DeserializeFromGameObjectDataEvent?.Invoke(this, gameObjectData);
    }
}
