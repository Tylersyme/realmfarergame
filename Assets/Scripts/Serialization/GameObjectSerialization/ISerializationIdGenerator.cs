﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ISerializationIdGenerator
{
    string GenerateId(string humanReadableKey = null);
}
