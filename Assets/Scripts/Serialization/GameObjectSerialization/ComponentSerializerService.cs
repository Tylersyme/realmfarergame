﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AutoMapper;
using System;

public class ComponentSerializerService<TComponent, TData> 
    : IComponentSerializerService<TComponent, TData> where TComponent : Component, ISerializedComponent where TData : new()
{
    public bool LoadDataIntoIfExists(GameObjectData gameObjectData ,TComponent component)
    {
        var gameObjectDataModule = GetGameObjectDataModule(component);

        if (gameObjectDataModule == null)
            return false;

        LoadDataInto(gameObjectData, component);

        return true;
    }

    public void LoadDataInto(GameObjectData gameObjectData, TComponent component)
    {
        var componentData = GetComponentData(gameObjectData, component);

        // Load data into the component
        AutoMapperConfig.ComponentSerializationMapper.Map(componentData, component);
    }

    public void SaveDataInto(GameObjectData gameObjectData, TComponent component)
    {
        // Transfer component data to serializable data structure
        var componentData = AutoMapperConfig.ComponentSerializationMapper.Map<TData>(component);


        gameObjectData.AddComponentData(component.UniqueSerializationId, componentData);
    }

    public GameObjectDataModule GetGameObjectDataModule(TComponent component)
    {
        return component.gameObject
            .GetRootParent()
            .GetComponent<GameObjectDataModule>();
    }

    /// <summary>
    /// Gets the serialization data for a specific component.
    /// </summary>
    /// <param name="component"></param>
    /// <returns></returns>
    private TData GetComponentData(GameObjectData gameObjectData, TComponent component)
    {
        return gameObjectData.GetComponentData<TData>(component.UniqueSerializationId);
    }
}
