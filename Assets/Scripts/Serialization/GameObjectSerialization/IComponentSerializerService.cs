﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IComponentSerializerService<TComponent, TData> where TComponent : Component, ISerializedComponent where TData : new()
{
    void LoadDataInto(GameObjectData gameObjectData, TComponent component);
    bool LoadDataIntoIfExists(GameObjectData gameObjectData, TComponent component);
    void SaveDataInto(GameObjectData gameObjectData, TComponent component);
    GameObjectDataModule GetGameObjectDataModule(TComponent component);
}
