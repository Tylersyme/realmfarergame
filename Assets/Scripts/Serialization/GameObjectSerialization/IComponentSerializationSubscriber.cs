﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IComponentSerializationSubscriber<TComponent, TData> where TComponent : Component, ISerializedComponent where TData : new()
{
    void SubscribeAsSerializable();
    void UnsubscribeAsSerializable();
}
