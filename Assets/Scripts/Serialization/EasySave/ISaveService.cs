﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ISaveService
{
    void Save(string saveFileName);
    void Load(string saveFileName);
    List<string> GetSavedGameNames();
}
