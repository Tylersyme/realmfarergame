﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

/// <summary>
/// Responsible for saving the game's current state.
/// </summary>
public class SaveService : ISaveService
{
    private const string DEFAULT_SAVE_DIRECTORY_PATH = "Saves";
    private const string DEFAULT_SAVE_EXTENSION = ".json";

    public void Save(string saveFileName)
    {
        var relativeFilePath = Path.Combine(DEFAULT_SAVE_DIRECTORY_PATH, $"{saveFileName}{DEFAULT_SAVE_EXTENSION}");

        var saveSettings = GetSettings(relativeFilePath);

        #if UNITY_EDITOR

        Debug.Log("Saving to: " + saveSettings.FullPath);

        #endif

        try
        {
            SaveWorld(WorldManager.Instance.World, saveSettings);
            SaveCurrentRegionPosition(saveSettings);
            SaveTilemapObjects(saveSettings);
        }
        catch (System.Exception ex)
        {
            #if UNITY_EDITOR

            Debug.LogError(ex);

            #endif

            throw;
        }
    }

    public void Load(string saveFileName)
    {
        var relativeFilePath = Path.Combine(DEFAULT_SAVE_DIRECTORY_PATH, $"{saveFileName}{DEFAULT_SAVE_EXTENSION}");

        var saveSettings = GetSettings(relativeFilePath);

        #if UNITY_EDITOR

        Debug.Log("Loading from: " + saveSettings.FullPath);

        #endif

        try
        {
            var world = LoadWorld(saveSettings);

            WorldManager.Instance.WorldSize = world.Size;
            WorldManager.Instance.RegionSize = new Vector2Int(50, 50);

            WorldManager.Instance.World = world;

            var currentRegionPosition = LoadCurrentRegionPosition(saveSettings);
            WorldManager.Instance.LoadRegion(currentRegionPosition);

            WorldManager.Instance.RenderCurrentRegion();

            TilemapManager.Instance.TilemapObjects = LoadTilemapObjects(saveSettings);
        }
        catch (System.Exception ex)
        {
            #if UNITY_EDITOR

            Debug.LogError(ex);

            #endif

            throw;
        }
    }

    public List<string> GetSavedGameNames()
    {
        return ES3.GetFiles(new ES3Settings
        {
            path = DEFAULT_SAVE_DIRECTORY_PATH,
            location = ES3.Location.File,
        })
        .Select(f => Path.GetFileNameWithoutExtension(f))
        .ToList();
    }

    private void SaveWorld(World world, ES3Settings saveSettings)
    {
        ES3.Save<World>("world", world, saveSettings);
    }

    private void SaveCurrentRegionPosition(ES3Settings saveSettings)
    {
        ES3.Save<Vector2Int>("currentRegionPosition", WorldManager.Instance.CurrentRegionPosition, saveSettings);
    }

    private void SaveTilemapObjects(ES3Settings saveSettings)
    {
        var tilemapGameObjectData = TilemapManager.Instance.TilemapObjects
            .Where(t => t.gameObject.HasComponent<GameObjectDataModule>())
            .Select(t => t.gameObject
                .GetComponent<GameObjectDataModule>()
                .GetGameObjectData())
            .ToList();

        ES3.Save<List<GameObjectData>>("objectData", tilemapGameObjectData, saveSettings);
    }

    private World LoadWorld(ES3Settings saveSettings)
    {
        var world = ES3.Load<World>("world", saveSettings);

        return world;
    }

    private Vector2Int LoadCurrentRegionPosition(ES3Settings saveSettings)
    {
        return ES3.Load<Vector2Int>("currentRegionPosition", saveSettings);
    }

    private List<TilemapObject> LoadTilemapObjects(ES3Settings saveSettings)
    {
        return ES3
            .Load<List<GameObjectData>>("objectData", saveSettings)
            .Select(d =>
            {
                if (d.PrefabContainer == null)
                    throw new System.NullReferenceException($"Error loading prefab container: The prefab container is null. This may be caused by the ES3 object reference not being refreshed.");

                var createdObject = Object.Instantiate(d.PrefabContainer.Prefab, Vector3.zero, Quaternion.identity);
                createdObject
                    .GetComponent<GameObjectDataModule>()
                    .LoadGameObjectData(d);

                return createdObject.GetComponent<TilemapObject>();
            })
            .ToList();
    }

    private ES3Settings GetSettings(string relativeFilePath)
    {
        return new ES3Settings()
        {
            location = ES3.Location.File,
            path = relativeFilePath,
            format = ES3.Format.JSON,
        };
    }
}
