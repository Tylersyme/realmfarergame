﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ISerializer
{
    void Serialize(object toSerialize, string fileName, string relativePath);
    T Deserialize<T>(string fileName, string relativePath);
}
