﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TransformData
{
    [SerializeField]
    private Vector3 _worldPosition;
    public Vector3 WorldPosition { get => _worldPosition; set => _worldPosition = value; }

    [SerializeField]
    private Vector3 _localPosition;
    public Vector3 LocalPosition { get => _localPosition; set => _localPosition = value; }

    [SerializeField]
    private Quaternion _worldRotation;
    public Quaternion WorldRotation { get => _worldRotation; set => _worldRotation = value; }

    [SerializeField]
    private Quaternion _localRotation;
    public Quaternion LocalRotation { get => _localRotation; set => _localRotation = value; }

    [SerializeField]
    private Vector3 _localScale;
    public Vector3 LocalScale { get => _localScale; set => _localScale = value; }
}
