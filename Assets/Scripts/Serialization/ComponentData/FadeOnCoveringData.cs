﻿using AutoMapper;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class FadeOnCoveringData
{
    [SerializeField]
    public float SecondsToFade { get; set; }
}

public class FadeOnCoveringProfile : Profile
{
    public FadeOnCoveringProfile()
    {
        CreateMap<FadeOnCoveringData, FadeOnCovering>();
        CreateMap<FadeOnCovering, FadeOnCoveringData>();
    }
}
