﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public static class SpriteRendererContextOptions
{
    [MenuItem("CONTEXT/SpriteRenderer/Snap to Tile Bottom Left")]
    public static void SnapToBottomLeft(MenuCommand menuCommand)
    {
        var spriteRenderer = (SpriteRenderer)menuCommand.context;

        var position = GetBottomLeftPosition(spriteRenderer);

        spriteRenderer.gameObject.transform.position = position;

        EditorUtility.SetDirty(spriteRenderer.gameObject);
    }

    [MenuItem("CONTEXT/SpriteRenderer/Print Tile Bottom Left")]
    public static void PrintBottomLeftPosition(MenuCommand menuCommand)
    {
        var spriteRenderer = (SpriteRenderer)menuCommand.context;

        var position = GetBottomLeftPosition(spriteRenderer);

        Debug.Log(position.x + ", " + position.y);
    }

    private static Vector2 GetBottomLeftPosition(SpriteRenderer spriteRenderer)
    {
        var pixelsPerUnit = spriteRenderer.sprite.pixelsPerUnit;

        var tileUnitSize = TilemapManager.Instance.TilePixelSize.Multiply(1 / pixelsPerUnit);

        var spriteUnitSize = spriteRenderer.sprite.texture
            .GetPixelSize()
            .AsVector2()
            .Multiply(1 / pixelsPerUnit);

        var position = spriteUnitSize
            .Divide(2)
            .Subtract(tileUnitSize.Divide(2));

        return position;
    }
}
