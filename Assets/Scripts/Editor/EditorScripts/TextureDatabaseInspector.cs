//Created by Scriptable Object Database plugin.

using MyLib.EditorTools;
using UnityEditor;

[CustomEditor(typeof(TextureDatabaseFile))]
public class TextureDatabaseInspector : DatabaseInspectorBase<TextureDatabaseFile, TextureContainer>
{
    public override void ReloadGUI()
    {
        pEditorGUI = new TextureDatabaseEditorGUI("TextureDatabase" + pThisDatabase.ID16 + ".Inspector");
        pEditorGUI.InitInspector();
    }
}

public class TextureDatabaseEditorGUI : DatabaseWindowEditor<TextureDatabaseFile, TextureContainer>
{
    public override string EditorName { get { return "Textures"; } }
    
    public override System.Type ParentType
    {
        get
        {
            return null;
        }
    }
    
    public TextureDatabaseEditorGUI(string editorPrefsPrefix)
        : base(editorPrefsPrefix) { }
}