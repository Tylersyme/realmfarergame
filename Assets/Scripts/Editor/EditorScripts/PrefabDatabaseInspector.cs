//Created by Scriptable Object Database plugin.

using MyLib.EditorTools;
using UnityEditor;

[CustomEditor(typeof(PrefabDatabaseFile))]
public class PrefabDatabaseInspector : DatabaseInspectorBase<PrefabDatabaseFile, PrefabContainer>
{
    public override void ReloadGUI()
    {
        pEditorGUI = new PrefabDatabaseEditorGUI("PrefabDatabase" + pThisDatabase.ID16 + ".Inspector");
        pEditorGUI.InitInspector();
    }
}

public class PrefabDatabaseEditorGUI : DatabaseWindowEditor<PrefabDatabaseFile, PrefabContainer>
{
    public override string EditorName { get { return "Prefabs"; } }
    
    public override System.Type ParentType
    {
        get
        {
            return null;
        }
    }
    
    public PrefabDatabaseEditorGUI(string editorPrefsPrefix)
        : base(editorPrefsPrefix) { }
}