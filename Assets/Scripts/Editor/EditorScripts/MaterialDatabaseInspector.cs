//Created by Scriptable Object Database plugin.

using MyLib.EditorTools;
using UnityEditor;

[CustomEditor(typeof(MaterialDatabaseFile))]
public class MaterialDatabaseInspector : DatabaseInspectorBase<MaterialDatabaseFile, MaterialContainer>
{
    public override void ReloadGUI()
    {
        pEditorGUI = new MaterialDatabaseEditorGUI("MaterialDatabase" + pThisDatabase.ID16 + ".Inspector");
        pEditorGUI.InitInspector();
    }
}

public class MaterialDatabaseEditorGUI : DatabaseWindowEditor<MaterialDatabaseFile, MaterialContainer>
{
    public override string EditorName { get { return "Materials"; } }
    
    public override System.Type ParentType
    {
        get
        {
            return null;
        }
    }
    
    public MaterialDatabaseEditorGUI(string editorPrefsPrefix)
        : base(editorPrefsPrefix) { }
}