//Created by Scriptable Object Database plugin.

using MyLib.EditorTools;
using UnityEditor;

[CustomEditor(typeof(ShaderDatabaseFile))]
public class ShaderDatabaseInspector : DatabaseInspectorBase<ShaderDatabaseFile, ShaderContainer>
{
    public override void ReloadGUI()
    {
        pEditorGUI = new ShaderDatabaseEditorGUI("ShaderDatabase" + pThisDatabase.ID16 + ".Inspector");
        pEditorGUI.InitInspector();
    }
}

public class ShaderDatabaseEditorGUI : DatabaseWindowEditor<ShaderDatabaseFile, ShaderContainer>
{
    public override string EditorName { get { return "Shaders"; } }
    
    public override System.Type ParentType
    {
        get
        {
            return null;
        }
    }
    
    public ShaderDatabaseEditorGUI(string editorPrefsPrefix)
        : base(editorPrefsPrefix) { }
}