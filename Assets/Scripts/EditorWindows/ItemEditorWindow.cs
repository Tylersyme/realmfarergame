﻿using Sirenix.OdinInspector;
using Sirenix.OdinInspector.Editor;
using Sirenix.Utilities;
using Sirenix.Utilities.Editor;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEditor.Animations;
using UnityEditor.Experimental.SceneManagement;
using UnityEngine;

public class ItemEditorWindow : OdinEditorWindow
{
    private static readonly string ANIMATIONS_RELATIVE_DIRECTORY_PATH = "Animations";
    private static readonly string ROTATION_ANIMATIONS_RELATIVE_DIRECTORY_PATH = Path.Combine(
        ANIMATIONS_RELATIVE_DIRECTORY_PATH, 
        "RotationAnimations");

    private const int WINDOW_WIDTH = 500;
    private const int WINDOW_HEIGHT = 500;

    [MenuItem("Tools/Item Editor")]
    public static void OpenWindow()
    {
        var window = GetWindow<ItemEditorWindow>();

        window.position = GUIHelper
            .GetEditorWindowRect()
            .AlignCenter(WINDOW_WIDTH, WINDOW_HEIGHT);
    }

    [Button]
    [EnableIf(nameof(IsPlaceablePrefabOpened))]
    private void CreateRotationAnimations()
    {
        var prefabObject = this.GetCurrentPrefab();
        var prefabAssetDirectoryPath = Path.GetDirectoryName(this.GetCurrentPrefabAssetPath());

        var directoryPath = Path.Combine(prefabAssetDirectoryPath, ROTATION_ANIMATIONS_RELATIVE_DIRECTORY_PATH);

        Directory.CreateDirectory(directoryPath);

        var rotationType = prefabObject.GetComponent<PlaceableBehavior>().RotationType;

        var animationControllerPath = Path.Combine(
            prefabAssetDirectoryPath, 
            ANIMATIONS_RELATIVE_DIRECTORY_PATH, 
            "RotationAnimatorController.controller");

        // An empty animation clip used so that no animations occur on entry transition
        var emptyAnimationClip = new AnimationClip();
        emptyAnimationClip.name = "Empty";

        if (!File.Exists(animationControllerPath))
            AnimatorController.CreateAnimatorControllerAtPathWithClip(animationControllerPath, emptyAnimationClip);

        var animatorController = AssetDatabase.LoadAssetAtPath<AnimatorController>(animationControllerPath);

        foreach (var direction in Direction.GetDirectionsOfRotationType(rotationType))
        {
            var animationName = $"{prefabObject.name.StripAllWhitespace()}{direction.Type.ToString()}Rotation";

            var animationPath = Path.Combine(directoryPath, $"{animationName}.anim");

            if (File.Exists(animationPath))
                continue;

            var animationClip = AnimatorController.AllocateAnimatorClip(animationName);

            AnimationUtility.SetAnimationClipSettings(animationClip, new AnimationClipSettings
            {
                loopTime = false,
            });

            AssetDatabase.CreateAsset(animationClip, animationPath);

            animatorController.AddMotion(animationClip);
        }

        AssetDatabase.SaveAssets();
    }

    private string GetCurrentPrefabAssetPath()
    {
        var prefabStage = PrefabStageUtility.GetCurrentPrefabStage();

        return prefabStage.prefabAssetPath;
    }

    private GameObject GetCurrentPrefab()
    {
        var prefabStage = PrefabStageUtility.GetCurrentPrefabStage();

        return prefabStage.prefabContentsRoot;
    }

    private bool IsPlaceablePrefabOpened
    {
        get
        {
            var prefabStage = PrefabStageUtility.GetCurrentPrefabStage();

            if (prefabStage == null)
                return false;

            var prefabObject = this.GetCurrentPrefab();

            return prefabObject.HasComponent<PlaceableBehavior>();
        }
    }
}
