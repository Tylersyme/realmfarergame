﻿#if UNITY_EDITOR

using Sirenix.OdinInspector;
using Sirenix.Utilities;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using Sirenix.OdinInspector.Editor;
using Sirenix.Utilities.Editor;
using UnityEngine;
using static FastNoise;

public class NoiseGeneratorPreviewWindow : OdinEditorWindow
{
    private const int WINDOW_WIDTH = 400;
    private const int WINDOW_HEIGHT = 600;

    [MenuItem("Tools/Noise/Noise Preview")]
    public static void OpenWindow()
    {
        var window = GetWindow<NoiseGeneratorPreviewWindow>();

        window.position = GUIHelper
            .GetEditorWindowRect()
            .AlignCenter(WINDOW_WIDTH, WINDOW_HEIGHT);
    }

    [SerializeField]
    [OnValueChanged(nameof(RefreshPreviewTexture))]
    [OnValueChanged(nameof(SetAsDirty))]
    private Vector2Int _previewSize = new Vector2Int(325, 250);
    public Vector2Int PreviewSize { get => _previewSize; set => _previewSize = value; }

    [SerializeField]
    [OnValueChanged(nameof(SetAsDirty))]
    private NoiseType _noiseType;
    public NoiseType NoiseType { get => _noiseType; set => _noiseType = value; }

    [SerializeField]
    [OnValueChanged(nameof(SetAsDirty))]
    private float _frequency;
    public float Frequency { get => _frequency; set => _frequency = value; }

    [SerializeField]
    [OnValueChanged(nameof(SetAsDirty))]
    private int _octaves;
    public int Octaves { get => _octaves; set => _octaves = value; }

    [SerializeField]
    [OnValueChanged(nameof(SetAsDirty))]
    private float _gain;
    public float Gain { get => _gain; set => _gain = value; }

    [SerializeField]
    [OnValueChanged(nameof(SetAsDirty))]
    private float _lacunarity;
    public float Lacunarity { get => _lacunarity; set => _lacunarity = value; }

    public bool showNormalized = true;

    [ReadOnly]
    public float maxValue;

    [ReadOnly]
    public float minValue;

    private bool IsDirty { get; set; } = false;
    private bool IsNotDirty { get => !this.IsDirty; }

    private NoiseService NoiseService { get; set; }
    private NoiseMapGenerator NoiseMapGenerator { get; set; }
    private Texture2D PreviewTexture { get; set; }

    [HideInInspector]
    public event EventHandler<NoiseMapPreviewClosedEventArgs> NoiseMapPreviewClosedEvent;

    protected override void OnEnable()
    {
        CopyNoiseGeneratorParameters();

        this.NoiseService = new NoiseService();
        this.NoiseMapGenerator = new NoiseMapGenerator();

        RefreshPreviewTexture();
    }

    protected override void OnGUI()
    {
        base.OnGUI();

        var window = EditorWindow.GetWindow<NoiseGeneratorPreviewWindow>();

        if (this.PreviewTexture == null)
            return;

        GUI.DrawTexture(new Rect((WINDOW_WIDTH - this.PreviewSize.x) / 2, 280, this.PreviewSize.x, this.PreviewSize.y), this.PreviewTexture);
    }

    private void CopyNoiseGeneratorParameters()
    {
        var noiseParameters = NoiseGeneratorPreviewManager.Instance.NoiseGeneratorParameters;
        this.NoiseType = noiseParameters.NoiseType;
        this.Frequency = noiseParameters.Frequency;
        this.Octaves = noiseParameters.Octaves;
        this.Gain = noiseParameters.Gain;
        this.Lacunarity = noiseParameters.Lacunarity;
    }

    private NoiseGeneratorParameters CreateNoiseGeneratorParameters()
    {
        var noiseGeneratorParameters = new NoiseGeneratorParameters();
        noiseGeneratorParameters.NoiseType = this.NoiseType;
        noiseGeneratorParameters.Frequency = this.Frequency;
        noiseGeneratorParameters.Octaves = this.Octaves;
        noiseGeneratorParameters.Gain = this.Gain;
        noiseGeneratorParameters.Lacunarity = this.Lacunarity;

        noiseGeneratorParameters.Seed = new Seed("debug");

        return noiseGeneratorParameters;
    }

    [Button("Refresh", ButtonSizes.Medium)]
    private void RefreshPreviewTexture()
    {
        UpdatePreviewTexture(GetNoiseGenerator());
    }

    [Button("Apply Changes", ButtonSizes.Medium)]
    [DisableIf(nameof(IsNotDirty))]
    private void ApplyChanges()
    {
        try
        {
            NoiseGeneratorPreviewManager.Instance.HasApplyChangesBeenClicked = true;
            NoiseGeneratorPreviewManager.Instance.StoreChanges(CreateNoiseGeneratorParameters());

            this.IsDirty = false;
        }
        catch (Exception exception)
        {
            Debug.LogError(exception.Message);
        }
    }

    protected override void OnDestroy()
    {
        this.NoiseMapPreviewClosedEvent?.Invoke(this, new NoiseMapPreviewClosedEventArgs());

        base.OnDestroy();
    }

    private void SetAsDirty()
    {
        this.IsDirty = true;
    }

    private FastNoise GetNoiseGenerator()
    {
        return this.NoiseService.BuildFromParameters(CreateNoiseGeneratorParameters());
    }

    private void UpdatePreviewTexture(FastNoise noiseGenerator)
    {
        var noiseMap = this.NoiseMapGenerator.GenerateNoiseMap(noiseGenerator, this.PreviewSize.x, this.PreviewSize.y);

        if (showNormalized)
            noiseMap = noiseMap.Normalize();

        this.minValue = noiseMap.MinValue;
        this.maxValue = noiseMap.MaxValue;

        this.PreviewTexture = noiseMap.ToTexture();
    }
}

#endif
