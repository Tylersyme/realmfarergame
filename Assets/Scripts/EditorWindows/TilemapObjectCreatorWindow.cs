﻿#if UNITY_EDITOR

using ES3Internal;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using Sirenix.Utilities;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using Sirenix.OdinInspector.Editor;
using Sirenix.Utilities.Editor;
using UnityEngine;
using System;

public class TilemapObjectCreatorWindow : OdinEditorWindow
{
    private const int WINDOW_WIDTH = 500;
    private const int WINDOW_HEIGHT = 500;

    [MenuItem("Tools/Tilemap/Create Tilemap Object")]
    public static void OpenWindow()
    {
        var window = GetWindow<TilemapObjectCreatorWindow>();

        window.position = GUIHelper
            .GetEditorWindowRect()
            .AlignCenter(WINDOW_WIDTH, WINDOW_HEIGHT);
    }

    #if UNITY_EDITOR

    private bool Disable_CreateButton =>
        this.Name == null ||
        (this.IncludeSpriteRenderer && this.Sprite == null);

    #endif

    [SerializeField]
    [Title("General")]
    [Required]
    private string _name = "New Game Object";
    public string Name { get => _name; set => _name = value; }

    [SerializeField]
    private bool _isInteractable = true;
    public bool IsInteractable { get => _isInteractable; set => _isInteractable = value; }

    [SerializeField]
    [Tooltip("Whether the prefab should be serializable")]
    [BoxGroup("Serialization")]
    private bool _isSerializable = false;
    public bool IsSerializable { get => _isSerializable; set => _isSerializable = value; }

    [SerializeField]
    [BoxGroup("Serialization")]
    [LabelText("Include Built In Serializer")]
    private bool _includeBuiltInComponentSerializer;
    public bool IncludeBuiltInComponentSerializer { get => _includeBuiltInComponentSerializer; set => _includeBuiltInComponentSerializer = value; }

    [SerializeField]
    [BoxGroup("Item")]
    private bool _isItem = false;
    public bool IsItem { get => _isItem; set => _isItem = value; }

    [SerializeField]
    [ShowIf(nameof(IsItem))]
    [BoxGroup("Item")]
    private bool _isPlaceable = false;
    public bool IsPlaceable { get => _isPlaceable; set => _isPlaceable = value; }

    [SerializeField]
    [BoxGroup("Rendering")]
    private bool _includeSpriteRenderer = true;
    public bool IncludeSpriteRenderer { get => _includeSpriteRenderer; set => _includeSpriteRenderer = value; }

    [SerializeField]
    [Required]
    [ShowIf(nameof(IncludeSpriteRenderer))]
    [BoxGroup("Rendering")]
    [LabelText("Sprite")]
    private Sprite _sprite;
    public Sprite Sprite { get => _sprite; set => _sprite = value; }

    [SerializeField]
    [ShowIf(nameof(IncludeSpriteRenderer))]
    [BoxGroup("Rendering")]
    private TilemapSortingLayer _sortingLayer = TilemapSortingLayer.TilemapObject;
    public TilemapSortingLayer SortingLayer { get => _sortingLayer; set => _sortingLayer = value; }

    [SerializeField]
    [ShowIf(nameof(IncludeSpriteRenderer))]
    [EnumToggleButtons]
    [BoxGroup("Rendering")]
    [LabelText("Sort Order Type")]
    private SortOrderType _sortOrderTypeSelection = SortOrderType.Static;
    public SortOrderType SortOrderTypeSelection { get => _sortOrderTypeSelection; set => _sortOrderTypeSelection = value; }

    [SerializeField]
    [ShowIf(nameof(IncludeSpriteRenderer))]
    [BoxGroup("Rendering")]
    private bool _useFadeOnCovering = false;
    public bool UseFadeOnCovering { get => _useFadeOnCovering; set => _useFadeOnCovering = value; }

    [SerializeField]
    [BoxGroup("Colliders")]
    private bool _includeColliders = true;
    public bool IncludeColliders { get => _includeColliders; set => _includeColliders = value; }

    [SerializeField]
    [BoxGroup("Colliders")]
    [ShowIf(nameof(IncludeColliders))]
    private List<ColliderType> _colliderTypes;
    public List<ColliderType> ColliderTypes { get => _colliderTypes; set => _colliderTypes = value; }

    [DisableIf(nameof(Disable_CreateButton))]
    [Button(ButtonSizes.Medium)]
    private void Create()
    {
        var gameObjectBuilder = new GameObjectBuilder();

        gameObjectBuilder
            .Create(this.Name)
            .WithComponent<TilemapObject>()
            .WithComponentIf<ItemModule>(this.IsItem)
            .WithComponentIf<PlaceableBehavior>(this.IsItem && this.IsPlaceable)
            .WithComponentIf<GameObjectDataModule>(this.IsSerializable)
            .WithComponentIf<BuiltInComponentSerializer>(this.IncludeBuiltInComponentSerializer)
            .WithComponentIf<InteractionEventHub>(this.IsInteractable)
            .WithChildIf("Renderer", this.IncludeSpriteRenderer, rendererBuilder =>
            {
                var sortingLayerName = this.GetSortingLayerName(this.SortingLayer);

                var sortOrderComponentType = this.GetSortOrderComponentType(this.SortOrderTypeSelection);

                return rendererBuilder
                    .WithComponent<SpriteRenderer>()
                        .WithValue(x => x.sortingLayerName, sortingLayerName)
                        .WithValue(x => x.sprite, this.Sprite)
                        .WithValueIf(x => x.sortingOrder, -short.MaxValue, this.SortOrderTypeSelection == SortOrderType.Background)
                    .WithComponentIf(sortOrderComponentType, 
                        this.SortOrderTypeSelection != SortOrderType.None && sortOrderComponentType != null)
                    .WithChildIf("FadeOnCovering", this.UseFadeOnCovering, fadeOnCoveringBuilder =>
                    {
                        return fadeOnCoveringBuilder
                            .WithComponent<BoxCollider2D>()
                                .WithValue(x => x.isTrigger, true)
                            .WithComponent<FadeOnCovering>()
                            .Build();
                    })
                    .Build();
            })
            .WithChildIf("Colliders", this.IncludeColliders, collidersBuilder =>
            {
                var colliderComponentTypes = this.GetColliderComponentTypes();

                for (var typeIndex = 0; typeIndex < colliderComponentTypes.Count; typeIndex++)
                    collidersBuilder
                        .WithChild($"Collider {(typeIndex + 1)}", colliderBuilder =>
                        {
                            return colliderBuilder
                                .WithComponent(colliderComponentTypes[typeIndex])
                                .Build();
                        });

                return collidersBuilder.Build();
            });
    }

    private IList<Type> GetColliderComponentTypes() =>
        this.ColliderTypes
            .Select(c =>
            {
                if (c == ColliderType.Box)
                    return typeof(BoxCollider2D);
                else if (c == ColliderType.Circle)
                    return typeof(CircleCollider2D);

                throw new System.Exception($"Collider creation error: The collider type \"{c}\" has not been defined.");
            })
            .ToList();

    private string GetSortingLayerName(TilemapSortingLayer tilemapSortingLayer)
    {
        if (tilemapSortingLayer == TilemapSortingLayer.TilemapObject)
            return global::SortingLayers.TILEMAP_OBJECTS;

        throw new System.Exception($"Sorting layer name error: The tilemap sorting layer name \"{tilemapSortingLayer}\" has not been defined.");
    }

    private Type GetSortOrderComponentType(SortOrderType sortOrderType) 
    {
        if (sortOrderType == SortOrderType.Dynamic)
            return typeof(TilemapDynamicSortingOrder);
        else if (sortOrderType == SortOrderType.Static)
            return typeof(TilemapStaticSortingOrder);
        else if (sortOrderType == SortOrderType.Background)
            return null;

        throw new System.Exception($"Sorting order creation error: The tilemap sorting order \"{sortOrderType}\" has not been defined.");
    }

    public enum SortOrderType
    {
        Static,
        Dynamic,
        Background,
        None,
    }

    public enum TilemapSortingLayer
    {
        TilemapObject,
    }

    public enum ColliderType
    {
        Circle,
        Box,
    }
}

#endif
