﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeIntervalTracker : ITimeIntervalTracker
{
    private float IntervalSeconds { get; set; }

    private float SecondsElapsedSinceInterval { get; set; }

    public event EventHandler<TimeIntervalOccurredEventArgs> TimeIntervalOccurredEvent;

    public TimeIntervalTracker(float intervalSeconds)
    {
        this.IntervalSeconds = intervalSeconds;
    }

    public void Update(float elapsedSeconds)
    {
        this.SecondsElapsedSinceInterval += elapsedSeconds;

        if (this.SecondsElapsedSinceInterval < this.IntervalSeconds)
            return;

        this.SecondsElapsedSinceInterval -= this.IntervalSeconds;

        this.TimeIntervalOccurredEvent?.Invoke(this, new TimeIntervalOccurredEventArgs
        {
            IntervalSeconds = this.IntervalSeconds,
        });
    }
}
