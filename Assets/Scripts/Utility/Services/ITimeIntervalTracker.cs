﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ITimeIntervalTracker
{
    event EventHandler<TimeIntervalOccurredEventArgs> TimeIntervalOccurredEvent;

    void Update(float elapsedSeconds);
}
