﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeIntervalOccurredEventArgs
{
    public float IntervalSeconds { get; set; }
}
