﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[System.Serializable]
public class RandomPool<T>
{
    [SerializeField]
    private List<WeightedElement<T>> _pool;
    private List<WeightedElement<T>> Pool { get => _pool; set => _pool = value; }

    public RandomPool()
    {
        this.Pool = new List<WeightedElement<T>>();
    }

    private RandomPool(List<WeightedElement<T>> pool)
    {
        this.Pool = pool;
    }

    [System.Serializable]
    private class WeightedElement<TElement>
    {
        [SerializeField]
        private TElement _element;
        public TElement Element { get => _element; set => _element = value; }

        [SerializeField]
        private float _weight;
        public float Weight { get => _weight; set => _weight = value; }

        public WeightedElement(TElement element, float weight)
        {
            this.Element = element;
            this.Weight = weight;
        }
    }

    public RandomPool<T> AddElement(T element, float weight)
    {
        this.Pool.Add(new WeightedElement<T>(element, weight));

        return this;
    }

    /// <summary>
    /// Returns a new instance of RandomPool without the elments set to be excluded.
    /// </summary>
    /// <param name="toExclude"></param>
    /// <returns></returns>
    public RandomPool<T> Exclude(List<T> toExclude)
    {
        return new RandomPool<T>(this.Pool
            .Where(e => !toExclude.Contains(e.Element))
            .ToList());
    }

    public RandomPool<T> Exclude(T toExclude)
    {
        return Exclude(new List<T> { toExclude });
    }

    public T GetElement()
    {
        var totalWeight = TotalWeight();
        var selectedPoint = Random.Range(0f, totalWeight);

        var currentSum = 0f;
        foreach (var weightedElement in this.Pool)
        {
            if (weightedElement.Weight + currentSum > selectedPoint)
                return weightedElement.Element;

            currentSum += weightedElement.Weight;
        }

        throw new System.Exception("The randomly selected point fell outside the bounds of the weighted sum.");
    }

    public List<T> GetElements(int total)
    {
        var elementList = new List<T>();

        for (var idx = 0; idx < total; idx++)
            elementList.Add(GetElement());

        return elementList;
    }

    public bool IsEmpty()
    {
        return !this.Pool.Any();
    }

    private float TotalWeight()
    {
        return this.Pool.Sum(w => w.Weight);
    }
}
