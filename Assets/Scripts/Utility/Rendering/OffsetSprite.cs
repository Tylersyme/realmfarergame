﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class OffsetSprite
{
    [SerializeField]
    [Required]
    private Sprite _sprite;
    public Sprite Sprite { get => _sprite; set => _sprite = value; }

    [SerializeField]
    private Vector2 _offset;
    public Vector2 Offset { get => _offset; set => _offset = value; }
}
