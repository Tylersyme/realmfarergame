﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleDebug : MonoBehaviour
{
    private IGameObjectBuilder _gameObjectBuilder;

    private void Awake()
    {
        _gameObjectBuilder = new GameObjectBuilder();
    }

    [Button]
    private void Spawn()
    {
        var requiredAttributes = ComponentExtensions.GetRequiredComponents<FadeOnCovering>();

        var gameObject = _gameObjectBuilder
            .Create("BuildTest")
            .WithComponent<BoxCollider2D>()
            .WithComponent<FadeOnCovering>()
                .WithValue(x => x.FadeAlpha, 0.1f)
            .WithEmptyChild("TestChild")
            .WithChild("Child", b => b
                .WithEmptyChild("InnerChild")
                .WithComponent<BoxCollider2D>()
                    .WithValue(c => c.size, new Vector2(3, 4))
                .Build())
            .Build();
    }
}
