﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vector3IntComparer : IEqualityComparer<Vector3Int>
{
    public bool Equals(Vector3Int x, Vector3Int y)
    {
        return x.IsEquivalent(y);
    }

    public int GetHashCode(Vector3Int obj)
    {
        return obj.GetHashCode();
    }
}
